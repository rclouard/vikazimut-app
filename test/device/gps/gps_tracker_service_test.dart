import 'package:flutter_test/flutter_test.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/device/gps/gps_tracker_service.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

class MockPosition extends Mock implements Position {}

void main() {
  test('isOlderThanPreviousLocation when older', () {
    GeodesicPoint previousLocation = GeodesicPoint(0, 0, 10);
    MockPosition currentLocation = MockPosition();
    when(() => currentLocation.timestamp).thenReturn(DateTime.fromMillisecondsSinceEpoch(9));
    bool actual = GpsTrackerService.positionIsOlderThanPreviousPosition_(currentLocation, previousLocation);
    expect(actual, true);
  });

  test('isOlderThanPreviousLocation when younger', () {
    GeodesicPoint previousLocation = GeodesicPoint(0, 0, 10);
    MockPosition currentLocation = MockPosition();
    when(() => currentLocation.timestamp).thenReturn(DateTime.fromMillisecondsSinceEpoch(11));
    bool actual = GpsTrackerService.positionIsOlderThanPreviousPosition_(currentLocation, previousLocation);
    expect(actual, false);
  });

  test('isOlderThanPreviousLocation when same', () {
    GeodesicPoint previousLocation = GeodesicPoint(0, 0, 10);
    MockPosition currentLocation = MockPosition();
    when(() => currentLocation.timestamp).thenReturn(DateTime.fromMillisecondsSinceEpoch(10));
    bool actual = GpsTrackerService.positionIsOlderThanPreviousPosition_(currentLocation, previousLocation);
    expect(actual, false);
  });

  test('isOlderThanPreviousLocation when null', () {
    GeodesicPoint? previousLocation;
    MockPosition currentLocation = MockPosition();
    when(() => currentLocation.timestamp).thenReturn(DateTime.fromMillisecondsSinceEpoch(10));
    bool actual = GpsTrackerService.positionIsOlderThanPreviousPosition_(currentLocation, previousLocation);
    expect(actual, false);
  });

  test('is Location inaccurate when accuracy zero', () {
    Position currentLocation = Position(
      longitude: 0,
      latitude: 0,
      timestamp: DateTime(1970),
      accuracy: 0,
      altitude: 0,
      heading: 0,
      speed: 0,
      speedAccuracy: 0,
      altitudeAccuracy: 0,
      headingAccuracy: 0,
    );
    bool actual = GpsTrackerService.positionIsAccurate_(currentLocation, 10);
    expect(actual, true);
  });

  test('isLocationInaccurate when accuracy low', () {
    Position currentLocation = Position(
      longitude: 0,
      latitude: 0,
      timestamp: DateTime(1970),
      accuracy: 100,
      altitude: 0,
      heading: 0,
      speed: 0,
      speedAccuracy: 0,
      altitudeAccuracy: 0,
      headingAccuracy: 0,
    );
    bool actual = GpsTrackerService.positionIsAccurate_(currentLocation, 10);
    expect(actual, false);
  });

  test('isLocationInaccurate when accuracy regular', () {
    Position currentLocation = Position(
      longitude: 0,
      latitude: 0,
      timestamp: DateTime(1970),
      accuracy: 5,
      altitude: 0,
      heading: 0,
      speed: 0,
      speedAccuracy: 0,
      altitudeAccuracy: 0,
      headingAccuracy: 0,
    );
    bool actual = GpsTrackerService.positionIsAccurate_(currentLocation, 10);
    expect(actual, true);
  });

  test('test positionIsOlderThanBaseTime when true', () {
    MockPosition currentLocation = MockPosition();
    when(() => currentLocation.timestamp).thenReturn(DateTime.fromMillisecondsSinceEpoch(11));
    bool actual = GpsTrackerService.positionTimeIsOlderThanBaseTime_(currentLocation);
    expect(actual, false);
  });

  test('test positionIsOlderThanBaseTime when false', () {
    MockPosition currentLocation = MockPosition();
    when(() => currentLocation.timestamp).thenReturn(DateTime.fromMillisecondsSinceEpoch(-1));
    bool actual = GpsTrackerService.positionTimeIsOlderThanBaseTime_(currentLocation);
    expect(actual, true);
  });
}
