import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/device/beacon/beacon_utility.dart';

void main() {
  test('test median case even 1', () {
    var actual = medianValue([1, 2, 3, 4, 5, 6]);
    expect(actual, 3);
  });

  test('test median case odd 1', () {
    var actual = medianValue([1, 2, 3, 4, 5, 6, 7]);
    expect(actual, 4);
  });

  test('test median case even 2', () {
    var actual = medianValue([6, 5, 4, 3, 2, 1]);
    expect(actual, 3);
  });

  test('test median case odd 2', () {
    var actual = medianValue([7, 6, 5, 4, 3, 2, 1]);
    expect(actual, 4);
  });

  test('test median case even 3', () {
    var actual = medianValue([6, 5, 5, 5, 2, 1]);
    expect(actual, 5);
  });
}
