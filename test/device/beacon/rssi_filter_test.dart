import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/device/beacon/rssi_filter.dart' show RssiArmaFilter;

void main() {
  test('test filter initialization', () {
    final filter = RssiArmaFilter(-10);
    var actual = filter.filterRssi(-10);
    expect(actual, -10);
  });

  test('test filter multiple same values', () {
    final filter = RssiArmaFilter(-10);
    var actual = filter.filterRssi(-10);
    expect(actual, -10);
  });

  test('test filter multiple different values', () {
    final filter = RssiArmaFilter(-10);
    var actual = filter.filterRssi(-15);
    expect(actual, -10);
    actual = filter.filterRssi(-16);
    expect(actual, -11);
    actual = filter.filterRssi(-17);
    expect(actual, -11);
  });
}
