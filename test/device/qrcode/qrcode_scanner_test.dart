import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/device/qrcode/qrcode_scanner.dart';

void main() {
  test('Test extractCheckpointIdFromQrCodeText when old standard', () {
    var extractCheckpointIdFromQrCodeText = QrCodeScanner.extractCheckpointIdFromQrCodeText("31");
    expect(extractCheckpointIdFromQrCodeText, "31");
  });

  test('Test extractCheckpointIdFromQrCodeText when new standard', () {
    var extractCheckpointIdFromQrCodeText = QrCodeScanner.extractCheckpointIdFromQrCodeText("https://vikazimut.vikazim.fr/landing/?cp=31");
    expect(extractCheckpointIdFromQrCodeText, "31");
  });

  test('Test extractCheckpointIdFromQrCodeText when old derived standard', () {
    var extractCheckpointIdFromQrCodeText = QrCodeScanner.extractCheckpointIdFromQrCodeText("https://toto.fr/31");
    expect(extractCheckpointIdFromQrCodeText, "https://toto.fr/31");
  });
}
