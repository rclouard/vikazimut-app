import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';
import 'package:vikazimut/orienteering_map/orienteering_kml_reader.dart';

void main() {
  const double coordinatePrecision = 1e-9;

  test('_readKmlFile_1', () {
    File xmlFile = File('test/resources/kml1.xml');
    LatLonBox bounds = OrienteeringMapKmlFileReader.readFile(xmlFile);
    expect(bounds, isNotNull);
    expect(bounds.north, moreOrLessEquals(49.218965810, epsilon: coordinatePrecision));
    expect(bounds.south, moreOrLessEquals(49.209716780, epsilon: coordinatePrecision));
    expect(bounds.east, moreOrLessEquals(-0.362510456, epsilon: coordinatePrecision));
    expect(bounds.west, moreOrLessEquals(-0.372461070, epsilon: coordinatePrecision));
    expect(bounds.rotation, moreOrLessEquals(2.446865069, epsilon: coordinatePrecision));
  });

  test('readKmlFile_2', () {
    File xmlFile = File('test/resources/kml2.xml');
    LatLonBox bounds = OrienteeringMapKmlFileReader.readFile(xmlFile);
    expect(bounds, isNotNull);
    expect(bounds.north, moreOrLessEquals(49.23319176120304, epsilon: coordinatePrecision));
    expect(bounds.south, moreOrLessEquals(49.22277288502361, epsilon: coordinatePrecision));
    expect(bounds.east, moreOrLessEquals(-0.2935683999360841, epsilon: coordinatePrecision));
    expect(bounds.west, moreOrLessEquals(-0.3047694062852322, epsilon: coordinatePrecision));
    expect(bounds.rotation, moreOrLessEquals(2.387386322021484, epsilon: coordinatePrecision));
  });

  test('readKmlFile_3', () {
    File xmlFile = File('test/resources/kml3.xml');
    LatLonBox bounds = OrienteeringMapKmlFileReader.readFile(xmlFile);
    expect(bounds, isNotNull);
    expect(bounds.north, moreOrLessEquals(49.23319176120304, epsilon: coordinatePrecision));
    expect(bounds.south, moreOrLessEquals(49.22277288502361, epsilon: coordinatePrecision));
    expect(bounds.east, moreOrLessEquals(-0.2935683999360841, epsilon: coordinatePrecision));
    expect(bounds.west, moreOrLessEquals(-0.3047694062852322, epsilon: coordinatePrecision));
    expect(bounds.rotation, moreOrLessEquals(2.387386322021484, epsilon: coordinatePrecision));
  });
}
