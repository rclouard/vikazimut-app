import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/orienteering_map/orienteering_iof_xml_reader.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/orienteering_map/xml_util.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:xml/xml.dart';

void main() {
  const double positionPrecision = 0.0001;

  test('Read formatted XML example 1', () {
    const String name = "EnsiCaen";
    File xmlFile = File('test/resources/ensicaen.xml');
    OrienteeringMap map = OrienteeringIofXmlFileReader.readFile(xmlFile, name);
    expect(map.name, name);
    int checkpointsCount = map.getCheckpointCount();
    expect(checkpointsCount, 7);
    const List<String> CHECKPOINTS_NAMES = ["S1", "31", "32", "33", "34", "35", "F1"];
    const List<double> LATITUDES = [49.213991, 49.213062, 49.213337, 49.210419, 49.211525, 49.214046, 49.213991];
    const List<double> LONGITUDES = [-0.367475, -0.368094, -0.370959, -0.370080, -0.367737, -0.365707, -0.367475];
    for (int i = 0; i < CHECKPOINTS_NAMES.length; i++) {
      Checkpoint checkpoint = map.getCheckpoint(i);
      expect(checkpoint.id, CHECKPOINTS_NAMES[i]);
      expect(checkpoint.location.latitude, moreOrLessEquals(LATITUDES[i], epsilon: positionPrecision));
      expect(checkpoint.location.longitude, moreOrLessEquals(LONGITUDES[i], epsilon: positionPrecision));
    }
  });

  test('Read formatted XML example 2', () {
    const String name = "Jeune";
    File xmlFile = File('test/resources/mdjeunes.xml');
    OrienteeringMap map = OrienteeringIofXmlFileReader.readFile(xmlFile, name);
    expect(name, map.name);
    int checkpointsCount = map.getCheckpointCount();
    expect(checkpointsCount, 11);
    const List<String> CHECKPOINTS_NAMES = ["S1", "47", "31", "37", "43", "42", "46", "32", "33", "48", "F1"];
    for (int i = 0; i < CHECKPOINTS_NAMES.length; i++) {
      Checkpoint checkpoint = map.getCheckpoint(i);
      expect(CHECKPOINTS_NAMES[i], checkpoint.id);
      final List<double> longitudes = [-0.395482, -0.395070, -0.394134, -0.391440, -0.388341, -0.387030, -0.38703, -0.391169, -0.391865, -0.393162, -0.395505];
      final List<double> latitudes = [49.111680, 49.111003, 49.109286, 49.108209, 49.106794, 49.107120, 49.108285, 49.109071, 49.109443, 49.110578, 49.111679];
      expect(checkpoint.location.latitude, moreOrLessEquals(latitudes[i], epsilon: positionPrecision));
      expect(checkpoint.location.longitude, moreOrLessEquals(longitudes[i], epsilon: positionPrecision));
    }
  });

  test('Read Xml File URL empty', () {
    const String name = "EnsiCaen";
    File xmlFile = File('test/resources/ensicaen.xml');
    OrienteeringMap map = OrienteeringIofXmlFileReader.readFile(xmlFile, name);
    expect(map.getCheckpoint(1).text, null);
  });

  test('Read XmlFile when augmented with checkpoint texts', () {
    const String name = "EnsiCaen";
    File xmlFile = File('test/resources/ensicaen-augmented.xml');
    OrienteeringMap map = OrienteeringIofXmlFileReader.readFile(xmlFile, name);
    expect(map.getCheckpoint(1).text, "Text 1");
    expect(map.getCheckpoint(2).text, "https://google.com");
    expect(map.getCheckpoint(4).text, "Text 4");
  });

  test('Read XmlFile when extension part', () {
    const String name = "EnsiCaen";
    File xmlFile = File('test/resources/ensicaen-extended.xml');
    OrienteeringMap map = OrienteeringIofXmlFileReader.readFile(xmlFile, name);
    expect(map.detectionRadius, 13);
    expect(map.discipline, Discipline.URBANO);
    expect(map.courseMode, CourseMode.SPORT);
    expect(map.courseFormat, CourseFormat.PRESET);
    expect(map.validationMode, ValidationMode.GPS);
  });

  test('Read score in XmlFile', () {
    Map<String, CheckpointData> checkpointFeatures = {
      'S1': CheckpointData()
        ..id = "S1"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '31': CheckpointData()
        ..id = "31"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '32': CheckpointData()
        ..id = "32"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '33': CheckpointData()
        ..id = "33"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '34': CheckpointData()
        ..id = "34"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '35': CheckpointData()
        ..id = "35"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      'F1': CheckpointData()
        ..id = "F1"
        ..latitude = 0
        ..longitude = 0
        ..text = null
    };
    List<int> expected = [1, 31, 32, 33, 34, 35, 1];
    File xmlFile = File('test/resources/ensicaen-augmented.xml');
    var xmlText = convertXmlTagsToLowerCase(xmlFile.readAsStringSync());
    final XmlDocument document = XmlDocument.parse(xmlText);
    XmlElement raceCourseDataElement = document.getElement('coursedata')!.getElement("racecoursedata")!;
    XmlElement courseElements = raceCourseDataElement.findElements('course').first;
    List<Checkpoint> checkpoints = OrienteeringIofXmlFileReader.extractCourseData_(courseElements, checkpointFeatures);
    for (int i = 0; i < checkpoints.length; ++i) {
      expect(checkpoints[i].points, expected[i]);
    }
  });

  test('Read score in XmlFile when no score given', () {
    Map<String, CheckpointData> checkpointFeatures = {
      'S': CheckpointData()
        ..id = "S"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '1': CheckpointData()
        ..id = "1"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      '2': CheckpointData()
        ..id = "2"
        ..latitude = 0
        ..longitude = 0
        ..text = null,
      'F': CheckpointData()
        ..id = "F"
        ..latitude = 0
        ..longitude = 0
        ..text = null
    };
    List<int> expected = [1, 10, 10, 1];
    File xmlFile = File('test/resources/oomap.xml');
    var xmlText = convertXmlTagsToLowerCase(xmlFile.readAsStringSync());
    final XmlDocument document = XmlDocument.parse(xmlText);
    XmlElement raceCourseDataElement = document.getElement('coursedata')!.getElement("racecoursedata")!;
    XmlElement courseElements = raceCourseDataElement.findElements('course').first;
    List<Checkpoint> checkpoints = OrienteeringIofXmlFileReader.extractCourseData_(courseElements, checkpointFeatures);
    for (int i = 0; i < checkpoints.length; ++i) {
      expect(checkpoints[i].points, expected[i]);
    }
  });
}
