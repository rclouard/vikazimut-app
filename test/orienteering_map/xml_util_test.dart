import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/orienteering_map/xml_util.dart';

void main() {
  test('test convert XML tags to lowercase', () {
    var actual = convertXmlTagsToLowerCase("<Tag><SubTag></SubTag></Tag>");
    expect(actual, "<tag><subtag></subtag></tag>");
  });

  test('test convert XML tags to lowercase when empty', () {
    var actual = convertXmlTagsToLowerCase("");
    expect(actual, "");
  });
}
