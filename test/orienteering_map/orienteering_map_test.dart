import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/orienteering_map/checkpoint_out_of_bounds_exception.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

void main() async {
  test('Test getCheckpoint start', () {
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "S1", 0, 0, 1),
      Checkpoint(1, "31", 0, 0, 1),
    ];
    OrienteeringMap map = OrienteeringMap("foo", checkpoints, 0, 0, Discipline.URBANO, 22);
    Checkpoint cp = map.getCheckpointFromName_("S1", 0);
    expect(0, cp.index);
  });

  test('Test getCheckpoint not punched()', () {
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "S1", 0, 0, 1),
      Checkpoint(1, "31", 0, 0, 1),
      Checkpoint(2, "32", 0, 0, 1),
    ];
    OrienteeringMap map = OrienteeringMap("foo", checkpoints, 0, 0, Discipline.URBANO, 22);
    Checkpoint cp = map.getCheckpointFromName_("31", 0);
    expect(1, cp.index);
  });

  test('Test getCheckpoint unknown()', () {
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "S1", 0, 0, 1),
      Checkpoint(1, "31", 0, 0, 1),
      Checkpoint(2, "32", 0, 0, 1),
    ];
    OrienteeringMap map = OrienteeringMap("foo", checkpoints, 0, 0, Discipline.URBANO, 22);
    expect(() => map.getCheckpointFromName_("33", 0), throwsA(isInstanceOf<CheckpointOutOfBoundsException>()));
  });

  test('Test getCheckpoint butterfly', () {
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "S1", 0, 0, 1),
      Checkpoint(1, "31", 0, 0, 1),
      Checkpoint(2, "32", 0, 0, 1),
      Checkpoint(3, "31", 0, 0, 1),
      Checkpoint(4, "33", 0, 0, 1),
    ];
    OrienteeringMap map = OrienteeringMap("foo", checkpoints, 0, 0, Discipline.URBANO, 22);
    Checkpoint cp = map.getCheckpointFromName_("31", 2);
    expect(3, cp.index);
  });

  test('Test isInBounds when in', () {
    OrienteeringMap map = OrienteeringMap("foo", [], 0, 0, Discipline.URBANO, 22);
    map.setLocation(20, 10, 10, -10);
    var actual = map.isInBounds(15, 0);
    expect(actual, true);
  });

  test('Test isInBounds when out on longitude', () {
    OrienteeringMap map = OrienteeringMap("foo", [], 0, 0, Discipline.URBANO, 22);
    map.setLocation(20, 10, 10, -10);
    var actual = map.isInBounds(15, -20);
    expect(actual, false);
  });

  test('Test isInBounds when out on latitude', () {
    OrienteeringMap map = OrienteeringMap("foo", [], 0, 0, Discipline.URBANO, 22);
    map.setLocation(20, 10, 10, -10);
    var actual = map.isInBounds(21, 0);
    expect(actual, false);
  });

  test('Test getCheckpointLocationFromId', () {
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "S1", 0, 0, 1),
      Checkpoint(1, "31", 0, 0, 1),
      Checkpoint(2, "32", 10, 20, 1),
      Checkpoint(3, "33", 0, 0, 1),
      Checkpoint(4, "34", 0, 0, 1),
    ];
    OrienteeringMap map = OrienteeringMap("foo", checkpoints, 0, 0, Discipline.URBANO, 22);

    var actual = map.getCheckpointLocationFromId(2);
    expect(actual.latitude, 10);
    expect(actual.longitude, 20);
  });

  test('Test map orienteering accessors', () {
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "S1", 0, 0, 1),
      Checkpoint(1, "31", 0, 0, 1),
      Checkpoint(2, "32", 10, 20, 1),
      Checkpoint(3, "33", 0, 0, 1),
      Checkpoint(4, "34", 0, 0, 1),
    ];
    OrienteeringMap map = OrienteeringMap("foo", checkpoints, 0, 0, Discipline.URBANO, 22);
    expect(map.getCheckpointCount(), 5);
    expect(map.getCheckpoint(1), checkpoints[1]);

    expect(map.getCheckpointNameFromId(3), checkpoints[3].id);
    GeodesicPoint currentLocation = GeodesicPoint(checkpoints[2].location.latitude, checkpoints[2].location.longitude);
    expect(map.getDistanceToCheckpoint(currentLocation, 2), 0);
    map.setLocation(1, 2, 3, 4);
    map.rotation = 5;
    LatLonBox box = map.getBoundingBox();
    expect(box.north, 1);
    expect(box.south, 2);
    expect(box.east, 3);
    expect(box.west, 4);
    expect(box.rotation, 5);
  });
}
