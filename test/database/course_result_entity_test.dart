import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_entity.dart';

void main() {
  test('Test course entity', () {
    const int mode = 1;
    const int format = 0;
    const int validationMode = 0;
    const String mapName = "Campus II Discovery";
    const int checkpointCount = 5;
    const int totalTime = 1000;
    const int courseId = 1;
    const int date = 0;
    const String punchTimes = "0;1000;0;2000";
    const String gpsTrack = "gpsTrack";
    const int assistanceCount = 10;
    const int quizTotalPoints = 1;
    const int discipline = 0;

    CourseResultEntity courseResultEntity = CourseResultEntity(
      mode: mode,
      format: format,
      validationMode: validationMode,
      totalTimeInMillisecond: totalTime,
      courseId: courseId,
      baseTimeInMillisecond: date,
      gpsTrack: gpsTrack,
      punchTimes: punchTimes,
      mapName: mapName,
      checkpointCount: checkpointCount,
      assistanceCount: assistanceCount,
      quizTotalPoints: quizTotalPoints,
      discipline: discipline,
    );
    expect(courseResultEntity.format, format);
    expect(courseResultEntity.totalTimeInMillisecond, totalTime);
    expect(courseResultEntity.courseId, courseId);
    expect(courseResultEntity.baseTimeInMillisecond, date);
    expect(courseResultEntity.gpsTrack, gpsTrack);
    expect(courseResultEntity.mapName, mapName);
    expect(courseResultEntity.assistanceCount, assistanceCount);
    expect(courseResultEntity.quizTotalPoints, 1);
    expect(courseResultEntity.discipline, 0);
    expect(courseResultEntity.isSent, false);
  });
}
