import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_dao.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/database/database_singleton.dart';
import 'package:vikazimut/database/punch_time_entity.dart';
import 'package:vikazimut/database/waypoint_entity.dart';

void main() {
  group('database tests', () {
    late DatabaseSingleton database;
    late CourseResultDao courseResultDao;

    setUp(() async {
      database = await $FloorDatabaseSingleton.inMemoryDatabaseBuilder().build();
      courseResultDao = database.courseResultDao;
    });

    tearDown(() async {
      await database.close();
    });

    test('test database initially is empty', () async {
      final actual = await courseResultDao.findAllFinishedCourseResults();
      expect(actual, isEmpty);
    });

    group('Test complete and unfinished course result', () {
      late DatabaseSingleton database;
      late CourseResultDao courseResultDao;

      setUp(() async {
        database = await $FloorDatabaseSingleton.inMemoryDatabaseBuilder().build();
        courseResultDao = database.courseResultDao;
      });

      tearDown(() async {
        await database.close();
      });

      test('Test find finished when insert finished result', () async {
        final courseResultEntity = buildCourseResultEntity(1);
        courseResultEntity.finished = 1;
        await courseResultDao.insertCourseResult(courseResultEntity);
        final actual = await courseResultDao.findAllFinishedCourseResults();
        expect(actual, hasLength(1));
      });

      test('Test find finished when insert unfinished result', () async {
        final courseResultEntity = buildCourseResultEntity(1);
        courseResultEntity.finished = 0;
        await courseResultDao.insertCourseResult(courseResultEntity);
        final actual = await courseResultDao.findAllFinishedCourseResults();
        expect(actual, hasLength(0));
      });

      test('Test find finished when insert unfinished result', () async {
        final courseResultEntity = buildCourseResultEntity(1);
        courseResultEntity.finished = 0;
        await courseResultDao.insertCourseResult(courseResultEntity);
        final actual = await courseResultDao.findAllUnfinishedCourse();
        expect(actual, isNotNull);
      });

      test('delete result', () async {
        var courseResultEntity = buildCourseResultEntity(2);
        int id = await courseResultDao.insertCourseResult(courseResultEntity);
        // create a copy with the correct id
        var courseResultEntity2 = CourseResultEntity(
          mode: courseResultEntity.mode,
          format: courseResultEntity.format,
          validationMode: courseResultEntity.validationMode,
          totalTimeInMillisecond: courseResultEntity.totalTimeInMillisecond,
          courseId: courseResultEntity.courseId,
          baseTimeInMillisecond: courseResultEntity.baseTimeInMillisecond,
          gpsTrack: courseResultEntity.gpsTrack,
          punchTimes: courseResultEntity.punchTimes,
          mapName: courseResultEntity.mapName,
          checkpointCount: courseResultEntity.checkpointCount,
          assistanceCount: courseResultEntity.assistanceCount,
          quizTotalPoints: courseResultEntity.quizTotalPoints,
          discipline: courseResultEntity.discipline,
          sent: courseResultEntity.sent,
          id: id,
        );
        await courseResultDao.deleteCourseResult(courseResultEntity2);
        final actual = await courseResultDao.findAllFinishedCourseResults();
        expect(actual, isEmpty);
      });

      test('update result', () async {
        final courseResultEntity = buildCourseResultEntity(3);
        courseResultEntity.sent = 0;
        int id = await courseResultDao.insertCourseResult(courseResultEntity);
        final actual = await courseResultDao.findCourseResult(id);
        expect(actual?.sent, 0);
        courseResultEntity.sent = 1;
        await courseResultDao.updateSentFlagForCourseResult(id);
        final actual2 = await courseResultDao.findCourseResult(id);
        expect(actual2?.sent, 1);
      });
    });
  });

  group('Test insert and delete waypoints', () {
    late DatabaseSingleton database;
    late CourseResultDao courseResultDao;

    setUp(() async {
      database = await $FloorDatabaseSingleton.inMemoryDatabaseBuilder().build();
      courseResultDao = database.courseResultDao;
    });

    tearDown(() async {
      await database.close();
    });

    test('Test insert and delete waypoint', () async {
      WaypointEntity waypointEntity1 = WaypointEntity(
        latitude: 10,
        longitude: -10,
        timestampInMillisecond: 20,
        altitude: 30,
      );
      WaypointEntity waypointEntity2 = WaypointEntity(
        latitude: 20,
        longitude: -20,
        timestampInMillisecond: 30,
        altitude: 40,
      );
      await courseResultDao.insertWaypoint(waypointEntity1);
      await courseResultDao.insertWaypoint(waypointEntity2);
      final actual = await courseResultDao.findAllWaypoints();
      expect(actual.length, 2);
      expect(actual[0].timestampInMillisecond, 20);
      expect(actual[1].timestampInMillisecond, 30);
    });

    test('delete waypoints', () async {
      await courseResultDao.deleteAllWaypoints();
      final actual = await courseResultDao.findAllWaypoints();
      expect(actual.isEmpty, true);
    });
  });

  group('Test insert and delete punch times', () {
    late DatabaseSingleton database;
    late CourseResultDao courseResultDao;

    setUp(() async {
      database = await $FloorDatabaseSingleton.inMemoryDatabaseBuilder().build();
      courseResultDao = database.courseResultDao;
    });

    tearDown(() async {
      await database.close();
    });

    test('Test insert and delete punch time', () async {
      PunchTimeEntity punchTime1 = PunchTimeEntity(
        checkpointId: 1,
        timestampInMillisecond: 20,
        forced: 1,
      );
      PunchTimeEntity punchTime2 = PunchTimeEntity(
        checkpointId: 2,
        timestampInMillisecond: 30,
        forced: 0,
      );
      await courseResultDao.insertPunchTime(punchTime1);
      await courseResultDao.insertPunchTime(punchTime2);
      final actual = await courseResultDao.findAllPunchTimes();
      expect(actual.length, 2);
      expect(actual[0].checkpointId, 1);
      expect(actual[1].checkpointId, 2);
    });

    test('delete all punch times', () async {
      await courseResultDao.deleteAllPunchTimes();
      final actual = await courseResultDao.findAllPunchTimes();
      expect(actual.isEmpty, true);
    });
  });

  group('Test updateAssistanceCountForCourseResult and updateQuizPointsForCourseResult', () {
    late DatabaseSingleton database;
    late CourseResultDao courseResultDao;

    setUp(() async {
      database = await $FloorDatabaseSingleton.inMemoryDatabaseBuilder().build();
      courseResultDao = database.courseResultDao;
    });

    tearDown(() async {
      await database.close();
    });

    test('Test updateAssistanceCountForCourseResult', () async {
      final courseResultEntity = buildCourseResultEntity(1);
      courseResultEntity.finished = 1;
      await courseResultDao.insertCourseResult(courseResultEntity);
      await courseResultDao.updateAssistanceCountForCourseResult(1, 3);
      final actual = await courseResultDao.findAllFinishedCourseResults();
      expect(actual[0].assistanceCount, 3);
    });

    test('test updateQuizPointsForCourseResult', () async {
      final courseResultEntity = buildCourseResultEntity(1);
      courseResultEntity.finished = 1;
      await courseResultDao.insertCourseResult(courseResultEntity);
      await courseResultDao.updateQuizPointsForCourseResult(1, 5);
      final actual = await courseResultDao.findAllFinishedCourseResults();
      expect(actual[0].quizTotalPoints, 5);
    });
  });
}

CourseResultEntity buildCourseResultEntity(int courseId) {
  const int mode = 1;
  const int format = 0;
  const int validationMode = 0;
  const String mapName = "Campus II Discovery";
  const int checkpointCount = 5;
  const int totalTime = 1000;
  const int date = 0;
  const String punchTimes = "0;1000;0;2000";
  const String gpsTrack = "gpsTrack";
  const int assistanceCount = 10;
  const int quizTotalPoints = 1;
  return CourseResultEntity(
    mode: mode,
    format: format,
    validationMode: validationMode,
    totalTimeInMillisecond: totalTime,
    courseId: courseId,
    baseTimeInMillisecond: date,
    gpsTrack: gpsTrack,
    punchTimes: punchTimes,
    mapName: mapName,
    checkpointCount: checkpointCount,
    assistanceCount: assistanceCount,
    quizTotalPoints: quizTotalPoints,
    discipline: 0,
  );
}
