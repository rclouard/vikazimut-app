import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_dao.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/database/database_singleton.dart';
import 'package:vikazimut/database/waypoint_entity.dart';

void main() {
  group('Test insert and delete waypoints', () {
    late DatabaseSingleton database;
    late CourseResultDao courseResultDao;

    setUp(() async {
      database = await $FloorDatabaseSingleton.inMemoryDatabaseBuilder().build();
      courseResultDao = database.courseResultDao;
    });

    tearDown(() async {
      await database.close();
    });

    test('Test find waypoint with sorted waypoints', () async {
      WaypointEntity waypointEntity1 = WaypointEntity(
        latitude: 10,
        longitude: -10,
        timestampInMillisecond: 2,
        altitude: 30,
      );
      WaypointEntity waypointEntity2 = WaypointEntity(
        latitude: 20,
        longitude: -20,
        timestampInMillisecond: 3,
        altitude: 40,
      );
      WaypointEntity waypointEntity3 = WaypointEntity(
        latitude: 20,
        longitude: -20,
        timestampInMillisecond: 1,
        altitude: 40,
      );

      await courseResultDao.insertWaypoint(waypointEntity1);
      await courseResultDao.insertWaypoint(waypointEntity2);
      await courseResultDao.insertWaypoint(waypointEntity3);

      final actual = await ResultDatabaseGateway.findAllWaypoints_(database);
      expect(actual.length, 3);
      expect(actual[0].timestampInMillisecond, 1);
      expect(actual[1].timestampInMillisecond, 2);
      expect(actual[2].timestampInMillisecond, 3);
    });
  });
}
