import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/waypoint_entity.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

void main() {
  test('Test sequence setGpsRoute and getGpsRouteAsString', () {
    List<WaypointEntity> waypoints = [
      WaypointEntity(
        latitude: 50.123456,
        longitude: 50.123456,
        timestampInMillisecond: 10,
        altitude: 100,
      ),
      WaypointEntity(latitude: -50.123456, longitude: -50.123456, timestampInMillisecond: 20, altitude: 200),
    ];
    var actual = WaypointEntity.convertToString(waypoints);
    String expected = "50.123456,50.123456,10,100;-50.123456,-50.123456,20,200;";
    expect(actual, expected);
  });

  test('Test sort waypoints', () {
    List<WaypointEntity> waypoints = [
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 100, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 20, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 220, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 219, altitude: 0),
    ];
    waypoints.sort();
    List<int> expected = [20, 100, 219, 220];
    for (int i = 0; i < waypoints.length; i++) {
      expect(waypoints[i].timestampInMillisecond, expected[i]);
    }
  });

  test('Test getTotalTime with empty list', () {
    List<WaypointEntity> waypoints = [];
    var actual = WaypointEntity.getTotalTimeInMs(waypoints);
    expect(actual, 0);
  });

  test('Test getTotalTime with not empty list', () {
    List<WaypointEntity> waypoints = [
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 20, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 110, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 200, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 219, altitude: 0),
    ];
    var actual = WaypointEntity.getTotalTimeInMs(waypoints);
    expect(actual, 219);
  });

  test('Test toGeodesicPoints', () {
    List<WaypointEntity> waypoints = [
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 20, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 110, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 200, altitude: 0),
      WaypointEntity(latitude: 0, longitude: 0, timestampInMillisecond: 219, altitude: 0),
    ];
    List<GeodesicPoint> geodesicPoints = [
      GeodesicPoint(0, 0, 20),
      GeodesicPoint(0, 0, 110),
      GeodesicPoint(0, 0, 200),
      GeodesicPoint(0, 0, 219),
    ];
    var actual = WaypointEntity.toGeodesicPoints(waypoints);
    for (var i = 0; i < actual.length; ++i) {
      expect(actual[i].timestampInMillisecond, geodesicPoints[i].timestampInMillisecond);
    }
  });

  test('Test convertFromString when no track', () {
    var actual = WaypointEntity.convertFromString(null);
    expect(actual.isEmpty, true);
  });

  test('Test convertFromString', () {
    String waypointString = "50.123456,-50.123456,10,100;-50.123456,50.123456,20,200;";
    List<WaypointEntity> waypoints = [
      WaypointEntity(
        latitude: 50.123456,
        longitude: -50.123456,
        timestampInMillisecond: 10,
        altitude: 100,
      ),
      WaypointEntity(
        latitude: -50.123456,
        longitude: 50.123456,
        timestampInMillisecond: 20,
        altitude: 200,
      ),
    ];
    var actual = WaypointEntity.convertFromString(waypointString);
    expect(actual.length, waypoints.length);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].latitude, waypoints[i].latitude);
      expect(actual[i].longitude, waypoints[i].longitude);
      expect(actual[i].timestampInMillisecond, waypoints[i].timestampInMillisecond);
      expect(actual[i].altitude, waypoints[i].altitude);
    }
  });
}
