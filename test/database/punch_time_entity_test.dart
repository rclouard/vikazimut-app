import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/punch_time_entity.dart';
import 'package:vikazimut/route_data/punch_time.dart';

void main() {
  test('Test toPunchTime', () {
    List<PunchTimeEntity> punchTimeEntities = [
      PunchTimeEntity(checkpointId: 4, timestampInMillisecond: 4, forced: 0),
      PunchTimeEntity(checkpointId: 2, timestampInMillisecond: 2, forced: 0),
      PunchTimeEntity(checkpointId: 6, timestampInMillisecond: 7, forced: 1),
      PunchTimeEntity(checkpointId: 1, timestampInMillisecond: 0, forced: 0),
    ];
    List<PunchTime> expected = [
      PunchTime(-1, false),
      PunchTime(0, false),
      PunchTime(2, false),
      PunchTime(-1, false),
      PunchTime(4, false),
      PunchTime(-1, false),
      PunchTime(7, true),
      PunchTime(-1, false),
    ];
    var actual = PunchTimeEntity.toPunchTimeList(punchTimeEntities, 8);
    for (var i = 0; i < actual.length; ++i) {
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
      expect(actual[i].forced, expected[i].forced);
    }
  });

  test('Test convertToString', () {
    List<PunchTime> punchTimes = [
      PunchTime(-1, false),
      PunchTime(0, false),
      PunchTime(2, true),
      PunchTime(-1, false),
      PunchTime(4, false),
      PunchTime(-1, false),
      PunchTime(7, true),
      PunchTime(-1, false),
    ];
    String expected = "-1;0;2:true;-1;4;-1;7:true;-1";

    String actual = PunchTimeEntity.convertToString_(punchTimes);
    expect(actual, expected);
  });

  test('Test convertFromString', () {
    String route = "-1;0;2:true;-1;4;-1;7:true;-1";
    List<PunchTimeEntity> expected = [
      PunchTimeEntity(checkpointId: 0, timestampInMillisecond: -1, forced: 0),
      PunchTimeEntity(checkpointId: 1, timestampInMillisecond: 0, forced: 0),
      PunchTimeEntity(checkpointId: 2, timestampInMillisecond: 2, forced: 1),
      PunchTimeEntity(checkpointId: 3, timestampInMillisecond: -1, forced: 0),
      PunchTimeEntity(checkpointId: 4, timestampInMillisecond: 4, forced: 0),
      PunchTimeEntity(checkpointId: 5, timestampInMillisecond: -1, forced: 0),
      PunchTimeEntity(checkpointId: 6, timestampInMillisecond: 7, forced: 1),
      PunchTimeEntity(checkpointId: 7, timestampInMillisecond: -1, forced: 0),
    ];

    List<PunchTimeEntity> actual = PunchTimeEntity.convertFromString(route);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].checkpointId, expected[i].checkpointId);
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
      expect(actual[i].forced, expected[i].forced);
    }
  });
}
