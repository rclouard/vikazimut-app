import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/help_activity/manual.dart';

void main() {
  test('Parse gcu json file', () {
    String jsonContent = '''
      {
        "image": "image.png",
         "sections": [
            {
              "menu_icon": "icon.png",
              "title": "title",
              "actions": [
                  "action1"
              ]
            }
         ] 
      }
    ''';
    var parsedJson = jsonDecode(jsonContent);
    var manual = Manual.fromJson(parsedJson);
    expect(manual.image, "image.png");
    expect(manual.sections[0].menuIcon, "icon.png");
    expect(manual.sections[0].actions[0], "action1");
  });
}
