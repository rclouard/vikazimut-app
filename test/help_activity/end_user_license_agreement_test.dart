import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/help_activity/end_user_license_agreement.dart';

void main() {
  test('Parse cgu json file', () {
    String jsonContent = '''
      {
        "preamble": "preamble",
        "articles": [
         {
            "title": "Article1",
            "color": "#4caf50",
            "body": [
                "sentence"
            ]
         }
        ]
      }
    ''';
    var parsedJson = jsonDecode(jsonContent);
    var gcu = EndUserLicenseAgreement.fromJson(parsedJson);
    expect(gcu.preamble, "preamble");
    expect(gcu.articles[0].title, "Article1");
    expect(gcu.articles[0].paragraphs[0], "sentence");
  });
}
