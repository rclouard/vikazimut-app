import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/help_activity/credits_view.dart';

void main() {
  test('Test format the list of strings as one string separated by end of line', () {
    expect(CreditsView.formatNamesInColumn_(["a", "b", "c"]), "a\nb\nc");
  });
}
