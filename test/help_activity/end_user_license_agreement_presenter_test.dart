import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/help_activity/end_user_license_agreement_presenter.dart';

void main() {
  test('Test format the list of strings as one string separated with end of line', () {
    var color = EndUserLicenseAgreementPresenter.getColorFromHex("#4caf50");
    expect(color, 0xff4caf50);
  });
}
