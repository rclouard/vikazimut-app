import 'package:flutter_test/flutter_test.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/server_gateway/live_tracking_gateway.dart';

void main() {
  Position createPosition(double latitude, double longitude, int time) {
    return Position(
      latitude: latitude,
      longitude: longitude,
      timestamp: DateTime.fromMillisecondsSinceEpoch(time * 1000),
      accuracy: 0,
      altitude: 0,
      altitudeAccuracy: 0,
      heading: 0,
      headingAccuracy: 0,
      speed: 0,
      speedAccuracy: 0,
    );
  }

  test('test getPositionsFromPendingPosition_ when no pending position', () {
    Position position = createPosition(49.12345, -0.12345, 30);
    var positions = LiveTrackingGateway.getPositionsFromPendingPosition_([], position);
    expect(positions['latitudes'], "49.12345");
    expect(positions['longitudes'], "-0.12345");
  });

  test('test getPositionsFromPendingPosition_ when one pending position at -9 s', () {
    var positions = LiveTrackingGateway.getPositionsFromPendingPosition_(
      [
        createPosition(49.12345, -0.12345, 9),
      ],
      createPosition(49.12345, -0.12345, 10),
    );
    expect(positions['latitudes'], "49.12345");
    expect(positions['longitudes'], "-0.12345");
  });

  test('test getPositionsFromPendingPosition_ when 3 pending position at -10, -20, -30 s', () {
    var positions = LiveTrackingGateway.getPositionsFromPendingPosition_(
      [
        createPosition(53.12345, -4.12345, 00),
        createPosition(52.12345, -3.12345, 10),
        createPosition(51.12345, -2.12345, 20),
        createPosition(50.12345, -1.12345, 30),
      ],
      createPosition(49.12345, -0.12345, 40),
    );
    expect(positions['latitudes'], "49.12345|50.12345|51.12345");
    expect(positions['longitudes'], "-0.12345|-1.12345|-2.12345");
  });

  test('test getPositionsFromPendingPosition_ when 1 pending position at -40 s', () {
    var positions = LiveTrackingGateway.getPositionsFromPendingPosition_(
      [
        createPosition(51.12345, -2.12345, 90),
        createPosition(50.12345, -1.12345, 1000),
      ],
      createPosition(49.12345, -0.12345, 40000),
    );
    expect(positions['latitudes'], "49.12345");
    expect(positions['longitudes'], "-0.12345");
  });

  test('test getPositionsFromPendingPosition_ when 2 pending position > -40 s', () {
    var positions = LiveTrackingGateway.getPositionsFromPendingPosition_(
      [
        createPosition(51.12345, -2.12345, 40),
        createPosition(50.12345, -1.12345, 60),
      ],
      createPosition(49.12345, -0.12345, 100),
    );
    expect(positions['latitudes'], "49.12345");
    expect(positions['longitudes'], "-0.12345");
  });

  test('test getPositionsFromPendingPosition_ when multiple pending positions', () {
    var positions = LiveTrackingGateway.getPositionsFromPendingPosition_(
      [
        createPosition(57.12345, -7.12345, 60),
        createPosition(56.12345, -6.12345, 69),
        createPosition(55.12345, -5.12345, 71),
        createPosition(54.12345, -4.12345, 79),
        createPosition(53.12345, -3.12345, 81),
        createPosition(52.12345, -2.12345, 90),
        createPosition(51.12345, -2.12345, 95),
        createPosition(50.12345, -1.12345, 99),
      ],
      createPosition(49.12345, -0.12345, 100),
    );
    expect(positions['latitudes'], '49.12345|52.12345|54.12345');
    expect(positions['longitudes'], '-0.12345|-2.12345|-4.12345');
  });
}
