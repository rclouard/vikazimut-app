import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/route_data/punch_time.dart';
import 'package:vikazimut/server_gateway/result_data.dart';

void main() {
  test('Result data to json when nominal', () {
    String orienteer = "user";
    int totalTime = 10;
    int courseId = 222;
    String trace = "49.214050,-0.367733,22,10.0;49.214050,-0.367752,3103,100.2";
    List<PunchTime> visitTimes = [
      PunchTime(0),
      PunchTime(100, true),
      PunchTime(330),
      PunchTime(470),
      PunchTime(3103),
    ];
    ResultData resultData = ResultData(
      orienteer: orienteer,
      format: 0,
      totalTimeInMillisecond: totalTime,
      gpsTrack: trace,
      courseId: courseId,
      punchTimeInMillisecondList: visitTimes.map((punchTime) => punchTime.timestampInMillisecond).toList(),
      punchForceList: visitTimes.map((punchTime) => punchTime.forced).toList(),
      runCount: 1,
    );
    String punchTimes1 = '[{controlPoint: 0, punchTime: 0}, {controlPoint: 1, punchTime: 100, forced: true}, {controlPoint: 2, punchTime: 330}, {controlPoint: 3, punchTime: 470}, {controlPoint: 4, punchTime: 3103}]';

    Map result = resultData.toJson();
    expect(result["orienteer"], orienteer);
    expect(result["totalTime"], totalTime);
    expect(result["courseId"], courseId);
    expect(result["controlPoints"].toString(), punchTimes1);
    expect(result["trace"], trace);
  });
}
