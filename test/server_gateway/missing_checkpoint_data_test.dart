import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/server_gateway/missing_checkpoint_data.dart';

void main() {
  test('Missing checkpoint data to json when nominal', () {
    String courseId = "1";
    String checkpointId = "2";
    MissingCheckpointData missionCheckpointData = MissingCheckpointData(courseId, checkpointId);
    Map result = missionCheckpointData.toJson();
    expect(result["courseId"], courseId);
    expect(result["controlPointId"], checkpointId);
  });
}
