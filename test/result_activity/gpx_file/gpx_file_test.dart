import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/result_activity/gpx_file/gpx_file.dart';
import 'package:vikazimut/result_activity/result.dart';

void main() {
  test('Test build gpx contents form waypoints', () {
    List<GeodesicPoint> geodesicPoints = [GeodesicPoint(50.1, -0.1, 1, 1), GeodesicPoint(-49.2, -0.2, 2, 2)];
    CourseResultEntity entity = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      totalTimeInMillisecond: 0,
      courseId: 1,
      baseTimeInMillisecond: 0,
      gpsTrack: Result.computeWaypointsAsString_(geodesicPoints),
      punchTimes: "",
      mapName: "Actual name",
      checkpointCount: 6,
      assistanceCount: 0,
      quizTotalPoints: -1,
      discipline: 0,
    );
    Result result = Result(entity);
    var actual = GPXFile.buildGPXContentFromResult_(result);
    const String expected = '<?xml version="1.0" encoding="UTF-8" standalone="no" ?><gpx version="1.1" creator="Vikazimut" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"><trk><name>Actual name</name><trkseg><trkpt lat="50.1" lon="-0.1"><time>1970-01-01T00:00:00.001Z</time><ele>1.0</ele></trkpt><trkpt lat="-49.2" lon="-0.2"><time>1970-01-01T00:00:00.002Z</time><ele>2.0</ele></trkpt></trkseg></trk></gpx>';
    expect(actual, expected);
  });
}
