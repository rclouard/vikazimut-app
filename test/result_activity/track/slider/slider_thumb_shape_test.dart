import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/result_activity/track/relief_helper.dart';
import 'package:vikazimut/result_activity/track/slider/slider_thumb_shape.dart';

void main() {
  test('Test get current speed related to slider thumb position when no waypoint', () {
    List<ReliefWaypoint> waypoints = [];
    final actual = AnimationSlideThumbShape.getSpeedInKmhForCurrentThumbPosition_(waypoints, 0, 0);
    expect(actual, 0);
  });

  test('Test get current speed related to slider at start position with 2 waypoints', () {
    List<ReliefWaypoint> waypoints = [
      const ReliefWaypoint(0, 0, 0),
      const ReliefWaypoint(0, 10, 10),
    ];
    final actual = AnimationSlideThumbShape.getSpeedInKmhForCurrentThumbPosition_(waypoints, 0, 10);
    expect(actual, moreOrLessEquals(0, epsilon: 0.01));
  });

  test('Test get current speed related to slider at half position with 2 waypoints', () {
    List<ReliefWaypoint> waypoints = [
      const ReliefWaypoint(0, 0, 0),
      const ReliefWaypoint(0, 10, 10),
    ];
    final actual = AnimationSlideThumbShape.getSpeedInKmhForCurrentThumbPosition_(waypoints, 0.5, 10);
    expect(actual, moreOrLessEquals(3.6 * 10, epsilon: 0.01));
  });

  test('Test get current speed related to slider at end position with 2 waypoints', () {
    List<ReliefWaypoint> waypoints = [
      const ReliefWaypoint(0, 0, 0),
      const ReliefWaypoint(0, 10, 10),
    ];
    final actual = AnimationSlideThumbShape.getSpeedInKmhForCurrentThumbPosition_(waypoints, 1, 10);
    expect(actual, moreOrLessEquals(36, epsilon: 0.01));
  });
}
