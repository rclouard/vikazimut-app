import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/result_activity/result.dart';
import 'package:vikazimut/result_activity/sport_global_result_presenter.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

void main() {
  test('Test sport results 1', () {
    List<GeodesicPoint> geodesicPoints = [
      GeodesicPoint(50.123456, 50.123456, 20, 13),
      GeodesicPoint(-50.123456, -50.123456, 23, 16),
    ];
    CourseResultEntity entity = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      totalTimeInMillisecond: 0,
      courseId: 0,
      baseTimeInMillisecond: 0,
      gpsTrack: Result.computeWaypointsAsString_(geodesicPoints),
      punchTimes: "",
      mapName: "",
      checkpointCount: 7,
      assistanceCount: 0,
      quizTotalPoints: -1,
      discipline: 0,
    );
    var presenter = SportGlobalResultPresenter(resultId: 1);
    presenter.result = Result(entity);
    expect(presenter.isSportMode, true);
    expect(presenter.isStatisticsAvailable, true);
    expect(presenter.createCourseFormatMessagePart1_(), "sport_mode");
    expect(presenter.createCourseFormatMessagePart2_(), "preset_order");
    entity.sent = 0;
    expect(presenter.isSendResultAvailable(Result(entity)), true);
  });

  test('Test sport results 2', () {
    List<GeodesicPoint> geodesicPoints = [
      GeodesicPoint(50.123456, 50.123456, 20, 13),
      GeodesicPoint(-50.123456, -50.123456, 23, 16),
    ];
    CourseResultEntity entity = CourseResultEntity(
      mode: 1,
      format: 1,
      validationMode: 1,
      totalTimeInMillisecond: 0,
      courseId: 0,
      baseTimeInMillisecond: 0,
      gpsTrack: Result.computeWaypointsAsString_(geodesicPoints),
      punchTimes: "",
      mapName: "",
      checkpointCount: 7,
      assistanceCount: 0,
      quizTotalPoints: -1,
      discipline: 0,
    );
    var presenter = SportGlobalResultPresenter(resultId: 1);
    presenter.result = Result(entity);
    expect(presenter.isSportMode, true);
    expect(presenter.isStatisticsAvailable, true);
    expect(presenter.createCourseFormatMessagePart1_(), "playful_mode");
    expect(presenter.createCourseFormatMessagePart2_(), "free_order");
    entity.sent = 1;
    expect(presenter.isSendResultAvailable(Result(entity)), false);
  });
}
