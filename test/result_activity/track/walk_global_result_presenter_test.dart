import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/result_activity/result.dart';
import 'package:vikazimut/result_activity/walk_global_result_presenter.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

void main() {
  test('Test sport results', () {
    List<GeodesicPoint> geodesicPoints = [
      GeodesicPoint(50.123456, 50.123456, 20, 13),
      GeodesicPoint(-50.123456, -50.123456, 23, 16),
    ];
    CourseResultEntity entity = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      totalTimeInMillisecond: 0,
      courseId: 0,
      baseTimeInMillisecond: 0,
      gpsTrack: Result.computeWaypointsAsString_(geodesicPoints),
      punchTimes: "",
      mapName: "",
      checkpointCount: 7,
      assistanceCount: 0,
      quizTotalPoints: -1,
      discipline: 0,
    );
    var presenter = WalkGlobalResultPresenter(resultId: 1);
    expect(presenter.isSportMode, false);
    expect(presenter.isStatisticsAvailable, false);
    expect(presenter.createCourseFormatMessage_(), "walk_mode");
    expect(presenter.isSendResultAvailable(Result(entity)), false);
  });
}
