import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/result_activity/elevation_helper.dart';
import 'package:vikazimut/result_activity/track/relief_helper.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

void main() {
  test('Test compute bounds when empty route', () {
    List<GeodesicPoint> route = [];
    var actual = ReliefHelper.computeAltitudeBounds(route);
    expect(actual, [0, 0]);
  });

  test('Test compute bounds when nominal', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 100),
      GeodesicPoint(0, 0, 0, 76),
      GeodesicPoint(0, 0, 0, 76),
      GeodesicPoint(0, 0, 0, 89),
      GeodesicPoint(0, 0, 0, 90),
      GeodesicPoint(0, 0, 0, 100),
      GeodesicPoint(0, 0, 0, 99),
    ];
    var actual = ReliefHelper.computeAltitudeBounds(route);
    expect(actual, [76, 100]);
  });

  test('Test compute bounds when no altitude values', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 0),
      GeodesicPoint(0, 0, 0, ElevationHelper.NO_ELEVATION),
      GeodesicPoint(0, 0, 0, ElevationHelper.NO_ELEVATION),
      GeodesicPoint(0, 0, 0, ElevationHelper.NO_ELEVATION),
    ];
    var actual = ReliefHelper.computeAltitudeBounds(route);
    expect(actual, [0, 0]);
  });

  test('Test compute bounds when some missing altitudes', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 11),
      GeodesicPoint(0, 0, 0, ElevationHelper.NO_ELEVATION),
      GeodesicPoint(0, 0, 0, 10),
      GeodesicPoint(0, 0, 0, 50),
    ];
    var actual = ReliefHelper.computeAltitudeBounds(route);
    expect(actual, [10, 50]);
  });

  test('Test shorten route when the size is larger then the number of points', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 100),
      GeodesicPoint(0, 1, 100, 76),
      GeodesicPoint(0, 2, 200, 76),
      GeodesicPoint(0, 3, 300, 89),
      GeodesicPoint(0, 4, 400, 90),
      GeodesicPoint(0, 5, 500, 100),
      GeodesicPoint(0, 6, 600, 99),
    ];
    var actual = ReliefHelper.shortenRoute_(route, 600, 6);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timeInMillisecond, route[i].timestampInMillisecond);
    }
  });

  test('Test shorten route when nominal', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 100),
      GeodesicPoint(0, 1, 100, 76),
      GeodesicPoint(0, 2, 200, 76),
      GeodesicPoint(0, 3, 300, 89),
      GeodesicPoint(0, 4, 400, 90),
      GeodesicPoint(0, 5, 500, 100),
      GeodesicPoint(0, 6, 600, 99),
    ];
    List<ReliefWaypoint> expected = [
      const ReliefWaypoint(0, 0, 0),
      const ReliefWaypoint(0, 100, 1113194.9079327357),
      const ReliefWaypoint(0, 300, 1113194.9079327357),
      const ReliefWaypoint(0, 400, 1113194.9079327357),
      const ReliefWaypoint(0, 600, 1113194.9079327357),
    ];
    var actual = ReliefHelper.shortenRoute_(route, 600, 4);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timeInMillisecond, expected[i].timeInMillisecond);
      expect(actual[i].speedInMeterPerSecond, expected[i].speedInMeterPerSecond);
    }
  });

  test('Test shorten route when holes', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0, 0, 100),
      GeodesicPoint(0, 1, 100, 76),
      GeodesicPoint(0, 2, 200, 76),
      GeodesicPoint(0, 3, 250, 89),
      GeodesicPoint(0, 4, 260, 90),
      GeodesicPoint(0, 5, 300, 100),
      GeodesicPoint(0, 6, 600, 99),
    ];
    List<ReliefWaypoint> expected = [
      const ReliefWaypoint(0, 0, 0),
      const ReliefWaypoint(0, 100, 1113194.9079327357),
      const ReliefWaypoint(0, 300, 2226389.8158654715),
      const ReliefWaypoint(0, 600, 371064.9693109119),
    ];
    var actual = ReliefHelper.shortenRoute_(route, 600, 4);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timeInMillisecond, expected[i].timeInMillisecond);
      expect(actual[i].speedInMeterPerSecond, expected[i].speedInMeterPerSecond);
    }
  });
}
