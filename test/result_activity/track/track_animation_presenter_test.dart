import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/result_activity/result.dart';
import 'package:vikazimut/result_activity/track/track_animation_presenter.dart';
import 'package:vikazimut/route_data/punch_time.dart';

void main() {
  String trackExample = '49.278524,-0.425337,02005,90.5;49.278511,-0.425321,03982,89.9;49.278501,-0.425309,08007,90.4';
  late List<GeodesicPoint> route;

  setUpAll(() {
    route = Result.getGpsRouteAsWaypoints_(trackExample);
  });

  test('Test find current GPS point when start', () {
    var actual = TrackAnimationPresenter.findCurrentGPSPoint_(route, 0);
    expect(actual, 0);
  });

  test('Test find current GPS point when before GPS time', () {
    var actual = TrackAnimationPresenter.findCurrentGPSPoint_(route, 3981);
    expect(actual, 1);
  });

  test('Test find current GPS point when equals GPS time', () {
    var actual = TrackAnimationPresenter.findCurrentGPSPoint_(route, 3982);
    expect(actual, 1);
  });

  test('Test find current GPS point when after GPS time', () {
    var actual = TrackAnimationPresenter.findCurrentGPSPoint_(route, 3983);
    expect(actual, 2);
  });

  test('Test find current GPS point when nd', () {
    var actual = TrackAnimationPresenter.findCurrentGPSPoint_(route, 8007);
    expect(actual, 2);
  });

  test('Get sublist when starting route', () {
    var actual = TrackAnimationPresenter.getSubList_(route, 0, 0);
    expect(actual, []);
  });

  test('Get sublist when 2 legs are done', () {
    var actual = TrackAnimationPresenter.getSubList_(route, 3982, 2);
    expect(actual.length, 3);
  });

  test('Get sublist even when index > length', () {
    var actual = TrackAnimationPresenter.getSubList_(route, 0, 10);
    expect(actual.length, 0);
  });

  test('Get sublist when all legs are done', () {
    var actual = TrackAnimationPresenter.getSubList_(route, 8007, 2);
    expect(actual.length, route.length);
  });

  test('Test punched location when start point', () {
    List<int> punchTimes = [0, 10, 20, 30, 40];
    var actual = TrackAnimationPresenter.isPunchLocation_(0, punchTimes, 10);
    expect(actual, true);
  });

  test('Test punched location when point not punched', () {
    List<int> punchTimes = [0, 10, -1, 30, 40];
    var actual = TrackAnimationPresenter.isPunchLocation_(20, punchTimes, 5);
    expect(actual, false);
  });

  test('Test punched location when true at last point', () {
    List<int> punchTimes = [0, 10, 20, 30, 40];
    var actual = TrackAnimationPresenter.isPunchLocation_(40, punchTimes, 5);
    expect(actual, true);
  });

  test('Test punched location when between two checkpoints', () {
    List<int> punchTimes = [0, 10, 20, 30, 40];
    var actual = TrackAnimationPresenter.isPunchLocation_(25, punchTimes, 2);
    expect(actual, false);
  });

  test('Test sort punch times with regular data', () {
    var actual = TrackAnimationPresenter.sortPunchTimesInChronologicalOrder([
      PunchTime(1),
      PunchTime(0),
      PunchTime(2),
    ]);
    expect(actual, [0, 1, 2]);
  });

  test('Test sort punch times with unset values', () {
    var actual = TrackAnimationPresenter.sortPunchTimesInChronologicalOrder([
      PunchTime(0),
      PunchTime(-1),
      PunchTime(2),
      PunchTime(-1),
      PunchTime(1),
    ]);
    expect(actual, [0, 1, 2, -1, -1]);
  });
}
