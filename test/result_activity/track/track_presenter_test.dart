import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/result_activity/track/track_presenter.dart';
import 'package:vikazimut/route_data/punch_time.dart';

void main() {
  test('Test compute punch position with regular route', () {
    List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(1),
      PunchTime(-1),
      PunchTime(2),
      PunchTime(4),
      PunchTime(3),
    ];
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(1, 1, 1),
      GeodesicPoint(2, 2, 2),
      GeodesicPoint(3, 3, 3),
      GeodesicPoint(4, 4, 4),
      GeodesicPoint(5, 5, 5),
    ];
    List<double> expected = [0, 1, 2, 3, 4];
    var actual = TrackPresenter.calculatePunchPositions(punchTimes, waypoints);
    expect(actual.length, expected.length);
    for (var i = 0; i < actual.length; ++i) {
      expect(actual[i].position.latitude, expected[i]);
    }
  });

  test('Test compute punch position when no waypoint at punch position', () {
    List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(-1),
      PunchTime(3),
      PunchTime(2),
    ];
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(0, 0, 0),
      GeodesicPoint(1, 1, 1),
      GeodesicPoint(2, 2, 2),
      GeodesicPoint(3, 3, 4),
      GeodesicPoint(4, 4, 5),
    ];
    List<double> expected = [0, 2, 2.5];
    var actual = TrackPresenter.calculatePunchPositions(punchTimes, waypoints);
    expect(actual.length, expected.length);
    for (var i = 0; i < actual.length; ++i) {
      expect(actual[i].position.latitude, expected[i]);
    }
  });
}
