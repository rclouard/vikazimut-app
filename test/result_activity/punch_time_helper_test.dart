import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/result_activity/punch_time_helper.dart';
import 'package:vikazimut/route_data/punch_time.dart';

void main() {
  test('Is checkpoints all scanned 1', () {
    List<PunchTime> cumulativeTimes = [PunchTime(0), PunchTime(10), PunchTime(20), PunchTime(30), PunchTime(40)];
    expect(true, PunchTimeHelper.isAllCheckpointsScanned(cumulativeTimes));
  });

  test('Is checkpoints all scanned 2', () {
    List<PunchTime> cumulativeTimes = [PunchTime(-1), PunchTime(-1), PunchTime(-1), PunchTime(-1), PunchTime(-1)];
    expect(false, PunchTimeHelper.isAllCheckpointsScanned(cumulativeTimes));
  });

  test('Is checkpoints all scanned3', () {
    List<PunchTime> cumulativeTimes = [PunchTime(0), PunchTime(10), PunchTime(-1), PunchTime(30), PunchTime(40)];
    expect(false, PunchTimeHelper.isAllCheckpointsScanned(cumulativeTimes));
  });

  test('Read punch times', () {
    String resultAsString = "0;16438:true;25762;46224;56587;68809:true;85494;100547";
    List<PunchTime> expected = [
      PunchTime(0, false),
      PunchTime(16438, true),
      PunchTime(25762, false),
      PunchTime(46224, false),
      PunchTime(56587, false),
      PunchTime(68809, true),
      PunchTime(85494, false),
      PunchTime(100547, false),
    ];
    List<PunchTime> actual = PunchTimeHelper.readPunchTimesFromString(resultAsString);
    for (int i = 0; i < expected.length; i++) {
      expect(expected[i].timestampInMillisecond, actual[i].timestampInMillisecond);
      expect(expected[i].forced, actual[i].forced);
    }
  });
}
