import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/result_activity/walk_global_result_presenter.dart';
import 'package:vikazimut/result_activity/result.dart';

void main() {
  test('Test createPenaltyMessage when no result', () {
    var actual = WalkGlobalResultPresenter(resultId: 0).createPenaltyMessage();
    expect(actual, "");
  });

  test('Test createPenaltyMessage when result with no penalty', () {
    var presenter = WalkGlobalResultPresenter(resultId: 0);
    Result result = Result(CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 0,
      checkpointCount: 1,
      punchTimes: "",
      gpsTrack: "",
      assistanceCount: 0,
    ));
    presenter.result = result;
    var actual = presenter.createPenaltyMessage();
    expect(actual, "");
  });

  test('Test createPenaltyMessage when result with penalty', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    var presenter = WalkGlobalResultPresenter(resultId: 0);
    Result result = Result(CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 0,
      checkpointCount: 1,
      punchTimes: "",
      gpsTrack: "",
      assistanceCount: 2,
    ));
    presenter.result = result;
    var actual = presenter.createPenaltyMessage();
    expect(actual.isEmpty, false);
  });
}
