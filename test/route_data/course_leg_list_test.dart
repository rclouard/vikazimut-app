import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/course_leg_list.dart';
import 'package:vikazimut/route_data/punch_time.dart';

void main() {
  test('Test initialisation', () {
    CourseLegList sections = CourseLegList(3);
    expect(sections.isCheckpointPunched(0), false);
    expect(sections.isCheckpointPunched(1), false);
    expect(sections.isCheckpointPunched(2), false);
  });

  test('Test isPunched', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, 10, false);
    sections.punch(2, 0, 1000, false);
    expect(sections.isCheckpointPunched(0), true);
    expect(sections.isCheckpointPunched(1), false);
    expect(sections.isCheckpointPunched(2), true);
  });

  test('Test isAllIntermediateCheckpointsValidated_when all unvalidated', () {
    CourseLegList sections = CourseLegList(3);
    expect(sections.isAllIntermediateValidatedCheckpoints(), false);
  });

  test('Test isAllIntermediateCheckpointsValidated when all validated', () {
    CourseLegList sections = CourseLegList(4);
    sections.punch(0, 0, 0, false);
    sections.punch(1, 0, 10, false);
    sections.punch(2, 0, 1000, false);
    expect(sections.isAllIntermediateValidatedCheckpoints(), true);
  });

  test('Test isAllIntermediateCheckpointsValidated when intermediates validated', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, -0, false);
    sections.punch(1, 0, 10, false);
    sections.punch(2, 0, -1, false);
    expect(sections.isAllIntermediateValidatedCheckpoints(), true);
  });

  test('Test allPunchedExcept when true', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, 0, false);
    sections.punch(1, 0, 10, false);
    sections.punch(2, 0, -1, false);
    expect(sections.isAllCheckpointPunchedExcept(2), true);
  });

  test('Test allPunchedExcept when false', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, 0, false);
    sections.punch(1, 0, -1, false);
    sections.punch(2, 0, -1, false);
    expect(sections.isAllCheckpointPunchedExcept(2), false);
  });

  test('Test punchedCheckpointCount when 0', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, -1, false);
    sections.punch(1, 0, -1, false);
    sections.punch(2, 0, -1, false);
    expect(sections.countPunchedCheckpoints(), 0);
  });

  test('Test punchedCheckpointCount when 1', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, 0, false);
    sections.punch(1, 0, -1, false);
    sections.punch(2, 0, -1, false);
    expect(sections.countPunchedCheckpoints(), 1);
  });

  test('Test punchedCheckpointCount when 3', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, 0, false);
    sections.punch(1, 0, 1, false);
    sections.punch(2, 0, 2, false);
    expect(sections.countPunchedCheckpoints(), 3);
  });

  test('Test unpunch', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(0, 0, 0, false);
    expect(sections.isCheckpointPunched(0), true);
    sections.punch(1, 0, 0, false);
    expect(sections.isCheckpointPunched(1), true);
    sections.unpunch(0, 0);
    expect(sections.isCheckpointPunched(0), false);
    expect(sections.isCheckpointPunched(1), true);
  });

  test('Test isLastCheckpointPunched', () {
    CourseLegList sections = CourseLegList(3);
    sections.punch(2, 0, 0, false);
    expect(sections.isLastCheckpointPunched(), true);
    sections.unpunch(2, 0);
    expect(sections.isLastCheckpointPunched(), false);
  });

  test('Test getNextCheckpointIndex_ when no one punched', () {
    List<PunchTime> punchTimes = [
      PunchTime(-1),
      PunchTime(-1),
    ];
    var nextIndex = CourseLegList.getNextCheckpointIndexInPresetOrder_(punchTimes);
    expect(nextIndex, 0);
  });

  test('Test getNextCheckpointIndex_ when one is punched', () {
    List<PunchTime> punchTimes = [
      PunchTime(-1),
      PunchTime(0),
    ];
    var nextIndex = CourseLegList.getNextCheckpointIndexInPresetOrder_(punchTimes);
    expect(nextIndex, 0);
  });

  test('Test getNextCheckpointIndex_ when the first ones are punched', () {
    List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(10),
      PunchTime(20),
      PunchTime(-1),
      PunchTime(30),
    ];
    var nextIndex = CourseLegList.getNextCheckpointIndexInPresetOrder_(punchTimes);
    expect(nextIndex, 3);
  });

  test('Test getLastPunchedCheckpointIndex', () {
    List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(-1),
      PunchTime(20),
      PunchTime(-1),
      PunchTime(10),
    ];
    var nextIndex = CourseLegList.getLastPunchedCheckpointIndex_(punchTimes);
    expect(nextIndex, 2);
  });

  test('Test setPunchTimes', () {
    List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(10),
      PunchTime(20),
      PunchTime(-1),
      PunchTime(30),
    ];
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 0, 0, 1),
      Checkpoint(1, "1", 0, 0, 2),
      Checkpoint(2, "2", 0, 0, 3),
      Checkpoint(3, "3", 0, 0, 4),
      Checkpoint(4, "4", 0, 0, 5),
    ];
    CourseLegList courseLegList = CourseLegList(5);
    courseLegList.resetPunchTimes(punchTimes, checkpoints);
    var actual = courseLegList.punchTimes;
    for (var i = 0; i < punchTimes.length; ++i) {
      expect(actual[i].timestampInMillisecond, punchTimes[i].timestampInMillisecond);
    }
    expect(courseLegList.currentScore, 5);
  });

  test('Test getPunchTimesInMilliseconds', () {
    List<PunchTime> punchTimes = [
      PunchTime(0),
      PunchTime(10),
      PunchTime(20),
      PunchTime(-1),
      PunchTime(30),
    ];
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 0, 0, 1),
      Checkpoint(1, "1", 0, 0, 2),
      Checkpoint(2, "2", 0, 0, 3),
      Checkpoint(3, "3", 0, 0, 4),
      Checkpoint(4, "4", 0, 0, 5),
    ];
    CourseLegList courseLegList = CourseLegList(5);
    courseLegList.resetPunchTimes(punchTimes, checkpoints);
    var actual = courseLegList.getPunchTimesAsMilliseconds();
    for (var i = 0; i < punchTimes.length; ++i) {
      expect(actual[i], punchTimes[i].timestampInMillisecond);
    }
  });

  test('Test score for start and finish', () {
    CourseLegList sections = CourseLegList(4);
    sections.punch(0, 1, 10, false);
    sections.punch(1, 2, 10, false);
    sections.punch(2, 3, 1000, false);
    sections.punch(3, 1, 1000, false);
    expect(sections.currentScore, 5);
    sections.unpunch(2, 3);
    expect(sections.currentScore, 2);
  });
}
