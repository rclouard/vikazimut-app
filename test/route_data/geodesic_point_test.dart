import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

void main() {
  test('Test distance null', () {
    GeodesicPoint positionCaen = GeodesicPoint(0, 0);
    GeodesicPoint positionENSICAEN = GeodesicPoint(0, 0);
    double distance = positionCaen.distanceToInMeter(positionENSICAEN);
    expect(distance, 0);
  });

  test('Test distance Caen Ensicaen', () {
    GeodesicPoint positionCaen = GeodesicPoint(49.183333, -0.350000);
    GeodesicPoint positionENSICAEN = GeodesicPoint(49.20922, -0.357627);
    double distance = positionCaen.distanceToInMeter(positionENSICAEN);
    expect(distance, moreOrLessEquals(2934.65, epsilon: 0.01));
  });

  test('Test distance Washington', () {
    GeodesicPoint positionPennsylvaniaAvenue = GeodesicPoint(38.898556, -77.037852);
    GeodesicPoint positionFStNW = GeodesicPoint(38.897147, -77.043934);
    double distance = positionPennsylvaniaAvenue.distanceToInMeter(positionFStNW);
    expect(distance, moreOrLessEquals(549.7, epsilon: 0.1));
  });

  test('Test distance zero', () {
    GeodesicPoint positionPennsylvaniaAvenue = GeodesicPoint(38.898556, -77.037852);
    double distance = positionPennsylvaniaAvenue.distanceToInMeter(positionPennsylvaniaAvenue);
    expect(distance, 0);
  });

  test('Test distance 1', () {
    GeodesicPoint loc1 = GeodesicPoint(49.214900, -0.368953);
    GeodesicPoint loc2 = GeodesicPoint(49.214269, -0.367512);
    double distance = loc2.distanceToInMeter(loc1);
    expect(distance, moreOrLessEquals(126.13, epsilon: 0.1));
  });

  test('Test distance 2', () {
    GeodesicPoint loc1 = GeodesicPoint(49.214323, -0.367526);
    GeodesicPoint loc2 = GeodesicPoint(49.318430, -0.350972);
    double distance = loc2.distanceToInMeter(loc1);
    expect(distance, moreOrLessEquals(11651.3, epsilon: 0.1));
  });

  test('Test sequence setGpsRoute and getGpsRouteAsString', () {
    List<GeodesicPoint> geodesicPoints = [
      GeodesicPoint(50.123456, 50.123456),
      GeodesicPoint(-50.123456, -50.123456),
    ];
    String actual = GeodesicPoint.waypointsToString(geodesicPoints);
    String expected = "50.123456,50.123456,0,-10000;-50.123456,-50.123456,0,-10000;";
    expect(actual, expected);
  });
}
