import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_time.dart';

void main() {
  test('Test createPunchTimesAsString', () {
    List<PunchTime> punchTimes = [
      PunchTime(0, false),
      PunchTime(2, false),
      PunchTime(0, true),
      PunchTime(4, false),
      PunchTime(-1, false),
      PunchTime(7, true),
    ];
    String expected = "0;2;0:true;4;-1;7:true";

    String actual = PunchTime.createPunchTimesAsFormattedString(punchTimes);
    expect(actual, expected);
  });

  test('Test punchCheckpointsAfterMissedPunches case of free order', () {
    List<GeodesicPoint> waypoints = []; // Not needed
    List<Checkpoint> checkpoints = []; // Not needed
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(2), PunchTime(5)];

    List<PunchTime> actual = PunchTime.punchCheckpointsAfterMissedPunches(
      punchTimes,
      waypoints,
      checkpoints,
      CourseFormat.FREE.index,
      35,
    );
    expect(actual, punchTimes);
  });

  test('Test punchCheckpointsAfterMissedPunches case all punched in preset order', () {
    List<GeodesicPoint> waypoints = []; // Not needed
    List<Checkpoint> checkpoints = []; // Not needed
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(2), PunchTime(5)];

    List<PunchTime> actual = PunchTime.punchCheckpointsAfterMissedPunches(
      punchTimes,
      waypoints,
      checkpoints,
      CourseFormat.PRESET.index,
      35,
    );
    expect(actual, punchTimes);
  });

  test('Test extract punchTimes case of last 2 checkpoints not punched', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1),
      GeodesicPoint(49.328519, -0.389189, 2),
      GeodesicPoint(49.328519, -10.389189, 4),
      GeodesicPoint(49.329825, -0.387567, 4),
      GeodesicPoint(49.329825, -10.387567, 5),
      GeodesicPoint(49.331249, -0.389705, 6),
    ];
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 49.328493, -0.389201, 1),
      Checkpoint(1, "1", 49.329825, -0.387567, 1),
      Checkpoint(2, "2", 49.331249, -0.389705, 1),
    ];
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(PunchTime.UNPUNCHED), PunchTime(PunchTime.UNPUNCHED)];
    List<PunchTime> expected = [PunchTime(0), PunchTime(4), PunchTime(6)];

    List<PunchTime> actual = PunchTime.punchCheckpointsAfterMissedPunches(
      punchTimes,
      waypoints,
      checkpoints,
      CourseFormat.PRESET.index,
      35,
    );
    expect(actual.length, 3);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
    }
  });

  test('Test extract punchTimes case of one inner checkpoint not punched', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1),
      GeodesicPoint(49.328519, -0.389189, 2),
      GeodesicPoint(49.328519, -10.389189, 4),
      GeodesicPoint(49.329825, -0.387567, 4),
      GeodesicPoint(49.329825, -10.387567, 5),
      GeodesicPoint(49.331249, -0.389705, 6),
    ];
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 49.328493, -0.389201, 1),
      Checkpoint(1, "1", 49.329825, -0.387567, 1),
      Checkpoint(2, "2", 49.331249, -0.389705, 1),
    ];
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(PunchTime.UNPUNCHED), PunchTime(6)];
    List<PunchTime> expected = [PunchTime(0), PunchTime(4), PunchTime(6)];

    List<PunchTime> actual = PunchTime.punchCheckpointsAfterMissedPunches(
      punchTimes,
      waypoints,
      checkpoints,
      CourseFormat.PRESET.index,
      35,
    );
    expect(actual.length, 3);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
    }
  });

  test('Test extract punchTimes case of two inner checkpoints not punched', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1),
      GeodesicPoint(49.328519, -0.389189, 2),
      GeodesicPoint(49.331249, -0.389705, 3), // Close to checkpoint 3
      GeodesicPoint(49.328519, -10.389189, 4),
      GeodesicPoint(49.329825, -0.387567, 4),
      GeodesicPoint(49.329825, -10.387567, 5),
      GeodesicPoint(49.331249, -0.389705, 6),
      GeodesicPoint(49, -0.4, 7),
    ];
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 49.328493, -0.389201, 1),
      Checkpoint(1, "1", 49.329825, -0.387567, 1),
      Checkpoint(2, "2", 49.331249, -0.389705, 1),
      Checkpoint(3, "3", 49.0, -0.4, 1),
    ];
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(4), PunchTime(6), PunchTime(PunchTime.UNPUNCHED)];
    List<PunchTime> expected = [PunchTime(0), PunchTime(4), PunchTime(6), PunchTime(7)];

    List<PunchTime> actual = PunchTime.punchCheckpointsAfterMissedPunches(
      punchTimes,
      waypoints,
      checkpoints,
      CourseFormat.PRESET.index,
      35,
    );
    expect(actual.length, expected.length);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
    }
  });

  test('Test extract punchTimes case of last checkpoint not punched', () {
    List<GeodesicPoint> waypoints = [
      GeodesicPoint(49.328493, -0.389201, 1),
      GeodesicPoint(49.328519, -0.389189, 2),
      GeodesicPoint(49.331249, -0.389705, 3), // Close to checkpoint 3
      GeodesicPoint(49.328519, -10.389189, 4),
      GeodesicPoint(49.329825, -0.387567, 4),
      GeodesicPoint(49.329825, -10.387567, 5),
      GeodesicPoint(49.331249, -0.389705, 6),
      GeodesicPoint(49, -0.4, 7),
    ];
    List<Checkpoint> checkpoints = [
      Checkpoint(0, "0", 49.328493, -0.389201, 1),
      Checkpoint(1, "1", 49.329825, -0.387567, 1),
      Checkpoint(2, "2", 49.331249, -0.389705, 1),
      Checkpoint(3, "3", 49.0, -0.4, 1),
    ];
    List<PunchTime> punchTimes = [PunchTime(0), PunchTime(PunchTime.UNPUNCHED), PunchTime(PunchTime.UNPUNCHED), PunchTime(7)];
    List<PunchTime> expected = [PunchTime(0), PunchTime(4), PunchTime(6), PunchTime(7)];

    List<PunchTime> actual = PunchTime.punchCheckpointsAfterMissedPunches(
      punchTimes,
      waypoints,
      checkpoints,
      CourseFormat.PRESET.index,
      35,
    );
    expect(actual.length, expected.length);
    for (int i = 0; i < actual.length; i++) {
      expect(actual[i].timestampInMillisecond, expected[i].timestampInMillisecond);
    }
  });
}
