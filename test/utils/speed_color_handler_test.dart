import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/utils/speed_color_handler.dart';

void main() {
  test('Test colorFromVelocity when negative', () {
    SpeedColorHandler colorHandler = SpeedFootoColorHandler();
    const double velocity = -1;
    int actual = colorHandler.colorIndexFromVelocity(velocity);
    expect(actual, 0);
  });

  test('Test colorFromVelocity when zero', () {
    SpeedColorHandler colorHandler = SpeedFootoColorHandler();
    const double velocity = 0;
    int actual = colorHandler.colorIndexFromVelocity(velocity);
    expect(actual, 0);
  });

  test('Test colorFromVelocity when greater than max', () {
    SpeedColorHandler colorHandler = SpeedFootoColorHandler();
    const double velocity = 50;
    int actual = colorHandler.colorIndexFromVelocity(velocity);
    expect(actual, 4);
  });

  test('Test colorFromVelocity when regular for footo', () {
    SpeedColorHandler colorHandler = SpeedFootoColorHandler();
    const velocities = [1.0, 2.0, 5.0, 8.0, 14.0, 21.0];
    const expected = [0, 1, 1, 2, 3, 4];
    for (int i = 0; i < velocities.length; i++) {
      int actualColor = colorHandler.colorIndexFromVelocity(velocities[i]);
      expect(actualColor, expected[i]);
    }
  });

  test('Test colorFromVelocity when regular for mtbo', () {
    SpeedColorHandler colorHandler = SpeedMtboColorHandler();
    const velocities = [1.0, 6.0, 7.0, 8.0, 20.0, 21.0, 27.0, 28.0, 50.0];
    const expected = [0, 0, 1, 1, 2, 3, 3, 4, 4];
    for (int i = 0; i < velocities.length; i++) {
      int actualColor = colorHandler.colorIndexFromVelocity(velocities[i]);
      expect(actualColor, expected[i]);
    }
  });

  test('Test convertMMsToKmh_ cas 1 kmh', () {
    double actual = SpeedColorHandler.computeVelocityInKmH_(1000, 60 * 60 * 1000);
    expect(actual, 1);
  });

  test('Test convertMMsToKmh_ cas 2 kmh', () {
    double actual = SpeedColorHandler.computeVelocityInKmH_(1000, 30 * 60 * 1000);
    expect(actual, 2);
  });

  test('Test convertMMsToKmh_ cas 1 kmh bis', () {
    double actual = SpeedColorHandler.computeVelocityInKmH_(500, 30 * 60 * 1000);
    expect(actual, 1);
  });

  test('Test  getRouteSpeedsAsColor_ failure cas 2', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(49.278524, -0.425337, 02005, 90.5),
      GeodesicPoint(49.278511, -0.425321, 03982, 89.9),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
    ];
    List<double> actual = SpeedColorHandler.filterSpeeds_(route, 5);
    expect(actual, [0.0, 3.379510568145884, 1.264466523085825]);
  });

  test('Test  getRouteSpeedsAsColor_ failure cas 3', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(49.278524, -0.425337, 02005, 90.5),
      GeodesicPoint(49.278511, -0.425321, 03982, 89.9),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
      GeodesicPoint(49.278501, -0.425309, 08007, 90.4),
    ];
    List<double> actual = SpeedColorHandler.filterSpeeds_(route, 5);
    expect(actual, [0.0, 3.379510568145884, 1.264466523085825, 0.0, 0.0]);
  });

  test('Test getRouteSpeedsAsColor with constant speeds', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0.0, 10000),
      GeodesicPoint(0, 0.1, 20000),
      GeodesicPoint(0, 0.0, 30000),
      GeodesicPoint(0, 0.1, 40000),
      GeodesicPoint(0, 0.0, 50000),
      GeodesicPoint(0, 0.1, 60000),
      GeodesicPoint(0, 0.0, 70000),
      GeodesicPoint(0, 0.1, 80000),
      GeodesicPoint(0, 0.0, 90000),
      GeodesicPoint(0, 0.1, 100000),
    ];
    List<double> actual = SpeedColorHandler.filterSpeeds_(route, 2);
    const double distance = 11131.94;
    const int time = 10;
    const double expected = distance * 3.6 / time;
    for (int i = 1; i < actual.length; i++) {
      expect(actual[i], moreOrLessEquals(expected, epsilon: 0.01));
    }
  });

  test('Test getRouteSpeedsAsColor with linear ascendant speeds', () {
    List<GeodesicPoint> route = [
      GeodesicPoint(0, 0.0, 0),
      GeodesicPoint(0, 0.1, 1000),
      GeodesicPoint(0, 0.0, 3000),
      GeodesicPoint(0, 0.1, 6000),
      GeodesicPoint(0, 0.0, 10000),
      GeodesicPoint(0, 0.1, 15000), //
      GeodesicPoint(0, 0.0, 20000),
      GeodesicPoint(0, 0.1, 25000),
      GeodesicPoint(0, 0.0, 29000),
      GeodesicPoint(0, 0.1, 32000),
      GeodesicPoint(0, 0.0, 33000),
    ];
    //     const double distance = 11131.94;
    var expected = [
      0.0,
      40075.016685578485,
      20037.508342789242,
      13358.33889519283,
      10018.754171394623,
      8587.503575481105,
      8015.003337115698, // speed: 11131.94 × 3 × 3.6 × 1000 ÷ 3 × 5000
      8587.503575481105,
      10018.754171394623,
      13358.33889519283,
      40075.016685578485
    ];
    List<double> actual = SpeedColorHandler.filterSpeeds_(route, 1);
    expect(actual, expected);
  });

  test('Test SpeedColorHandlerFactory case 1', () {
    SpeedColorHandler handler = SpeedColorHandlerFactory.build(Discipline.URBANO);
    expect(handler.runtimeType, SpeedFootoColorHandler);
  });

  test('Test SpeedColorHandlerFactory case 2', () {
    SpeedColorHandler handler = SpeedColorHandlerFactory.build(Discipline.FORESTO);
    expect(handler.runtimeType, SpeedFootoColorHandler);
  });

  test('Test SpeedColorHandlerFactory case 3', () {
    SpeedColorHandler handler = SpeedColorHandlerFactory.build(Discipline.MTBO);
    expect(handler.runtimeType, SpeedMtboColorHandler);
  });

  test('Test SpeedColorHandlerFactory case 4', () {
    SpeedColorHandler handler = SpeedColorHandlerFactory.build(Discipline.SKIO);
    expect(handler.runtimeType, SpeedSkioColorHandler);
  });
}
