import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:vikazimut/utils/time.dart';

void main() {
  test('test get localized date', () {
    String expectedDate = "Jan 1, 1970";
    const int ms = 10000000;
    initializeDateFormatting("en_US");
    String actualDate = getLocalizedDate_(ms, "en_US");
    expect(actualDate, expectedDate);
  });

  test('Test format time as string when 0 and no hours', () {
    String actual = formatTimeAsString(0, withHour: false);
    expect(actual, "00:00");
  });

  test('Test format time as string when 0 and hour', () {
    String actual = formatTimeAsString(60 * 60 * 2 * 1000, withHour: false);
    expect(actual, "2:00:00");
  });

  test('Test format time as string when 0 and with hours', () {
    String actual = formatTimeAsString(0, withHour: true);
    expect(actual, "0:00:00");
  });

  test('Test format time as string when 1 minute and 33 seconds', () {
    Duration time = const Duration(minutes: 1, seconds: 33);
    String actual = formatTimeAsString(time.inMilliseconds, withHour: true);
    expect(actual, "0:01:33");
  });
}
