import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_choice_activity/search_widget/diacritic.dart';

void main() {
  test('Test diacritic with empty string', () {
    var actual = removeDiacritics("");
    expect(actual, "");
  });

  test('Test diacritic without any special characters', () {
    var actual = removeDiacritics("Toto tata");
    expect(actual, "Toto tata");
  });

  test('Test diacritic with special characters', () {
    var actual = removeDiacritics("êèàôé @");
    expect(actual, "eeaoe @");
  });
}
