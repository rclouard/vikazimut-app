import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_choice_activity/course_map.dart';
import 'package:vikazimut/map_choice_activity/course_map_filter.dart';

void main() {
  test('filter MapList From map name when one match', () {
    List<CourseMap> maps = [
      CourseMap(id: 0, name: "toto", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 1, name: "tata", latitude: 0, longitude: 0, length: 0),
    ];
    String query = "to";
    var actual = CourseMapFilter.filterMapListFromQuery_(maps, query);
    expect(actual.length, 1);
  });

  test('filter MapList From creator name then one match', () {
    List<CourseMap> maps = [
      CourseMap(id: 0, name: "toto", latitude: 0, longitude: 0, length: 0, club: "Toto"),
      CourseMap(id: 1, name: "tata", latitude: 0, longitude: 0, length: 0, club: "Tata"),
    ];
    String query = "@to";
    var actual = CourseMapFilter.filterMapListFromQuery_(maps, query);
    expect(actual.length, 1);
  });

  test('Compare creators when null then false', () {
    var element = CourseMap(id: 0, name: "toto", latitude: 0, longitude: 0, length: 0);
    var actual = CourseMapFilter.compareCreators_(element, "creator");
    expect(actual, false);
  });

  test('Compare creators when name then ok', () {
    var element = CourseMap(id: 0, name: "toto", latitude: 0, longitude: 0, length: 0, club: "Creator");
    var actual = CourseMapFilter.compareCreators_(element, "creator");
    expect(actual, true);
  });

  test('Compare orienteering_map when accent then ok', () {
    var element = CourseMap(id: 0, name: "êtç", latitude: 0, longitude: 0, length: 0);
    var actual = CourseMapFilter.compareMapNames_(element, "etc");
    expect(actual, true);
  });

  test('Compare creators when accent then ok', () {
    var element = CourseMap(id: 0, name: "toto", latitude: 0, longitude: 0, length: 0, club: "Club");
    var actual = CourseMapFilter.compareCreators_(element, "club");
    expect(actual, true);
  });
}
