import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/map_choice_activity/course_map.dart';
import 'package:vikazimut/map_choice_activity/map_choice_presenter.dart';
import 'package:vikazimut/map_choice_activity/memory_widget/memory_info_presenter.dart';

class MockMemoryInfoPresenter extends Mock implements MemoryInfoPresenter {}

void main() {
  test('Test stopDownloading', () {
    var courseMap = CourseMap(id: -10000, name: "toto", latitude: 0, longitude: 0, length: 0, club: "Club");
    MapChoicePresenter presenter = MapChoicePresenter(CourseMode.WALK);
    courseMap.isDownloaded = true;
    courseMap.isDownloading = true;
    MockMemoryInfoPresenter memoryPresenter = MockMemoryInfoPresenter();
    when(() => memoryPresenter.updateMemorySize()).thenAnswer((value) async {});
    presenter.clearDownloadedMap_(courseMap, memoryPresenter);
    expect(courseMap.isDownloaded, false);
  });

  test('Test stopDownloading', () {
    var courseMap = CourseMap(id: -10000, name: "toto", latitude: 0, longitude: 0, length: 0, club: "Club");
    MapChoicePresenter presenter = MapChoicePresenter(CourseMode.WALK);
    courseMap.isDownloaded = true;
    courseMap.isDownloading = true;
    presenter.stopDownloading_(courseMap);
    expect(courseMap.isDownloaded, false);
    expect(courseMap.isDownloading, false);
  });

  test('Test updateAfterMapDownloadSuccess', () {
    var courseMap = CourseMap(id: -10000, name: "toto", latitude: 0, longitude: 0, length: 0, club: "Club");
    MapChoicePresenter presenter = MapChoicePresenter(CourseMode.WALK);
    presenter.updateAfterMapDownloadSuccess_(courseMap);
    expect(courseMap.isDownloaded, true);
    expect(courseMap.isDownloading, false);
  });

  test('Test filterPlayfulCourse', () {
    List<CourseMap> list = [
      CourseMap(id: 10, name: "toto", latitude: 0, longitude: 0, length: 0, isTouristic: true),
      CourseMap(id: 20, name: "toto", latitude: 0, longitude: 0, length: 0, isTouristic: false),
    ];
    List<CourseMap> actual = MapChoicePresenter.filterPlayfulCourse(list);
    expect(actual.length, 1);
    expect(actual[0].id, 10);
  });

  test('Test computeFileSize_ nominal case', () {
    int? actual = MapChoicePresenter.computeFileSize_(10, "0");
    expect(actual, 10);
  });

  test('Test computeFileSize_ header case', () {
    int? actual = MapChoicePresenter.computeFileSize_(null, "11");
    expect(actual, 11);
  });

  test('Test computeFileSize_ null case', () {
    int? actual = MapChoicePresenter.computeFileSize_(null, null);
    expect(actual, 0);
  });
}
