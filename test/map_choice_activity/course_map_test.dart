import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_choice_activity/course_map.dart';

void main() {
  test('CourseMap creation From Json File()', () {
    Map<String, dynamic> json = {"id": 16, "name": "Ensicaen", "latitude": "49.213", "longitude": "-0.367475", "length": 2636};
    CourseMap courseMap = CourseMap.fromJson(json);
    expect(courseMap.id, 16);
    expect(courseMap.name, "Ensicaen");
    expect(courseMap.latitude, moreOrLessEquals(49.213));
    expect(courseMap.longitude, moreOrLessEquals(-0.367475));
    expect(courseMap.length, 2636);
    expect(courseMap.club, null);
    expect(courseMap.club, null);
    expect(courseMap.clubUrl, null);
    expect(courseMap.isTouristic, false);
  });

  test('CourseMap creation From Json File() with club', () {
    Map<String, dynamic> json = {"id": 16, "name": "Ensicaen", "latitude": "49.213", "longitude": "-0.367475", "length": 2636, "club": "Vik'Azim"};
    CourseMap courseMap = CourseMap.fromJson(json);
    expect(courseMap.id, 16);
    expect(courseMap.name, "Ensicaen");
    expect(courseMap.latitude, moreOrLessEquals(49.213));
    expect(courseMap.longitude, moreOrLessEquals(-0.367475));
    expect(courseMap.length, 2636);
    expect(courseMap.club, "Vik'Azim");
  });

  test('CourseMap creation From Json File() with club name', () {
    Map<String, dynamic> json = {"id": 16, "name": "Ensicaen", "latitude": "49.213", "longitude": "-0.367475", "length": 2636, "club": "Vik'Azim", "cluburl": "https://vikazim.fr"};
    CourseMap courseMap = CourseMap.fromJson(json);
    expect(courseMap.id, 16);
    expect(courseMap.name, "Ensicaen");
    expect(courseMap.latitude, moreOrLessEquals(49.213));
    expect(courseMap.longitude, moreOrLessEquals(-0.367475));
    expect(courseMap.length, 2636);
    expect(courseMap.club, "Vik'Azim");
    expect(courseMap.clubUrl, "https://vikazim.fr");
  });

  test('CourseMap creation From Json File() with empty touristic route', () {
    Map<String, dynamic> json = {"id": 16, "name": "Ensicaen", "latitude": "49.213", "longitude": "-0.367475", "length": 2636, "club": "Vik'Azim"};
    CourseMap courseMap = CourseMap.fromJson(json);
    expect(courseMap.isTouristic, false);
  });

  test('CourseMap creation From Json File() with touristic route', () {
    Map<String, dynamic> json = {"id": 16, "name": "Ensicaen", "latitude": "49.213", "longitude": "-0.367475", "length": 2636, "club": "Vik'Azim", "touristic": 1};
    CourseMap courseMap = CourseMap.fromJson(json);
    expect(courseMap.isTouristic, true);
  });

  test('CourseMap change distance', () {
    CourseMap courseMap = CourseMap(id: 0, longitude: 0, latitude: 0, length: 0, name: 'test');
    courseMap.distance = 1000;
    expect(courseMap.distance, 1000);
  });

  test('CourseMap change downloading', () {
    CourseMap courseMap = CourseMap(id: 0, longitude: 0, latitude: 0, length: 0, name: 'test');
    expect(courseMap.isDownloading, false);
    courseMap.isDownloading = true;
    expect(courseMap.isDownloading, true);
    courseMap.isDownloading = false;
    expect(courseMap.isDownloading, false);
    expect(courseMap.isDownloaded, false);
    courseMap.isDownloaded = true;
    expect(courseMap.isDownloaded, true);
  });
}
