import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_choice_activity/memory_widget/memory_info_presenter.dart';

void main() {
  test('Convert Byte To MegaByte when 1Mb', () {
    MemoryInfoPresenter presenter = MemoryInfoPresenter();
    List result = presenter.formatSize_(1);
    expect(result[0] as double, moreOrLessEquals(1));
    String expected = "megabyte";
    expect(result[1], expected);
  });

  test('Convert Byte To MegaByte when 1Gb', () {
    MemoryInfoPresenter presenter = MemoryInfoPresenter();
    List result = presenter.formatSize_(2.0 * 1024);
    expect(result[0] as double, moreOrLessEquals(2));
    String expected = "gigabyte";
    expect(result[1], expected);
  });
}
