import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/map_choice_activity/course_map.dart';
import 'package:vikazimut/map_choice_activity/course_map_list.dart';

void main() {
  // Workaround for using path_provider
  setUpAll(() {
    TestWidgetsFlutterBinding.ensureInitialized();
    const MethodChannel channel = MethodChannel('plugins.flutter.io/path_provider');
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(channel, (MethodCall methodCall) async {
      return ".";
    });
  });

  test('Store and restore map list', () async {
    List<Map<String, dynamic>> json = [
      {"id": 16, "name": "Ensicaen", "latitude": "49.213", "longitude": "-0.367475", "length": 2636},
      {"id": 201, "name": "Paris", "latitude": "49.513", "longitude": "-0.368", "length": 2400}
    ];
    String expected = json.toString();
    final Directory dir = await getTemporaryDirectory();
    String tempFileName = '${dir.path}/test/resources/temp.json';
    CourseMapList.saveMapList_(expected, tempFileName);
    String actual = CourseMapList.readMapList_(tempFileName);
    expect(actual, expected);
  });

  test('Sort map without position', () {
    List<CourseMap> maps = [
      CourseMap(id: 0, name: "a", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 1, name: "b", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 2, name: "c", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 3, name: "d", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 4, name: "e", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 5, name: "f", latitude: 0, longitude: 0, length: 0),
    ];
    expect(maps[0].id, 0);
    expect(maps[1].id, 1);
    expect(maps[2].id, 2);
    expect(maps[3].id, 3);
    expect(maps[4].id, 4);
    expect(maps[5].id, 5);
  });

  test('Sort map from position', () {
    List<CourseMap> maps = [
      CourseMap(id: 0, name: "a", latitude: 0, longitude: 0, length: 0),
      CourseMap(id: 1, name: "b", latitude: 6, longitude: 0, length: 0),
      CourseMap(id: 2, name: "c", latitude: 2, longitude: 0, length: 0),
      CourseMap(id: 3, name: "d", latitude: 5, longitude: 0, length: 0),
      CourseMap(id: 4, name: "e", latitude: 4, longitude: 0, length: 0),
      CourseMap(id: 5, name: "f", latitude: 3, longitude: 0, length: 0),
    ];
    maps[3].isDownloaded = true;
    Position position = Position(
      latitude: 0.0,
      longitude: 0.0,
      speed: 0,
      speedAccuracy: 0,
      accuracy: 0,
      altitude: 0,
      heading: 0,
      timestamp: DateTime(1970),
      altitudeAccuracy: 0,
      headingAccuracy: 0,
    );
    CourseMapList.sortMapsFromDistanceCurrentPosition_(position, maps);
    expect(maps[0].id, 0);
    expect(maps[1].id, 2);
    expect(maps[2].id, 5);
    expect(maps[3].id, 4);
    expect(maps[4].id, 3);
    expect(maps[5].id, 1);
  });

  test('Test parseMaps_ when null', () {
    String responseBody = "";
    var maps = CourseMapList.parseJsonToExtractMaps_(responseBody);
    expect(maps.isEmpty, true);
  });

  test('Sort map from position', () {
    String responseBody = """
    [
    {"id":1,"name":"Creully 3.5Km","latitude":"49.285667","longitude":"-0.538659","length":2460,"club":"Vikazim","cluburl":"https://vikazim.fr/"},
    {"id":8,"name":"Airelles Sud","latitude":"42.514995","longitude":"2.037272","length":3420},
    {"id":138,"name":"Basly Test","latitude":"49.278411","longitude":"-0.425267","length":640,"club":"Vik\u0027Azim","touristic":1},
    {"id":16,"name":"Fouras","latitude":"45.992964","longitude":"-1.098904","length":2050}
    ]
    """;
    var mapDataList = CourseMapList.parseJsonToExtractMaps_(responseBody);
    expect(mapDataList[0].id, 1);
    expect(mapDataList[0].club, "Vikazim");
    expect(mapDataList[0].clubUrl, "https://vikazim.fr/");
    expect(mapDataList[1].latitude, 42.514995);
    expect(mapDataList[2].isTouristic, true);
    expect(mapDataList[3].longitude, -1.098904);
  });

  test('Test isTouristic when true', () {
    var mapChoice = CourseMapList(CourseMode.WALK);
    var courseMap = CourseMap(id: 0, name: "name", latitude: 0, longitude: 0, length: 0, isTouristic: true);
    var actual = mapChoice.isPlayful(courseMap);
    expect(actual, true);
  });

  test('Test isTouristic when false', () {
    var mapChoice = CourseMapList(CourseMode.WALK);
    var courseMap = CourseMap(id: 0, name: "name", latitude: 0, longitude: 0, length: 0, isTouristic: false);
    var actual = mapChoice.isPlayful(courseMap);
    expect(actual, false);
  });

  test('test getMapListFilePath', () async {
    String actual = await CourseMapList.getMapListFilePath_();
    expect(actual, './map_list.json');
  });
}
