import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/global_constants.dart';

void main() {
  test('Test fromInt of Discipline with null value', () {
    var actual = Discipline.fromInt(null);
    expect(actual, Discipline.URBANO);
  });

  test('Test fromInt of Discipline when regular URBANO', () {
    var actual = Discipline.fromInt(0);
    expect(actual, Discipline.URBANO);
  });

  test('Test fromInt of Discipline when regular FORESTO', () {
    var actual = Discipline.fromInt(1);
    expect(actual, Discipline.FORESTO);
  });

  test('Test fromInt of Discipline when regular MTBO', () {
    var actual = Discipline.fromInt(2);
    expect(actual, Discipline.MTBO);
  });

  test('Test fromInt of Discipline when regular SKIO', () {
    var actual = Discipline.fromInt(3);
    expect(actual, Discipline.SKIO);
  });

  test('Test fromInt of Discipline when outlier value', () {
    var actual = Discipline.fromInt(400);
    expect(actual, Discipline.URBANO);
  });

  test('Test valueOf of Discipline when null', () {
    var actual = CourseMode.fromString("MTBO1");
    expect(actual, null);
  });

  test('Test valueOf of CourseMode with null value', () {
    var actual = CourseMode.fromString(null);
    expect(actual, null);
  });

  test('Test valueOf of CourseMode when regular', () {
    var actual = CourseMode.fromString("SPORT");
    expect(actual, CourseMode.SPORT);
  });

  test('Test valueOf of CourseMode when null', () {
    var actual = CourseMode.fromString("SPORT1");
    expect(actual, null);
  });

  test('Test valueOf of CourseMode when regular', () {
    var actual = CourseMode.fromString("WALK");
    expect(actual, CourseMode.WALK);
  });

  test('Test valueOf of CourseMode when null', () {
    var actual = CourseMode.fromString("WALK1");
    expect(actual, null);
  });

  test('Test CourseMode.isWalkIndex when true', () {
    var actual = 2.isWalkMode;
    expect(actual, true);
  });

  test('Test CourseMode.isWalkIndex when false', () {
    var actual = 0.isWalkMode;
    expect(actual, false);
  });

  test('Test isGeocachingMode when true', () {
    var actual = 1.isGeocachingMode;
    expect(actual, true);
  });

  test('Test isGeocachingMode when false', () {
    var actual = 0.isGeocachingMode;
    expect(actual, false);
  });

  test('Test CourseMode.fromInt when sport', () {
    var actual = CourseMode.fromInt(0);
    expect(actual, CourseMode.SPORT);
  });

  test('Test CourseMode.fromInt when geocaching', () {
    var actual = CourseMode.fromInt(1);
    expect(actual, CourseMode.PLAYFUL);
  });

  test('Test CourseMode.fromInt when walk', () {
    var actual = CourseMode.fromInt(2);
    expect(actual, CourseMode.WALK);
  });

  test('Test CourseMode.fromInt when outlier', () {
    var actual = CourseMode.fromInt(200);
    expect(actual, CourseMode.SPORT);
  });

  test('Test isPresetFormat when true', () {
    var actual = 0.isPresetFormat;
    expect(actual, true);
  });

  test('Test isPresetFormat when false', () {
    var actual = 1.isPresetFormat;
    expect(actual, false);
  });

  test('Test isFreeFormat when true', () {
    var actual = 1.isFreeFormat;
    expect(actual, true);
  });

  test('Test isFreeFormat when false', () {
    var actual = 0.isFreeFormat;
    expect(actual, false);
  });

  test('Test isFooto when URBANO', () {
    var actual = Discipline.URBANO.isFootOrienteering();
    expect(actual, true);
  });

  test('Test isFooto when FORESTO', () {
    var actual = Discipline.FORESTO.isFootOrienteering();
    expect(actual, true);
  });

  test('Test isFooto when MTBO', () {
    var actual = Discipline.MTBO.isFootOrienteering();
    expect(actual, false);
  });

  test('Test isMTBO when UrbanO', () {
    var actual = Discipline.URBANO.isMTBOrienteering();
    expect(actual, false);
  });

  test('Test isMTBO when MTBO', () {
    var actual = Discipline.MTBO.isMTBOrienteering();
    expect(actual, true);
  });

  test('Test isSkiO when UrbanO', () {
    var actual = Discipline.URBANO.isSkiOrienteering();
    expect(actual, false);
  });

  test('Test isSkiO when SKIO', () {
    var actual = Discipline.SKIO.isSkiOrienteering();
    expect(actual, true);
  });

  test('Test fromStringToEnum when null', () {
    var actual = CourseMode.fromString(null);
    expect(actual, null);
  });

  test('Test fromStringToEnum when wrong value', () {
    var actual = CourseMode.fromString("none");
    expect(actual, null);
  });

  test('Test CourseType.fromStringToEnum when null', () {
    var actual = CourseMode.fromString(null);
    expect(actual, null);
  });

  test('Test CourseType.stringToEnum when regular value', () {
    var actual = CourseMode.fromString("SPORT");
    expect(actual, CourseMode.SPORT);
  });

  test('Test Discipline.stringToEnum when null', () {
    var actual = Discipline.fromString(null);
    expect(actual, Discipline.URBANO);
  });

  test('Test Discipline.stringToEnum when regular value', () {
    var actual = Discipline.fromString("URBANO");
    expect(actual, Discipline.URBANO);
  });

  test('Test Discipline.fromIntToEnum when regular value', () {
    var actual = Discipline.fromInt(1);
    expect(actual, Discipline.FORESTO);
  });

  test('Test Discipline.isFootOrienteering when true with regular value', () {
    var actual = Discipline.URBANO.isFootOrienteering();
    expect(actual, true);
  });

  test('Test Discipline.isFootOrienteering when false with regular value', () {
    var actual = Discipline.MTBO.isFootOrienteering();
    expect(actual, false);
  });

  test('Test Discipline.isMTBOrienteering when true with regular value', () {
    var actual = Discipline.MTBO.isMTBOrienteering();
    expect(actual, true);
  });

  test('Test Discipline.isMTBOrienteering when false with regular value', () {
    var actual = Discipline.URBANO.isMTBOrienteering();
    expect(actual, false);
  });

  test('Test Discipline.isSkiOrienteering when true with regular value', () {
    var actual = Discipline.SKIO.isSkiOrienteering();
    expect(actual, true);
  });

  test('Test Discipline.isSkiOrienteering when false with regular value', () {
    var actual = Discipline.URBANO.isSkiOrienteering();
    expect(actual, false);
  });

  test('Test fromString of CourseFormat with null value', () {
    var actual = CourseFormat.fromString(null);
    expect(actual, null);
  });

  test('Test fromInt of CourseFormat when regular preset', () {
    var actual = CourseFormat.fromInt(0);
    expect(actual, CourseFormat.PRESET);
  });

  test('Test fromInt of CourseFormat when regular free', () {
    var actual = CourseFormat.fromInt(1);
    expect(actual, CourseFormat.FREE);
  });

  test('Test fromInt of CourseFormat when outlier', () {
    var actual = CourseFormat.fromInt(400);
    expect(actual, CourseFormat.PRESET);
  });

  test('Test fromString of ValidationMode with null value', () {
    var actual = ValidationMode.fromString(null);
    expect(actual, null);
  });

  test('Test fromString of ValidationMode when regular', () {
    var actual = ValidationMode.fromString("GPS");
    expect(actual, ValidationMode.GPS);
  });

  test('Test fromString of ValidationMode when null', () {
    var actual = ValidationMode.fromString("GPS1");
    expect(actual, null);
  });

  test('Test fromInt of ValidationMode when regular', () {
    var actual = ValidationMode.fromInt(0);
    expect(actual, ValidationMode.GPS);
  });

  test('Test fromInt of ValidationMode when regular', () {
    var actual = ValidationMode.fromInt(1);
    expect(actual, ValidationMode.MANUAL);
  });

  test('Test fromInt of ValidationMode when regular', () {
    var actual = ValidationMode.fromInt(2);
    expect(actual, ValidationMode.BEACON);
  });

  test('Test fromInt of ValidationMode when outlier', () {
    var actual = ValidationMode.fromInt(200);
    expect(actual, ValidationMode.GPS);
  });

  test('Test isFreeOrder when FREE', () {
    var format = CourseFormat.FREE;
    expect(format.isFreeOrder(), true);
  });

  test('Test isFreeOrder when PRESET', () {
    var format = CourseFormat.PRESET;
    expect(format.isFreeOrder(), false);
  });

  test('Test isPresetOrder when PRESET', () {
    var format = CourseFormat.PRESET;
    expect(format.isPresetOrder(), true);
  });

  test('Test isPresetOrder when FREE', () {
    var format = CourseFormat.FREE;
    expect(format.isPresetOrder(), false);
  });

  test('Test isManual when MANUAL', () {
    var format = ValidationMode.MANUAL;
    expect(format.isManual(), true);
  });

  test('Test isManual when GPS', () {
    var format = ValidationMode.GPS;
    expect(format.isManual(), false);
  });

  test('Test nickname regular expression', () {
    const String text = "Nom - normal _ 123";
    final Iterable<Match> matches = nicknameRegExp.allMatches(text);
    expect(matches.length, text.length);
  });

  test('Test nickname irregular expression 1', () {
    const String text = "!!12aaa";
    final Iterable<Match> matches = nicknameRegExp.allMatches(text);
    expect(matches.length, text.length - 2);
  });

  test('Test nickname irregular expression 2', () {
    const String text = "@a#&";
    final Iterable<Match> matches = nicknameRegExp.allMatches(text);
    expect(matches.length, 1);
  });
}
