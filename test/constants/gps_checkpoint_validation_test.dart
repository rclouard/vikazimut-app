import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/gps_constants.dart';
import 'package:vikazimut/device/gps/gps_scanner.dart';

void main() {
  test('Test DistanceToCpForGpsDetection with radius', () {
    var actual = GpsConstants.setGpsDetectionRadius(1500, 13);
    expect(actual, 13);
  });

  test('Test DistanceToCpForGpsDetection nominal', () {
    var actual = GpsConstants.setGpsDetectionRadius(1500, null);
    expect(actual, GpsScanner.MIN_DISTANCE_TO_CP_IN_METERS_SMALL_SCALE);
  });

  test('Test DistanceToCpForGpsDetection nominal', () {
    var actual = GpsConstants.setGpsDetectionRadius(7000, null);
    expect(actual, GpsScanner.MIN_DISTANCE_TO_CP_IN_METERS_LARGE_SCALE);
  });
}
