import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/storage_constants.dart';

void main() {
  // Workaround for using path_provider
  setUpAll(() {
    TestWidgetsFlutterBinding.ensureInitialized();
    const MethodChannel channel = MethodChannel('plugins.flutter.io/path_provider');
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(channel, (MethodCall methodCall) async {
      return ".";
    });
  });

  test('test_directory_names', () async {
    await Storage.createMapDirectory();
    String dir = Storage.applicationFileDirectory;
    String actualImageDirectory = Storage.mapDirectory;
    expect(actualImageDirectory, "$dir/maps");

    String actualImageFileName = Storage.getImgFileName(10);
    expect(actualImageFileName, "$dir/maps/10.zip");

    String actualXmlFileName = Storage.getXmlFileName(100);
    expect(actualXmlFileName, "$dir/maps/100.xml");

    String actualKmlFileName = Storage.getKmlFileName(1);
    expect(actualKmlFileName, "$dir/maps/1.kml");

    String actualTxtFileName = Storage.getTxtFileName(1);
    expect(actualTxtFileName, "$dir/maps/1.json");

    final Directory directory = Directory("$dir/maps");
    directory.delete();
  });
}
