import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/server_constants.dart';

void main() {
  test('test getXmlURL regular', () {
    String xmlURL = ServerConstants.getXmlURL(1, null);
    expect(xmlURL, "${ServerConstants.serverBaseUrl}/data/1/xml");

    String kmlURL = ServerConstants.getKmlURL(12, null);
    expect(kmlURL, "${ServerConstants.serverBaseUrl}/data/12/kml");

    String imgURL = ServerConstants.getImgURL(102, null);
    expect(imgURL, "${ServerConstants.serverBaseUrl}/data/102/img");
  });

  test('test getXmlURL with secret key', () {
    String xmlURL = ServerConstants.getXmlURL(1, "0000");
    expect(xmlURL, "${ServerConstants.serverBaseUrl}/data/1/xml/0000");

    String kmlURL = ServerConstants.getKmlURL(12, "0000");
    expect(kmlURL, "${ServerConstants.serverBaseUrl}/data/12/kml/0000");

    String imgURL = ServerConstants.getImgURL(102, "0000");
    expect(imgURL, "${ServerConstants.serverBaseUrl}/data/102/img/0000");
  });

  test('test getWebResultPageURL', () {
    String url = ServerConstants.getWebResultPageURL(0);
    expect(url, "${ServerConstants.serverBaseUrl}/web/#/course/tracks/0");
  });
}
