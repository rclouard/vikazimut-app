import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/history_activity/history_presenter.dart';

void main() {
  test('Test sortByDate when equals', () {
    CourseResultEntity e1 = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 1,
      checkpointCount: 0,
    );
    CourseResultEntity e2 = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 1,
      checkpointCount: 0,
    );
    var actual = HistoryPresenter.sortByDecreasingDate_(e1, e2);
    expect(actual, 0);
  });

  test('Test sortByDate when lower', () {
    CourseResultEntity e1 = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 1,
      checkpointCount: 0,
    );
    CourseResultEntity e2 = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 2,
      checkpointCount: 0,
    );
    var actual = HistoryPresenter.sortByDecreasingDate_(e1, e2);
    expect(actual, 1);
  });

  test('Test sortByDate when higher', () {
    CourseResultEntity e1 = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 2,
      checkpointCount: 0,
    );
    CourseResultEntity e2 = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 1,
      checkpointCount: 0,
    );
    var actual = HistoryPresenter.sortByDecreasingDate_(e1, e2);
    expect(actual, -1);
  });
}
