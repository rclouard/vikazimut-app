import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_choice_activity/memory_widget/directory_cleaner.dart';
import 'package:vikazimut/map_choice_activity/course_map.dart';

void main() {
  test('Get ids for map list', () {
    List<String> expectedNames = ["0", "1"];
    List<CourseMap> maps = [
      CourseMap(id: 0, name: "Ensicaen", length: 0, latitude: 0, longitude: 0),
      CourseMap(id: 1, name: "Boston", length: 0, latitude: 0, longitude: 0),
    ];
    List<String> actualNames = getMapIds_(maps);
    expect(expectedNames, actualNames);
  });

  test('Is name in the map list when in', () {
    String fileName = "1";
    List<String> names = ["2", "1", "3"];
    expect(isMapInMapList_(fileName, names), true);
  });

  test('Is name in the map list when not in', () {
    List<String> names = ["10", "11", "12", "13"];
    expect(isMapInMapList_("5", names), false);
  });
}
