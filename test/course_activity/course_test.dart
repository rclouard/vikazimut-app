import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/course.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/device/chronometer.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

class MockCourse extends Mock implements Course {}

class MockOrienteeringMap extends Mock implements OrienteeringMap {}

class MockChronometer extends Mock implements Chronometer {}

void main() {
  test('Test isStartCheckpoint when true', () {
    OrienteeringMap map = OrienteeringMap("name", [], 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    bool startCheckpoint = course.isStartCheckpoint(0);
    expect(startCheckpoint, true);
  });

  test('Test isStartCheckpoint when false 1', () {
    OrienteeringMap map = OrienteeringMap("name", [], 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    bool startCheckpoint = course.isStartCheckpoint(1);
    expect(startCheckpoint, false);
  });

  test('test isStartCheckpoint when false 2', () {
    OrienteeringMap map = OrienteeringMap("name", [], 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    bool startCheckpoint = course.isStartCheckpoint(-1);
    expect(startCheckpoint, false);
  });

  test('Test isLastCheckpoint true', () {
    Checkpoint cp = Checkpoint(0, "name", 0, 0, 1);
    List<Checkpoint> checkpointArray = [cp, cp, cp, cp];
    OrienteeringMap map = OrienteeringMap("test", checkpointArray, 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    bool startCheckpoint = course.isLastCheckpoint(3);
    expect(startCheckpoint, true);
  });

  test('Test isLastCheckpoint false', () {
    Checkpoint cp = Checkpoint(0, "name", 0, 0, 1);
    List<Checkpoint> checkpointArray = [cp, cp, cp, cp];
    OrienteeringMap map = OrienteeringMap("test", checkpointArray, 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    bool startCheckpoint = course.isLastCheckpoint(1);
    expect(startCheckpoint, false);
  });

  test('Test validation', () {
    Checkpoint cp = Checkpoint(0, "name", 0, 0, 1);
    List<Checkpoint> checkpointArray = [cp, cp, cp, cp];
    OrienteeringMap map = OrienteeringMap("test", checkpointArray, 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    Checkpoint checkpoint = Checkpoint(1, "1", 1, 1, 3);
    GeodesicPoint? point = course.punchCheckpoint_(1, checkpoint, 10, false, CourseFormat.FREE);
    expect(course.isCheckpointAlreadyPunched(1), true);
    expect(course.getExpectedCheckpointIndex(), 2);
    expect(course.currentScore, 3);
    expect(point?.timestampInMillisecond, 10);
  });

  test('Test isStartAndFinishAtSamePosition when true', () {
    Checkpoint cp0 = Checkpoint(0, "name", 0.1, 0.1, 1);
    Checkpoint cp1 = Checkpoint(0, "name", 0.2, 0.2, 1);
    Checkpoint cp2 = Checkpoint(0, "name", 0.3, 0.3, 1);
    Checkpoint cp3 = Checkpoint(0, "name", 0.1, 0.1, 1);
    List<Checkpoint> checkpointArray = [cp0, cp1, cp2, cp3];
    OrienteeringMap map = OrienteeringMap("test", checkpointArray, 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    expect(course.isStartAndFinishAtSamePosition(), true);
  });

  test('Test isStartAndFinishAtSamePosition when true', () {
    Checkpoint cp0 = Checkpoint(0, "name", 0.1, 0.1, 1);
    Checkpoint cp1 = Checkpoint(0, "name", 0.2, 0.2, 1);
    Checkpoint cp2 = Checkpoint(0, "name", 0.3, 0.3, 1);
    Checkpoint cp3 = Checkpoint(0, "name", 0.4, 0.3, 1);
    List<Checkpoint> checkpointArray = [cp0, cp1, cp2, cp3];
    OrienteeringMap map = OrienteeringMap("test", checkpointArray, 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    expect(course.isStartAndFinishAtSamePosition(), false);
  });

  test('Test getUnpunchedCheckpoints when empty', () {
    OrienteeringMap map = MockOrienteeringMap();
    Course course = MockCourse();
    when(() => map.getCheckpointCount()).thenReturn(2);
    Checkpoint cp0 = Checkpoint(0, "foo", 0, 0, 1);
    when(() => map.getCheckpoint(0)).thenReturn(cp0);
    Checkpoint cp1 = Checkpoint(1, "foo", 0, 0, 1);
    when(() => map.getCheckpoint(1)).thenReturn(cp1);
    when(() => course.isCheckpointAlreadyPunched(0)).thenReturn(true);
    when(() => course.isCheckpointAlreadyPunched(1)).thenReturn(true);
    List<Checkpoint> actual = Course.getAllUnpunchedCheckpoints_(map, course);
    List<Checkpoint> expected = [];
    expect(expected, actual);
  });

  test('Test getUnpunchedCheckpoints when nominal 1', () {
    OrienteeringMap map = MockOrienteeringMap();
    Course course = MockCourse();
    when(() => map.getCheckpointCount()).thenReturn(2);
    Checkpoint cp0 = Checkpoint(0, "foo", 0, 0, 1);
    when(() => map.getCheckpoint(0)).thenReturn(cp0);
    Checkpoint cp1 = Checkpoint(1, "foo", 0, 0, 1);
    when(() => map.getCheckpoint(1)).thenReturn(cp1);
    when(() => course.isCheckpointAlreadyPunched(0)).thenReturn(false);
    when(() => course.isCheckpointAlreadyPunched(1)).thenReturn(true);
    List<Checkpoint> actual = Course.getAllUnpunchedCheckpoints_(map, course);
    List<Checkpoint> expected = [cp0];
    expect(expected, actual);
  });

  test('Test getUnpunchedCheckpoints when nominal 2', () {
    OrienteeringMap map = MockOrienteeringMap();
    Course course = MockCourse();
    when(() => map.getCheckpointCount()).thenReturn(2);
    Checkpoint cp0 = Checkpoint(0, "foo", 0, 0, 1);
    when(() => map.getCheckpoint(0)).thenReturn(cp0);
    Checkpoint cp1 = Checkpoint(1, "foo", 0, 0, 1);
    when(() => map.getCheckpoint(1)).thenReturn(cp1);
    when(() => course.isCheckpointAlreadyPunched(0)).thenReturn(false);
    when(() => course.isCheckpointAlreadyPunched(1)).thenReturn(false);
    List<Checkpoint> actual = Course.getAllUnpunchedCheckpoints_(map, course);
    List<Checkpoint> expected = [cp0, cp1];
    expect(expected, actual);
  });

  test('Test getClosestUnpunchedCheckpoints when all equals', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    List<Checkpoint> unPunchedCheckpoints = [
      Checkpoint(1, "toto", 0, 0, 1),
      Checkpoint(2, "toto", 0, 0, 1),
      Checkpoint(3, "toto", 0, 0, 1),
      Checkpoint(4, "toto", 0, 0, 1),
      Checkpoint(5, "toto", 0, 0, 1),
    ];
    List<int> i = Course.getIndexesOfClosestUnpunchedCheckpoints(currentLocation, unPunchedCheckpoints);
    expect(i, [1, 2, 3, 4, 5]);
  });

  test('Test getClosestUnpunchedCheckpoints when nominal', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    List<Checkpoint> unPunchedCheckpoints = [
      Checkpoint(1, "toto", 49, 10, 1),
      Checkpoint(2, "toto", 49, 20, 1),
      Checkpoint(3, "toto", 49, 3, 1),
      Checkpoint(4, "toto", 49, 15, 1),
      Checkpoint(5, "toto", 49, 10, 1),
    ];
    List<int> i = Course.getIndexesOfClosestUnpunchedCheckpoints(currentLocation, unPunchedCheckpoints);
    expect(i, [3, 1, 5, 4, 2]);
  });

  test('Test getClosestUnpunchedCheckpoints when nominal 1', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    List<Checkpoint> unPunchedCheckpoints = [
      Checkpoint(1, "toto", 49, 0.0002, 1),
      Checkpoint(2, "toto", 49, 0.0001, 1),
    ];
    int i = Course.getFirstUnpunchedCheckpoint(currentLocation, unPunchedCheckpoints, 100);
    expect(i, 1);
  });

  test('Test getFirstUnpunchedCheckpoint when nominal 2', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    List<Checkpoint> unPunchedCheckpoints = [
      Checkpoint(1, "toto", 49, 1, 1),
      Checkpoint(2, "toto", 49, 0.0002, 1),
      Checkpoint(3, "toto", 49, 0.0001, 1),
    ];
    int i = Course.getFirstUnpunchedCheckpoint(currentLocation, unPunchedCheckpoints, 100);
    expect(i, 2);
  });

  test('Test isCloseToCheckpoint when null', () {
    bool actual = Course.isCloseToCheckpoint_(null, GeodesicPoint(49, 0), 150);
    expect(actual, false);
  });

  test('Test isCloseToCheckpoint when yes', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    GeodesicPoint checkpointLocation = GeodesicPoint(49, 0);
    bool actual = Course.isCloseToCheckpoint_(currentLocation, checkpointLocation, 150);
    expect(actual, true);
  });

  test('Test isCloseToCheckpoint when no', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    GeodesicPoint checkpointLocation = GeodesicPoint(-49, 0);
    bool actual = Course.isCloseToCheckpoint_(currentLocation, checkpointLocation, 150);
    expect(actual, false);
  });

  test('Test isCloseToCheckpoint when just yes', () {
    GeodesicPoint currentLocation = GeodesicPoint(49, 0);
    GeodesicPoint checkpointLocation = GeodesicPoint(49, 1);
    double distance = checkpointLocation.distanceToInMeter(currentLocation);
    bool actual = Course.isCloseToCheckpoint_(currentLocation, checkpointLocation, distance.floor() + 1);
    expect(actual, true);
  });

  test('Test bypassPreviousCheckpoints', () {
    // OrienteeringMap map = MockOrienteeringMap();
    OrienteeringMap map = OrienteeringMap("name", [], 0, 0, Discipline.URBANO, 22);
    Course course = Course(map);
    course.bypassPreviousCheckpoints(9);
    expect(course.getExpectedCheckpointIndex(), 10);
  });

  test('Test bypassPreviousCheckpoints', () {
    Chronometer chronometer = MockChronometer();
    CourseResultEntity courseResultEntity = CourseResultEntity(
      mode: 0,
      format: 0,
      validationMode: 0,
      discipline: 0,
      mapName: "",
      courseId: 0,
      baseTimeInMillisecond: 1,
      checkpointCount: 0,
    );
    OrienteeringMap map = OrienteeringMap("name", [], 0, 0, Discipline.URBANO, 22);
    Course course = Course(map, chronometer);
    course.setCourseResultEntity(courseResultEntity);
    when(() => chronometer.elapsedInMillisecond).thenReturn(11);
    course.stopChronometer();
    verify(() => chronometer.stop());
    expect(courseResultEntity.totalTimeInMillisecond, 11);
  });

  test('Test startChronometer without base time', () {
    OrienteeringMap map = MockOrienteeringMap();
    Chronometer chronometer = MockChronometer();
    when(() => map.getCheckpointCount()).thenReturn(10);
    Course course = Course(map, chronometer);
    when(() => chronometer.start(null)).thenReturn(12);
    var value = course.startChronometer();
    expect(value, 12);
  });

  test('Test startChronometer with base time', () {
    OrienteeringMap map = MockOrienteeringMap();
    Chronometer chronometer = MockChronometer();
    when(() => map.getCheckpointCount()).thenReturn(10);
    Course course = Course(map, chronometer);
    when(() => chronometer.start(1000)).thenReturn(1000);
    var value = course.startChronometer(1000);
    expect(value, 1000);
  });
}
