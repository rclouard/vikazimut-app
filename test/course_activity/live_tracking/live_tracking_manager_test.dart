import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/course_activity/live_tracking/live_tracking_manager.dart';

class MyLiveTrackingGateway {
  Future<int> getCurrentLiveTrackingSessionIfAny(int courseId) async => 0;

  Future<bool> startSessionWithServer(int liveTrackingId, String nickname) async => true;

  Future<void> closeSessionWithServer() async {}
}

class MyLiveTrackingDialog {
  Future<bool> askPermissionForLiveTracking() async => true;

  Future<String?> askNickname() async => null;
}

class MockLiveTrackingGateway extends Mock implements MyLiveTrackingGateway {}

class MockLiveTrackingDialog extends Mock implements MyLiveTrackingDialog {}

void main() {
  test('test connectToLiveTrackingIfAny if nominal', () async {
    var mockLiveTrackingGateway = MockLiveTrackingGateway();
    var mockLiveTrackingDialog = MockLiveTrackingDialog();

    when(() => mockLiveTrackingGateway.getCurrentLiveTrackingSessionIfAny(any())).thenAnswer((_) => Future.value(12));
    when(() => mockLiveTrackingGateway.startSessionWithServer(12, "toto")).thenAnswer((_) => Future.value(true));
    when(() => mockLiveTrackingDialog.askPermissionForLiveTracking()).thenAnswer((_) => Future.value(true));
    when(() => mockLiveTrackingDialog.askNickname()).thenAnswer((_) => Future.value("toto"));
    await LiveTrackingManager.connectToLiveTrackingIfAny_(
      123,
      mockLiveTrackingGateway.getCurrentLiveTrackingSessionIfAny,
      mockLiveTrackingDialog.askPermissionForLiveTracking,
      mockLiveTrackingDialog.askNickname,
      mockLiveTrackingGateway.startSessionWithServer,
    );
    verify(() => mockLiveTrackingDialog.askNickname());
    verify(() => mockLiveTrackingGateway.startSessionWithServer(12, "toto"));

    when(() => mockLiveTrackingGateway.closeSessionWithServer()).thenAnswer((_) => Future.value());
    await LiveTrackingManager.closeSessionWithServer_(
      mockLiveTrackingGateway.closeSessionWithServer,
    );
    verify(() => mockLiveTrackingGateway.closeSessionWithServer());
    await LiveTrackingManager.closeSessionWithServer_(
      mockLiveTrackingGateway.closeSessionWithServer,
    );
    verifyNever(() => mockLiveTrackingGateway.closeSessionWithServer());
  });

  test('test connectToLiveTrackingIfAny when no current session', () async {
    var mockLiveTrackingGateway = MockLiveTrackingGateway();
    var mockLiveTrackingDialog = MockLiveTrackingDialog();

    when(() => mockLiveTrackingGateway.getCurrentLiveTrackingSessionIfAny(any())).thenAnswer(
      (invocation) {
        return Future.value(-1);
      },
    );
    when(() => mockLiveTrackingGateway.startSessionWithServer(12, "toto")).thenAnswer(
      (invocation) {
        return Future.value(true);
      },
    );
    when(() => mockLiveTrackingDialog.askPermissionForLiveTracking()).thenAnswer(
      (invocation) {
        return Future.value(true);
      },
    );
    when(() => mockLiveTrackingDialog.askNickname()).thenAnswer(
      (invocation) {
        return Future.value("toto");
      },
    );
    await LiveTrackingManager.connectToLiveTrackingIfAny_(
      123,
      mockLiveTrackingGateway.getCurrentLiveTrackingSessionIfAny,
      mockLiveTrackingDialog.askPermissionForLiveTracking,
      mockLiveTrackingDialog.askNickname,
      mockLiveTrackingGateway.startSessionWithServer,
    );
    verifyNever(() => mockLiveTrackingDialog.askNickname());
    verifyNever(() => mockLiveTrackingGateway.startSessionWithServer(12, "toto"));
  });
}
