import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';
import 'package:vikazimut/course_activity/forcing/abstract_checkpoint_validation_forcing.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

class MockBuildContext extends Mock implements BuildContext {}

class MockNotificationDialog extends Mock implements NotificationDialog {}

void main() {
  test('Test execute when no location', () {
    NotificationDialog dialogNoLocationError = MockNotificationDialog();
    NotificationDialog dialogDistanceError = MockNotificationDialog();
    NotificationDialog dialogValidationSuccess = MockNotificationDialog();

    MockCoursePresenter coursePresenter = MockCoursePresenter();
    MockBuildContext context = MockBuildContext();

    ManualValidationForcingHandler handler = ManualValidationForcingHandler(context, coursePresenter);
    // Given
    when(() => coursePresenter.getLastKnownLocation()).thenReturn(null);
    // When
    int missingCheckpointId = 10;
    handler.execute_(missingCheckpointId, dialogNoLocationError, dialogDistanceError, dialogValidationSuccess);
    // Then
    verify(() => dialogNoLocationError.show(context));
    verifyNever(() => dialogDistanceError.show(context));
    verifyNever(() => dialogValidationSuccess.show(context));
  });

  test('Test execute when unknown CP', () {
    NotificationDialog dialogNoLocationError = MockNotificationDialog();
    NotificationDialog dialogDistanceError = MockNotificationDialog();
    NotificationDialog dialogValidationSuccess = MockNotificationDialog();
    MockCoursePresenter coursePresenter = MockCoursePresenter();
    MockBuildContext context = MockBuildContext();

    ManualValidationForcingHandler handler = ManualValidationForcingHandler(context, coursePresenter);
    // Given
    GeodesicPoint currentLocation = GeodesicPoint(1, 1);
    when(() => coursePresenter.getLastKnownLocation()).thenReturn(currentLocation);
    // When
    int missingCheckpointId = -1;
    handler.execute_(missingCheckpointId, dialogNoLocationError, dialogDistanceError, dialogValidationSuccess);
    // Then
    verifyNever(() => dialogNoLocationError.show(context));
    verify(() => dialogDistanceError.show(context));
    verifyNever(() => dialogValidationSuccess.show(context));
  });

  test('Test execute when start CP', () {
    NotificationDialog dialogNoLocationError = MockNotificationDialog();
    NotificationDialog dialogDistanceError = MockNotificationDialog();
    NotificationDialog dialogValidationSuccess = MockNotificationDialog();
    MockCoursePresenter coursePresenter = MockCoursePresenter();
    MockBuildContext context = MockBuildContext();

    ManualValidationForcingHandler handler = ManualValidationForcingHandler(context, coursePresenter);
    // Given
    const int missingCheckpointId = 0;
    GeodesicPoint currentLocation = GeodesicPoint(1, 1);
    when(() => coursePresenter.getLastKnownLocation()).thenReturn(currentLocation);
    // When
    handler.execute_(missingCheckpointId, dialogNoLocationError, dialogDistanceError, dialogValidationSuccess);
    // Then
    verifyNever(() => dialogNoLocationError.show(context));
    verifyNever(() => dialogDistanceError.show(context));
    verify(() => dialogValidationSuccess.show(context));
  });

  test('Test execute when distance error', () {
    NotificationDialog dialogNoLocationError = MockNotificationDialog();
    NotificationDialog dialogDistanceError = MockNotificationDialog();
    NotificationDialog dialogValidationSuccess = MockNotificationDialog();
    MockCoursePresenter coursePresenter = MockCoursePresenter();
    MockBuildContext context = MockBuildContext();

    ManualValidationForcingHandler handler = ManualValidationForcingHandler(context, coursePresenter);
    // Given
    const int distance = ManualValidationForcingHandler.REAL_MIN_DISTANCE_TO_MISSING_CP_IN_METERS + 1;
    const int missingCheckpointId = 10;
    GeodesicPoint currentLocation = GeodesicPoint(1, 1);
    when(() => coursePresenter.getLastKnownLocation()).thenReturn(currentLocation);
    when(() => coursePresenter.distanceToNextCheckpoint(currentLocation, missingCheckpointId)).thenReturn(distance);

    // When
    handler.execute_(missingCheckpointId, dialogNoLocationError, dialogDistanceError, dialogValidationSuccess);
    // Then
    verifyNever(() => dialogNoLocationError.show(context));
    verify(() => dialogDistanceError.show(context));
    verifyNever(() => dialogValidationSuccess.show(context));
  });
}
