import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';
import 'package:vikazimut/course_activity/forcing/abstract_checkpoint_validation_forcing.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

class MockBuildContext extends Mock implements BuildContext {}

CoursePresenter _coursePresenter = MockCoursePresenter();

BuildContext context = MockBuildContext();

class AbstractCheckpointValidationForcingTest extends AbstractCheckpointValidationForcing {
  const AbstractCheckpointValidationForcingTest(super.context, super.coursePresenter);
}

void main() {
  test('Test getCheckpointName when start', () {
    AbstractCheckpointValidationForcingTest handler = AbstractCheckpointValidationForcingTest(context, _coursePresenter);
    // Given
    when(() => _coursePresenter.format).thenReturn(CourseFormat.PRESET);
    when(() => _coursePresenter.nextCheckpoint).thenReturn(98);
    // When
    String text = handler.getCheckpointName_(0, " start", " finish");
    expect(text, "0 start");
  });

  test('Test getCheckpointName when finish', () {
    AbstractCheckpointValidationForcingTest handler = AbstractCheckpointValidationForcingTest(context, _coursePresenter);
    // Given
    when(() => _coursePresenter.format).thenReturn(CourseFormat.PRESET);
    when(() => _coursePresenter.isLastCheckpoint(10)).thenReturn(true);
    // When
    String text = handler.getCheckpointName_(10, " start", " finish");
    expect(text, "10 finish");
  });

  test('Test getCheckpointName when intermediate checkpoint', () {
    AbstractCheckpointValidationForcingTest handler = AbstractCheckpointValidationForcingTest(context, _coursePresenter);
    // Given
    when(() => _coursePresenter.format).thenReturn(CourseFormat.PRESET);
    when(() => _coursePresenter.isLastCheckpoint(5)).thenReturn(false);
    // When
    String text = handler.getCheckpointName_(5, " start", " finish");
    expect(text, "5");
  });
}
