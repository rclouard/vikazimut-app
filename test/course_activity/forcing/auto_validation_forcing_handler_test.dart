import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';
import 'package:vikazimut/course_activity/forcing/abstract_checkpoint_validation_forcing.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

class MockBuildContext extends Mock implements BuildContext {}

MockCoursePresenter _coursePresenter = MockCoursePresenter();

MockBuildContext context = MockBuildContext();

void main() {
  test('test getCheckpointId when no location', () {
    AutoValidationForcingHandler handler = AutoValidationForcingHandler(context, _coursePresenter);
    // Given
    when(() => _coursePresenter.format).thenReturn(CourseFormat.FREE);
    when(() => _coursePresenter.getLastKnownLocation()).thenReturn(null);
    when(() => _coursePresenter.isStartCheckpoint(any())).thenReturn(false);
    when(() => _coursePresenter.nextCheckpoint).thenReturn(102);
    // When
    List<int> checkpointIds = handler.getCheckpointIds();
    expect(checkpointIds.isEmpty, true);
  });

  test('Test getCheckpointId when location', () {
    AutoValidationForcingHandler handler = AutoValidationForcingHandler(context, _coursePresenter);
    // Given
    when(() => _coursePresenter.format).thenReturn(CourseFormat.FREE);
    GeodesicPoint currentLocation = GeodesicPoint(1, 1);
    when(() => _coursePresenter.nextCheckpoint).thenReturn(1);
    when(() => _coursePresenter.getLastKnownLocation()).thenReturn(currentLocation);
    when(() => _coursePresenter.isStartCheckpoint(any())).thenReturn(false);
    when(() => _coursePresenter.getClosestUnpunchedCheckpoints(currentLocation)).thenReturn([102]);
    // // When
    List<int> checkpointIds = handler.getCheckpointIds();
    expect(checkpointIds, [102]);
  });

  test('Test getCheckpointId_when preset order', () {
    AutoValidationForcingHandler handler = AutoValidationForcingHandler(context, _coursePresenter);
    // Given
    when(() => _coursePresenter.format).thenReturn(CourseFormat.PRESET);
    when(() => _coursePresenter.nextCheckpoint).thenReturn(98);
    // When
    List<int> checkpointIds = handler.getCheckpointIds();
    expect(checkpointIds, [98]);
  });
}
