import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/course_activity/course.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/device/gps/gps_tracker_service.dart';

class MockCourse extends Mock implements Course {}

class MockGpsTrackerService extends Mock implements GpsTrackerService {}

class MockCourseResultEntity extends Mock implements CourseResultEntity {}

void main() {
  test('Test startRogaine', () {
    Course course = MockCourse();
    GpsTrackerService gpsTracker = MockGpsTrackerService();
    MockCourseResultEntity courseResultEntity = MockCourseResultEntity();

    when(() => course.startChronometer(any())).thenReturn(10);
    when(() => courseResultEntity.runCount).thenReturn(1);
    when(() => courseResultEntity.totalTimeInMillisecond).thenReturn(11);

    CoursePresenter.startRogaine_(course, gpsTracker, courseResultEntity, useDatabase: false);
    verify(() => course.startChronometer(any()));
    verify(() => course.setCourseResultEntity(courseResultEntity));
    verify(() => gpsTracker.setBaseTime(any()));
    verify(() => courseResultEntity.runCount);
    verify(() => courseResultEntity.totalTimeInMillisecond);
  });

  test('Test resumeAfterCrash', () {
    Course course = MockCourse();
    GpsTrackerService gpsTracker = MockGpsTrackerService();
    MockCourseResultEntity courseResultEntity = MockCourseResultEntity();

    when(() => course.startChronometer(any())).thenReturn(10);
    when(() => courseResultEntity.baseTimeInMillisecond).thenReturn(11);
    CoursePresenter.resumeAfterCrash_(course, gpsTracker, courseResultEntity);

    verify(() => course.startChronometer(any()));
    verify(() => course.setCourseResultEntity(courseResultEntity));
    verify(() => gpsTracker.setBaseTime(any()));
  });

  test('Test startRegularCourse', () {
    Course course = MockCourse();
    GpsTrackerService gpsTracker = MockGpsTrackerService();

    when(() => course.startChronometer(any())).thenReturn(10);
    when(() => course.createAndStoreCourseResultEntity(
          mode: 0,
          format: 0,
          validationMode: 0,
          baseTimeInMillisecond: 10,
        )).thenAnswer((invocation) async {});
    CoursePresenter.startRegularCourse_(course, gpsTracker, 0, 0, 0);
    verify(() => course.startChronometer(any()));
    verify(() => gpsTracker.reset(any()));
    verify(() => course.createAndStoreCourseResultEntity(
          mode: 0,
          format: 0,
          validationMode: 0,
          baseTimeInMillisecond: 10,
        ));
  });
}
