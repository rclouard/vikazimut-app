import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/validation/walk_course_manager.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('When course not started and user hits start button', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = -1;
    manager.doPunchCheckpointFromMenu(coursePresenter, controlId);
    verify(() => coursePresenter.prepareCourse());
    verify(() => coursePresenter.startCourse());
  });

  test('When course started and user hits stop button', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);

    manager.doPunchCheckpointFromMenu(coursePresenter, controlId);

    verify(() => coursePresenter.stopCourse());
    verifyNever(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
    verifyNever(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction")));
  });

  test('When course not started and user punches CP other than start then do nothing', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 0;
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verifyNever(() => coursePresenter.displayMultimediaContents(controlId));
  });

  test('When course not started and GPS detects a checkpoint', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(false);
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verifyNever(() => coursePresenter.startCourse());
  });

  test('When course started and GPS detects intermediate CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.isAllCheckpointsPunchedExcept(controlId)).thenReturn(false);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    when(() => coursePresenter.validate(controlId, "scanned_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
  });

  test('When GPS started and GPS detects expected Last', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isAllCheckpointsPunchedExcept(1)).thenReturn(true);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false)).thenReturn(null);
    verifyNever(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false));
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: 'postAction')));
  });

  test('When GPS is just started and GPS detects last at the same initial position', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.isAllCheckpointsPunchedExcept(1)).thenReturn(false);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.isStartAndFinishAtSamePosition()).thenReturn(true);
    when(() => coursePresenter.getPunchedCheckpointCount()).thenReturn(1);
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verifyNever(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
  });

  test('When GPS is started and some controls are punched and GPS detects last at the same initial position', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    WalkValidationCourseManager manager = WalkValidationCourseManager(coursePresenter, CourseMode.WALK, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.isAllCheckpointsPunchedExcept(1)).thenReturn(false);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.isStartAndFinishAtSamePosition()).thenReturn(true);
    when(() => coursePresenter.getPunchedCheckpointCount()).thenReturn(2);
    when(() => coursePresenter.validate(controlId, "scanned_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {return Future(() {});});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
  });
}
