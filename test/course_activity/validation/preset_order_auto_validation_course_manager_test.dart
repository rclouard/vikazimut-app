import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/validation/preset_order_auto_validation_course_manager.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('When device not started and start CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderAutoValidationCourseManager manager = PresetOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = -1;
    when(() => coursePresenter.isStartCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.isStarted).thenReturn(false);
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.prepareCourse());
  });

  test('When device started and last CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderAutoValidationCourseManager manager = PresetOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false));
  });

  test('When GPS not started and first CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderAutoValidationCourseManager manager = PresetOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 0;
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.isStartCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.validate(controlId, "scanned_start_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.startCourse());
  });

  test('When GPS not started not first CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderAutoValidationCourseManager manager = PresetOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.isStartCheckpoint(controlId)).thenReturn(false);
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verifyNever(() => coursePresenter.startCourse());
  });

  test('When GPS started and expected last', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderAutoValidationCourseManager manager = PresetOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.getExpectedCheckpointIndex()).thenReturn(controlId);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false));
  });

  test('When GPS started and expected CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderAutoValidationCourseManager manager = PresetOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.getExpectedCheckpointIndex()).thenReturn(controlId);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    when(() => coursePresenter.validate(controlId, "scanned_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
  });
}
