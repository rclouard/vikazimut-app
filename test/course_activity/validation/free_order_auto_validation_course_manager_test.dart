import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/validation/free_order_auto_validation_course_manager.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('When course is not started and the start button is pressed then just prepare course', () {
    var coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int controlId = -1;
    manager.doPunchCheckpointFromMenu(coursePresenter, controlId);
    verify(() => coursePresenter.prepareCourse());
    verifyNever(() => coursePresenter.startCourse());
  });

  test('When course device started and course not started and start CP', () {
    var coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int checkpointId = 0;
    when(() => coursePresenter.isStartCheckpoint(checkpointId)).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(checkpointId)).thenReturn(false);
    when(() => coursePresenter.isCheckpointAlreadyPunched(checkpointId)).thenReturn(false);
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.validate(checkpointId, "scanned_start_checkpoint", false)).thenReturn(null);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, checkpointId);
    verify(() => coursePresenter.startCourse());
  });

  test('When start CP is detected by GPS then start course', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int controlId = 0;
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.isStartCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.startCourse());
  });

  test('When regular CP is detected GPS and course is started then validate CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
  });

  test('When course is started stop button is pressed then stop the course', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false));
  });

  test('When last CP is detected by GPS and all intermediate CP are validated then stop course', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.allIntermediateCheckpointsValidated()).thenReturn(true);
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {});
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_end_checkpoint", false));
  });

  test('When last CP is detected by GPS and all intermediate CP are validated then stop course', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    FreeOrderAutoValidationCourseManager manager = FreeOrderAutoValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.FREE);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.allIntermediateCheckpointsValidated()).thenReturn(false);
    manager.doPunchCheckpointFromGPS(coursePresenter, controlId);
    verifyNever(() => coursePresenter.stopCourse());
  });
}
