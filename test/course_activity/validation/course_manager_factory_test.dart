import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';
import 'package:vikazimut/course_activity/validation/abstract_course_manager.dart';
import 'package:vikazimut/course_activity/validation/course_manager_factory.dart';
import 'package:vikazimut/course_activity/validation/free_order_auto_validation_course_manager.dart';
import 'package:vikazimut/course_activity/validation/free_order_manual_validation_course_manager.dart';
import 'package:vikazimut/course_activity/validation/preset_order_auto_validation_course_manager.dart';
import 'package:vikazimut/course_activity/validation/preset_order_manual_validation_course_manager.dart';
import 'package:vikazimut/course_activity/validation/walk_course_manager.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

void main() {
  test('test CourseManagerFactory case 1', () {
    var coursePresenter = MockCoursePresenter();
    CourseMode courseMode = CourseMode.WALK;
    CourseFormat courseFormat = CourseFormat.PRESET;
    ValidationMode validationMode = ValidationMode.GPS;
    AbstractCourseManager actual = CourseManagerFactory.create(
      coursePresenter,
      courseMode,
      courseFormat,
      validationMode,
    );
    expect(actual.runtimeType, WalkValidationCourseManager);
  });

  test('test CourseManagerFactory case 2', () {
    var coursePresenter = MockCoursePresenter();
    CourseMode courseMode = CourseMode.SPORT;
    CourseFormat courseFormat = CourseFormat.FREE;
    ValidationMode validationMode = ValidationMode.MANUAL;
    AbstractCourseManager actual = CourseManagerFactory.create(
      coursePresenter,
      courseMode,
      courseFormat,
      validationMode,
    );
    expect(actual.runtimeType, FreeOrderManualValidationCourseManager);
  });

  test('test CourseManagerFactory case 3', () {
    var coursePresenter = MockCoursePresenter();
    CourseMode courseMode = CourseMode.SPORT;
    CourseFormat courseFormat = CourseFormat.PRESET;
    ValidationMode validationMode = ValidationMode.MANUAL;
    AbstractCourseManager actual = CourseManagerFactory.create(
      coursePresenter,
      courseMode,
      courseFormat,
      validationMode,
    );
    expect(actual.runtimeType, PresetOrderManualValidationCourseManager);
  });

  test('test CourseManagerFactory case 4', () {
    var coursePresenter = MockCoursePresenter();
    CourseMode courseMode = CourseMode.SPORT;
    CourseFormat courseFormat = CourseFormat.FREE;
    ValidationMode validationMode = ValidationMode.GPS;
    AbstractCourseManager actual = CourseManagerFactory.create(
      coursePresenter,
      courseMode,
      courseFormat,
      validationMode,
    );
    expect(actual.runtimeType, FreeOrderAutoValidationCourseManager);
  });

  test('test CourseManagerFactory case 5', () {
    var coursePresenter = MockCoursePresenter();
    CourseMode courseMode = CourseMode.SPORT;
    CourseFormat courseFormat = CourseFormat.PRESET;
    ValidationMode validationMode = ValidationMode.GPS;
    AbstractCourseManager actual = CourseManagerFactory.create(
      coursePresenter,
      courseMode,
      courseFormat,
      validationMode,
    );
    expect(actual.runtimeType, PresetOrderAutoValidationCourseManager);
  });
}
