import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/validation/preset_order_manual_validation_course_manager.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';

class MockCoursePresenter extends Mock implements CoursePresenter {}

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('When device not started and start CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderManualValidationCourseManager manager = PresetOrderManualValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = -1;
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.isStartCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.startCourse());
  });

  test('When device not started and not start CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderManualValidationCourseManager manager = PresetOrderManualValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = -1;
    when(() => coursePresenter.isStarted).thenReturn(false);
    when(() => coursePresenter.isStartCheckpoint(controlId)).thenReturn(false);
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.invalidate("checkpoint_error_title", "course_not_started_message"));
  });

  test('When device started and last CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderManualValidationCourseManager manager = PresetOrderManualValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(true);
    when(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction"))).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.displayMultimediaContents(controlId, postAction: any(named: "postAction")));
  });

  test('When device started and already punched CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderManualValidationCourseManager manager = PresetOrderManualValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(true);
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.invalidate("checkpoint_error_title", "already_scanned_checkpoint_message"));
  });

  test('When device started and expected CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderManualValidationCourseManager manager = PresetOrderManualValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.getExpectedCheckpointIndex()).thenReturn(controlId);
    when(() => coursePresenter.displayMultimediaContents(any())).thenAnswer((_) async {});
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.validate(controlId, "scanned_checkpoint", false));
  });

  test('When device started and not expected CP', () {
    CoursePresenter coursePresenter = MockCoursePresenter();
    PresetOrderManualValidationCourseManager manager = PresetOrderManualValidationCourseManager(coursePresenter, CourseMode.SPORT, CourseFormat.PRESET);
    const int controlId = 1;
    when(() => coursePresenter.isStarted).thenReturn(true);
    when(() => coursePresenter.isLastCheckpoint(controlId)).thenReturn(false);
    when(() => coursePresenter.isCheckpointAlreadyPunched(controlId)).thenReturn(false);
    when(() => coursePresenter.getExpectedCheckpointIndex()).thenReturn(controlId + 1);
    manager.doPunchCheckpointFromDevice(coursePresenter, controlId);
    verify(() => coursePresenter.invalidateButAskForBypass(controlId, "checkpoint_error_title", "wrong_checkpoint_message", false));
  });
}
