import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/abstract_course_view.dart';
import 'package:vikazimut/course_activity/course_view_factory.dart';
import 'package:vikazimut/course_activity/geocaching_course_view.dart';
import 'package:vikazimut/course_activity/sport_course_view.dart';
import 'package:vikazimut/course_activity/walk_course_view.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';

void main() {
  final orienteeringMap = OrienteeringMap(
    "name",
    [],
    10000,
    1,
    Discipline.URBANO,
    15,
  );
  final courseResultEntity = CourseResultEntity(
    mode: CourseMode.SPORT.index,
    format: 0,
    validationMode: 0,
    discipline: 0,
    mapName: '',
    courseId: 1,
    baseTimeInMillisecond: 0,
    checkpointCount: 0,
  );

  test('Test create sport course view', () {
    AbstractCourseView actual = CourseViewFactory.createCourse(
      CourseMode.SPORT,
      orienteeringMap,
      preconfigured: true,
      courseResultEntity: courseResultEntity,
    );
    expect(actual.runtimeType, SportCourseView);
  });

  test('Test create geocaching course view', () {
    AbstractCourseView actual = CourseViewFactory.createCourse(
      CourseMode.PLAYFUL,
      orienteeringMap,
      preconfigured: true,
      courseResultEntity: courseResultEntity,
    );
    expect(actual.runtimeType, GeocachingCourseView);
  });

  test('Test create walk course view', () {
    AbstractCourseView actual = CourseViewFactory.createCourse(
      CourseMode.WALK,
      orienteeringMap,
      preconfigured: true,
      courseResultEntity: courseResultEntity,
    );
    expect(actual.runtimeType, WalkCourseView);
  });
}
