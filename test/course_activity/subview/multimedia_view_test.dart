import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/course_activity/subview/multimedia_view.dart';

void main() {
  test('Test createUrlFromText_ with regular url', () {
    var actualUrl = createUrlFromText_("https://www.ensicaen.fr/informatique");
    expect(actualUrl, "https://www.ensicaen.fr/informatique");
  });

  test('Test createUrlFromText_ with regular url but surrounded by spaces', () {
    var actualUrl = createUrlFromText_("\n     https://www.ensicaen.fr/informatique     \n");
    expect(actualUrl, "https://www.ensicaen.fr/informatique");
  });

  test('Test createUrlFromText_ with regular url with args', () {
    var actualUrl = createUrlFromText_("\n     https://www.ensicaen.fr/informatique?arg=1     \n");
    expect(actualUrl, "https://www.ensicaen.fr/informatique?arg=1");
  });

  test('Test createUrlFromText_ with regular text', () {
    var actualUrl = createUrlFromText_("Essai d'un texte avec accents éàù^");
    expect(actualUrl, 'data:text/html;charset=utf-8,%3C!DOCTYPE%20html%3E%3Chtml%20lang=%22en%22%3E%3Chead%3E%3Cmeta%20charset=%22UTF-8%22%3E%3Ctitle%3E%3C/title%3E%3Cmeta%20name=%22viewport%22%20content=%22width=device-width,%20initial-scale=1.0%22%3E%3C/head%3E%3Cbody%20style=%22margin-top:%2020px;%22%3E%3Cpre%20style=%22font-family:%20helvetica,%20sans-serif%22%3EEssai%20d\'un%20texte%20avec%20accents%20%C3%A9%C3%A0%C3%B9%5E%3C/pre%3E%3C/body%3E%3C/html%3E');
  });
}
