import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/mode_choice_activity/version_number_widget.dart';

void main() {
  test('test_directory_names 1', () {
    var filterVersionNumber = VersionNumberWidget.filterVersionNumber_("3.1.5");
    expect(filterVersionNumber, "v3.1.5");
  });

  test('test_directory_names 2', () {
    var filterVersionNumber = VersionNumberWidget.filterVersionNumber_("3.1.5-b2");
    expect(filterVersionNumber, "v3.1.5-b2");
  });
}
