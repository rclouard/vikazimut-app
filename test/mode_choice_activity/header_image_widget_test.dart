import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/mode_choice_activity/header_image_widget.dart';

void main() {
  test('Test random once', () {
    var number = HeaderImageWidget.getRandomNumber_(5);
    expect(number, lessThan(5));
  });

  test('Test random twice in a row should be the same', () {
    var number1 = HeaderImageWidget.getRandomNumber_(5);
    var number2 = HeaderImageWidget.getRandomNumber_(5);
    expect(number1, number2);
  });
}
