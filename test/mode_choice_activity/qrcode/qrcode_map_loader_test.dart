import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/mode_choice_activity/qrcode/qrcode_map_loader.dart';

void main() {
  test('extract map and id when bad magic name', () {
    String text = "toto-12-Ceci est un essai";
    var actual = QrCodeMapLoader.extractMapIdFromQRCodeContent_(text);
    expect(actual, null);
  });

  test('extract map and id when bad id', () {
    String text = "vikazimut-a-Ceci est un essai";
    var actual = QrCodeMapLoader.extractMapIdFromQRCodeContent_(text);
    expect(actual, null);
  });

  test('extract map and id when no name', () {
    String text = "vikazimut-12";
    var actual = QrCodeMapLoader.extractMapIdFromQRCodeContent_(text);
    expect(actual!.id, 12);
    expect(actual.name.isEmpty, true);
  });

  test('extract map and id when regular', () {
    String text = "vikazimut-12-Ceci est un essai";
    var actual = QrCodeMapLoader.extractMapIdFromQRCodeContent_(text);
    expect(actual!.id, 12);
    expect(actual.name, "Ceci est un essai");
  });

  test('Test extractQrCodeTextFromInitialUri when bad scheme', () {
    String deepLink = "toto://vikazimut.vikazim.fr/course/?id=51&name=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, null);
  });

  test('Test extractQrCodeTextFromInitialUri when bad host', () {
    String deepLink = "https://toto.vikazim.fr/course/?id=51&name=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, null);
  });

  test('Test extractQrCodeTextFromInitialUri when bad path', () {
    String deepLink = "https://vikazimut.vikazim.fr/toto/?id=51&name=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, null);
  });

  test('Test extractQrCodeTextFromInitialUri when empty parameters', () {
    String deepLink = "https://vikazimut.vikazim.fr/course/";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, null);
  });

  test('Test extractQrCodeTextFromInitialUri when bad id parameter', () {
    String deepLink = "https://vikazimut.vikazim.fr/course/?toto=51&name=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, null);
  });

  test('Test extractQrCodeTextFromInitialUri when bad name parameter', () {
    String deepLink = "https://vikazimut.vikazim.fr/course/?id=51&toto=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, null);
  });

  test('Test extractQrCodeTextFromInitialUri when regular', () {
    String deepLink = "https://vikazimut.vikazim.fr/course/?id=51&name=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, "vikazimut-51-Découverte du Campus II");
  });

  test('Test extractQrCodeTextFromInitialUri when regular with simple checkpoint', () {
    String deepLink = "https://vikazimut.vikazim.fr/course/?cp=31&id=51&name=Prieur%C3%A9%20de%20Saint%20Grabriel%20Br%C3%A9cy";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, "vikazimut-51-Prieuré de Saint Grabriel Brécy");
  });

  test('Test extractQrCodeTextFromInitialUri when regular with composite checkpoint', () {
    String deepLink = "https://vikazimut.vikazim.fr/course/?cp=31&id=51&name=Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, "vikazimut-51-Découverte du Campus II");
  });

  test('Test extractQrCodeTextFromInitialUri when old fashion', () {
    String deepLink = "vikazimut-51-Découverte du Campus II";
    var actual = QrCodeMapLoader.extractQrCodeTextFromInitialUri_(deepLink);
    expect(actual, "vikazimut-51-Découverte du Campus II");
  });
}
