import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_view/image_decrypter.dart';

void main() {
  test('Test loadEncryptImage with regular encrypted image file', () async {
    const String key = "ffffffffffffffffffffffffffffffffffffffffffg=";
    const String iv = "ffffffffffffffffffffBQ==";
    File gzipFile = File('test/resources/1309.bin.gz');
    var image = await loadEncryptImage_(gzipFile, key: key, iv: iv);
    expect(image.width, 1215);
    expect(image.height, 1158);
  });

  test('Test loadEncryptImage with bad image file', () {
    const String key = "ffffffffffffffffffffffffffffffffffffffffffg=";
    const String iv = "ffffffffffffffffffffBQ==";
    File gzipFile = File('test/resources/ensicaen.xml');
    expect(() async => loadEncryptImage_(gzipFile, key: key, iv: iv), throwsA(isA<Exception>()));
  });
}
