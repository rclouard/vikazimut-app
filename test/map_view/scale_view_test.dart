import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_view/scale_view.dart';

void main() async {
  test('Test computeLineLengthInMeter when 1000', () {
    int actual = computeLineLengthInMeter_(1000, 1);
    expect(actual, 900);
  });

  test('Test computeLineLengthInMeter when 100', () {
    int actual = computeLineLengthInMeter_(100, 1);
    expect(actual, 50);
  });

  test('Test computeLineLengthInMeter when 10', () {
    int actual = computeLineLengthInMeter_(100, 5);
    expect(actual, 10);
  });

  test('Test calculateCorners when nominal', () {
    const List<Offset> expected = [Offset(7, 83), Offset(7, 93), Offset(207, 93), Offset(207, 83)];
    List<Offset> actual = calculateCorners_(200, 100, 10, 5, 5);
    expect(actual, expected);
  });

  test('Test computeImageScale when vertical orientation', () {
    const int width = 2000;
    const int height = 3000;
    double boundWidth = 1000;
    double boundHeight = 1000;
    double actualScale = ScaleView.computeImageScale_(width, height, boundWidth, boundHeight);
    expect(actualScale, moreOrLessEquals(0.3333, epsilon: 0.1));
  });

  test('Test computeImageScale when horizontal orientation', () {
    const int width = 3000;
    const int height = 2000;
    double boundWidth = 1000;
    double boundHeight = 1000;
    double actualScale = ScaleView.computeImageScale_(width, height, boundWidth, boundHeight);
    expect(actualScale, moreOrLessEquals(0.3333, epsilon: 0.1));
  });

  test('Test computeNumberOfPixelsPerMeter', () {
    ScaleView scaleView = const ScaleView(
      currentZoom: 2,
      imageWidth: 2000,
      imageHeight: 3000,
      renderBoxWidth: 1000,
      renderBoxHeight: 1000,
      pixelsPerMeter: 0.1,
    );
    double actualScale = scaleView.computeNumberOfPixelsPerMeter_();
    expect(actualScale, moreOrLessEquals(0.06, epsilon: 0.01));
  });
}
