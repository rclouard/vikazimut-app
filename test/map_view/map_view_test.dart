import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_view/map_view.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';

void main() {
  test('Test convertGeoPointIntoMapCoordinates with regular points', () {
    LatLonBox latLonBox = const LatLonBox(north: 10, south: 4, west: -2, east: 2, rotation: 0);

    int imageWidth = 1000;
    int imageHeight = 2000;
    double latitude = 8;
    double longitude = 0;
    Offset actual = MapViewState.convertGeoPointIntoMapCoordinates_(latitude, longitude, latLonBox, imageWidth, imageHeight);
    Offset expected = const Offset(500, 666);
    expect(actual, expected);
  });

  test('Test centerOn when in already in center', () {
    Offset positionInMapImage = const Offset(500.0, 400.0);
    Matrix4 value = Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
    Size widgetSize = const Size(1000, 800);
    Size imageSize = const Size(1000, 800);
    double initialScale = 1;
    var centerOn_ = MapViewState.centerOn_(
      positionInMapImage,
      value,
      widgetSize,
      imageSize,
      initialScale,
    );
    expect(centerOn_.dx, 0);
    expect(centerOn_.dy, 0);
  });

  test('Test centerOn when in left border', () {
    Offset positionInMapImage = const Offset(0.0, 400.0);
    Matrix4 value = Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
    Size widgetSize = const Size(1000, 800);
    Size imageSize = const Size(1000, 800);
    double initialScale = 1;
    var centerOn_ = MapViewState.centerOn_(
      positionInMapImage,
      value,
      widgetSize,
      imageSize,
      initialScale,
    );
    expect(centerOn_.dx, 500);
    expect(centerOn_.dy, 0);
  });

  test('Test centerOn when in top border', () {
    Offset positionInMapImage = const Offset(500.0, 0.0);
    Matrix4 value = Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
    Size widgetSize = const Size(1000, 800);
    Size imageSize = const Size(1000, 800);
    double initialScale = 1;
    var centerOn_ = MapViewState.centerOn_(
      positionInMapImage,
      value,
      widgetSize,
      imageSize,
      initialScale,
    );
    expect(centerOn_.dx, 0);
    expect(centerOn_.dy, 400);
  });

  test('Test centerOn when in already in top left with scale =0.5', () {
    Offset positionInMapImage = const Offset(0.0, 0.0);
    Matrix4 value = Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
    Size widgetSize = const Size(1000, 800);
    Size imageSize = const Size(1000, 800);
    double initialScale = 0.5;
    var centerOn_ = MapViewState.centerOn_(
      positionInMapImage,
      value,
      widgetSize,
      imageSize,
      initialScale,
    );
    expect(centerOn_.dx, 250);
    expect(centerOn_.dy, 200);
  });
}
