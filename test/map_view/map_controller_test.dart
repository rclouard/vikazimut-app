import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/map_view/map_controller.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';

void main() {
  test('Test mComputeImageScale when horizontal orientation', () {
    const LatLonBox latLonBox = LatLonBox(north: 49.22025813, south: 49.20967057, east: -0.363686103, west: -0.374991451, rotation: 0);
    double actualPixelsPerMeters = MapController.computePixelsPerMeter(latLonBox, 2435, 3481);
    expect(actualPixelsPerMeters, moreOrLessEquals(2.956278565970704, epsilon: 0.00000001));
  });

  test('Test mComputeImageScale when horizontal orientation', () {
    const LatLonBox latLonBox = LatLonBox(north: 49.28126885, south: 49.27774755, east: -0.423041568, west: -0.428691794, rotation: 0);
    double actualPixelsPerMeters = MapController.computePixelsPerMeter(latLonBox, 1215, 1158);
    expect(actualPixelsPerMeters, moreOrLessEquals(2.9577648767340174, epsilon: 0.00000001));
  });
}
