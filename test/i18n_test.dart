import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:vikazimut/l10n.dart';

void main() {
  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  test('Test existence of translation file for all supported languages', () {
    for (final locale in SUPPORTED_LOCALES) {
      L10n.load(Locale(locale));
      expect(L10n.currentLanguage, locale);
    }
  });

  test('Test formatNumber when 0, 1, 1000, 1000.14 and locale is French', () {
    L10n.load(const Locale("fr", "FR"));
    expect(L10n.formatNumber(0, 2), '0');
    expect(L10n.formatNumber(1, 2), '1');
    expect(L10n.formatNumber(1000, 2), '1 000');
    expect(L10n.formatNumber(0.11, 2), '0,11');
    expect(L10n.formatNumber(1000.12, 2), '1 000,12');
    expect(L10n.formatNumber(1000.12, 1), '1 000,1');
    expect(L10n.formatNumber(0.121, 1), '0,1');
  });

  test('Test existence buildListWithSupportedLocales', () {
    expect(
      buildListWithSupportedLocales_(['en', 'fr', 'it']),
      [
        const Locale('en', ''),
        const Locale('fr', ''),
        const Locale('it', ''),
      ],
    );
  });
}
