# L'application mobile Vikazimut

[![pipeline status](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-flutter/badges/master/pipeline.svg)](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-flutter/-/commits/master)
[![coverage report](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-flutter/badges/master/coverage.svg?min_medium=15&min_acceptable=20&min_good=30?)](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-flutter/-/commits/master)

## Le projet Vikazimut

Vikazimut est un projet d'étudiants en informatique de l'[ENSICAEN](https://www.ensicaen.fr).

Le projet répond à une demande du club normand de course d'orientation et de raids multisports [Vik'Azim](https://vikazim.fr)
visant la réalisation d'une application mobile pour la pratique de la course d'orientation
en autonomie.

L’application mobile Vikazimut remplace la carte papier, la boussole et le poinçon de validation des postes de contrôle.
Un parcours d'orientation consiste en une suite de postes de contrôle matérialisés sur le terrain
par une balise type fédération internationale de course d’orientation (IOF).
L’orienteur utilise l’application pour se repérer à partir de la carte et
valider son passage aux points de contrôle en utilisant le lecteur de code QR,
le lecteur de tag NFC ou le lecteur de iBeacons selon l'équipement qui est associé à la balise.
Un mode automatique permet la validation directement à partir de la
position GPS sans utiliser d'équipement physique au poste de contrôle.

Le site web compagnon fournit les parcours d'orientation sous la forme
d'une carte avec les positions des postes de contrôle.
Ces parcours sont proposés par des clubs, des enseignants d'EPS, des collectivités ou des
particuliers qui ont un compte sur le site.
Le site web offre aussi des outils d'analyse rétrospective de la performance réalisée
pour les parcours dont les orienteurs ont volontairement téléversé la trace sur le site.

## L'application mobile

Le présent dépôt correspond à l'application mobile (Android et iOS) du projet.

L’orienteur utilise l’application pour se repérer à partir de la carte et valider son passage aux
points de contrôle avec le lecteur de code QR, le lecteur de tag NFC, le lecteur de iBeacons ou la
position GPS.
L’application affiche en fin du parcours des statistiques sur la réalisation du parcours :
le temps total, le temps intermédiaire entre chaque balise, le dénivelé positif cumulé et le tracé
du parcours réalisé.

L’application se présente sous trois modes : un mode course où l’orienteur n’est pas aidé pour sa
localisation, un mode géocaching où l'application affiche en plus un questionnaire ou un contenu
multimédia aux postes de contrôle, et un mode promenade qui affiche en plus la position de
l’orienteur sur la carte en temps réel.

Les parcours d'orientation sont téléchargeables sur l'application à partir du serveur web associé.

En fin de parcours, l'orienteur a la possibilité d'envoyer sa trace sur le site web pour profiter
des outils d'analyse de la performance proposés par le site.

## Code source

Le projet est développé avec le kit de développement Flutter et le langage Dart.
Le code source de l'application Vikazimut est accessible sur le GitLab du projet&nbsp;:
[https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app).

## Licence

Le projet est à code source ouvert, libre et gratuit.
IL est placé sous la licence publique générale GNU amoindrie
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html).

    Licence LGPL V3

    Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conformément
    aux dispositions de la Licence Publique Générale GNU, telle que publiée par la Free Software
    Foundation ; version 2 de la licence, ou encore (à votre choix) toute version ultérieure.

    Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même
    la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN USAGE PARTICULIER.
    Pour plus de détail, voir la Licence Publique Générale GNU.

## Projets liés

- Le site web développé en Flutter - partie frontend ([dépôt gitlab](https://gitlab.com/clouardregis/vikazimut-website)).
- Le serveur web développé en Symfony - partie backend ([dépôt gitlab](https://gitlab.com/clouardregis/vikazimut-server)).

## Téléchargement de l'application

[Téléchargement sur Google Play](https://play.google.com/store/apps/details?id=fr.ensicaen.vikazimut)

[Téléchargement sur App Store](https://apps.apple.com/fr/app/vikazimut/id1571112577)

## Contributeurs ([ENSICAEN](https://www.ensicaen.fr/) - [spécialité informatique](https://www.ensicaen.fr/formations/ingenieur-informatique/))

### 2015/2016

- Florentin BLANC - e-Paiement & Cybersécurité
- Denis CHEN - e-Paiement & Cybersécurité
- Tristan FAUQUETTE - e-Paiement & Cybersécurité
- Annick VOLCY - e-Paiement & Cybersécurité

### 2016/2017

- Vincent DUPLESSIS - Image, Son & Intelligence Artificielle
- Benjamin HOUX - Image, Son & Intelligence Artificielle
- Axel OLLIVIER - e-Paiement & Cybersécurité
- Yann PELLEGRINI - Image, Son & Intelligence Artificielle
- Elodie PROUX - e-Paiement & Cybersécurité

### 2017/2018

- Samuel ANSEL - Image, Son & Intelligence Artificielle
- Sylvain GAUVREAU - Image, Son & Intelligence Artificielle
- Antonio MANCUSO - Image, Son & Intelligence Artificielle
- Loïc PETIT - e-Paiement & Cybersécurité
- Clément PODEVIN - e-Paiement & Cybersécurité

### 2018/2019

- Lauren SILVA ROLAN SAMPAIO - Image, Son & Intelligence Artificielle
- Aboubakrine NIANE - e-Paiement & Cybersécurité
- Mathieu SERAPHIM - Image, Son & Intelligence Artificielle

### Tuteurs

- Éric PIGEON
- Alain LEBRET
- Régis CLOUARD

### Traducteurs

* Italiano : Olivier PATTUS
* Deutsch : Koni EHRBAR, Eric HOYOIS
* Português : Margarida GONÇALVES NOVO
* Čeština : Pavla CLAQUIN, Petra KAŇÁKOVÁ
* Polski : Oliwier KUSMIERCZYK
