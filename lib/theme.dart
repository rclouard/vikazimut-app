// coverage:ignore-file
import 'package:flutter/material.dart';

const Color kOrangeColor = Color(0xFFff671f);
const Color kOrangeColorUltraLight = Color(0xFFffece2);
const Color kOrangeColorLight = Color(0xFFff8e4c);
const Color kOrangeColorLighter = Color(0xFFffd0b0);
const Color kOrangeColorDisabled = Color(0xFFffb28f);
const Color kOrangeColorBright = Color(0xFFff7110);
const Color kOrangeColorDark = Color(0xFFBB4B16);

const Color kGreenColor = Color(0xFF119f52);
const Color kRedColor = Color(0xFFff0000);
const Color kRedDarkColor = Color(0xffc0392b);
const Color kBlueColor = Color(0xFF007BFF);
const Color kYellowColor = Color(0xffffb142);

@immutable
class ApplicationTheme {
  static final ThemeData defaultTheme = ThemeData(
    primaryColor: kOrangeColor,
    colorScheme: const ColorScheme(
      brightness: Brightness.light,
      primary: kOrangeColor,
      onPrimary: Color(0xFFFFFFFF),
      secondary: Color(0xFF206C2E),
      onSecondary: Color(0xFFFFFFFF),
      error: Color(0xFFBA1A1A),
      onError: kRedColor,
      surface: Color(0xFFFFFBFF),
      onSurface: Color(0xFF201A18),
      surfaceTint: Colors.white,
      outlineVariant: kOrangeColorDisabled,
    ),
    appBarTheme: const AppBarTheme(
      backgroundColor: kOrangeColor,
      toolbarTextStyle: TextStyle(color: Colors.white),
      iconTheme: IconThemeData(color: Colors.white),
      titleTextStyle: TextStyle(color: Colors.white, fontSize: 16),
    ),
    bottomAppBarTheme: const BottomAppBarTheme(
      color: kOrangeColor,
      surfaceTintColor: kOrangeColor,
      padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
      elevation: 0,
      height: 55,
    ),
    cardTheme: CardTheme(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
    ),
    dialogTheme: DialogTheme(
      backgroundColor: Colors.white,
    ),
    iconTheme: const IconThemeData(
      color: Colors.white,
    ),
    textTheme: const TextTheme(
      bodyLarge: TextStyle(color: Colors.black, height: 1.2),
      bodyMedium: TextStyle(color: Colors.black, height: 1.2),
    ),
    radioTheme: RadioThemeData(
      fillColor: WidgetStateProperty.resolveWith<Color?>((Set<WidgetState> states) {
        if (states.contains(WidgetState.disabled)) {
          return kGreenColor;
        }
        if (states.contains(WidgetState.selected)) {
          return kGreenColor;
        }
        return kGreenColor;
      }),
    ),
    progressIndicatorTheme: const ProgressIndicatorThemeData(
      color: kGreenColor,
      circularTrackColor: kOrangeColor,
      linearTrackColor: kOrangeColorUltraLight,
    ),
  );
}
