import 'package:flutter/material.dart';
import 'package:vikazimut/utils/vikazimut_icons.dart';

RegExp nicknameRegExp = RegExp(r"[ \da-zA-Zá-úÁ-Ú_\-]");

enum CourseMode {
  SPORT,
  PLAYFUL,
  WALK;

  static CourseMode? fromString(String? value) => _stringToEnum<CourseMode>(value, CourseMode.values.byName);

  static CourseMode fromInt(int index) {
    if (index < 0 || index >= CourseMode.values.length) {
      index = 0;
    }
    return CourseMode.values[index];
  }
}

enum CourseFormat {
  PRESET,
  FREE;

  static CourseFormat? fromString(String? value) => _stringToEnum<CourseFormat>(value, CourseFormat.values.byName);

  static CourseFormat fromInt(int index) {
    if (index < 0 || index >= CourseFormat.values.length) {
      index = 0;
    }
    return CourseFormat.values[index];
  }

  bool isFreeOrder() => this == FREE;

  bool isPresetOrder() => this == PRESET;
}

enum ValidationMode {
  GPS,
  MANUAL,
  BEACON;

  static ValidationMode? fromString(String? value) => _stringToEnum<ValidationMode>(value, ValidationMode.values.byName);

  static ValidationMode fromInt(int index) {
    if (index < 0 || index >= ValidationMode.values.length) {
      index = 0;
    }
    return ValidationMode.values[index];
  }

  bool isManual() => this == MANUAL;
}

enum Discipline {
  URBANO,
  FORESTO,
  MTBO,
  SKIO;

  static Discipline fromString(String? value) {
    if (value == null) {
      return URBANO;
    } else {
      return _stringToEnum<Discipline>(value, Discipline.values.byName)!;
    }
  }

  static Discipline fromInt(int? index) {
    if (index == null || index >= Discipline.values.length) {
      index = 0;
    }
    return Discipline.values[index];
  }

  static IconData getIcon(Discipline? discipline) {
    if (discipline == null) {
      return Icons.directions_run;
    } else if (discipline == SKIO) {
      return VikazimutIcons.skiing_nordic;
    } else if (discipline == MTBO) {
      return Icons.directions_bike;
    } else {
      return Icons.directions_run;
    }
  }

  bool isFootOrienteering() => this == URBANO || this == FORESTO;

  bool isMTBOrienteering() => this == MTBO;

  bool isSkiOrienteering() => this == SKIO;
}

T? _stringToEnum<T>(String? value, T Function(String name) byName) {
  if (value == null) {
    return null;
  }
  try {
    return byName(value.toUpperCase());
  } catch (_) {
    return null;
  }
}

extension IntExtensions on int {
  bool get isWalkMode => this == CourseMode.WALK.index;

  bool get isGeocachingMode => this == CourseMode.PLAYFUL.index;

  bool get isPresetFormat => this == CourseFormat.PRESET.index;

  bool get isFreeFormat => this == CourseFormat.FREE.index;
}
