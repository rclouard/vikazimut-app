import 'package:sprintf/sprintf.dart';

class ServerConstants {
//   static const String serverBaseUrl = "http://10.0.2.2:8000"; // Test with emulator
  static const String serverBaseUrl = "https://vikazimut.vikazim.fr";

  static const String serverCourseListUrl = "$serverBaseUrl/data/courses";
  static const String serverPrivateCourseListUrl = "$serverBaseUrl/data/private-courses";

  static const String _serverCourseXmlUrl = "$serverBaseUrl/data/%d/xml";
  static const String _serverCourseKmlUrl = "$serverBaseUrl/data/%d/kml";
  static const String _serverCourseImgUrl = "$serverBaseUrl/data/%d/img";

  static const String _webResultPageUrl = "$serverBaseUrl/web/#/course/tracks/%d";

  static const String serverPostResultUrl = "$serverBaseUrl/data/track";
  static const String serverMissingCheckpointUrl = "$serverBaseUrl/data/missing-checkpoint";

  static const String gpsCoordinateTimedFormat = "%.6f,%.6f,%d,%.0f;";

  static String getXmlURL(int id, String? secretKey) {
    if (secretKey == null) {
      return sprintf(_serverCourseXmlUrl, [id]);
    } else {
      return "${sprintf(_serverCourseXmlUrl, [id])}/$secretKey";
    }
  }

  static String getKmlURL(int id, String? secretKey) {
    if (secretKey == null) {
      return sprintf(_serverCourseKmlUrl, [id]);
    } else {
      return "${sprintf(_serverCourseKmlUrl, [id])}/$secretKey";
    }
  }

  static String getImgURL(int id, String? secretKey) {
    if (secretKey == null) {
      return sprintf(_serverCourseImgUrl, [id]);
    } else {
      return "${sprintf(_serverCourseImgUrl, [id])}/$secretKey";
    }
  }

  static String getWebResultPageURL(int mapId) {
    return sprintf(ServerConstants._webResultPageUrl, [mapId]);
  }
}
