class GpsConstants {
  static const int MIN_DISTANCE_TO_CP_IN_METERS_LARGE_SCALE = 22;
  static const int MIN_DISTANCE_TO_CP_IN_METERS_SMALL_SCALE = 12;

  static late int _minDistanceToCpInMeter;

  static int get minDistanceToCpInMeter => _minDistanceToCpInMeter;

  static int setGpsDetectionRadius(int scale, int? detectionRadius) {
    if (detectionRadius != null) {
      _minDistanceToCpInMeter = detectionRadius;
    } else if (scale <= 5000) {
      _minDistanceToCpInMeter = MIN_DISTANCE_TO_CP_IN_METERS_SMALL_SCALE;
    } else {
      _minDistanceToCpInMeter = MIN_DISTANCE_TO_CP_IN_METERS_LARGE_SCALE;
    }
    return minDistanceToCpInMeter;
  }
}
