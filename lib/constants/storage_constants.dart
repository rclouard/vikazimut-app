import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sprintf/sprintf.dart';

const String _NICKNAME_PREFERENCE_KEY = "ORIENTEER_NICKNAME_PREFERENCE";

class Storage {
  static const String _mapDirectory = "maps";
  static const String _xmlFileAbsoluteNameFormat = '%d.xml';
  static const String _kmlFileAbsoluteNameFormat = '%d.kml';
  static const String _imgFileAbsoluteNameFormat = '%d.zip';
  static const String _txtFileAbsoluteNameFormat = '%d.json';
  static const String mapListFilename = "map_list.json";

  static String? _applicationFileDirectory;

  @visibleForTesting
  static String get applicationFileDirectory => '$_applicationFileDirectory';

  static String get mapDirectory => '$_applicationFileDirectory/$_mapDirectory';

  static Future<void> createMapDirectory() async {
    Directory applicationDocumentDir = await getApplicationDocumentsDirectory();
    _applicationFileDirectory = applicationDocumentDir.path;
    final Directory directory = Directory(mapDirectory);
    if (!await directory.exists()) {
      await directory.create(recursive: true);
    }
  }

  static String getXmlFileName(int id) => '$mapDirectory/${sprintf(_xmlFileAbsoluteNameFormat, [id])}';

  static String getKmlFileName(int id) => '$mapDirectory/${sprintf(_kmlFileAbsoluteNameFormat, [id])}';

  static String getImgFileName(int id) => '$mapDirectory/${sprintf(_imgFileAbsoluteNameFormat, [id])}';

  static String getTxtFileName(int id) => '$mapDirectory/${sprintf(_txtFileAbsoluteNameFormat, [id])}';
}

Future<String?> getStoredNickname() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString(_NICKNAME_PREFERENCE_KEY);
}

Future<void> storeNickname(String nickname) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(_NICKNAME_PREFERENCE_KEY, nickname);
}
