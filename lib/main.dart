// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:vikazimut/constants/storage_constants.dart';
import 'package:vikazimut/mode_choice_activity/mode_choice_view.dart';
import 'package:vikazimut/mode_choice_activity/version_number_widget.dart';

import 'l10n.dart';
import 'splash_screen.dart';
import 'theme.dart';

void main() {
  // DEBUG: To track repaint consumption issues
  // debugRepaintRainbowEnabled = true;
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarColor: kOrangeColor,
      statusBarBrightness: Brightness.dark, // For iOS (white icons)
      statusBarIconBrightness: Brightness.light, // For Android (white icons)
    ),
  );
  runApp(const VikazimutApplication());
}

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

final GlobalKey<NavigatorState> globalKeyContext = GlobalKey<NavigatorState>();

@immutable
class VikazimutApplication extends StatelessWidget {
  const VikazimutApplication({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: globalKeyContext,
      supportedLocales: buildListWithSupportedLocales(),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        TranslationsDelegate(),
      ],
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      theme: ApplicationTheme.defaultTheme,
      // DEBUG: Uncomment to track accessibility issues
      // builder: (context, child) => AccessibilityTools(child: child),
      home: Scaffold(
        backgroundColor: kOrangeColor,
        body: SplashScreen(
          home: ModeChoiceView(),
          doInBackground: _doSomethingInBackground,
        ),
      ),
      navigatorObservers: [routeObserver],
    );
  }

  void _doSomethingInBackground() async {
    await Storage.createMapDirectory();
    await AppVersionNumber.build();
  }
}