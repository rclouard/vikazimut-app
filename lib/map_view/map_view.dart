// coverage:ignore-file
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';

import 'layer.dart';
import 'map_controller.dart';
import 'scale_view.dart';

@immutable
class MapView extends StatefulWidget {
  final MapController mapController;
  final ui.Image image;
  final LatLonBox geoPosition;
  late final double _pixelsPerMeter;
  final Size size;
  final bool visible;

  MapView({
    required this.image,
    required this.geoPosition,
    required this.size,
    required this.mapController,
    this.visible = true,
  }) : super() {
    _pixelsPerMeter = MapController.computePixelsPerMeter(geoPosition, image.width, image.height);
  }

  @override
  MapViewState createState() => MapViewState();
}

class MapViewState extends State<MapView> {
  static const double _minScale = 1;
  static const double _maxScale = 10;

  final List<Layer> _layers = [];
  final TransformationController _transformationController = TransformationController();
  double _actualScale = 1;
  double _currentScale = 1;
  int _repaintId = 0;

  @override
  void initState() {
    widget.mapController.mapView = this;
    super.initState();
  }

  @override
  void dispose() {
    _transformationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      children: [
        if (widget.visible)
          Container(
            constraints: BoxConstraints(minHeight: widget.size.height, minWidth: widget.size.width),
            child: InteractiveViewer(
              maxScale: _maxScale,
              minScale: _minScale,
              transformationController: _transformationController,
              onInteractionUpdate: (ScaleUpdateDetails scaleUpdateDetails) {
                setState(() {
                  _currentScale = _actualScale * scaleUpdateDetails.scale;
                  if (_currentScale > _maxScale) {
                    _currentScale = _maxScale;
                  }
                  if (_currentScale < _minScale) {
                    _currentScale = _minScale;
                  }
                });
              },
              onInteractionEnd: (details) {
                _actualScale = _currentScale;
              },
              child: CustomPaint(
                painter: _MapPainter(
                  image: widget.image,
                  repaintId: _repaintId,
                  layers: _layers,
                  transformationController: _transformationController,
                  mapController: widget.mapController,
                ),
              ),
            ),
          ),
        if (widget.visible)
          Container(
            padding: const EdgeInsets.only(left: 20, bottom: 20),
            child: ScaleView(
              currentZoom: _currentScale,
              imageWidth: widget.image.width,
              imageHeight: widget.image.height,
              renderBoxWidth: widget.size.width,
              renderBoxHeight: widget.size.height,
              pixelsPerMeter: widget._pixelsPerMeter,
            ),
          ),
      ],
    );
  }

  void addLayer(Layer layer) {
    _layers.add(layer);
  }

  void removeLayer(Layer layer) {
    _layers.remove(layer);
    layer.stop();
  }

  Offset convertGeoPointIntoMapCoordinates(double latitude, double longitude) {
    return convertGeoPointIntoMapCoordinates_(latitude, longitude, widget.geoPosition, widget.image.width, widget.image.height);
  }

  @visibleForTesting
  static Offset convertGeoPointIntoMapCoordinates_(double latitude, double longitude, LatLonBox boundingBox, int imageWidth, int imageHeight) {
    int x = (longitude - boundingBox.west) * imageWidth ~/ (boundingBox.east - boundingBox.west);
    int y = (latitude - boundingBox.north) * imageHeight ~/ (boundingBox.south - boundingBox.north);
    x -= imageWidth ~/ 2;
    y -= imageHeight ~/ 2;
    double theta = boundingBox.rotation * pi / 180.0;
    double x1 = (x * cos(theta) - y * sin(theta));
    double y1 = (x * sin(theta) + y * cos(theta));
    x1 += imageWidth / 2;
    y1 += imageHeight / 2;
    return Offset(x1, y1);
  }

  void centerOn(latitude, longitude, double initialScale) {
    Offset translate = centerOn_(
      convertGeoPointIntoMapCoordinates(latitude, longitude),
      _transformationController.value,
      widget.size,
      Size(widget.image.width.toDouble(), widget.image.height.toDouble()),
      initialScale,
    );
    _transformationController.value = _transformationController.value..translate(translate.dx, translate.dy);
  }

  @visibleForTesting
  static Offset centerOn_(
    Offset positionInMapImage,
    Matrix4 value,
    Size widgetSize,
    Size imageSize,
    double initialScale,
  ) {
    double xCurrentOffset = value.row0[3];
    double yCurrentOffset = value.row1[3];
    double xScale = value.row0[0];
    double yScale = value.row1[1];
    double xExpectedPosition = positionInMapImage.dx;
    double yExpectedPosition = positionInMapImage.dy;
    double xInitialOffset = (widgetSize.width - imageSize.width * initialScale) / 2.0;
    double yInitialOffset = (widgetSize.height - imageSize.height * initialScale) / 2.0;
    double xTranslate = -xCurrentOffset + (widgetSize.width / 2.0) - (xExpectedPosition * xScale * initialScale);
    double yTranslate = -yCurrentOffset + (widgetSize.height / 2.0) - (yExpectedPosition * yScale * initialScale);
    xTranslate = xTranslate / xScale - xInitialOffset;
    yTranslate = yTranslate / yScale - yInitialOffset;
    return Offset(xTranslate, yTranslate);
  }

  void invalidate() {
    if (mounted) {
      setState(() {
        _repaintId++;
      });
    }
  }

  double getPixelsPerMeter() {
    return widget._pixelsPerMeter;
  }
}

class _MapPainter extends CustomPainter {
  final ui.Image image;
  final int repaintId;
  final List<Layer> layers;
  final TransformationController transformationController;
  final MapController mapController;

  const _MapPainter({
    required this.image,
    required this.repaintId,
    required this.layers,
    required this.transformationController,
    required this.mapController,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..isAntiAlias = true
      ..filterQuality = FilterQuality.medium;
    final vScale = size.width / image.width;
    final hScale = size.height / image.height;
    final scale = min(vScale, hScale);
    mapController.initialScale = scale;
    canvas.translate((size.width - image.width * scale) / 2.0, (size.height - image.height * scale) / 2.0);
    canvas.scale(scale);
    canvas.drawImage(image, Offset.zero, paint);
    for (var layer in layers) {
      layer.draw(canvas, scale: scale * transformationController.value.row0[0]);
    }
  }

  @override
  bool shouldRepaint(_MapPainter oldDelegate) => oldDelegate.repaintId != repaintId;
}
