import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';

import 'image_decrypter.dart' as decrypter;
import 'layer.dart';
import 'map_view.dart';

class MapController {
  late final MapViewState _mapView;
  double initialScale = 1;

  set mapView(MapViewState mapView) => _mapView = mapView;

  static Future<ui.Image> loadImage(File file) async => decrypter.loadEncryptImage(file);

  void addLayer(Layer layer) => _mapView.addLayer(layer);

  void removeLayer(Layer layer) => _mapView.removeLayer(layer);

  void centerOn(double latitude, double longitude) => _mapView.centerOn(latitude, longitude, initialScale);

  void invalidate() => _mapView.invalidate();

  Offset convertGeoPointIntoMapCoordinates(double latitude, double longitude) => _mapView.convertGeoPointIntoMapCoordinates(latitude, longitude);

  double getNumberOfPixelsPerMeter() => _mapView.getPixelsPerMeter();

  static double computePixelsPerMeter(LatLonBox geoPosition, int imageWidth, int imageHeight) {
    GeodesicPoint geodesicPoint1 = GeodesicPoint(geoPosition.north, geoPosition.west);
    GeodesicPoint geodesicPoint2 = GeodesicPoint(geoPosition.south, geoPosition.east);
    double distanceInMeter = geodesicPoint1.distanceToInMeter(geodesicPoint2);
    return sqrt(imageHeight * imageHeight + imageWidth * imageWidth) / distanceInMeter;
  }
}
