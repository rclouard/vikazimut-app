// coverage:ignore-file
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_position.dart';

import '../map_controller.dart';
import '../layer.dart';

abstract class AbstractRouteLayer implements Layer {
  final MapController _mapController;
  List<GeodesicPoint>? _route;
  List<PunchPosition>? _punchPositions;
  GeodesicPoint? _lastLocation;
  bool? _isPunchLocation;

  AbstractRouteLayer(this._mapController);

  @mustCallSuper
  void drawRoute(List<GeodesicPoint> route, {GeodesicPoint? lastLocation, bool? isPunchLocation, List<PunchPosition>? punchPositions}) {
    _route = route;
    _lastLocation = lastLocation;
    _isPunchLocation = isPunchLocation;
    _punchPositions = punchPositions;
    _mapController.invalidate();
  }

  @nonVirtual
  @override
  void draw(Canvas canvas, {required double scale}) {
    if (_route != null && _route!.length >= 2) {
      drawPolyLine(canvas, _route!, _lastLocation, _isPunchLocation, scale, punchPositions: _punchPositions);
    }
  }

  void centerOn(double latitude, double longitude) => _mapController.centerOn(latitude, longitude);

  void drawPolyLine(Canvas canvas, List<GeodesicPoint> route, GeodesicPoint? lastLocation, bool? isPunchLocation, double scale, {List<PunchPosition>? punchPositions});

  Offset convertGeodesicPointToMapPoint(GeodesicPoint currentPoint) => _mapController.convertGeoPointIntoMapCoordinates(currentPoint.latitude, currentPoint.longitude);

  @override
  void onResume() {}

  @override
  void stop() {}
}
