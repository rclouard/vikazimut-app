// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/punch_position.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'abstract_route_layer.dart';

class FlatRouteLayer extends AbstractRouteLayer {
  static const double _STROKE_WIDTH = 3;
  late final Paint _paint;

  FlatRouteLayer(super._mapController) {
    _paint = Paint()
      ..style = PaintingStyle.stroke
      ..color = const Color(0xFFf44336)
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true;
  }

  @override
  void drawPolyLine(
    Canvas canvas,
    List<GeodesicPoint> route,
    GeodesicPoint? lastLocation,
    bool? isPunchLocation,
    double scale, {
    List<PunchPosition>? punchPositions,
  }) {
    _paint.strokeWidth = _STROKE_WIDTH / scale;
    Offset pt1 = convertGeodesicPointToMapPoint(route[0]);
    for (int i = 1; i < route.length; i++) {
      Offset pt2 = convertGeodesicPointToMapPoint(route[i]);
      canvas.drawLine(pt1, pt2, _paint);
      pt1 = pt2;
    }
    if (lastLocation != null) {
      Offset pt2 = convertGeodesicPointToMapPoint(lastLocation);
      canvas.drawLine(pt1, pt2, _paint);
    }

    // Draw punch position
    if (punchPositions != null) {
      final validationCirclePaint = Paint()
        ..style = PaintingStyle.fill
        ..color = Colors.white;
      final validationCircleOutlinePaint = Paint()
        ..style = PaintingStyle.stroke
        ..color = Colors.black
        ..strokeWidth = 2
        ..blendMode = BlendMode.srcOver;
      for (int i = 0; i < punchPositions.length; i++) {
        Offset offset = convertGeodesicPointToMapPoint(punchPositions[i].position);
        const double radius = 4;
        canvas.drawCircle(offset, radius, validationCirclePaint);
        canvas.drawCircle(offset, radius, validationCircleOutlinePaint);
      }
    }
  }
}
