// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_position.dart';

import 'abstract_route_layer.dart';

class AnimatedRouteLayer extends AbstractRouteLayer {
  late final Paint _tailPaint;
  late final Paint _dotPaint;
  late final Paint _dotBorderPaint;
  double _trackHeadRadius = 5;

  AnimatedRouteLayer(super._mapController) {
    _tailPaint = Paint()
      ..color = const Color(0xAAf44336)
      ..style = PaintingStyle.stroke;
    _dotBorderPaint = Paint()
      ..color = const Color(0xFFf44336)
      ..style = PaintingStyle.stroke;
    _dotPaint = Paint();
  }

  @override
  void drawRoute(List<GeodesicPoint> route, {GeodesicPoint? lastLocation, bool? isPunchLocation, List<PunchPosition>? punchPositions}) {
    if (route.isNotEmpty) {
      centerOn(route.last.latitude, route.last.longitude);
    }
    super.drawRoute(route, lastLocation: lastLocation, isPunchLocation: isPunchLocation);
  }

  @override
  void drawPolyLine(
    Canvas canvas,
    List<GeodesicPoint> route,
    GeodesicPoint? lastLocation,
    bool? isPunchLocation,
    double scale, {
    List<PunchPosition>? punchPositions,
  }) {
    _tailPaint.strokeWidth = 4 / scale;
    _dotBorderPaint.strokeWidth = 1 / scale;
    _trackHeadRadius = 6 / scale;

    Path path = Path();
    Offset pt1 = convertGeodesicPointToMapPoint(route[0]);
    path.moveTo(pt1.dx, pt1.dy);
    for (int i = 1; i < route.length; i++) {
      Offset pt2 = convertGeodesicPointToMapPoint(route[i]);
      path.lineTo(pt2.dx, pt2.dy);
    }
    canvas.drawPath(path, _tailPaint);

    GeodesicPoint lastLocation = route.last;
    Offset pt2 = convertGeodesicPointToMapPoint(lastLocation);
    if (isPunchLocation ?? true) {
      _dotPaint.color = Colors.yellowAccent;
    } else {
      _dotPaint.color = const Color(0xAAf44336);
    }
    canvas.drawCircle(pt2, _trackHeadRadius, _dotBorderPaint);
    canvas.drawCircle(pt2, _trackHeadRadius, _dotPaint);
  }
}
