// coverage:ignore-file
import 'dart:async';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/device/gps/gps_location_listener.dart';
import 'package:vikazimut/device/gps/location_provider.dart';

import '../map_controller.dart';
import '../layer.dart';

class LocationLayer implements Layer, GpsLocationListener {
  static const _ACCURACY_CIRCLE_COLOR = Color(0x2A6464FF);
  static const _ACCURACY_BORDER_COLOR = Color(0x776464FF);

  final MapController _mapController;
  late Offset _avatarHotspot;
  ui.Image? _avatarBitmap;
  late Offset _arrowCenter;
  ui.Image? _arrowBitmap;
  final Paint _positionPaint = Paint();
  final Paint _circlePaint = Paint();
  final bool onlyAvatar;
  LocationProvider? _locationProvider;
  Position? _position;
  bool _isLocationEnabled = false;
  double _bearing = 0;

  LocationLayer(this._mapController, {required this.onlyAvatar}) {
    _circlePaint.color = _ACCURACY_CIRCLE_COLOR;
    _circlePaint.isAntiAlias = true;
    _createAvatarBitmap();
    _createArrowBitmap();
    _setLocationProvider(LocationProvider());
    _start();
  }

  @override
  void onLocationChanged(final Position location) {
    _setLocation(location);
  }

  @override
  void stop() {
    _isLocationEnabled = false;
    _stopLocation();
  }

  @override
  void onResume() {
    if (_isLocationEnabled) {
      _enableLocation();
    }
  }

  @override
  void draw(Canvas canvas, {required double scale}) {
    if (_position == null) {
      return;
    }
    Offset pt = _mapController.convertGeoPointIntoMapCoordinates(_position!.latitude, _position!.longitude);
    _drawAccuracyCircleAroundLocation(canvas, pt);
    _drawLocation(canvas, pt, scale);
  }

  Future<void> _createArrowBitmap() async {
    _arrowBitmap = await _loadAssetImage("assets/images/arrow.png");
    _arrowCenter = Offset(_arrowBitmap!.width / 2.0 - 0.5, _arrowBitmap!.height / 2.0 - 0.5);
  }

  Future<void> _createAvatarBitmap() async {
    _avatarBitmap = await _loadAssetImage("assets/images/avatar.png");
    _avatarHotspot = Offset(_avatarBitmap!.width / 2.0 - 0.5, _avatarBitmap!.height.toDouble());
  }

  Future<ui.Image> _loadAssetImage(String assetPath) async {
    final ByteData data = await rootBundle.load(assetPath);
    final Uint8List list = Uint8List.view(data.buffer);
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(list, (ui.Image img) => completer.complete(img));
    return completer.future;
  }

  void _start() {
    _isLocationEnabled = true;
    _enableLocation();
  }

  void _stopLocation() {
    _stopLocationProvider();
    _mapController.invalidate();
  }

  void _setLocationProvider(LocationProvider myLocationProvider) {
    _stopLocationProvider();
    _locationProvider = myLocationProvider;
  }

  void _computeBearing(Position location) {
    if (_position != null) {
      _bearing = Geolocator.bearingBetween(_position!.latitude, _position!.longitude, location.latitude, location.longitude);
    }
  }

  void _drawLocation(Canvas canvas, Offset pt, scale) {
    if (!onlyAvatar && _position!.speed > 0.1) {
      _drawArrow(canvas, pt, 1 / scale);
    } else {
      _drawAvatar(canvas, pt, 1 / scale);
    }
  }

  void _drawAvatar(Canvas canvas, Offset pt, double scale) {
    if (_avatarBitmap == null) return;
    canvas.save();
    final translateX = pt.dx;
    final translateY = pt.dy;
    canvas.translate(translateX, translateY);
    canvas.scale(scale);
    canvas.drawImage(_avatarBitmap!, Offset(-_avatarHotspot.dx, -_avatarHotspot.dy), _positionPaint);
    canvas.restore();
  }

  void _drawArrow(Canvas canvas, Offset pt, double scale) {
    if (_arrowBitmap == null) return;
    canvas.save();
    final translateX = pt.dx;
    final translateY = pt.dy;
    canvas.translate(translateX, translateY);
    final double mapAngleInRadians = (_bearing % 360) * pi / 180;
    canvas.rotate(mapAngleInRadians);
    canvas.scale(scale);
    canvas.drawImage(_arrowBitmap!, Offset(-_arrowCenter.dx, -_arrowCenter.dy), _positionPaint);
    canvas.restore();
  }

  void _drawAccuracyCircleAroundLocation(Canvas canvas, Offset pt) {
    final double pixelsPerMeter = _mapController.getNumberOfPixelsPerMeter();
    final double radius = _position!.accuracy * pixelsPerMeter;
    _circlePaint.color = _ACCURACY_CIRCLE_COLOR;
    _circlePaint.style = PaintingStyle.fill;
    canvas.drawCircle(pt, radius, _circlePaint);
    _circlePaint.color = _ACCURACY_BORDER_COLOR;
    _circlePaint.style = PaintingStyle.stroke;
    canvas.drawCircle(pt, radius, _circlePaint);
  }

  void _stopLocationProvider() {
    if (_locationProvider != null) {
      _locationProvider!.stopLocationProvider();
    }
  }

  void _setLocation(Position location) {
    _computeBearing(location);
    _position = location;
    _mapController.invalidate();
  }

  void _enableLocation() {
    _locationProvider!.startLocationProvider(this);
    _locationProvider!.getLastKnownLocation().then((position) {
      if (position != null) {
        _setLocation(position);
      }
    });
    _mapController.invalidate();
  }
}
