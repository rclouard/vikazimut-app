// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import '../map_controller.dart';
import '../layer.dart';

class StartMarkerLayer extends Layer {
  static const double _TRIANGLE_HEIGHT = 20.0;
  static const double _CIRCLE_RADIUS = 5.0;
  static const double _STRIKE_WIDTH = 5.0;

  late final Paint _paintOuter;
  late final Paint _paintInner;
  late Path _triangle;
  late Offset _circleOuter;
  Offset _startCoordinates = const Offset(-1000, -1000);
  double _radius = 0;

  StartMarkerLayer(MapController mapController, GeodesicPoint location) {
    _paintOuter = Paint()
      ..color = const Color(0xFFF33A6A)
      ..strokeWidth = _STRIKE_WIDTH
      ..style = PaintingStyle.stroke
      ..isAntiAlias = true;
    _paintInner = Paint()
      ..color = const Color(0xFFF33A6A)
      ..isAntiAlias = true;
    _startCoordinates = mapController.convertGeoPointIntoMapCoordinates(location.latitude, location.longitude);
  }

  @override
  void draw(Canvas canvas, {required double scale}) {
    _paintOuter.strokeWidth = _STRIKE_WIDTH / scale;
    Offset point = _startCoordinates;
    _circleOuter = Offset(point.dx, point.dy - _TRIANGLE_HEIGHT / scale);
    _triangle = Path();
    _triangle.moveTo(point.dx, point.dy);
    final dx = (((_CIRCLE_RADIUS + (_STRIKE_WIDTH / 2.0)) * (_TRIANGLE_HEIGHT - _CIRCLE_RADIUS + (_STRIKE_WIDTH / 2.0))) / _TRIANGLE_HEIGHT) / scale;
    final dy = (_TRIANGLE_HEIGHT - _CIRCLE_RADIUS / 2) / scale;
    _triangle.lineTo(point.dx - dx, point.dy - dy);
    _triangle.lineTo(point.dx + dx, point.dy - dy);
    _triangle.close();
    _radius = _CIRCLE_RADIUS / scale;

    canvas.drawCircle(_circleOuter, _radius, _paintOuter);
    canvas.drawPath(_triangle, _paintInner);
  }

  @override
  void onResume() {}

  @override
  void stop() {}
}
