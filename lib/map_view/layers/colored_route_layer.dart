// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_position.dart';
import 'package:vikazimut/utils/speed_color_handler.dart';

import 'abstract_route_layer.dart';

class ColoredRouteLayer extends AbstractRouteLayer {
  static const double _STROKE_WIDTH = 5.0;
  late final Paint _backgroundLinePaint;
  late final Paint _foregroundLinePaint;
  late final SpeedColorHandler _speedColorHandler;

  ColoredRouteLayer(super._mapController, Discipline discipline) {
    _backgroundLinePaint = Paint()
      ..strokeCap = StrokeCap.round
      ..color = Colors.black;
    _foregroundLinePaint = Paint()..strokeCap = StrokeCap.round;
    _speedColorHandler = SpeedColorHandlerFactory.build(discipline);
  }

  @override
  void drawPolyLine(
    Canvas canvas,
    List<GeodesicPoint> route,
    GeodesicPoint? lastLocation,
    bool? isPunchLocation,
    double scale, {
    List<PunchPosition>? punchPositions,
  }) {
    _backgroundLinePaint.strokeWidth = (_STROKE_WIDTH + 1) / scale;
    _foregroundLinePaint.strokeWidth = _STROKE_WIDTH / scale;

    var speeds = SpeedColorHandler.filterSpeeds(route);

    List<_Line> lines = [];
    Offset pt1 = convertGeodesicPointToMapPoint(route[0]);
    for (int i = 1; i < route.length; i++) {
      GeodesicPoint location2 = route[i];
      Offset pt2 = convertGeodesicPointToMapPoint(location2);
      canvas.drawLine(pt1, pt2, _backgroundLinePaint);
      lines.add(_Line(pt1, pt2, _speedColorHandler.colorIndexFromVelocity(speeds[i])));
      pt1 = pt2;
    }

    lines.sort((a, b) => b.colorIndex.compareTo(a.colorIndex));

    for (final line in lines) {
      _foregroundLinePaint.color = _speedColorHandler.colorFromColorIndex(line.colorIndex);
      canvas.drawLine(line.start, line.end, _foregroundLinePaint);
    }
    _drawPunchPositions(punchPositions, canvas);
  }

  void _drawPunchPositions(List<PunchPosition>? punchPositions, Canvas canvas) {
    if (punchPositions != null) {
      final validationCirclePaint = Paint()
        ..style = PaintingStyle.fill
        ..color = Colors.white;
      final validationCircleOutlinePaint = Paint()
        ..style = PaintingStyle.stroke
        ..color = Colors.black
        ..strokeWidth = 2
        ..blendMode = BlendMode.srcOver;
      for (int i = 0; i < punchPositions.length; i++) {
        Offset offset = convertGeodesicPointToMapPoint(punchPositions[i].position);
        const double radius = 4;
        canvas.drawCircle(offset, radius, validationCirclePaint);
        canvas.drawCircle(offset, radius, validationCircleOutlinePaint);
        if (punchPositions[i].forced) {
          canvas.drawLine(Offset(offset.dx - radius * 0.707, offset.dy - radius * 0.707), Offset(offset.dx + radius * 0.707, offset.dy + radius * 0.707), validationCircleOutlinePaint);
          canvas.drawLine(Offset(offset.dx - radius * 0.707, offset.dy + radius * 0.707), Offset(offset.dx + radius * 0.707, offset.dy - radius * 0.707), validationCircleOutlinePaint);
        }
      }
    }
  }
}

class _Line {
  final Offset start;
  final Offset end;
  final int colorIndex;

  const _Line(this.start, this.end, this.colorIndex);
}
