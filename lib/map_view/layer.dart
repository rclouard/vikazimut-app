// coverage:ignore-file
import 'package:flutter/material.dart';

abstract class Layer {
  void draw(Canvas canvas, {required double scale});

  void onResume();

  void stop();
}
