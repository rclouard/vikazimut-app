import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:archive/archive.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/foundation.dart';
import 'package:vikazimut/keys.dart';

Future<ui.Image> loadEncryptImage(File gzipFile) async {
  return await loadEncryptImage_(gzipFile, key: Secret.key, iv: Secret.initializationVector);
}

@visibleForTesting
Future<ui.Image> loadEncryptImage_(File gzipFile, {required String key, required String iv}) async {
  Uint8List imageData = await compute((File gzipFile) async {
    final String data = await _unarchive(gzipFile);
    return await _decryptFile(data, encrypt.Key.fromBase64(key), encrypt.IV.fromBase64(iv));
  }, gzipFile);
  final Completer<ui.Image> completer = Completer();
  ui.decodeImageFromList(imageData, (ui.Image image) => completer.complete(image));
  return completer.future;
}

Future<String> _unarchive(File zippedFile) async {
  final Uint8List bytes = zippedFile.readAsBytesSync();
  try {
    return String.fromCharCodes(GZipDecoder().decodeBytes(bytes));
  } catch (_) {
    throw Exception("Bad image contents");
  }
}

Future<Uint8List> _decryptFile(String encryptedBase64EncodedString, encrypt.Key key, encrypt.IV iv) async {
  var decoded = base64.decode(encryptedBase64EncodedString);
  final encrypter = encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
  return Uint8List.fromList(encrypter.decryptBytes(encrypt.Encrypted(decoded), iv: iv));
}
