import 'dart:math';

import 'package:flutter/material.dart';

@immutable
class ScaleView extends StatelessWidget {
  final double currentZoom;
  final int imageWidth;
  final int imageHeight;
  final double renderBoxWidth;
  final double renderBoxHeight;
  final double pixelsPerMeter;

  const ScaleView({
    super.key,
    required this.currentZoom,
    required this.imageWidth,
    required this.imageHeight,
    required this.renderBoxWidth,
    required this.renderBoxHeight,
    required this.pixelsPerMeter,
  });

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: const Size(200, 50),
      painter: _ScalePainter(
        currentZoom: currentZoom,
        numberOfPixelsPerMeter: computeNumberOfPixelsPerMeter_(),
      ),
    );
  }

  @visibleForTesting
  double computeNumberOfPixelsPerMeter_() {
    double imageScale = computeImageScale_(imageWidth, imageHeight, renderBoxWidth, renderBoxHeight);
    return pixelsPerMeter * imageScale * currentZoom;
  }

  @visibleForTesting
  static double computeImageScale_(int width, int height, double boundWidth, double boundHeight) {
    return min(boundWidth / width, boundHeight / height);
  }
}

class _ScalePainter extends CustomPainter {
  static const int RULER_HEIGHT_IN_PIXEL = 10;
  static const double MARGIN = 5;
  static const double STROKE_SIZE = 2;
  static const Color BLUE_COLOR = Color(0xFF0000FF);

  final double numberOfPixelsPerMeter;
  final double currentZoom;
  final Paint _paint = Paint()
    ..isAntiAlias = true
    ..color = BLUE_COLOR
    ..strokeWidth = STROKE_SIZE;

  _ScalePainter({
    required this.currentZoom,
    required this.numberOfPixelsPerMeter,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final lengthInMeter = computeLineLengthInMeter_(size.width, numberOfPixelsPerMeter);
    final lengthInPixel = (lengthInMeter * numberOfPixelsPerMeter).toInt();
    _drawLines(canvas, lengthInPixel, size.height);
    _drawLegend(canvas, lengthInMeter, lengthInPixel, size.height);
  }

  void _drawLines(Canvas canvas, int lengthInPixel, double height) {
    final points = calculateCorners_(lengthInPixel, height, RULER_HEIGHT_IN_PIXEL, STROKE_SIZE, MARGIN);
    canvas.drawLine(points[0], points[1], _paint);
    canvas.drawLine(points[1], points[2], _paint);
    canvas.drawLine(points[2], points[3], _paint);
  }

  void _drawLegend(Canvas canvas, int lengthInMeter, int lengthInPixel, double height) {
    final textPainter = TextPainter(
      text: TextSpan(
        text: '$lengthInMeter m',
        style: const TextStyle(
          color: BLUE_COLOR,
          fontSize: 14,
          fontWeight: FontWeight.bold,
        ),
      ),
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();
    final offset = Offset(lengthInPixel / 2 - textPainter.size.width / 2 + MARGIN, height - RULER_HEIGHT_IN_PIXEL - textPainter.size.height);
    textPainter.paint(canvas, offset);
  }

  @override
  bool shouldRepaint(_ScalePainter oldDelegate) => currentZoom != oldDelegate.currentZoom;
}

@visibleForTesting
List<Offset> calculateCorners_(int length, double height, int rulerHeight, double strokeSize, double margin) {
  final double halfStrokeSize = strokeSize ~/ 2 + margin;
  final double visibleHeight = height - halfStrokeSize;
  return [
    Offset(halfStrokeSize, visibleHeight - rulerHeight),
    Offset(halfStrokeSize, visibleHeight),
    Offset(length + halfStrokeSize, visibleHeight),
    Offset(length + halfStrokeSize, visibleHeight - rulerHeight),
  ];
}

@visibleForTesting
int computeLineLengthInMeter_(double width, double numberOfPixelsPerMeter) {
  int i = 5000;
  while (i * numberOfPixelsPerMeter > width - 10.0) {
    if (i <= 50) {
      i -= 10;
    } else if (i <= 100) {
      i -= 50;
    } else {
      i -= 100;
    }
  }
  return i;
}
