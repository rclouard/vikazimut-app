import 'package:flutter/foundation.dart';

@immutable
class MissingCheckpointData {
  final String courseId;
  final String checkpointId;

  const MissingCheckpointData(this.courseId, this.checkpointId);

  Map<String, dynamic> toJson() => {
        "courseId": courseId,
        "controlPointId": checkpointId,
      };
}
