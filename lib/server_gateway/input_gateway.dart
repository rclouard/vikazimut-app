import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut/constants/server_constants.dart';

@immutable
class InputGateway {
  static Future<http.Response> getXmlFile(int id, String? secretKey) async {
    final xmlURL = ServerConstants.getXmlURL(id, secretKey);
    return await http.get(Uri.parse(xmlURL));
  }

  static Future<http.Response> getKmlFile(int id, String? secretKey) async {
    final kmlURL = ServerConstants.getKmlURL(id, secretKey);
    return await http.get(Uri.parse(kmlURL));
  }

  static Future<http.StreamedResponse> getImgFile(int id, String? secretKey) async {
    final imgURL = ServerConstants.getImgURL(id, secretKey);
    return await http.Client().send(http.Request('GET', Uri.parse(imgURL)));
  }

  static Future<http.Response> getCourseMapList(bool withPrivateCourses) async {
    String serverCourseListUrl = withPrivateCourses ? ServerConstants.serverPrivateCourseListUrl : ServerConstants.serverCourseListUrl;
    return await http.get(Uri.parse(serverCourseListUrl)).timeout(const Duration(seconds: 10));
  }
}
