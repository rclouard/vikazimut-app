import 'package:flutter/foundation.dart';

@immutable
class ResultData {
  final String orienteer;
  final int format;
  final int totalTimeInMillisecond;
  final String gpsTrack;
  final int courseId;
  final List<int> punchTimeInMillisecondList;
  final List<bool> punchForceList;
  final int runCount;

  ResultData({
    required this.orienteer,
    required this.format,
    required this.totalTimeInMillisecond,
    required this.gpsTrack,
    required this.courseId,
    required this.punchTimeInMillisecondList,
    required this.punchForceList,
    required this.runCount,
  }) {
    assert(punchTimeInMillisecondList.length == punchForceList.length);
  }

  Map<String, dynamic> toJson() {
    List<Map> controlPoints = [];
    for (int i = 0; i < punchTimeInMillisecondList.length; i++) {
      int time = punchTimeInMillisecondList[i];
      bool isForced = punchForceList[i];
      Map<String, dynamic> jo;
      // Forced is the default value so to minimize communication footprint don't send force = true
      if (isForced) {
        jo = {"controlPoint": i, "punchTime": time, "forced": true};
      } else {
        jo = {"controlPoint": i, "punchTime": time};
      }
      controlPoints.add(jo);
    }
    return {
      'orienteer': orienteer,
      'format': format,
      'totalTime': totalTimeInMillisecond,
      'courseId': courseId,
      'controlPoints': controlPoints,
      'trace': gpsTrack,
      'runCount': runCount,
    };
  }
}
