// coverage:ignore-file

import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/foundation.dart';
import 'package:vikazimut/keys.dart';

@immutable
class Cryptography {
  static String encryptJsonMessage(String message) {
    final key = encrypt.Key.fromBase64(Secret.key);
    final iv = encrypt.IV.fromBase64(Secret.initializationVector);
    final encrypter = encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));
    encrypt.Encrypted? encrypted = encrypter.encrypt(message, iv: iv);
    return encrypted.base64;
  }
}
