abstract class GatewayListener {
  void handlePositiveSendingResultsToServerResponse(int code);

  void handleNegativeSendingResultsToServerResponse(int code, String message);
}
