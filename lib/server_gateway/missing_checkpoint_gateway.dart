import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/l10n.dart';

import 'cryptography.dart';
import 'missing_checkpoint_data.dart';
import 'gateway_listener.dart';

@immutable
class MissingCheckpointGateway {
  Future<void> sendToServer(
    final MissingCheckpointData missingCheckpointData,
    final GatewayListener requestResponseListener,
    final int requestCode,
  ) async {
    String jsonMessage = jsonEncode(missingCheckpointData);
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    final response = await http.post(
      Uri.parse(ServerConstants.serverMissingCheckpointUrl),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: encryptedMessage,
    );

    if (response.statusCode == HttpStatus.ok) {
      requestResponseListener.handlePositiveSendingResultsToServerResponse(requestCode);
    } else {
      String errorMessage = L10n.getString("send_result_error_no_connection");
      requestResponseListener.handleNegativeSendingResultsToServerResponse(requestCode, errorMessage);
    }
  }
}
