import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut/constants/server_constants.dart';

import 'cryptography.dart';

@immutable
class LiveTrackingGateway {
  static const String _LIVE_ID_PREFERENCE_KEY = "LIVE_ID";
  static const String _ORIENTEER_LIVE_ID_PREFERENCE_KEY = "ORIENTEER_LIVE_ID";
  static const int NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION = 5; //  degrees 0.00001 -> precision 1.11 meter
  static int _orienteerLiveTrackingId = -1;
  static int _previousSendingTimeInMillisecond = 0;
  static final List<Position> _pendingPositions = [];

  static Future<int> getCurrentLiveTrackingSessionIfAny(int courseId) async {
    try {
      final response = await http.get(
        Uri.parse("${ServerConstants.serverBaseUrl}/data/live_tracking/session/$courseId"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );
      if (response.statusCode == HttpStatus.ok) {
        var jsonContents = json.decode(response.body);
        return jsonContents["live_id"] as int;
      } else {
        return -1;
      }
    } catch (_) {
      return -1;
    }
  }

  static Future<bool> startSessionWithServer(int liveTrackingId, String nickname) async {
    final liveId = await _getFromPreferences(_LIVE_ID_PREFERENCE_KEY);
    if (liveId == liveTrackingId) {
      var value = await _getFromPreferences(_ORIENTEER_LIVE_ID_PREFERENCE_KEY);
      if (value != null) {
        _orienteerLiveTrackingId = value;
        return true;
      }
    }
    String jsonMessage = jsonEncode({
      "id": liveTrackingId,
      "nickname": nickname,
    });
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    try {
      final response = await http.post(
        Uri.parse("${ServerConstants.serverBaseUrl}/data/live_tracking/connect"),
        headers: <String, String>{
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: encryptedMessage,
      );
      if (response.statusCode == HttpStatus.ok) {
        var jsonContents = json.decode(response.body);
        _orienteerLiveTrackingId = jsonContents["live_id"] as int;
        await _setInPreferences(_LIVE_ID_PREFERENCE_KEY, liveTrackingId);
        await _setInPreferences(_ORIENTEER_LIVE_ID_PREFERENCE_KEY, _orienteerLiveTrackingId);
        return true;
      }
    } catch (_) {}
    return false;
  }

  static void sendOrienteerPosition({
    required Position position,
    required int score,
  }) async {
    var elapsedTimeSinceSending = position.timestamp.millisecondsSinceEpoch - _previousSendingTimeInMillisecond;
    if (elapsedTimeSinceSending < 20 * 1000) {
      _pendingPositions.add(position);
    } else {
      Map<String, String> positions = getPositionsFromPendingPosition_(_pendingPositions, position);
      String jsonMessage = jsonEncode({
        "id": _orienteerLiveTrackingId,
        "latitudes": positions['latitudes'],
        "longitudes": positions['longitudes'],
        "score": score,
      });
      _pendingPositions.clear();
      _pendingPositions.add(position);
      _previousSendingTimeInMillisecond = position.timestamp.millisecondsSinceEpoch;
      await _sendPositionsToServer(jsonMessage);
    }
  }

  static Future<void> _sendPositionsToServer(String jsonMessage) async {
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    try {
      final response = await http.patch(
        Uri.parse("${ServerConstants.serverBaseUrl}/data/live_tracking/positions"),
        headers: <String, String>{
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: encryptedMessage,
      );
      final isLiveTrackingSessionClosed = response.statusCode == HttpStatus.requestTimeout;
      if (isLiveTrackingSessionClosed) {
        _closeSession();
      }
    } catch (_) {
      // Nothing to do
    }
  }

  static Future<void> closeSessionWithServer() async {
    String jsonMessage = jsonEncode({
      "id": _orienteerLiveTrackingId,
    });
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    try {
      await http.post(
        Uri.parse("${ServerConstants.serverBaseUrl}/data/live_tracking/closing"),
        headers: <String, String>{
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: encryptedMessage,
      );
    } catch (_) {
      // Nothing to do
    }
    _closeSession();
  }

  static void _closeSession() async {
    _orienteerLiveTrackingId = -1;
    await _clearPreferences();
  }

  static Future<int?> _getFromPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  static Future<void> _setInPreferences(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

  static Future<void> _clearPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(_LIVE_ID_PREFERENCE_KEY);
    prefs.remove(_ORIENTEER_LIVE_ID_PREFERENCE_KEY);
  }

  @visibleForTesting
  static Map<String, String> getPositionsFromPendingPosition_(List<Position> pendingPositions, Position position) {
    String latitudes = position.latitude.toStringAsFixed(NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION);
    String longitudes = position.longitude.toStringAsFixed(NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION);

    final int currentTime = position.timestamp.millisecondsSinceEpoch;
    String lat10s = "";
    String lon10s = "";
    String lat20s = "";
    String lon20s = "";
    for (final Position p in pendingPositions) {
      if (currentTime - p.timestamp.millisecondsSinceEpoch > 35 * 1000) {
      } else if (currentTime - p.timestamp.millisecondsSinceEpoch >= 20 * 1000) {
        lat20s = p.latitude.toStringAsFixed(NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION);
        lon20s = p.longitude.toStringAsFixed(NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION);
      } else if (currentTime - p.timestamp.millisecondsSinceEpoch >= 10 * 1000) {
        lat10s = p.latitude.toStringAsFixed(NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION);
        lon10s = p.longitude.toStringAsFixed(NUMBER_DECIMAL_FOR_METER_ORDER_PRECISION);
      }
    }
    if (lat10s.isNotEmpty) {
      latitudes += "|$lat10s";
      longitudes += "|$lon10s";
    }
    if (lat20s.isNotEmpty) {
      latitudes += "|$lat20s";
      longitudes += "|$lon20s";
    }

    return {'latitudes': latitudes, 'longitudes': longitudes};
  }
}
