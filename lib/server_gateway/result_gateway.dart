import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/l10n.dart';

import 'cryptography.dart';
import 'gateway_listener.dart';
import 'result_data.dart';

@immutable
class ResultGateway {
  Future<bool> sendToServer(
    ResultData resultData,
    GatewayListener requestResponseListener,
    int requestCode,
  ) async {
    String jsonMessage = jsonEncode(resultData);
    String encryptedMessage = Cryptography.encryptJsonMessage(jsonMessage);
    try {
      final response = await http.post(
        Uri.parse(ServerConstants.serverPostResultUrl),
        headers: <String, String>{
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: encryptedMessage,
      );
      if (response.statusCode == HttpStatus.ok) {
        requestResponseListener.handlePositiveSendingResultsToServerResponse(requestCode);
        return true;
      } else {
        String errorMessage = L10n.getString("send_result_error_no_connection");
        requestResponseListener.handleNegativeSendingResultsToServerResponse(requestCode, errorMessage);
        return false;
      }
    } catch (_) {
      String errorMessage = L10n.getString("send_result_error_no_connection");
      requestResponseListener.handleNegativeSendingResultsToServerResponse(requestCode, errorMessage);
      return false;
    }
  }
}
