import 'dart:io';

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/gps_constants.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:xml/xml.dart';

import 'orienteering_map.dart';
import 'xml_util.dart';

/// Reference for the xml file standard https://orienteering.sport/iof/it/data-standard-3-0/
/// xml file contains the course description
class OrienteeringIofXmlFileReader {
  static OrienteeringMap readFile(File xmlFile, String name) {
    var xmlText = convertXmlTagsToLowerCase(xmlFile.readAsStringSync());
    final XmlDocument document = XmlDocument.parse(xmlText);
    // 1 <RaceCourseData>
    final XmlElement raceCourseDataElement = document.getElement('coursedata')!.getElement("racecoursedata")!;
    final int scale = _extractScale(raceCourseDataElement);
    final String? detectionRadiusText = _extractExtensionFeature(raceCourseDataElement, "detectionradius");
    int? detectionRadius = detectionRadiusText == null ? null : int.tryParse(detectionRadiusText);
    detectionRadius = GpsConstants.setGpsDetectionRadius(scale, detectionRadius);

    final String? disciplineText = _extractExtensionFeature(raceCourseDataElement, "discipline");
    final Discipline discipline = Discipline.fromString(disciplineText);
    final String? courseModeText = _extractExtensionFeature(raceCourseDataElement, "coursetype");
    final CourseMode? courseMode = CourseMode.fromString(courseModeText);
    final String? courseFormatText = _extractExtensionFeature(raceCourseDataElement, "courseformat");
    final CourseFormat? courseFormat = CourseFormat.fromString(courseFormatText);
    final String? courseValidationModeText = _extractExtensionFeature(raceCourseDataElement, "coursevalidationmode");
    final ValidationMode? courseValidationMode = ValidationMode.fromString(courseValidationModeText);

    // 2 <Control>
    final Iterable<XmlElement> controlElements = raceCourseDataElement.findElements('control');
    final Map<String, CheckpointData> checkpointData = _extractCheckpointData(controlElements);
    // 3 <Course>
    final XmlElement courseElements = raceCourseDataElement.findElements('course').first;
    final List<Checkpoint> checkpoints = extractCourseData_(courseElements, checkpointData);
    final double theoreticalLengthInMeter = _extractLength(courseElements);
    return OrienteeringMap(
      name,
      checkpoints,
      theoreticalLengthInMeter,
      scale,
      discipline,
      detectionRadius,
      courseMode: courseMode,
      courseFormat: courseFormat,
      validationMode: courseValidationMode,
    );
  }

  static int _extractScale(final XmlElement element) {
    XmlElement mapXmlElement = element.getElement("map")!.getElement("scale")!;
    return int.parse(mapXmlElement.innerText);
  }

  static String? _extractExtensionFeature(final XmlElement element, final String featureName) {
    XmlElement? xmlElement = element.getElement("map")!.getElement("extensions")?.getElement(featureName);
    if (xmlElement == null) {
      return null;
    }
    return xmlElement.innerText;
  }

  static double _extractLength(final XmlElement element) {
    XmlElement mapElement = element.getElement("length")!;
    return double.parse(mapElement.innerText);
  }

  static Map<String, CheckpointData> _extractCheckpointData(final Iterable<XmlElement> elements) {
    final Map<String, CheckpointData> checkpointData = {};
    for (final element in elements) {
      final CheckpointData info = CheckpointData();
      final id = element.getElement("id");
      info.id = id!.innerText;

      final position = element.getElement("position");
      if (position != null) {
        info.longitude = double.parse(position.getAttribute("lng")!);
        info.latitude = double.parse(position.getAttribute("lat")!);
      }
      final XmlElement? text = _getInfoTextElement(element);
      info.text = text?.innerText;
      checkpointData[info.id] = info;
    }
    return checkpointData;
  }

  static XmlElement? _getInfoTextElement(final XmlElement element) {
    final text = element.getElement("text");
    if (text != null) {
      return text;
    }
    return element.getElement("extensions")?.getElement("text");
  }

  @visibleForTesting
  static List<Checkpoint> extractCourseData_(
    final XmlElement courseElements,
    final Map<String, CheckpointData> checkpointFeatures,
  ) {
    int index = 0;
    final List<Checkpoint> checkpoints = [];
    final courseControlElements = courseElements.findElements("coursecontrol");
    for (final courseControlElement in courseControlElements) {
      final String id = courseControlElement.getElement('control')!.innerText;
      final String? pointText = courseControlElement.getElement('score')?.innerText;
      int? points;
      if (pointText == null) {
        points = null;
      } else {
        points = int.tryParse(pointText);
      }
      points ??= 1;
      final CheckpointData checkpoint = checkpointFeatures[id]!;
      checkpoints.add(
        Checkpoint(
          index++,
          checkpoint.id,
          checkpoint.latitude,
          checkpoint.longitude,
          points,
          checkpoint.text,
        ),
      );
    }
    return checkpoints;
  }
}

@immutable
@visibleForTesting
class CheckpointData {
  late final String id;
  late final double longitude;
  late final double latitude;
  late final String? text;
}
