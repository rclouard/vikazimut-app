import 'dart:convert';
import 'dart:io';

/// Txt file contains additional information on course such as club and clubUrl
class OrienteeringMapTxtFileReader {
  static Map<String, dynamic> readFile(File txtFile) => jsonDecode(txtFile.readAsStringSync());
}
