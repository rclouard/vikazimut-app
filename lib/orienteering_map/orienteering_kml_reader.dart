import 'dart:io';

import 'package:xml/xml.dart';

import 'lat_lon_box.dart';
import 'xml_util.dart';

/// kml file contains the geolocation of the map
class OrienteeringMapKmlFileReader {
  static LatLonBox readFile(File kmlFile) {
    final xmlText = convertXmlTagsToLowerCase(kmlFile.readAsStringSync());
    final XmlDocument document = XmlDocument.parse(xmlText);
    final Iterable<XmlElement> latLonBox = document.findAllElements("latlonbox");
    if (latLonBox.isNotEmpty) {
      XmlElement first = latLonBox.first;
      final north = double.parse(first.getElement("north")!.innerText);
      final south = double.parse(first.getElement("south")!.innerText);
      final east = double.parse(first.getElement("east")!.innerText);
      final west = double.parse(first.getElement("west")!.innerText);
      final rotationText = first.getElement("rotation");
      double rotation;
      if (rotationText == null) {
        rotation = 0;
      } else {
        rotation = double.parse(rotationText.innerText);
      }
      return LatLonBox(north: north, south: south, east: east, west: west, rotation: rotation);
    } else {
      throw Exception("Bad KML file");
    }
  }
}
