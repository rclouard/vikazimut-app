import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'checkpoint_out_of_bounds_exception.dart';
import 'lat_lon_box.dart';

class OrienteeringMap {
  late final int id;
  final double theoreticalLengthInMeter;
  final String name;
  final int scale;
  late final String drawableName;
  late final double _north;
  late final double _south;
  late final double _east;
  late final double _west;
  late final double rotation;
  final Discipline discipline;
  late final String? clubName;
  late final String? clubUrl;
  final int detectionRadius;
  final List<Checkpoint> _checkpoints;
  CourseMode? courseMode;
  final CourseFormat? courseFormat;
  final ValidationMode? validationMode;

  OrienteeringMap(
    this.name,
    List<Checkpoint> checkpoints,
    this.theoreticalLengthInMeter,
    this.scale,
    this.discipline,
    this.detectionRadius, {
    this.courseMode,
    this.courseFormat,
    this.validationMode,
  })  : _checkpoints = checkpoints;

  void setLocation(double north, double south, double east, double west) {
    _north = north;
    _south = south;
    _east = east;
    _west = west;
  }

  List<Checkpoint> get checkpoints => _checkpoints;

  int getCheckpointCount() => _checkpoints.length;

  Checkpoint getCheckpoint(int index) => _checkpoints[index];

  int getCheckpointIdFromName(String checkpointName, int nextCheckpointIndex) => getCheckpointFromName_(checkpointName, nextCheckpointIndex).index;

  String getCheckpointNameFromId(int checkpointId) => _checkpoints[checkpointId].id;

  GeodesicPoint getCheckpointLocationFromId(int id) => _checkpoints[id].location;

  int getDistanceToCheckpoint(GeodesicPoint currentLocation, int checkpointId) {
    GeodesicPoint checkpoint = _checkpoints[checkpointId].location;
    return checkpoint.distanceToInMeter(currentLocation).toInt();
  }

  LatLonBox getBoundingBox() => LatLonBox(north: _north, south: _south, east: _east, west: _west, rotation: rotation);

  bool isInBounds(double latitude, double longitude) => latitude <= _north && latitude >= _south && longitude <= _east && longitude >= _west;

  @visibleForTesting
  Checkpoint getCheckpointFromName_(String checkpointName, int nextCheckpointIndex) {
    for (int i = nextCheckpointIndex; i < _checkpoints.length; i++) {
      Checkpoint cpToBePunched = _checkpoints[i];
      if (cpToBePunched.id == checkpointName) {
        return cpToBePunched;
      }
    }
    for (int i = 0; i < nextCheckpointIndex; i++) {
      Checkpoint cpAlreadyPunched = _checkpoints[i];
      if (cpAlreadyPunched.id == checkpointName) {
        return cpAlreadyPunched;
      }
    }
    throw CheckpointOutOfBoundsException();
  }
}
