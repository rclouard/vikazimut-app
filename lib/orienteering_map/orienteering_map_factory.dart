import 'dart:io';

import 'package:vikazimut/constants/storage_constants.dart';

import 'lat_lon_box.dart';
import 'orienteering_iof_xml_reader.dart';
import 'orienteering_kml_reader.dart';
import 'orienteering_map.dart';
import 'orienteering_txt_reader.dart';

class OrienteeringMapFactory {
  static OrienteeringMap createOrienteeringMapFromXmlAndKmlAndImgFiles(int courseId, String courseName) {
    File xmlFile = File(Storage.getXmlFileName(courseId));
    File kmlFile = File(Storage.getKmlFileName(courseId));
    File imgFile = File(Storage.getImgFileName(courseId));
    File txtFile = File(Storage.getTxtFileName(courseId));

    LatLonBox latLonBox = OrienteeringMapKmlFileReader.readFile(kmlFile);
    Map<String, dynamic> json = OrienteeringMapTxtFileReader.readFile(txtFile);
    OrienteeringMap map = OrienteeringIofXmlFileReader.readFile(xmlFile, courseName);
    map.setLocation(latLonBox.north, latLonBox.south, latLonBox.east, latLonBox.west);
    map.rotation = latLonBox.rotation;
    map.drawableName = imgFile.path;
    map.clubName = json["club"];
    map.clubUrl = json["clubUrl"];
    map.id = courseId;
    return map;
  }
}
