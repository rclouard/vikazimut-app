// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/web_navigator.dart';

import 'course_map.dart';
import 'map_choice_presenter.dart';

@immutable
class MapWidget extends StatefulWidget {
  final int index;
  final MapChoicePresenter presenter;

  const MapWidget(this.index, this.presenter);

  @override
  MapWidgetState createState() => MapWidgetState();
}

class MapWidgetState extends State<MapWidget> implements IProgressBarWidget {
  double _progress = 0;

  @override
  Widget build(BuildContext context) {
    final CourseMap courseMap = widget.presenter.filteredCourseMaps[widget.index];
    return Container(
      margin: const EdgeInsets.all(5),
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        border: Border.all(width: 1.5, color: kOrangeColorLighter),
        borderRadius: BorderRadius.circular(15),
      ),
      child: IntrinsicHeight(
        child: Row(
          children: [
            Expanded(
              child: ElevatedButton(
                onPressed: () => widget.presenter.onPressedCourseMapButton(context, courseMap, this),
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.resolveWith((states) {
                    if (!states.contains(WidgetState.pressed)) {
                      return courseMap.isDownloaded ? kOrangeColorBright : Colors.white;
                    }
                    return kOrangeColorLighter;
                  }),
                  elevation: WidgetStateProperty.all(5),
                  padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.all(3)),
                  shape: WidgetStateProperty.all<OutlinedBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  ),
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8, bottom: 5),
                      child: Visibility(
                        visible: courseMap.isDownloading,
                        replacement: const SizedBox(height: 5),
                        child: LinearProgressIndicator(
                          value: (courseMap.isDownloading) ? _progress : 0,
                          minHeight: 5,
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: [
                                  _CircleIcon(
                                    icon: Discipline.getIcon(courseMap.discipline),
                                    iconColor: courseMap.isDownloaded ? kOrangeColor : Colors.white,
                                    backgroundColor: courseMap.isDownloaded ? Colors.white : kOrangeColor,
                                  ),
                                  if (courseMap.isLocked) Icon(Icons.lock, color: courseMap.isDownloaded ? Colors.white : kRedColor),
                                  const SizedBox(width: 5),
                                  Expanded(
                                    child: Text(
                                      courseMap.name.toUpperCase(),
                                      style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color: courseMap.isDownloaded ? Colors.white : kOrangeColor),
                                    ),
                                  ),
                                  if (courseMap.isDownloaded)
                                    IconButton(
                                      icon: const Icon(
                                        Icons.close,
                                        semanticLabel: "Clear text",
                                        color: Colors.white,
                                      ),
                                      padding: const EdgeInsets.all(0),
                                      onPressed: () async {
                                        await showConfirmationDialog(
                                          context,
                                          title: "",
                                          message: L10n.getString("map_proxy_delete_downloaded_course_message"),
                                          yesButtonText: L10n.getString("map_proxy_delete_downloaded_course_yes"),
                                          noButtonText: L10n.getString("map_proxy_delete_downloaded_course_no"),
                                          yesButtonAction: () {
                                            widget.presenter.clearDownloadedMap(courseMap);
                                            updateProgress(0);
                                          },
                                          noButtonAction: null,
                                        );
                                      },
                                    ),
                                ],
                              ),
                              if (widget.presenter.isPlayfulCourse(courseMap))
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 13.0),
                                      child: Icon(Icons.quiz_outlined, color: courseMap.isDownloaded ? Colors.white : kGreenColor),
                                    ),
                                    Flexible(
                                      child: Text(
                                        L10n.getString("playful_content"),
                                        style: TextStyle(fontSize: 9.0, color: courseMap.isDownloaded ? Colors.white : kGreenColor),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              (courseMap.club != null && courseMap.club!.isNotEmpty)
                                  ? Container(
                                      width: double.infinity,
                                      margin: const EdgeInsets.fromLTRB(2, 4, 2, 0),
                                      padding: const EdgeInsets.all(2),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: (courseMap.isDownloaded) ? kOrangeColorDisabled : kOrangeColor,
                                          ),
                                          borderRadius: BorderRadius.circular(15)),
                                      child: Text(
                                        "${L10n.getString("course_creator")} ${courseMap.club}",
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(fontSize: 12.0, color: courseMap.isDownloaded ? kOrangeColorLighter : kOrangeColor),
                                      ),
                                    )
                                  : const SizedBox.shrink(),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                                child: Text(
                                  L10n.getString("course_length", L10n.formatNumber(courseMap.length, 2)),
                                  style: TextStyle(color: courseMap.isDownloaded ? kOrangeColorLighter : Colors.grey[600]),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.fromLTRB(10.0, 0, 0, 0),
                                child: Text(
                                  _formatDistance(courseMap.distance),
                                  style: TextStyle(color: courseMap.isDownloaded ? kOrangeColorLighter : Colors.grey[600]),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _TextIconButton(
                  textColor: Colors.black,
                  onPressed: () => _redirectToLocalDrivingApp(context, courseMap),
                  text: L10n.getString("map_proxy_button_text_drive"),
                  icon: Stack(
                    children: [
                      Container(
                        width: 37,
                        height: 37,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                      ),
                      Positioned(
                        left: -1,
                        top: -1,
                        child: Icon(
                          Icons.assistant_direction,
                          color: kBlueColor,
                          size: 37,
                          semanticLabel: "Localize course site ${courseMap.name}",
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void updateProgress(double value) {
    if (mounted) {
      setState(() => _progress = value);
    }
  }

  String _formatDistance(double distance) {
    if (distance > 1000) {
      final distanceInKm = distance ~/ 1000;
      return L10n.getString("map_distance_km", L10n.formatNumber(distanceInKm, 2));
    } else if (distance > 0) {
      final distanceInMeter = distance.toInt();
      return L10n.getString("map_distance_m", L10n.formatNumber(distanceInMeter, 2));
    } else {
      return "";
    }
  }

  Future _redirectToLocalDrivingApp(BuildContext context, CourseMap courseMap) async {
    await showConfirmationDialog(
      context,
      title: L10n.getString("map_proxy_navigator_dialog_title"),
      message: L10n.getString("map_proxy_navigator_dialog_message"),
      yesButtonText: L10n.getString("map_proxy_navigator_dialog_yes_button"),
      noButtonText: L10n.getString("map_proxy_navigator_dialog_cancel_button"),
      yesButtonAction: () => _openExternalNavigationApp(context, courseMap),
    );
  }

  Future _openExternalNavigationApp(BuildContext context, CourseMap courseMap) async {
    final double latitude = courseMap.latitude;
    final double longitude = courseMap.longitude;
    bool result = false;
    try {
      result = await launchUrl(MapChoicePresenter.createCoordinatesUri(latitude, longitude, courseMap.name));
    } catch (_) {}
    if (!result) {
      var urlAsString = MapChoicePresenter.getOpenStreetMapUrl(latitude, longitude);
      if (context.mounted) {
        openWebBrowserOnVikazimutPage(context, urlAsString);
      }
    }
  }
}

abstract class IProgressBarWidget {
  const IProgressBarWidget();

  void updateProgress(double value);
}

@immutable
class _CircleIcon extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final Color backgroundColor;

  const _CircleIcon({
    required this.icon,
    required this.iconColor,
    required this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 34,
      height: 34,
      decoration: BoxDecoration(
        color: backgroundColor,
        shape: BoxShape.circle,
      ),
      child: Icon(
        icon,
        color: iconColor,
      ),
    );
  }
}

@immutable
class _TextIconButton extends StatelessWidget {
  final String text;
  final Widget icon;
  final VoidCallback onPressed;
  final Color textColor;

  const _TextIconButton({
    required this.icon,
    required this.text,
    required this.onPressed,
    required this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: SizedBox(
        width: 65,
        // height: 50,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            icon,
            Flexible(
              child: Text(
                text,
                // maxLines: 1,
                softWrap: true,
                style: TextStyle(fontSize: 9, color: textColor),
                textAlign: TextAlign.center,
                // overflow: TextOverflow.clip,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
