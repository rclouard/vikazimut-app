// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut/l10n.dart';

@immutable
class SearchFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged<String> onValueChanged;

  const SearchFieldWidget({required this.controller, required this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    String hinText = L10n.getString("search_map_hint_message");
    return Semantics(
      label: hinText,
      child: TextFormField(
        controller: controller,
        autofocus: true,
        cursorColor: Colors.white,
        keyboardType: TextInputType.emailAddress,
        inputFormatters: [
          LengthLimitingTextInputFormatter(80),
          FilteringTextInputFormatter.allow(RegExp(r"[ \da-zA-Zá-úÁ-Ú_.@\-']")),
        ],
        decoration: InputDecoration(
          hintText: hinText,
          border: InputBorder.none,
          hintStyle: const TextStyle(
            color: Colors.white70,
          ),
        ),
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
        onChanged: (query) => onValueChanged(query),
      ),
    );
  }
}
