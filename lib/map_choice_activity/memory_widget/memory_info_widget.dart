// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';

import 'memory_info_presenter.dart';

@immutable
class MemoryInfoWidget extends StatelessWidget {
  static const _VALUE_FONT_STYLE = TextStyle(color: Colors.white, fontSize: 14);
  static const _UNIT_FONT_STYLE = TextStyle(color: Colors.white, fontSize: 10);

  final MemoryInfoPresenter presenter;

  const MemoryInfoWidget({required this.presenter});

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        style: _VALUE_FONT_STYLE,
        text: L10n.getString("memory_map_used", [presenter.memoryUsed]),
        children: <InlineSpan>[
          TextSpan(
            text: ' ${L10n.getString(presenter.unitMemoryUsed)}',
            style: _UNIT_FONT_STYLE,
          ),
          TextSpan(
            text: ' (${L10n.getString("memory_map_free", [presenter.memoryFree])}',
            style: _VALUE_FONT_STYLE,
          ),
          TextSpan(
            text: ' ${L10n.getString(presenter.unitMemoryFree)}',
            style: _UNIT_FONT_STYLE,
          ),
          const TextSpan(
            text: ")",
            style: _VALUE_FONT_STYLE,
          ),
        ],
      ),
      textAlign: TextAlign.center,
    );
  }
}
