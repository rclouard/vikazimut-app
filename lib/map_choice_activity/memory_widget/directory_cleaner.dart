import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as path;
import 'package:vikazimut/constants/storage_constants.dart';

import '../course_map.dart';

void removeUnlistedMaps(List<CourseMap> courseMaps) {
  List<String> mapIds = getMapIds_(courseMaps);
  _clean(Storage.mapDirectory, mapIds);
}

void _clean(String directoryPath, List<String> mapIds) {
  var directory = Directory(directoryPath);
  try {
    if (directory.existsSync()) {
      directory.listSync(recursive: true, followLinks: false).forEach((FileSystemEntity entity) {
        if (entity is File) {
          if (!isMapInMapList_(path.basenameWithoutExtension(entity.path), mapIds)) {
            entity.delete();
          }
        }
      });
    }
  } catch (_) {}
}

@visibleForTesting
List<String> getMapIds_(List<CourseMap> courseMap) {
  return courseMap.map((mapInfo) => '${mapInfo.id}').toList();
}

@visibleForTesting
bool isMapInMapList_(String filename, List<String> ids) {
  return ids.contains(filename);
}
