import 'dart:io';

import 'package:disk_space_2/disk_space_2.dart';
import 'package:flutter/foundation.dart';
import 'package:vikazimut/constants/storage_constants.dart';

class MemoryInfoPresenter extends ChangeNotifier {
  static const double BYTE_TO_MEGA = 1024.0 * 1024.0;
  double _memoryUsed = 0;
  String _unitMemoryUsed = "megabyte";
  double _memoryFree = 0;
  String _unitMemoryFree = "megabyte";

  double get memoryUsed => _memoryUsed;

  String get unitMemoryUsed => _unitMemoryUsed;

  double get memoryFree => _memoryFree;

  String get unitMemoryFree => _unitMemoryFree;

  Future<void> updateMemorySize() async {
    final memoryUsed = _getMemorySizeInMb();
    final memoryUsedFormat = formatSize_(memoryUsed);
    _memoryUsed = memoryUsedFormat[0];
    _unitMemoryUsed = memoryUsedFormat[1];
    final freeMemory = await DiskSpace.getFreeDiskSpace ?? 0;
    final memoryFreeFormat = formatSize_(freeMemory);
    _memoryFree = memoryFreeFormat[0];
    _unitMemoryFree = memoryFreeFormat[1];
    notifyListeners();
  }

  double _getMemorySizeInMb() {
    int memorySizeInB = _getFolderSize(Storage.mapDirectory);
    return memorySizeInB / BYTE_TO_MEGA;
  }

  int _getFolderSize(String directoryPath) {
    int totalSize = 0;
    var directory = Directory(directoryPath);
    try {
      if (directory.existsSync()) {
        directory.listSync(recursive: true, followLinks: false).forEach((FileSystemEntity entity) {
          if (entity is File) {
            totalSize += entity.lengthSync();
          }
        });
      }
    } catch (_) {}
    return totalSize;
  }

  @visibleForTesting
  List formatSize_(double sizeInMb) {
    if (sizeInMb > 1024.0) {
      return [sizeInMb / 1024.0, "gigabyte"];
    } else {
      return [sizeInMb, "megabyte"];
    }
  }
}
