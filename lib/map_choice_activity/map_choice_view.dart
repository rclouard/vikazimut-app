// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/help_activity/manual_view.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/bottom_app_bar_item.dart';
import 'package:vikazimut/utils/web_navigator.dart';

import 'map_choice_presenter.dart';
import 'map_list_widget.dart';
import 'memory_widget/memory_info_presenter.dart';
import 'memory_widget/memory_info_widget.dart';
import 'search_widget/search_field_widget.dart';
import 'title_widget/title_presenter.dart';
import 'title_widget/title_widget.dart';

@immutable
class MapChoiceView extends StatefulWidget {
  final CourseMode _courseMode;
  final bool _isPlannerMode;

  const MapChoiceView(this._courseMode, {bool isPlannerMode = false}) : _isPlannerMode = isPlannerMode;

  @override
  MapChoiceViewState createState() => MapChoiceViewState();
}

class MapChoiceViewState extends State<MapChoiceView> {
  late final TextEditingController _searchQueryController;
  late final MapChoicePresenter _presenter;
  bool _isSearching = false;
  String _searchQuery = "";

  @override
  void initState() {
    _searchQueryController = TextEditingController();
    _presenter = MapChoicePresenter(widget._courseMode, widget._isPlannerMode);
    super.initState();
  }

  @override
  void dispose() {
    _searchQueryController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<TitlePresenter>.value(value: _presenter.titlePresenter),
        ChangeNotifierProvider<MemoryInfoPresenter>.value(value: _presenter.memoryInfoPresenter),
      ],
      child: Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          title: Consumer<TitlePresenter>(builder: (context, presenter, child) {
            if (_isSearching) {
              return SearchFieldWidget(controller: _searchQueryController, onValueChanged: _updateSearchQuery);
            } else {
              return TitleWidget(numberOfMaps: presenter.numberOfMaps);
            }
          }),
          actions: _buildSearchActionIcons(),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(14.0),
            child: Consumer<MemoryInfoPresenter>(builder: (context, presenter, child) {
              return MemoryInfoWidget(presenter: presenter);
            }),
          ),
        ),
        body: MapListWidget(
          searchQuery: _searchQuery,
          presenter: _presenter,
        ),
        bottomNavigationBar: BottomAppBar(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Theme(
            data: Theme.of(context).copyWith(iconTheme: const IconThemeData(size: 32, color: Colors.white)),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                BottomAppBarItem(
                  icon: const Icon(
                    Icons.help,
                    size: 34,
                  ),
                  text: L10n.getString("display_help_menu_item"),
                  onPressed: (() => Navigator.push(context, MaterialPageRoute(builder: (context) => ManualView()))),
                ),
                BottomAppBarItem(
                  icon: const ImageIcon(
                    AssetImage('assets/icons/icon_menu_web.png'),
                    size: 32,
                  ),
                  text: L10n.getString("mode_choice_menu_course_map"),
                  onPressed: () => openWebBrowserWithConfirmation(context, '${ServerConstants.serverBaseUrl}/worldmap'),
                ),
                BottomAppBarItem(
                  icon: const Icon(
                    Icons.delete,
                  ),
                  text: L10n.getString("map_choice_remove_all"),
                  onPressed: () async {
                    await showConfirmationDialog(
                      context,
                      title: "",
                      message: L10n.getString("map_choice_delete_downloaded_course_message"),
                      yesButtonText: L10n.getString("map_choice_delete_downloaded_course_yes"),
                      noButtonText: L10n.getString("map_choice_delete_downloaded_course_no"),
                      yesButtonAction: () {
                        setState(() => _presenter.clearAllDownloadedMaps());
                      },
                      noButtonAction: null,
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildSearchActionIcons() {
    if (_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(
            Icons.clear,
            semanticLabel: "Clear text",
          ),
          padding: const EdgeInsets.all(0),
          onPressed: () {
            if (_searchQueryController.text.isEmpty) {
              Navigator.pop(context);
              return;
            }
            _clearSearchQuery();
          },
        ),
      ];
    } else {
      return <Widget>[
        IconButton(
          icon: const Icon(
            Icons.search,
            semanticLabel: "Search for a course",
          ),
          onPressed: _startSearch,
        ),
      ];
    }
  }

  void _startSearch() {
    ModalRoute.of(context)!.addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));
    setState(() => _isSearching = true);
  }

  void _updateSearchQuery(String newQuery) {
    setState(() => _searchQuery = newQuery);
  }

  void _stopSearching() {
    _clearSearchQuery();
    setState(() => _isSearching = false);
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      _updateSearchQuery("");
    });
  }
}
