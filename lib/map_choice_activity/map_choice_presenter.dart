import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/abstract_course_view.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/orienteering_map/orienteering_map_factory.dart';
import 'package:vikazimut/server_gateway/input_gateway.dart';

import 'course_map.dart';
import 'course_map_list.dart';
import 'course_map_widget.dart';
import 'memory_widget/memory_info_presenter.dart';
import 'secret_code_dialog.dart';
import 'title_widget/title_presenter.dart';

class MapChoicePresenter {
  final TitlePresenter _titlePresenter = TitlePresenter();
  final MemoryInfoPresenter _memoryInfoPresenter = MemoryInfoPresenter();
  late final CourseMapList _courseMapList;
  final bool _isPlannerMode;
  List<CourseMap> _filteredCourseMaps = [];
  bool _isDownloading = false;

  MapChoicePresenter(
    CourseMode courseMode, [
    this._isPlannerMode = false,
  ]) : _courseMapList = CourseMapList(courseMode);

  List<CourseMap> get filteredCourseMaps => _filteredCourseMaps;

  set courseMaps(List<CourseMap> mapProxies) => _courseMapList.courseMaps = mapProxies;

  bool isPlayfulCourse(CourseMap courseMap) => _courseMapList.isPlayful(courseMap);

  TitlePresenter get titlePresenter => _titlePresenter;

  MemoryInfoPresenter get memoryInfoPresenter => _memoryInfoPresenter;

  bool get isDownloading => _isDownloading;

  bool get isLocalMapList => _courseMapList.isLocalMapList;

  static String getOpenStreetMapUrl(double latitude, double longitude) => "http://www.openstreetmap.org/?mlat=$latitude&mlon=$longitude";

  Future<List<CourseMap>> fetchCourseMapList() async => _courseMapList.fetchCourseMapList(_isPlannerMode);

  Future<void> startDownloading(
    BuildContext context,
    CourseMap courseMap,
    IProgressBarWidget progressWidget, {
    VoidCallback? onSuccess,
    void Function({String? key})? onFailure,
  }) async {
    ScaffoldMessengerState scaffoldMessengerState = ScaffoldMessenger.of(context);
    // Case of a course map locked by secret code
    String? secretKey;
    if (courseMap.isLocked) {
      secretKey = await SecretCodeDialog.showAndExecActionAtTheEnd(context, courseMap);
      if (secretKey == null) {
        return;
      }
      onFailure?.call(key: secretKey);
    }
    final int id = courseMap.id;

    progressWidget.updateProgress(0);
    courseMap.isDownloading = true;
    _isDownloading = true;

    // 1. Read xml file
    http.Response responseXml = await InputGateway.getXmlFile(id, secretKey);
    if (responseXml.statusCode != 200) {
      String errorMessage;
      if (responseXml.statusCode == 400 || responseXml.statusCode == 401) {
        if (courseMap.isLocked) {
          errorMessage = "map_downloading_error_message3";
        } else {
          errorMessage = "map_downloading_error_message2";
        }
      } else {
        errorMessage = "map_downloading_error_message1";
      }
      stopDownloading_(courseMap);
      _isDownloading = false;
      final snackBar = SnackBar(
        content: Text(
          L10n.getString(errorMessage),
          textAlign: TextAlign.center,
        ),
        duration: const Duration(seconds: 4),
      );
      scaffoldMessengerState.showSnackBar(snackBar);
      onFailure?.call();
      return;
    }

    progressWidget.updateProgress(0.06);
    if (!courseMap.isDownloading) {
      _isDownloading = false;
      onFailure?.call();
      return;
    }

    // 2 read kml file
    http.Response responseKml = await InputGateway.getKmlFile(id, secretKey);
    if (responseKml.statusCode != 200) {
      stopDownloading_(courseMap);
      _isDownloading = false;
      final snackBar = SnackBar(
        content: Text(
          L10n.getString("map_downloading_error_message1"),
          textAlign: TextAlign.center,
        ),
        duration: const Duration(seconds: 4),
      );
      scaffoldMessengerState.showSnackBar(snackBar);
      onFailure?.call();
      return;
    }

    progressWidget.updateProgress(0.1);
    if (!courseMap.isDownloading) {
      _isDownloading = false;
      onFailure?.call();
      return;
    }

    // 3. Read img file
    final http.StreamedResponse responseImg = await InputGateway.getImgFile(id, secretKey);
    var responseContentLength = responseImg.contentLength;
    var headerContentLength = responseImg.headers['x-decompressed-content-length'];
    final int totalDownloadSize = computeFileSize_(responseContentLength, headerContentLength);
    List<int> bytes = [];
    late StreamSubscription<List<int>> listen;
    var timer = Stopwatch()..start();
    var timeTick = 250;
    listen = responseImg.stream.listen(
      (List<int> newBytes) {
        bytes.addAll(newBytes);
        final downloadedLength = bytes.length;
        var time = timer.elapsedMilliseconds;
        if (time >= timeTick) {
          if (totalDownloadSize == 0) {
            progressWidget.updateProgress(0.60); // Arbitrary!
          } else {
            progressWidget.updateProgress((downloadedLength * 0.9) / totalDownloadSize + 0.1);
          }
          timeTick += 250;
        }
        if (!courseMap.isDownloading) {
          listen.cancel();
        }
      },
      onDone: () async {
        _isDownloading = false;
        await _courseMapList.saveData(id, responseXml.body, responseKml.body, bytes, courseMap);
        updateAfterMapDownloadSuccess_(courseMap);
        progressWidget.updateProgress(0);
        if (onSuccess != null) {
          onSuccess();
        } else {
          _memoryInfoPresenter.updateMemorySize();
        }
      },
      onError: (e) {
        onFailure?.call();
        _isDownloading = false;
        stopDownloading_(courseMap);
        final snackBar = SnackBar(
          content: Text(
            L10n.getString("error_download_map"),
            textAlign: TextAlign.center,
          ),
          duration: const Duration(seconds: 4),
        );
        scaffoldMessengerState.showSnackBar(snackBar);
      },
      cancelOnError: true,
    );
  }

  @visibleForTesting
  static int computeFileSize_(int? responseContentLength, String? headerContentLength) {
    if (responseContentLength != null) {
      return responseContentLength;
    } else {
      String? headerField = headerContentLength;
      if (headerField != null) {
        return int.tryParse(headerField) ?? 0;
      }
    }
    return 0;
  }

  void clearDownloadedMap(CourseMap courseMap) => clearDownloadedMap_(courseMap, _memoryInfoPresenter);

  @visibleForTesting
  void clearDownloadedMap_(CourseMap courseMap, MemoryInfoPresenter memoryInfoPresenter) {
    courseMap.isDownloaded = false;
    CourseMapList.deleteSelectedMap(courseMap);
    memoryInfoPresenter.updateMemorySize();
  }

  void clearAllDownloadedMaps() {
    _courseMapList.clearAllDownloadedMaps();
    _memoryInfoPresenter.updateMemorySize();
  }

  void onPressedCourseMapButton(BuildContext context, CourseMap courseMap, MapWidgetState view) {
    if (courseMap.isDownloading) {
      stopDownloading_(courseMap);
      view.updateProgress(0);
    } else if (courseMap.isDownloaded) {
      _launchCourse(courseMap, context);
    } else if (!isDownloading) {
      startDownloading(context, courseMap, view);
    }
  }

  @visibleForTesting
  void stopDownloading_(CourseMap courseMap) {
    courseMap.isDownloaded = false;
    courseMap.isDownloading = false;
    CourseMapList.deleteSelectedMap(courseMap);
    _isDownloading = false;
  }

  List<CourseMap> filterCourseMapListFromQuery(String query) {
    _filteredCourseMaps = _courseMapList.filterMapListFromQuery(query);
    return _filteredCourseMaps;
  }

  void _launchCourse(CourseMap courseMap, BuildContext context) async {
    try {
      final courseView = _courseMapList.loadAndCreateCourse(courseMap);
      _openCourseView(context, courseView);
    } catch (_) {
      // Nothing to do
    }
  }

  void openCourseViewWithoutOpeningSettingDialog(AbstractCourseView view) {
    Navigator.of(globalKeyContext.currentContext!).push(MaterialPageRoute(builder: (context) => view));
  }

  OrienteeringMap loadCourse(int mapId, String mapName) => _courseMapList.loadCourse(mapId, mapName);

  AbstractCourseView createCourseFromQrCode(OrienteeringMap map) => _courseMapList.createCourseFromQrCode(map);

  bool loadCourseFromDatabase(CourseResultEntity courseResultEntity) {
    final map = OrienteeringMapFactory.createOrienteeringMapFromXmlAndKmlAndImgFiles(courseResultEntity.courseId, courseResultEntity.mapName);
    if (CourseMapList.isAlreadyDownloaded(map.id)) {
      final course = _courseMapList.restoreCourse(map, courseResultEntity);
      openCourseViewWithoutOpeningSettingDialog(course);
      return true;
    } else {
      return false;
    }
  }

  @visibleForTesting
  void updateAfterMapDownloadSuccess_(CourseMap courseMap) {
    courseMap.isDownloaded = true;
    courseMap.isDownloading = false;
  }

  void _openCourseView(BuildContext context, AbstractCourseView view) {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => view));
  }

  static List<CourseMap> filterPlayfulCourse(List<CourseMap> courseMaps) {
    return courseMaps.where((CourseMap courseMap) => courseMap.isTouristic).toList();
  }

  void updateCourseMap(List<CourseMap> list) {
    if (_courseMapList.courseMode == CourseMode.PLAYFUL) {
      courseMaps = MapChoicePresenter.filterPlayfulCourse(list);
    } else {
      courseMaps = list;
    }
  }

  static Uri createCoordinatesUri(double latitude, double longitude, String label) {
    Uri uri;
    if (Platform.isAndroid) {
      var query = '$latitude,$longitude';
      uri = Uri.parse("geo:0,0?q=$query");
    } else if (Platform.isIOS) {
      var params = {
        'll': '$latitude,$longitude',
        'q': label,
      };
      uri = Uri.https('maps.apple.com', '/', params);
    } else {
      uri = Uri.https(MapChoicePresenter.getOpenStreetMapUrl(latitude, longitude));
    }
    return uri;
  }
}
