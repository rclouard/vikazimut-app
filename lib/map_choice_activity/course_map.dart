import 'package:vikazimut/constants/global_constants.dart';

import 'search_widget/diacritic.dart' as diacritic;

class CourseMap {
  final int id;
  final String name;
  late final String strippedName;
  final String? club;
  late final String? strippedClub;
  final String? clubUrl;
  final double latitude;
  final double longitude;
  final int length;
  final bool isTouristic;
  final bool isLocked;
  final Discipline discipline;

  double distance = 0;
  bool isDownloading = false;
  bool isDownloaded = false;

  CourseMap({
    required this.id,
    required this.name,
    required this.latitude,
    required this.longitude,
    required this.length,
    this.discipline = Discipline.URBANO,
    this.club,
    this.clubUrl,
    this.isTouristic = false,
    this.isLocked = false,
  }) {
    strippedName = diacritic.removeDiacritics(name.trim().toLowerCase());
    strippedClub = club == null ? null : diacritic.removeDiacritics(club!.trim().toLowerCase());
  }

  factory CourseMap.fromJson(Map<String, dynamic> json) {
    var touristic = json["touristic"] as int?;
    var key = json["key"] as int?;
    return CourseMap(
      id: json["id"] as int,
      name: json["name"] as String,
      latitude: double.parse(json["latitude"] as String),
      longitude: double.parse(json["longitude"] as String),
      length: json["length"] as int,
      club: json["club"] as String?,
      clubUrl: json["cluburl"] as String?,
      discipline: Discipline.fromInt(json["discipline"] as int?),
      isTouristic: touristic != null,
      isLocked: key != null,
    );
  }

  static CourseMap fromMapName({required int id, required String name}) {
    return CourseMap(id: id, name: name, latitude: 0, longitude: 0, length: 0);
  }
}
