// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';

@immutable
class TitleWidget extends StatelessWidget {
  final int numberOfMaps;

  const TitleWidget({required this.numberOfMaps});

  @override
  Widget build(BuildContext context) {
    return Text(
      numberOfMaps == 0 ? "" : L10n.getString("map_choice_title", [numberOfMaps]),
    );
  }
}
