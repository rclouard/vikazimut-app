import 'package:flutter/material.dart';

class TitlePresenter extends ChangeNotifier {
  int _numberOfMaps = 0;

  int get numberOfMaps => _numberOfMaps;

  set numberOfMaps(int value) {
    _numberOfMaps = value;
    notifyListeners();
  }
}
