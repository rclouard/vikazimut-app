import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/storage_constants.dart';
import 'package:vikazimut/course_activity/abstract_course_view.dart';
import 'package:vikazimut/course_activity/course_view_factory.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/orienteering_map/orienteering_map_factory.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/server_gateway/input_gateway.dart';

import 'course_map.dart';
import 'course_map_filter.dart';

class CourseMapList {
  final CourseMode courseMode;
  List<CourseMap>? courseMaps;
  bool _isLocalMapList = true;

  CourseMapList(this.courseMode);

  get isLocalMapList => _isLocalMapList;

  Future<List<CourseMap>> fetchCourseMapList(bool withPrivateCourses) async {
    var currentPositionFuture = _determinePosition();
    try {
      http.Response response = await InputGateway.getCourseMapList(withPrivateCourses);
      if (response.statusCode == HttpStatus.ok) {
        _isLocalMapList = false;
        _saveMapList(response.body);
        return await _createMapListFromJson(response.body, currentPositionFuture);
      }
    } catch (_) {}

    _isLocalMapList = true;
    final String jsonMapList = await _readMapList();
    return await _createMapListFromJson(jsonMapList, currentPositionFuture);
  }

  Future<List<CourseMap>> _createMapListFromJson(String jsonMapList, Future<Position?> currentPositionFuture) async {
    var mapProxies = parseJsonToExtractMaps_(jsonMapList);
    _markMapsIfAlreadyDownloaded(mapProxies);
    var currentPosition = await currentPositionFuture;
    if (currentPosition != null) {
      sortMapsFromDistanceCurrentPosition_(currentPosition, mapProxies);
    }
    return mapProxies;
  }

  @visibleForTesting
  static void sortMapsFromDistanceCurrentPosition_(Position position, List<CourseMap> courseMapList) {
    for (final mapInfo in courseMapList) {
      double distanceInMeter = GeodesicPoint.distanceBetweenInMeter(position.latitude, position.longitude, mapInfo.latitude, mapInfo.longitude);
      mapInfo.distance = distanceInMeter;
    }
    courseMapList.sort((a, b) => a.distance.compareTo(b.distance));
  }

  static void _saveMapList(String body) async {
    final String filename = await getMapListFilePath_();
    saveMapList_(body, filename);
  }

  @visibleForTesting
  static void saveMapList_(String body, String filename) {
    File file = File(filename);
    file.writeAsStringSync(body);
  }

  static Future<String> _readMapList() async {
    String filename = await getMapListFilePath_();
    return readMapList_(filename);
  }

  @visibleForTesting
  static String readMapList_(String filename) {
    File file = File(filename);
    if (file.existsSync()) {
      return file.readAsStringSync();
    } else {
      return "";
    }
  }

  @visibleForTesting
  static Future<String> getMapListFilePath_() async {
    Directory applicationDocumentDir = await getApplicationDocumentsDirectory();
    String applicationDocumentPath = applicationDocumentDir.path;
    return '$applicationDocumentPath/${Storage.mapListFilename}';
  }

  @visibleForTesting
  static List<CourseMap> parseJsonToExtractMaps_(String responseBody) {
    if (responseBody.isEmpty) {
      return [];
    }
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    List<CourseMap> courseMapList = [];
    for (var json in parsed) {
      try {
        var courseMap = CourseMap.fromJson(json);
        courseMapList.add(courseMap);
      } catch (_) {}
    }
    return courseMapList;
  }

  static bool isAlreadyDownloaded(int id) {
    File xml = File(Storage.getXmlFileName(id));
    File kml = File(Storage.getKmlFileName(id));
    File img = File(Storage.getImgFileName(id));
    File txt = File(Storage.getTxtFileName(id));
    return xml.existsSync() && img.existsSync() && kml.existsSync() && txt.existsSync();
  }

  void _markMapsIfAlreadyDownloaded(List<CourseMap> courseMapList) {
    for (final CourseMap courseMap in courseMapList) {
      courseMap.isDownloaded = isAlreadyDownloaded(courseMap.id);
    }
  }

  Future<Position?> _determinePosition() async {
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return null;
    }
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return null;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return null;
    }
    try {
      return await Geolocator.getCurrentPosition(
        locationSettings: const LocationSettings(
          accuracy : LocationAccuracy.low,
          timeLimit : Duration(seconds: 7),
        ),
      );
    } catch (_) {
      return await Geolocator.getLastKnownPosition();
    }
  }

  static void deleteSelectedMap(CourseMap courseMap) {
    int id = courseMap.id;
    File xmlFile = File(Storage.getXmlFileName(id));
    if (xmlFile.existsSync()) xmlFile.delete();
    File kmlFile = File(Storage.getKmlFileName(id));
    if (kmlFile.existsSync()) kmlFile.delete();
    File imgFile = File(Storage.getImgFileName(id));
    if (imgFile.existsSync()) imgFile.delete();
    File txtFile = File(Storage.getTxtFileName(id));
    if (txtFile.existsSync()) txtFile.delete();
  }

  Future<void> _saveXmlData(String filename, String body) async {
    File file = File(filename);
    await file.writeAsString(body);
  }

  Future<void> _saveImgData(String filename, List<int> body) async {
    File file = File(filename);
    await file.writeAsBytes(body);
  }

  Future<void> _saveTxtData(String filename, CourseMap courseMap) async {
    Map<String, dynamic> json = {
      "club": courseMap.club,
      "clubUrl": courseMap.clubUrl,
    };
    File file = File(filename);
    await file.writeAsString(jsonEncode(json));
  }

  void _clearDownloadedMap(CourseMap courseMap) {
    courseMap.isDownloaded = false;
    deleteSelectedMap(courseMap);
  }

  List<CourseMap> filterMapListFromQuery(String query) => CourseMapFilter.filterMapListFromQuery(courseMaps!, query);

  void clearAllDownloadedMaps() {
    if (courseMaps != null) {
      for (final courseMap in courseMaps!) {
        _clearDownloadedMap(courseMap);
      }
    }
  }

  OrienteeringMap loadCourse(int mapId, String mapName) {
    return OrienteeringMapFactory.createOrienteeringMapFromXmlAndKmlAndImgFiles(mapId, mapName);
  }

  AbstractCourseView createCourseFromQrCode(OrienteeringMap map) {
    return CourseViewFactory.createCourse(
      map.courseMode??CourseMode.SPORT,
      map,
      preconfigured: true,
    );
  }

  AbstractCourseView restoreCourse(OrienteeringMap map, CourseResultEntity courseResultEntity) {
    return CourseViewFactory.createCourse(
      courseMode,
      map,
      preconfigured: true,
      courseResultEntity: courseResultEntity,
    );
  }

  AbstractCourseView loadAndCreateCourse(CourseMap courseMap) {
    final orienteeringMap = OrienteeringMapFactory.createOrienteeringMapFromXmlAndKmlAndImgFiles(courseMap.id, courseMap.name);
    return CourseViewFactory.createCourse(
      courseMode,
      orienteeringMap,
      preconfigured: false,
    );
  }

  bool isPlayful(CourseMap courseMap) => (courseMode == CourseMode.WALK || courseMode == CourseMode.PLAYFUL) && courseMap.isTouristic;

  Future<void> saveData(int id, String xmlBody, String kmlBody, List<int> imageBytes, CourseMap courseMap) async {
    await _saveXmlData(Storage.getXmlFileName(id), xmlBody);
    await _saveXmlData(Storage.getKmlFileName(id), kmlBody);
    await _saveImgData(Storage.getImgFileName(id), imageBytes);
    await _saveTxtData(Storage.getTxtFileName(id), courseMap);
  }
}
