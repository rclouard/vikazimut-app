import 'package:flutter/material.dart';

import 'course_map.dart';
import 'search_widget/diacritic.dart' as diacritic;

class CourseMapFilter {
  static List<CourseMap> filterMapListFromQuery(List<CourseMap> courseMaps, String query) => filterMapListFromQuery_(courseMaps, query);

  @visibleForTesting
  static List<CourseMap> filterMapListFromQuery_(List<CourseMap> courseMaps, String query) {
    if (query.isEmpty) {
      return courseMaps;
    }
    if (query.startsWith("@")) {
      var creator = diacritic.removeDiacritics(query.substring(1).toLowerCase());
      return courseMaps.where((element) => compareCreators_(element, creator)).toList();
    } else {
      var mapName = diacritic.removeDiacritics(query.toLowerCase());
      return courseMaps.where((element) => compareMapNames_(element, mapName)).toList();
    }
  }

  @visibleForTesting
  static bool compareCreators_(CourseMap map, String query) {
    if (map.strippedClub == null || map.strippedClub!.isEmpty) {
      return false;
    } else {
      return map.strippedClub!.contains(query);
    }
  }

  @visibleForTesting
  static bool compareMapNames_(CourseMap map, String mapName) {
    return map.strippedName.contains(mapName);
  }
}
