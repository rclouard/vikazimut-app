// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/toast.dart';

import 'course_map.dart';
import 'course_map_widget.dart';
import 'map_choice_presenter.dart';
import 'memory_widget/directory_cleaner.dart';
import 'memory_widget/memory_info_presenter.dart';
import 'title_widget/title_presenter.dart';

@immutable
class MapListWidget extends StatefulWidget {
  final String searchQuery;
  final MapChoicePresenter presenter;

  const MapListWidget({
    required this.presenter,
    required this.searchQuery,
  });

  @override
  MapListWidgetState createState() => MapListWidgetState();
}

class MapListWidgetState extends State<MapListWidget> {
  late final Future<List<CourseMap>> _courseMaps;

  @override
  void initState() {
    _courseMaps = widget.presenter.fetchCourseMapList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<CourseMap>>(
      future: _courseMaps,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return _ErrorWidget();
        } else if (snapshot.hasData) {
          removeUnlistedMaps(snapshot.data!);
          WidgetsBinding.instance.addPostFrameCallback((_) {
            final memoryInfoPresenter = Provider.of<MemoryInfoPresenter>(context, listen: false);
            memoryInfoPresenter.updateMemorySize();
            var titlePresenter = Provider.of<TitlePresenter>(context, listen: false);
            titlePresenter.numberOfMaps = snapshot.data!.length;
            if (snapshot.data!.isEmpty) {
              showErrorDialog(
                context,
                title: L10n.getString("error_title"),
                message: L10n.getString("no_network_and_no_stored_map_error_message"),
              );
            } else if (widget.presenter.isLocalMapList) {
              Toast.makeText(
                context,
                L10n.getString("server_connection_error_use_memorized_map"),
                Toast.LENGTH_LONG,
                color: Colors.black,
              ).show();
            }
          });
          widget.presenter.updateCourseMap(snapshot.data!);
          List<CourseMap> filteredCourseMaps = widget.presenter.filterCourseMapListFromQuery(widget.searchQuery);
          return ListView.builder(
            itemCount: filteredCourseMaps.length,
            itemBuilder: (context, index) {
              return MapWidget(index, widget.presenter);
            },
          );
        } else {
          return _WaitingWidget();
        }
      },
    );
  }
}

@immutable
class _ErrorWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            L10n.getString("error_title"),
            style: const TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          Text(
            L10n.getString("no_network_and_no_stored_map_error_message"),
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 16.0),
          ),
        ],
      ),
    );
  }
}

@immutable
class _WaitingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(
          left: 8.0,
          right: 8.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              L10n.getString("map_list_downloading_message"),
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.black),
            ),
            const SizedBox(height: 10),
            const CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
