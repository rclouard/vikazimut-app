// coverage:ignore-file
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

import 'course_map.dart';

class SecretCodeDialog {
  late final AlertDialog _dialog;
  final TextEditingController _controller = TextEditingController();
  String? _secretKey;

  static Future<String?> showAndExecActionAtTheEnd(BuildContext context, CourseMap courseMap) async {
    final SecretCodeDialog view = SecretCodeDialog._(context);
    await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return view._dialog;
      },
    );
    return view._secretKey;
  }

  SecretCodeDialog._(BuildContext context) {
    bool isObscure = true;
    _dialog = QuestionDialog(
      title: L10n.getString("secret_key_dialog_title"),
      content: StatefulBuilder(builder: (localContext, setState) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            TextField(
              controller: _controller,
              autofocus: true,
              keyboardType: TextInputType.number,
              obscureText: isObscure,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(8),
              ],
              decoration: InputDecoration(
                icon: const Icon(Icons.lock, color: kGreenColor),
                labelText: L10n.getString("secret_key_dialog_label"),
                suffixIcon: IconButton(
                  icon: Icon(isObscure ? Icons.visibility : Icons.visibility_off, color: kGreenColor),
                  onPressed: () {
                    setState(() {
                      isObscure = !isObscure;
                    });
                  },
                ),
                labelStyle: const TextStyle(color: kGreenColor),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: const BorderSide(
                    color: kGreenColor,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: const BorderSide(
                    color: kGreenColor,
                  ),
                ),
                fillColor: kGreenColor.withValues(alpha: 0.1),
                filled: true,
              ),
            ),
          ],
        );
      }),
      actions: [
        TextButton(
          child: Text(L10n.getString("secret_key_dialog_button"), style: const TextStyle(color: kGreenColor)),
          onPressed: () async {
            _secretKey = _controller.text;
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
