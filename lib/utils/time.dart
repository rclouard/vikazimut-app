import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:vikazimut/l10n.dart';

String getLocalizedDate(int dateInMillisecondSinceEpoch) => getLocalizedDate_(dateInMillisecondSinceEpoch, L10n.locale.toString());

@visibleForTesting
String getLocalizedDate_(int dateInMillisecondSinceEpoch, String locale) {
  var dateTime = DateTime.fromMillisecondsSinceEpoch(dateInMillisecondSinceEpoch);
  final DateFormat formatter = DateFormat.yMMMd(locale);
  return formatter.format(dateTime);
}

String formatTimeAsString(int milliseconds, {bool withHour = true}) {
  Duration duration = Duration(milliseconds: milliseconds);
  String start = (duration.inHours > 0)
      ? "${duration.inHours}:"
      : withHour
          ? "0:"
          : "";
  return "$start${(duration.inMinutes % 60).toString().padLeft(2, "0")}:${(duration.inSeconds % 60).toString().padLeft(2, "0")}";
}
