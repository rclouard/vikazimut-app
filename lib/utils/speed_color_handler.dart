import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

abstract class SpeedColorHandlerFactory {
  static SpeedColorHandler build(Discipline? discipline) {
    if (discipline == null || discipline.isFootOrienteering()) {
      return SpeedFootoColorHandler();
    } else if (discipline.isMTBOrienteering()){
      return SpeedMtboColorHandler();
    } else {
      return SpeedSkioColorHandler();
    }
  }
}

abstract class SpeedColorHandler {
  abstract List<int> speedThreshold;
  static const _speedColors = [
    Color(0xFFF44336),
    Color(0xFFFF9800),
    Color(0xFFFFEB3B),
    Color(0xFF2ECF81),
    Color(0xFF1A9056),
  ];

  Color colorFromColorIndex(int colorIndex) => _speedColors[colorIndex];

  Color colorFromVelocity(double velocityInKmH) => _speedColors[colorIndexFromVelocity(velocityInKmH)];

  int colorIndexFromVelocity(double velocityInKmH) {
    for (int i = 0; i < speedThreshold.length; i++) {
      if (velocityInKmH < speedThreshold[i]) {
        return i;
      }
    }
    return _speedColors.length - 1;
  }

  static List<double> filterSpeeds(List<GeodesicPoint> route) {
    return filterSpeeds_(route, 3);
  }

  @visibleForTesting
  static List<double> filterSpeeds_(List<GeodesicPoint> route, int filterHalfSize) {
    if (route.isEmpty) {
      return [];
    }
    List<double> speeds = [0];
    // Start of the track: no filtering
    for (int i = 1; i <= min(filterHalfSize + 1, route.length - 1); i++) {
      var distanceInMeter = GeodesicPoint.distanceBetweenInMeter(route[i - 1].latitude, route[i - 1].longitude, route[i].latitude, route[i].longitude);
      var timeInMilliSecond = route[i].timestampInMillisecond - route[i - 1].timestampInMillisecond;
      final velocityInKmH = computeVelocityInKmH_(distanceInMeter, timeInMilliSecond);
      speeds.add(velocityInKmH);
    }

    if (route.length >= 2 * filterHalfSize + 1) {
      // 1 Filter speeds using simple moving average
      double distanceInMeter = 0.0;
      for (int i = 1; i <= filterHalfSize * 2 + 1; i++) {
        distanceInMeter += GeodesicPoint.distanceBetweenInMeter(route[i - 1].latitude, route[i - 1].longitude, route[i].latitude, route[i].longitude);
      }
      for (int i = filterHalfSize + 2; i < route.length - filterHalfSize - 1; i++) {
        final last = i + filterHalfSize;
        final first = i - filterHalfSize - 1;
        var distance0 = GeodesicPoint.distanceBetweenInMeter(route[first - 1].latitude, route[first - 1].longitude, route[first].latitude, route[first].longitude);
        var distance1 = GeodesicPoint.distanceBetweenInMeter(route[last - 1].latitude, route[last - 1].longitude, route[last].latitude, route[last].longitude);
        distanceInMeter += -distance0 + distance1;
        int timeInMs = route[last].timestampInMillisecond - route[first].timestampInMillisecond;
        final velocityInKmH = computeVelocityInKmH_(distanceInMeter, timeInMs);
        speeds.add(velocityInKmH);
      }
    }

    if (route.length > filterHalfSize + 1) {
      // End of the track: no filtering
      for (int i = route.length - filterHalfSize - 1; i < route.length; i++) {
        var distanceInMeter = GeodesicPoint.distanceBetweenInMeter(route[i - 1].latitude, route[i - 1].longitude, route[i].latitude, route[i].longitude);
        var timeInMilliSecond = route[i].timestampInMillisecond - route[i - 1].timestampInMillisecond;
        final velocityInKmH = computeVelocityInKmH_(distanceInMeter, timeInMilliSecond);
        speeds.add(velocityInKmH);
      }
    }
    return speeds;
  }

  @visibleForTesting
  static double computeVelocityInKmH_(double distanceInMeter, int timeInMilliSecond) {
    const double MS_TO_KMH_CONVERSION = 3.6;
    if (timeInMilliSecond == 0) {
      return 0;
    } else {
      return MS_TO_KMH_CONVERSION * 1000 * distanceInMeter / timeInMilliSecond;
    }
  }
}

class SpeedFootoColorHandler extends SpeedColorHandler {
  @override
  List<int> speedThreshold = const [2, 6, 10, 15];
}

class SpeedMtboColorHandler extends SpeedColorHandler {
  @override
  List<int> speedThreshold = const [7, 14, 21, 28];
}

class SpeedSkioColorHandler extends SpeedColorHandler {
  @override
  List<int> speedThreshold = const [7, 14, 21, 28];
}
