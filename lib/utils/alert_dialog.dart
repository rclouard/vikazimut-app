// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/pattern_lock.dart';

/// Credit: https://pub.dev/packages/commons
Future<void> showErrorDialog(BuildContext context, {required title, required String message}) async {
  const Icon icon = Icon(Icons.close, size: 64, color: Colors.white);

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: kRedDarkColor,
    message: message,
    positiveButtonText: L10n.getString("ok"),
  );
}

Future<void> showConfirmationDialog(
  BuildContext context, {
  required String title,
  required String message,
  required String yesButtonText,
  required String noButtonText,
  VoidCallback? yesButtonAction,
  VoidCallback? noButtonAction,
}) async {
  const color = kOrangeColor;
  const icon = Icon(
    Icons.help_outline,
    size: 64,
    color: Colors.white,
    semanticLabel: "Question dialog",
  );

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: color,
    message: message,
    positiveButtonText: yesButtonText,
    positiveButtonAction: yesButtonAction,
    negativeButtonText: noButtonText,
    negativeButtonAction: noButtonAction,
  );
}

Future<void> showPatternConfirmationDialog(
  BuildContext context, {
  required String title,
  required String message,
  VoidCallback? yesButtonAction,
}) async {
  var color = Theme.of(context).colorScheme.secondary;
  const icon = Icon(
    Icons.help_outline,
    size: 64,
    color: Colors.white,
    semanticLabel: "Question dialog",
  );

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: color,
    message: message,
    positiveButtonText: "",
    positiveButtonAction: yesButtonAction,
    usePattern: true,
    negativeButtonText: L10n.getString("cancel"),
  );
}

Future<void> showWarningDialog(
  BuildContext context, {
  required String title,
  required String message,
  required String yesButtonText,
  VoidCallback? yesAction,
  String? checkboxMessage,
  VoidCallback? checkboxAction,
}) async {
  const color = Color(0xffFF8C00);
  const icon = Icon(
    Icons.warning,
    size: 64,
    color: Colors.white,
    semanticLabel: "Warning dialog",
  );

  await _buildAndShowDialog(
    context: context,
    dialogIcon: icon,
    title: title,
    color: color,
    message: message,
    positiveButtonText: yesButtonText,
    positiveButtonAction: yesAction,
    checkboxMessage: checkboxMessage,
    checkboxAction: checkboxAction,
  );
}

Future<void> _buildAndShowDialog({
  required BuildContext context,
  required Icon dialogIcon,
  required String title,
  required Color color,
  required String message,
  required String positiveButtonText,
  String? negativeButtonText,
  VoidCallback? positiveButtonAction,
  VoidCallback? negativeButtonAction,
  String? checkboxMessage,
  VoidCallback? checkboxAction,
  bool usePattern = false,
}) async {
  return await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return buildDialogContent(
          context,
          dialogIcon: dialogIcon,
          title: title,
          color: color,
          message: message,
          positiveButtonText: positiveButtonText,
          positiveButtonAction: positiveButtonAction,
          negativeButtonText: negativeButtonText,
          negativeButtonAction: negativeButtonAction,
          checkboxMessage: checkboxMessage,
          checkboxAction: checkboxAction,
          usePattern: usePattern,
        );
      });
}

Widget buildDialogContent(
  BuildContext context, {
  required Icon dialogIcon,
  required String title,
  required Color color,
  required String message,
  required String positiveButtonText,
  String? negativeButtonText,
  VoidCallback? positiveButtonAction,
  VoidCallback? negativeButtonAction,
  String? checkboxMessage,
  VoidCallback? checkboxAction,
  usePattern = false,
}) {
  var screenWidth = MediaQuery.of(context).size.width;
  List<bool> remember = [false];
  return Dialog(
    insetPadding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Stack(
      children: <Widget>[
        Container(
          width: screenWidth >= 600 ? 500 : screenWidth,
          padding: const EdgeInsets.only(
            top: 45.0 + 16.0,
            bottom: 16.0,
            left: 16.0,
            right: 16.0,
          ),
          margin: const EdgeInsets.only(top: 55.0),
          decoration: BoxDecoration(
            color: Theme.of(context).dialogTheme.backgroundColor,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (title.isNotEmpty)
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: color,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              const SizedBox(height: 16.0),
              Flexible(
                fit: FlexFit.loose,
                child: SingleChildScrollView(
                  child: Text(
                    message,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ),
              if (usePattern)
                PatternLock(
                  secretCode: const [0, 1, 2, 3],
                  onSuccess: () {
                    Navigator.of(context).pop();
                    if (positiveButtonAction != null) {
                      positiveButtonAction();
                    }
                  },
                ),
              const SizedBox(height: 16.0),
              if (checkboxMessage != null && checkboxMessage.isNotEmpty)
                CheckBoxContainer(
                  checkboxValue: remember,
                  checkboxMessage: checkboxMessage,
                  color: color,
                ),
              ClipRect(
                clipBehavior: Clip.hardEdge,
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      if (!usePattern)
                        Expanded(
                          child: TextButton(
                            child: Text(
                              positiveButtonText,
                              style: TextStyle(color: color, fontSize: 13),
                            ),
                            onPressed: () {
                              if (remember[0] && checkboxAction != null) {
                                checkboxAction();
                              }
                              Navigator.of(context).pop();
                              if (positiveButtonAction != null) {
                                positiveButtonAction();
                              }
                            },
                          ),
                        ),
                      const SizedBox(width: 5.0),
                      if (negativeButtonText != null)
                        Expanded(
                          child: TextButton(
                            child: Text(
                              negativeButtonText,
                              style: TextStyle(color: color, fontSize: 13),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                              if (negativeButtonAction != null) {
                                negativeButtonAction();
                              }
                            },
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 16.0,
          right: 16.0,
          child: CircleAvatar(
            backgroundColor: color,
            radius: 45.0,
            child: dialogIcon,
          ),
        ),
      ],
    ),
  );
}

@immutable
class CheckBoxContainer extends StatefulWidget {
  final List<bool> checkboxValue;
  final String checkboxMessage;
  final Color color;

  const CheckBoxContainer({
    super.key,
    required this.checkboxValue,
    required this.color,
    required this.checkboxMessage,
  });

  @override
  CheckBoxContainerState createState() => CheckBoxContainerState();
}

class CheckBoxContainerState extends State<CheckBoxContainer> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Theme(
          data: ThemeData(primarySwatch: Colors.yellow, unselectedWidgetColor: widget.color),
          child: Checkbox(
            semanticLabel: "Don't display this message again",
            value: widget.checkboxValue[0],
            checkColor: Colors.white,
            activeColor: widget.color,
            onChanged: (value) {
              setState(() {
                widget.checkboxValue[0] = value!;
              });
            },
          ),
        ),
        Text(widget.checkboxMessage),
      ],
    );
  }
}

class QuestionDialog extends AlertDialog {
  QuestionDialog({
    required String title,
    required Widget content,
    required List<Widget> actions,
    String? subtitle,
  }) : super(
          contentPadding: const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 10.0),
          semanticLabel: title,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          title: Container(
            alignment: Alignment.center,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 5),
            decoration: const BoxDecoration(
              color: kGreenColor,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            child: Column(
              children: [
                Text(
                  title,
                  style: const TextStyle(color: Colors.white, fontSize: 18),
                ),
                if (subtitle != null)
                  Text(
                    subtitle,
                    style: const TextStyle(color: Colors.white, fontSize: 15),
                  ),
              ],
            ),
          ),
          content: content,
          actionsPadding: const EdgeInsets.fromLTRB(0, 0, 25, 5),
          actions: actions,
        );
}
