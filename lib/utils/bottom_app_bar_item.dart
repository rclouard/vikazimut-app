// coverage:ignore-file
import 'package:flutter/material.dart';

@immutable
class BottomAppBarItem extends StatelessWidget {
  final String text;
  final Widget icon;
  final VoidCallback onPressed;

  const BottomAppBarItem({
    required this.icon,
    required this.text,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        constraints: const BoxConstraints(minWidth: 48, minHeight: 48),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            icon,
            Text(
              text,
              style: TextStyle(
                fontSize: 13,
                color: Theme.of(context).appBarTheme.toolbarTextStyle?.color,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
