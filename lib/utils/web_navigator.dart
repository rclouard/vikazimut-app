// coverage:ignore-file

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as web;
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

void openWebBrowserWithConfirmation(BuildContext context, String url) async {
  await showConfirmationDialog(
    context,
    title: L10n.getString("web_browser_dialog_title1"),
    message: L10n.getString("web_browser_dialog_message1"),
    yesButtonText: L10n.getString("web_browser_dialog_yes_button"),
    noButtonText: L10n.getString("no"),
    yesButtonAction: () => openWebBrowserOnVikazimutPage(context, url),
  );
}

void openWebBrowserOnVikazimutPage(BuildContext context, String url) async {
  final Uri? uri = Uri.tryParse(url);
  try {
    if (uri != null) {
      if (!await web.launchUrl(uri, mode: web.LaunchMode.externalApplication)) {
        throw Exception();
      }
    } else {
      throw Exception();
    }
  } catch (_) {
    if (context.mounted) {
      await showErrorDialog(
        context,
        title: L10n.getString("error_title"),
        message: L10n.getString("no_web_navigator"),
      );
    }
  }
}
