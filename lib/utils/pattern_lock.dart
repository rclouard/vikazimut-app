// coverage:ignore-file
import 'dart:math';

import 'package:flutter/foundation.dart';

/// Credit: https://medium.com/@baobao1996mn/pattern-lock-screen-with-flutter-2811e44ed826
import 'package:flutter/material.dart';

@immutable
class PatternLock extends StatefulWidget {
  final List<int> secretCode;
  final VoidCallback onSuccess;

  const PatternLock({
    required this.secretCode,
    required this.onSuccess,
  });

  @override
  PatternLockState createState() => PatternLockState();
}

class PatternLockState extends State<PatternLock> {
  Offset? _currentCursorPosition;
  List<int> codes = [];

  @override
  Widget build(BuildContext context) {
    final width = min(460.0, 2 * MediaQuery.of(context).size.width / 3);
    final sizePainter = Size.square(width);
    return Container(
      margin: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondary,
        borderRadius: BorderRadius.circular(12),
      ),
      child: GestureDetector(
        onPanStart: _onPanStart,
        onPanUpdate: _onPanUpdate,
        onPanEnd: _onPanEnd,
        child: CustomPaint(
          painter: _LockScreenPainter(
            codes: codes,
            currentPosition: _currentCursorPosition,
            onSelect: _onSelect,
          ),
          size: sizePainter,
        ),
      ),
    );
  }

  _onPanStart(DragStartDetails event) {
    return _clearCodes();
  }

  _onPanUpdate(DragUpdateDetails event) {
    setState(() => _currentCursorPosition = event.localPosition);
  }

  _onPanEnd(DragEndDetails event) {
    setState(() => _currentCursorPosition = null);
    if (_isExpectedCode()) {
      widget.onSuccess();
    } else {
      _clearCodes();
    }
  }

  bool _isExpectedCode() => listEquals(codes, widget.secretCode) || listEquals(codes, widget.secretCode.reversed.toList());

  _onSelect(int code) {
    if (codes.isEmpty || codes.last != code) {
      codes.add(code);
    }
  }

  _clearCodes() => setState(() {
        codes = [];
        _currentCursorPosition = null;
      });
}

class _LockScreenPainter extends CustomPainter {
  static const int _totalDots = 4;
  static const int _columns = 2;
  final List<int> codes;
  final Offset? currentPosition;
  final Function(int code) onSelect;
  Size? _size;

  _LockScreenPainter({
    required this.codes,
    required this.currentPosition,
    required this.onSelect,
  });

  double get _sizeCode => _size!.width / _columns;

  Paint get _painter => Paint()
    ..color = Colors.white
    ..strokeWidth = 2.0;

  @override
  void paint(Canvas canvas, Size size) {
    _size = size;
    for (var i = 0; i < _totalDots; i++) {
      var offset = _getOffsetByIndex(i);
      var color = _getColorByIndex(i);

      var radiusIn = _sizeCode / 2.0 * 0.1;
      _drawCircle(canvas, offset, radiusIn, color, true);

      var radiusOut = _sizeCode / 2.0 * 0.4;
      _drawCircle(canvas, offset, radiusOut, color);

      var pathGesture = _getCirclePath(offset, radiusOut);
      if (currentPosition != null && pathGesture.contains(currentPosition!)) onSelect(i);
    }

    for (var i = 0; i < codes.length; i++) {
      var start = _getOffsetByIndex(codes[i]);
      if (i + 1 < codes.length) {
        var end = _getOffsetByIndex(codes[i + 1]);
        _drawLine(canvas, start, end);
      } else if (currentPosition != null) {
        var end = currentPosition!;
        _drawLine(canvas, start, end);
      }
    }
  }

  Path _getCirclePath(Offset offset, double radius) {
    var rect = Rect.fromCircle(radius: radius, center: offset);
    return Path()..addOval(rect);
  }

  void _drawCircle(Canvas canvas, Offset offset, double radius, Color color, [bool isDot = false]) {
    var path = _getCirclePath(offset, radius);
    var painter = _painter
      ..color = color
      ..style = isDot ? PaintingStyle.fill : PaintingStyle.stroke;
    canvas.drawPath(path, painter);
  }

  void _drawLine(Canvas canvas, Offset start, Offset end) {
    var painter = _painter
      ..color = Colors.lightGreenAccent.shade700
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0;
    var path = Path();
    path.moveTo(start.dx, start.dy);
    path.lineTo(end.dx, end.dy);
    canvas.drawPath(path, painter);
  }

  Color _getColorByIndex(int index) {
    return codes.contains(index) ? Colors.lightGreenAccent.shade700 : Colors.white;
  }

  Offset _getOffsetByIndex(int index) {
    var dxCode = _sizeCode * (index % _columns + .5);
    var dyCode = _sizeCode * ((index / _columns).floor() + .5);
    return Offset(dxCode, dyCode);
  }

  @override
  bool shouldRepaint(_LockScreenPainter oldDelegate) => currentPosition != oldDelegate.currentPosition;
}
