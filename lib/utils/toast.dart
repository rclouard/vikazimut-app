// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/theme.dart';

@immutable
class Toast {
  static const int LENGTH_LONG = 5;
  static const int INFINITY = 24 * 60 * 60;

  final BuildContext _context;
  late final SnackBar _snackBar;

  Toast(this._context, String message, int duration, [Color? color]) {
    _snackBar = SnackBar(
      backgroundColor: color ?? kGreenColor,
      padding: const EdgeInsets.fromLTRB(2, 15, 2, 15),
      duration: Duration(seconds: duration),
      content: Text(
        message,
        textAlign: TextAlign.center,
      ),
    );
  }

  static Toast makeText(BuildContext context, String message, int duration, {Color? color}) {
    return Toast(context, message, duration, color);
  }

  void show() {
    ScaffoldMessenger.of(_context).removeCurrentSnackBar();
    ScaffoldMessenger.of(_context).showSnackBar(_snackBar);
  }

  void dismiss() {
    ScaffoldMessenger.of(_context).removeCurrentSnackBar();
  }
}
