int medianValue(List<int> array) {
  final p = (array.length + 1) ~/ 2;
  var left = 0;
  var right = array.length - 1;
  while (true) {
    if (left == right) {
      return array[p - 1];
    }
    final pivot = _partition(array, left, right);
    if (pivot == p - 1) {
      return array[p - 1];
    }
    if (pivot > p - 1) {
      right = pivot - 1;
    } else {
      left = pivot + 1;
    }
  }
}

int _partition(List<int> array, int left, int right) {
  final pivot = array[left];
  var i = left + 1;
  var j = left + 1;
  while (j <= right) {
    if (array[j] <= pivot) {
      int tmp = array[i];
      array[i] = array[j];
      array[j] = tmp;
      i += 1;
    }
    j += 1;
  }
  int tmp = array[left];
  array[left] = array[i - 1];
  array[i - 1] = tmp;
  return i - 1;
}
