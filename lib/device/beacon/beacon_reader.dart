// coverage:ignore-file
import 'dart:async';

import 'package:beacon_scanner/beacon_scanner.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'beacon_utility.dart';
import 'rssi_filter.dart';

class BeaconReader {
  static const String _RSSI_PREFERENCE_KEY = "BEACON RSSI";
  static const int _DEFAULT_RSSI_THRESHOLD = -60;
  static const String START_BEACON_ID = "1111";
  static const String END_BEACON_ID = "9999";
  static const int _MAX_CALIBRATION_VALUES = 20;
  static const int _SCAN_PERIOD_IN_MS = 250;

  static StreamSubscription<ScanResult>? _streamRanging;
  static BeaconScanner? _beaconScanner;
  static const List<Region> _regions = <Region>[
    Region(
      identifier: 'Vikazimut',
      beaconId: IBeaconId(proximityUUID: '01122334-4556-6778-899A-ABBCCDDEEFF0'),
    ),
  ];

  BeaconReader._();

  static Future<void> initialize() async {
    try {
      await Permission.bluetoothScan.request();
      await Permission.bluetooth.request();
      _beaconScanner = BeaconScanner.instance;
      await _beaconScanner!.initialize(true);
      await _beaconScanner!.setScanPeriod(const Duration(milliseconds: _SCAN_PERIOD_IN_MS));
    } on PlatformException catch (_) {
      // library failed to initialize
    }
  }

  static Stopwatch timer = Stopwatch();

  static Future<void> startListening({
    required void Function(String) callback,
    required bool withCalibration,
  }) async {
    if (withCalibration) {
      int count = _MAX_CALIBRATION_VALUES;
      List<int> rssiList = [];
      timer.start();
      _streamRanging = _beaconScanner?.ranging(_regions).listen(
        (ScanResult result) {
          for (var beacon in result.beacons) {
            if (beacon.id.minorId.toString() == START_BEACON_ID && beacon.rssi <= 0) {
              rssiList.add(beacon.rssi);
              if (--count == 0) {
                _streamRanging?.cancel();
                _streamRanging = null;
                final rssiThreshold = medianValue(rssiList);
                _storeRssiIntoPreferences(rssiThreshold);
                _scanBeacons(callback: callback, rssiThreshold: rssiThreshold);
              }
            }
          }
        },
      );
    } else {
      int rssiThreshold = await _getRssiFromPreferences();
      _scanBeacons(callback: callback, rssiThreshold: rssiThreshold);
    }
  }

  static void stopListening() {
    _streamRanging?.cancel();
    _beaconScanner?.close();
    _beaconScanner = null;
    timer.stop();
  }

  static void _scanBeacons({required void Function(String) callback, required int rssiThreshold}) {
    final Map<int, RssiArmaFilter> armaFilters = {};
    _streamRanging = _beaconScanner?.ranging(_regions).listen((ScanResult result) {
      for (var beacon in result.beacons) {
        final minorId = beacon.id.minorId;
        final int rssi;
        if (!armaFilters.containsKey(minorId)) {
          armaFilters[minorId] = RssiArmaFilter(beacon.rssi);
          rssi = beacon.rssi;
        } else {
          rssi = armaFilters[minorId]!.filterRssi(beacon.rssi);
        }
        if (rssi >= rssiThreshold) {
          callback(beacon.id.minorId.toString());
        }
      }
    });
  }

  static Future<int> _getRssiFromPreferences() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_RSSI_PREFERENCE_KEY) ?? _DEFAULT_RSSI_THRESHOLD;
  }

  static Future<void> _storeRssiIntoPreferences(int rssiThreshold) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(_RSSI_PREFERENCE_KEY, rssiThreshold);
  }
}
