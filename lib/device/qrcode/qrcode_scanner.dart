// coverage:ignore-file
import 'dart:async';
import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:vikazimut/l10n.dart';

@immutable
class QrCodeScanner extends StatefulWidget {
  QrCodeScanner() {
    _freezeOrientation();
  }

  void _freezeOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  State<StatefulWidget> createState() => _QrCodeScannerState();

  static String extractCheckpointIdFromQrCodeText(String qrCodeText) {
    if (qrCodeText.startsWith("https://vikazimut.vikazim.fr/landing/")) {
      Uri? uri = Uri.tryParse(qrCodeText);
      if (uri != null) {
        String? cp = uri.queryParameters["cp"];
        if (cp != null) {
          return cp;
        }
      }
    }
    return qrCodeText;
  }
}

class _QrCodeScannerState extends State<QrCodeScanner> {
  QRViewController? _cameraController;
  final GlobalKey _qrKey = GlobalKey();
  StreamSubscription<Barcode>? _listen;
  static bool _torchEnabled = false;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      _cameraController!.pauseCamera();
    } else if (Platform.isIOS) {
      _cameraController!.resumeCamera();
    }
  }

  @override
  void dispose() {
    _cameraController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.getString("qrcode_scanner_title")),
        actions: [
          IconButton(
            icon: FutureBuilder<bool?>(
              future: _cameraController?.getFlashStatus(),
              builder: (BuildContext context, AsyncSnapshot<bool?> snapshot) {
                if (snapshot.hasData) {
                  _torchEnabled = snapshot.data!;
                }
                if (_torchEnabled) {
                  return const Icon(
                    Icons.flash_on,
                    color: Colors.yellow,
                    semanticLabel: "Turn off flash",
                  );
                } else {
                  return const Icon(
                    Icons.flash_off,
                    semanticLabel: "Turn on flash",
                  );
                }
              },
            ),
            iconSize: 32.0,
            onPressed: () async {
              if (_torchEnabled) {
                setState(() async {
                  await _cameraController?.toggleFlash();
                });
              }
            },
          ),
        ],
      ),
      body: Semantics(
        label: L10n.getString("qrcode_scanner_title"),
        child: Container(
          child: _buildQrView(context),
        ),
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    var scanArea = math.min(math.min(MediaQuery.of(context).size.width - 20, 300.0), math.min(MediaQuery.of(context).size.height - 20, 300.0));
    return QRView(
      key: _qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
        borderColor: Colors.red,
        borderRadius: 10,
        borderLength: 30,
        borderWidth: 10,
        cutOutSize: scanArea,
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    _cameraController = controller;
    _cameraController!.resumeCamera(); // TODO temporary fix for qr_code_scanner package 1.0.1
    _listen = controller.scannedDataStream.listen((Barcode scanData) {
      _listen?.cancel();
      if (mounted) {
        Navigator.pop<String>(context, scanData.code);
      }
    });
    if (_torchEnabled) {
      _cameraController?.toggleFlash();
    }
  }
}
