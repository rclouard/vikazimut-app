// coverage:ignore-file
import 'dart:convert';
import 'dart:io' show Platform;

import 'package:flutter/foundation.dart';
import 'package:nfc_manager/nfc_manager.dart';

/// See NFC forum specification for "Text Record Type Definition"
/// https://blog.zenika.com/2012/04/24/nfc-iv-les-types-de-messages-du-nfc-forum-wkt/
///
/// bit_7 defines encoding
/// bit_6 reserved for future use, must be 0
/// bit_5..0 length of IANA language code
@immutable
class NfcScanner {
  static const int _TEXT_TYPE = 0x54;
  static bool _isRunning = false;

  const NfcScanner._();

  static void startListening({required void Function(String) callback}) {
    if (Platform.isAndroid) {
      NfcManager.instance.isAvailable().then((value) {
        if (value) {
          _isRunning = true;
          NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
            String? decodedNfcTag = NfcScanner._decodedNfcTag(tag);
            if (decodedNfcTag != null) {
              callback(decodedNfcTag);
            }
          });
        }
      });
    }
  }

  static void stopListening() {
    if (Platform.isAndroid && _isRunning) {
      NfcManager.instance.stopSession();
    }
  }

  static String? _decodedNfcTag(NfcTag tag) {
    Ndef? ndef = Ndef.from(tag);
    if (ndef == null || ndef.cachedMessage == null) {
      return null;
    }
    var record = ndef.cachedMessage!.records.first;
    if (record.typeNameFormat == NdefTypeNameFormat.nfcWellknown && record.type.isNotEmpty && record.type.first == _TEXT_TYPE) {
      final languageCodeLength = record.payload.first;
      return utf8.decode(record.payload.sublist(1 + languageCodeLength));
    }
    return null;
  }
}
