// coverage:ignore-file
import 'dart:io' show Platform;

import 'package:flutter/foundation.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/l10n.dart';

@immutable
class NfcHelper {
  static const String _SHOW_NO_NFC_WARNING_KEY = "NFC unavailable notification";

  static Future<void> checkNfcSupport() async {
    // Right now, we cannot use NFC on iOS since it displays a popup that masks the map
    // See https://stackoverflow.com/questions/50464810/is-it-possible-to-hide-ios-system-alert-for-nfc-reading-session
    // https://stackoverflow.com/questions/52223128/nfc-read-tag-in-ios
    // https://stackoverflow.com/questions/47708668/how-to-remove-popup-message-of-nfc-tag-in-ios
    if (Platform.isAndroid) {
      bool isAvailable = await NfcManager.instance.isAvailable();
      if (!isAvailable && await _isAllowedToShowNfcWarningDialog()) {
        await showWarningDialog(
          globalKeyContext.currentContext!,
          title: L10n.getString("warning_title"),
          message: L10n.getString("nfc_unavailable_message"),
          yesButtonText: L10n.getString("ok"),
          checkboxMessage: L10n.getString("never_display_again"),
          checkboxAction: () => _saveDoNotShowAgain(),
        );
      }
    }
  }

  static Future<void> resetPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(_SHOW_NO_NFC_WARNING_KEY);
  }

  static Future<bool> _isAllowedToShowNfcWarningDialog() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_SHOW_NO_NFC_WARNING_KEY) ?? true;
  }

  static void _saveDoNotShowAgain() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_SHOW_NO_NFC_WARNING_KEY, false);
  }
}
