// coverage:ignore-file
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/utils/time.dart';

class Chronometer extends ChangeNotifier {
  int _baseTimeInMillisecond = 0;
  int _initialValueUsedBeforeBaseTimeIsSet = 0;
  Timer? _timer;

  void setValue(int totalTimeInMillisecond) {
    _initialValueUsedBeforeBaseTimeIsSet = totalTimeInMillisecond;
    notifyListeners();
  }

  int start([int? baseTimeInMillisecond]) {
    _baseTimeInMillisecond = baseTimeInMillisecond ?? DateTime.now().millisecondsSinceEpoch;
    _timer = Timer.periodic(const Duration(milliseconds: 500), _update);
    notifyListeners();
    return _baseTimeInMillisecond;
  }

  void stop() => _timer?.cancel();

  Widget getWidget() => ChronometerView(chronometer: this);

  @override
  void dispose() {
    stop();
    super.dispose();
  }

  int get elapsedInMillisecond => _baseTimeInMillisecond > 0 ? DateTime.now().millisecondsSinceEpoch - _baseTimeInMillisecond : _initialValueUsedBeforeBaseTimeIsSet;

  void _update(Timer _) => notifyListeners();
}

class ChronometerView extends StatefulWidget {
  final Chronometer chronometer;

  const ChronometerView({required this.chronometer});

  @override
  ChronometerState createState() => ChronometerState();
}

class ChronometerState extends State<ChronometerView> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: widget.chronometer,
      child: Consumer<Chronometer>(
        builder: (context, chronometer, child) {
          return Text(
            formatTimeAsString(chronometer.elapsedInMillisecond),
            style: const TextStyle(fontSize: 18, color: Colors.white),
          );
        },
      ),
    );
  }
}
