part of 'compass_widget.dart';

class CompassPresenter implements FlutterCompass {
  double _preValue = 0;
  double _turns = 0;

  static final CompassPresenter _instance = CompassPresenter._();

  factory CompassPresenter() {
    return _instance;
  }

  CompassPresenter._();

  Stream<CompassEvent>? events() => FlutterCompass.events;

  double headingToTurns(double heading, double mapRotationInDegree) {
    double directionInDegree = heading + mapRotationInDegree;
    if (directionInDegree < 0) {
      directionInDegree = 360 + directionInDegree;
    }

    double diff = directionInDegree - _preValue;
    if (diff.abs() > 180) {
      if (_preValue > directionInDegree) {
        diff = 360 - (directionInDegree - _preValue).abs();
      } else {
        diff = 360 - (-directionInDegree).abs();
        diff = diff * -1;
      }
    }
    _turns += (diff / 360);
    _preValue = directionInDegree;
    return _turns;
  }
}
