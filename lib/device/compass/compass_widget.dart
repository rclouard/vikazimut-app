// coverage:ignore-file
library;

import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_compass/flutter_compass.dart';

part 'compass_presenter.dart';

@immutable
class CompassWidget extends StatelessWidget {
  final int? rotationSpeed;
  final double mapRotationInDegree;

  const CompassWidget({
    this.rotationSpeed = 200,
    required this.mapRotationInDegree,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CompassEvent>(
      stream: CompassPresenter().events(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const SizedBox.shrink();
        }
        if (snapshot.hasError) {
          return const SizedBox.shrink();
        }
        if (snapshot.data!.heading == null) {
          return const SizedBox.shrink();
        }
        final double turns = CompassPresenter().headingToTurns(snapshot.data!.heading!, mapRotationInDegree);
        return AnimatedRotation(
          turns: turns * -1,
          duration: Duration(milliseconds: rotationSpeed!),
          child: const CustomPaint(
            size: Size(100, 100),
            painter: _CompassPainter(),
          ),
        );
      },
    );
  }
}

@immutable
class _CompassPainter extends CustomPainter {
  static const double _needleWidth = 10;

  const _CompassPainter();

  @override
  void paint(Canvas canvas, Size size) {
    final Paint northPaint = Paint()
      ..color = const Color(0xFFE74C3C)
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    final Paint circlePaint = Paint()
      ..color = const Color(0xFF000000)
      ..style = PaintingStyle.fill;
    final Paint southPaint1 = Paint()
      ..color = const Color(0xFFBDBDBD)
      ..style = PaintingStyle.stroke;
    final Paint southPaint2 = Paint()
      ..color = const Color(0xFF000000)
      ..style = PaintingStyle.stroke;

    Offset center = Offset(size.width / 2, size.height / 2);
    final int needleLength = (math.min(size.width, size.height) * .45).ceil();
    canvas.save();
    northPaint.strokeWidth = _needleWidth;
    canvas.drawLine(center, Offset(center.dx, center.dy - needleLength), northPaint);
    southPaint2.strokeWidth = _needleWidth;
    canvas.drawLine(center, Offset(center.dx, center.dy + needleLength), southPaint2);
    southPaint1.strokeWidth = _needleWidth - 2;
    canvas.drawLine(center, Offset(center.dx, center.dy + needleLength - 1), southPaint1);

    canvas.drawCircle(center, _needleWidth / 2, circlePaint);
    canvas.restore();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
