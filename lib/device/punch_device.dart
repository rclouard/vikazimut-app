import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/course_activity/course_presenter.dart';

import 'beacon/beacon_reader.dart';
import 'gps/gps_scanner.dart';
import 'gps/gps_tracker_service.dart';
import 'nfc/nfc_scanner.dart';

class PunchDevice {
  static Future<void> initialize(ValidationMode validationMode) async {
    if (validationMode == ValidationMode.BEACON) {
      await BeaconReader.initialize();
    }
  }

  static void startListening(
    CoursePresenter coursePresenter,
    ValidationMode validationMode,
    bool withCalibration,
    GpsTrackerService gpsTracker,
  ) {
    if (validationMode == ValidationMode.GPS) {
      gpsTracker.addListener(GpsScanner(
        getFirstUnpunchedCheckpointCloseToLocationFunction: coursePresenter.getFirstUnpunchedCheckpointCloseToLocation,
        callback: coursePresenter.punchCheckpointFromGPS,
      ));
    } else if (validationMode == ValidationMode.BEACON) {
      BeaconReader.startListening(
        callback: coursePresenter.punchNamedCheckpointFromBeacon,
        withCalibration: withCalibration,
      );
    }
  }

  static void stopListening() {
    NfcScanner.stopListening();
    BeaconReader.stopListening();
  }
}
