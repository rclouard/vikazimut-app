import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/constants/gps_constants.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'gps_location_listener.dart';

@immutable
class GpsScanner implements GpsLocationListener {
  static const int MIN_DISTANCE_TO_CP_IN_METERS_LARGE_SCALE = 22;
  static const int MIN_DISTANCE_TO_CP_IN_METERS_SMALL_SCALE = 12;
  final int Function(GeodesicPoint, int) getFirstUnpunchedCheckpointCloseToLocationFunction;
  final void Function(int) callback;

  const GpsScanner({required this.getFirstUnpunchedCheckpointCloseToLocationFunction, required this.callback});

  @override
  void onLocationChanged(Position location) {
    GeodesicPoint currentLocation = GeodesicPoint(location.latitude, location.longitude);
    int checkpoint = getFirstUnpunchedCheckpointCloseToLocationFunction(currentLocation, GpsConstants.minDistanceToCpInMeter);
    if (checkpoint >= 0) {
      callback(checkpoint);
    }
  }
}
