// coverage:ignore-file
import 'package:geolocator/geolocator.dart';

abstract class GpsLocationListener {
  void onLocationChanged(Position location);
}
