// coverage:ignore-file
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';

import 'gps_location_listener.dart';

class LocationProvider {
  static const int _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER = 1;
  static const int _MIN_TIME_BETWEEN_UPDATES_IN_MILLISECOND = 1500;

  StreamSubscription<Position>? _positionStreamSubscription;
  GpsLocationListener? _locationListener;

  void startLocationProvider(GpsLocationListener locationConsumer) {
    _locationListener = locationConsumer;
    if (_positionStreamSubscription == null) {
      late LocationSettings locationSettings;
      if (defaultTargetPlatform == TargetPlatform.android) {
        locationSettings = AndroidSettings(
          accuracy: LocationAccuracy.best,
          distanceFilter: _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER,
          forceLocationManager: false,
          intervalDuration: const Duration(milliseconds: _MIN_TIME_BETWEEN_UPDATES_IN_MILLISECOND),
        );
      } else if (defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.macOS) {
        locationSettings = AppleSettings(
          accuracy: LocationAccuracy.best,
          distanceFilter: _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER,
          activityType: ActivityType.fitness,
          pauseLocationUpdatesAutomatically: true,
          showBackgroundLocationIndicator: false,
        );
      } else {
        locationSettings = const LocationSettings(
          accuracy: LocationAccuracy.best,
          distanceFilter: _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER,
        );
      }
      final positionStream = Geolocator.getPositionStream(locationSettings: locationSettings);
      _positionStreamSubscription = positionStream.handleError((error) {
        _positionStreamSubscription?.cancel();
        _positionStreamSubscription = null;
      }).listen((position) {
        _onLocationChanged(position);
      });
    }
  }

  void stopLocationProvider() {
    _locationListener = null;
    if (_positionStreamSubscription != null) {
      _positionStreamSubscription!.cancel();
      _positionStreamSubscription = null;
    }
  }

  Future<Position?> getLastKnownLocation() async => Geolocator.getLastKnownPosition();

  void _onLocationChanged(final Position position) => _locationListener?.onLocationChanged(position);
}
