import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'gps_location_listener.dart';

class GpsTrackerService {
  static const int _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER = 1;
  static const int _MIN_TIME_BETWEEN_UPDATES_IN_MILLISECOND = 1500;
  static const int _LOW_ACCURACY_LIMIT_IN_METER = 30;
  static const int _HIGH_ACCURACY_LIMIT_IN_METER = 45;

  static int _accuracyThresholdInMeter = _LOW_ACCURACY_LIMIT_IN_METER;
  static int _numberOfRejectedPoints = 0;
  final List<GpsLocationListener> _locationListeners = [];
  final List<GeodesicPoint> _waypoints = [];
  StreamSubscription<Position>? _locationSubscription;
  int? _baseTimeInMillisecond;
  Position? _lastKnownPosition;
  GeodesicPoint? _lastRegularPosition;
  bool _withWaypointList = false;

  List<GeodesicPoint> get waypoints => _waypoints;

  void start({required bool withWaypointList}) {
    _withWaypointList = withWaypointList;
    LocationSettings locationSettings;
    if (defaultTargetPlatform == TargetPlatform.android) {
      locationSettings = AndroidSettings(
        accuracy: LocationAccuracy.best,
        distanceFilter: _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER,
        forceLocationManager: false,
        intervalDuration: const Duration(milliseconds: _MIN_TIME_BETWEEN_UPDATES_IN_MILLISECOND),
        foregroundNotificationConfig: ForegroundNotificationConfig(
          notificationTitle: L10n.getString("gps_background_notification_title"),
          notificationText: L10n.getString("gps_background_notification_message"),
        ),
        useMSLAltitude: true, // Same altitude values as iOS
      );
    } else if (defaultTargetPlatform == TargetPlatform.iOS || defaultTargetPlatform == TargetPlatform.macOS) {
      locationSettings = AppleSettings(
        accuracy: LocationAccuracy.best,
        activityType: ActivityType.fitness,
        distanceFilter: _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER,
        pauseLocationUpdatesAutomatically: false,
        showBackgroundLocationIndicator: false,
        allowBackgroundLocationUpdates: true,
      );
    } else {
      locationSettings = const LocationSettings(
        accuracy: LocationAccuracy.best,
        distanceFilter: _MIN_DISTANCE_BETWEEN_UPDATES_IN_METER,
      );
    }
    final positionStream = Geolocator.getPositionStream(locationSettings: locationSettings);
    _locationSubscription = positionStream.handleError((error) {
      _locationSubscription?.cancel();
      _locationSubscription = null;
    }).listen((Position? position) {
      if (position != null) {
        _onLocationChanged(position);
      }
    });
  }

  void stop() {
    _locationSubscription?.cancel();
    _locationSubscription = null;
    _locationListeners.clear();
  }

  void setBaseTime(int baseTimeInMillisecond) {
    _baseTimeInMillisecond = baseTimeInMillisecond;
  }

  void reset(int baseTimeInMillisecond) {
    _baseTimeInMillisecond = baseTimeInMillisecond;
  }

  void addListener(GpsLocationListener locationListener) {
    _locationListeners.add(locationListener);
  }

  void setWaypoints(List<GeodesicPoint> geodesicPoints) {
    _waypoints.clear();
    _waypoints.addAll(geodesicPoints);
  }

  GeodesicPoint? getLastKnownPosition() {
    if (_lastKnownPosition == null) {
      return null;
    } else {
      return GeodesicPoint(_lastKnownPosition!.latitude, _lastKnownPosition!.longitude);
    }
  }

  void _onLocationChanged(Position position) {
    if (_baseTimeInMillisecond != null) {
      final currentTime = DateTime.now();
      // Fixed the bug that can happen when the system clock and the GPS clock are out of sync
      var timestamp = math.min(position.timestamp.millisecondsSinceEpoch, currentTime.millisecondsSinceEpoch) - _baseTimeInMillisecond!;
      Position rebasedPosition = Position(
        latitude: position.latitude,
        longitude: position.longitude,
        timestamp: DateTime.fromMillisecondsSinceEpoch(timestamp),
        accuracy: position.accuracy,
        altitude: position.altitude,
        heading: position.heading,
        speed: position.speed,
        speedAccuracy: position.speedAccuracy,
        altitudeAccuracy: position.altitudeAccuracy,
        headingAccuracy: position.headingAccuracy,
      );
      _addGpsPointToWaypoints(rebasedPosition);
    }
    _lastKnownPosition = position;
    _notifyListeners(position);
  }

  void _notifyListeners(Position location) {
    for (var l in _locationListeners) {
      l.onLocationChanged(location);
    }
  }

  void _addGpsPointToWaypoints(Position position) {
    if (_isRegularWaypoint(position, _lastRegularPosition)) {
      final waypoint = GeodesicPoint(
        position.latitude,
        position.longitude,
        position.timestamp.millisecondsSinceEpoch,
        position.altitude,
      );
      ResultDatabaseGateway.addWaypoint(waypoint);
      _lastRegularPosition = waypoint;
      if (_withWaypointList) {
        _waypoints.add(waypoint);
      }
    }
  }

  bool _isRegularWaypoint(Position currentPosition, GeodesicPoint? previousPosition) {
    if (positionTimeIsOlderThanBaseTime_(currentPosition)) {
      return false;
    }
    if (positionIsOlderThanPreviousPosition_(currentPosition, previousPosition)) {
      return false;
    }
    return positionIsAccurate_(currentPosition, _accuracyThresholdInMeter);
  }

  @visibleForTesting
  static bool positionTimeIsOlderThanBaseTime_(Position position) => position.timestamp.millisecondsSinceEpoch < 0;

  @visibleForTesting
  static bool positionIsOlderThanPreviousPosition_(Position currentPosition, GeodesicPoint? previousPosition) => previousPosition != null && currentPosition.timestamp.millisecondsSinceEpoch < previousPosition.timestampInMillisecond;

  @visibleForTesting
  static bool positionIsAccurate_(Position currentPosition, int accuracyLimit) {
    if (currentPosition.accuracy > accuracyLimit) {
      _numberOfRejectedPoints++;
      if (_numberOfRejectedPoints >= 10) {
        _accuracyThresholdInMeter = _HIGH_ACCURACY_LIMIT_IN_METER;
      }
      return false;
    } else {
      _accuracyThresholdInMeter = _LOW_ACCURACY_LIMIT_IN_METER;
      _numberOfRejectedPoints = 0;
    }
    return true;
  }
}
