library;

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/server_gateway/gateway_listener.dart';
import 'package:vikazimut/server_gateway/missing_checkpoint_data.dart';
import 'package:vikazimut/server_gateway/missing_checkpoint_gateway.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

import '../course_presenter.dart';

part 'auto_validation_forcing_handler.dart';
part 'manual_validation_forcing_handler.dart';
part 'notification_dialog.dart';

@immutable
abstract class AbstractCheckpointValidationForcing {
  final BuildContext context;
  final CoursePresenter coursePresenter;

  const AbstractCheckpointValidationForcing(this.context, this.coursePresenter);

  String getCheckpointIndexName(int missingCheckpointId) => getCheckpointName_(missingCheckpointId, " (${L10n.getString("cp_start")})", " (${L10n.getString("cp_finish")})");

  String getCheckpointName(int missingCheckpointId) => coursePresenter.getCheckpointName(missingCheckpointId);

  List<int> getCheckpointIds() {
    if (coursePresenter.format == CourseFormat.PRESET) {
      return [coursePresenter.nextCheckpoint];
    } else {
      if (coursePresenter.isStartCheckpoint(coursePresenter.nextCheckpoint)) {
        return [coursePresenter.nextCheckpoint];
      } else {
        GeodesicPoint? currentLocation = coursePresenter.getLastKnownLocation();
        if (currentLocation == null) {
          return [];
        } else {
          return coursePresenter.getClosestUnpunchedCheckpoints(currentLocation);
        }
      }
    }
  }

  @visibleForTesting
  String getCheckpointName_(int missingCheckpointId, String start, String finish) {
    if (missingCheckpointId == 0) {
      return missingCheckpointId.toString() + start;
    } else if (coursePresenter.isLastCheckpoint(missingCheckpointId)) {
      return missingCheckpointId.toString() + finish;
    } else {
      return missingCheckpointId.toString();
    }
  }
}
