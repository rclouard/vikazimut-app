// coverage:ignore-file
part of 'abstract_checkpoint_validation_forcing.dart';

enum DialogType { ERROR, CONFIRMATION }

@immutable
class NotificationDialog {
  final DialogType dialogType;
  final String title;
  final String message;
  final String? yesButtonText;
  final String? noButtonText;
  final void Function()? yesButtonAction;

  const NotificationDialog(
    this.dialogType, {
    required this.title,
    required this.message,
    this.yesButtonText,
    this.yesButtonAction,
    this.noButtonText,
  });

  void show(BuildContext context) {
    switch (dialogType) {
      case DialogType.ERROR:
        showErrorDialog(
          context,
          title: title,
          message: message,
        );
        break;
      case DialogType.CONFIRMATION:
        showConfirmationDialog(
          context,
          title: title,
          message: message,
          yesButtonText: yesButtonText!,
          noButtonText: noButtonText!,
          yesButtonAction: yesButtonAction,
        );
        break;
      }
  }
}
