part of 'abstract_checkpoint_validation_forcing.dart';

@immutable
class ManualValidationForcingHandler extends AbstractCheckpointValidationForcing implements GatewayListener {
  @visibleForTesting
  static const int REAL_MIN_DISTANCE_TO_MISSING_CP_IN_METERS = 30;
  static const int _DISPLAYED_MIN_DISTANCE_TO_MISSING_CP_IN_METERS = 10;
  static const int _RETURN_CODE_MISSING_CP = 0;

  const ManualValidationForcingHandler(super.context, super.coursePresenter);

  void execute(final bool reportMissingCheckpointToServer) {
    int missingCheckpointId;
    if (getCheckpointIds().isEmpty && !coursePresenter.isStarted) {
      missingCheckpointId = 0;
    } else {
      missingCheckpointId = getCheckpointIds().first;
    }

    NotificationDialog dialogNoLocationError = NotificationDialog(
      DialogType.ERROR,
      title: L10n.getString("missing_cp_error_title"),
      message: L10n.getString("missing_cp_error_no_location"),
    );

    NotificationDialog dialogDistanceError = NotificationDialog(
      DialogType.ERROR,
      title: L10n.getString("missing_cp_error_title"),
      message: L10n.getString("missing_cp_error_distance", _DISPLAYED_MIN_DISTANCE_TO_MISSING_CP_IN_METERS),
    );

    NotificationDialog dialogConfirmValidation = NotificationDialog(
      DialogType.CONFIRMATION,
      title: L10n.getString("confirm_title"),
      message: reportMissingCheckpointToServer ? L10n.getString("missing_cp_notification_plus_report", [getCheckpointIndexName(missingCheckpointId), getCheckpointName(missingCheckpointId)]) : L10n.getString("force_validation_cp_simple", [getCheckpointIndexName(missingCheckpointId), getCheckpointName(missingCheckpointId)]),
      yesButtonText: L10n.getString("yes"),
      yesButtonAction: () {
        final String missingCheckpointName = coursePresenter.getCheckpointName(missingCheckpointId);
        final String mapId = coursePresenter.mapId.toString();
        coursePresenter.punchCheckpointFromDevice(missingCheckpointId, forced: true);
        if (reportMissingCheckpointToServer) {
          _notifyServerOfMissingCheckpoint(mapId, missingCheckpointName);
        }
      },
      noButtonText: L10n.getString("no"),
    );
    execute_(missingCheckpointId, dialogNoLocationError, dialogDistanceError, dialogConfirmValidation);
  }

  @visibleForTesting
  void execute_(int missingCheckpointId, NotificationDialog dialogNoLocationError, NotificationDialog dialogDistanceError, NotificationDialog dialogConfirmValidation) {
    if (missingCheckpointId == 0) {
      dialogConfirmValidation.show(context);
      return;
    }
    GeodesicPoint? currentLocation = coursePresenter.getLastKnownLocation();
    if (currentLocation == null) {
      dialogNoLocationError.show(context);
      return;
    }
    if (missingCheckpointId < 0) {
      dialogDistanceError.show(context);
      return;
    }
    final int distanceToNextCheckpoint = coursePresenter.distanceToNextCheckpoint(currentLocation, missingCheckpointId);
    if (distanceToNextCheckpoint <= REAL_MIN_DISTANCE_TO_MISSING_CP_IN_METERS) {
      dialogConfirmValidation.show(context);
    } else {
      dialogDistanceError.show(context);
    }
  }

  void _notifyServerOfMissingCheckpoint(String mapId, String missingCheckpointName) {
    MissingCheckpointGateway server = MissingCheckpointGateway();
    MissingCheckpointData parameters = MissingCheckpointData(mapId, missingCheckpointName);
    server.sendToServer(parameters, this, _RETURN_CODE_MISSING_CP);
  }

  @override
  Future<void> handlePositiveSendingResultsToServerResponse(int code) async {
    // Nothing to do
  }

  @override
  Future<void> handleNegativeSendingResultsToServerResponse(int code, String message) async {
    // Nothing to do
  }
}
