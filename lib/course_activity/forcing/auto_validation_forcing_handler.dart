part of 'abstract_checkpoint_validation_forcing.dart';

@immutable
class AutoValidationForcingHandler extends AbstractCheckpointValidationForcing {
  const AutoValidationForcingHandler(super.context, super.coursePresenter);

  void execute(bool displayWithCheckpointId) {
    List<int> checkpointIds = getCheckpointIds();
    if (checkpointIds.isEmpty) {
      showErrorDialog(
        context,
        title: L10n.getString("validating_cp_error_title"),
        message: L10n.getString("validating_cp_error_no_location"),
      );
    } else {
      int currentCheckpointId;
      if (coursePresenter.lastPunchedCheckpoint == 0 && coursePresenter.getPunchedCheckpointCount() == 1 && checkpointIds.length > 1) {
        currentCheckpointId = checkpointIds[1];
      } else {
        currentCheckpointId = checkpointIds.first;
      }
      String message;
      if (displayWithCheckpointId) {
        message = L10n.getString("force_validation_cp_detailed", [getCheckpointIndexName(currentCheckpointId), getCheckpointName(currentCheckpointId)]);
      } else {
        message = L10n.getString("force_validation_cp_simple", [getCheckpointIndexName(currentCheckpointId)]);
      }
      NotificationDialog(
        DialogType.CONFIRMATION,
        title: L10n.getString("confirm_title"),
        message: message,
        yesButtonText: L10n.getString("yes_validate"),
        yesButtonAction: () {
          coursePresenter.punchCheckpointFromDevice(currentCheckpointId, forced: true);
        },
        noButtonText: L10n.getString("no"),
      ).show(context);
    }
  }
}
