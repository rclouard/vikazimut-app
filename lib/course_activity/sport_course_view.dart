// coverage:ignore-file
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/help_activity/manual_view.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_view/layers/location_layer.dart';
import 'package:vikazimut/result_activity/sport_global_result_view.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

import 'abstract_course_view.dart';
import 'setting_widget/abstract_course_settings_view.dart';
import 'setting_widget/sport_course_settings_view.dart';
import 'start_widget/start_stop_button.dart';
import 'start_widget/text_bubble.dart';
import 'subview/last_punched_view.dart';

@immutable
class SportCourseView extends AbstractCourseView {
  const SportCourseView(
    super.selectedMap, {
    required super.courseMode,
    required super.preconfiguredByPlanner,
    required super.courseResultEntity,
    super.withMultimediaContent = false,
  });

  @override
  AbstractCourseViewState createState() => SportCourseViewState();
}

class SportCourseViewState extends AbstractCourseViewState {
  final GlobalKey<StartStopButtonState> _startMenuIconKey = GlobalKey<StartStopButtonState>();
  final GlobalKey<PopupMenuButtonState> _popupMenuKey = GlobalKey<PopupMenuButtonState>();
  final ValueNotifier<ValidationMode?> _validationMode = ValueNotifier<ValidationMode?>(null);
  LocationLayer? _locationHelpOverlay;
  bool _isReady = false;
  bool _isStarted = false;

  @override
  void initializeCourseMenu(ValidationMode validationMode) {
    _validationMode.value = validationMode;
    _startMenuIconKey.currentState?.addMenuIcon(validationMode);
  }

  @override
  void dispose() {
    _validationMode.dispose();
    super.dispose();
  }

  @override
  PreferredSizeWidget buildCourseAppBarWidget() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(95.0),
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: AppBar(
          backgroundColor: (darkMode) ? Colors.black : kOrangeColor,
          toolbarHeight: 75,
          automaticallyImplyLeading: false,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              chronometerWidget,
              LastPunchedText(),
            ],
          ),
          actions: <Widget>[
            Align(
              alignment: Alignment.bottomCenter,
              child: IconButton(
                tooltip: L10n.getString("course_menu_button_light_mode"),
                icon: darkMode ? const Icon(Icons.sunny, semanticLabel: "Light theme") : const Icon(Icons.nights_stay_outlined, semanticLabel: "Dark theme"),
                onPressed: () {
                  setState(() {
                    darkMode = !darkMode;
                  });
                },
                iconSize: 32,
              ),
            ),
            const SizedBox(width: 8),
            StartStopButton(
              key: _startMenuIconKey,
              parent: this,
              onPressed: closeTextBubble,
            ),
            const SizedBox(width: 8),
            Align(
              alignment: Alignment.bottomCenter,
              child: IconButton(
                icon: const Icon(
                  Icons.playlist_add_check,
                  semanticLabel: "Show course state",
                ),
                tooltip: L10n.getString("course_state_message"),
                onPressed: () {
                  closeTextBubble();
                  showCourseState();
                },
                iconSize: 32,
              ),
            ),
            const SizedBox(width: 8),
            Align(
              alignment: Alignment.bottomCenter,
              child: PopupMenuButton(
                icon: const Icon(
                  Icons.more_vert,
                  semanticLabel: "Show menu",
                ),
                key: _popupMenuKey,
                color: Theme.of(context).primaryColor,
                surfaceTintColor: Theme.of(context).primaryColor,
                itemBuilder: (context) {
                  closeTextBubble();
                  return <PopupMenuEntry>[
                    PopupMenuItem(
                      value: 1,
                      child: Text(
                        L10n.getString("display_help_menu_item"),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                    const PopupMenuDivider(height: 10),
                    PopupMenuItem(
                      value: 2,
                      enabled: _isReady,
                      child: Text(
                        L10n.getString("skip_missing_checkpoint"),
                        style: _isReady ? const TextStyle(color: Colors.white) : const TextStyle(color: kOrangeColorDisabled),
                      ),
                    ),
                    if (_validationMode.value == ValidationMode.MANUAL)
                      PopupMenuItem(
                        value: 3,
                        child: Text(
                          L10n.getString("validate_missing_checkpoint"),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    PopupMenuItem(
                      value: 4,
                      enabled: _isStarted,
                      child: Text(
                        L10n.getString("panic"),
                        style: _isStarted ? const TextStyle(color: Colors.white) : const TextStyle(color: kOrangeColorDisabled),
                      ),
                    ),
                    const PopupMenuDivider(height: 10),
                    PopupMenuItem(
                      value: 5,
                      child: Text(
                        L10n.getString("quit_course"),
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ];
                },
                onSelected: (value) async {
                  closeTextBubble();
                  switch (value) {
                    case 1:
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ManualView()));
                      break;
                    case 2:
                      if (_validationMode.value == ValidationMode.MANUAL || _validationMode.value == ValidationMode.BEACON) {
                        forceValidationQRCheckpoint();
                      } else {
                        forceValidationGPSCheckpoint(detailed: true);
                      }
                      break;
                    case 3:
                      notifyQRCheckpointIsMissing();
                      break;
                    case 4:
                      _askAidConfirmation(context);
                      break;
                    case 5:
                      if (isCourseStarted) {
                        askStopConfirmation(context);
                      } else {
                        askQuitConfirmation(context);
                      }
                      break;
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void displayTextBubbleAndStartLocation(TextBubble textBubble) {
    super.displayTextBubbleAndStartLocation(textBubble);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      textBubble.show(_startMenuIconKey);
    });
  }

  @override
  void activateStartButton() {
    _startMenuIconKey.currentState?.setStarted();
  }

  @override
  CourseSettingsView buildCourseSettingsDialog(CourseFormat? defaultCourseFormat, ValidationMode? defaultValidationMode) {
    return SportCourseSettingsView(context, widget.withMultimediaContent, defaultCourseFormat, defaultValidationMode);
  }

  @override
  Widget? buildBottomNavigationBar() {
    return ValueListenableBuilder<ValidationMode?>(
      valueListenable: _validationMode,
      builder: (BuildContext context, ValidationMode? value, child) {
        if (value == ValidationMode.GPS) {
          return BottomAppBar(
            color: Colors.black,
            padding: const EdgeInsets.fromLTRB(0, 5, 0, -0),
            height: 30,
            child: gpsInformationWidget,
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  @override
  void onLocationChanged(Position location) {
    super.onLocationChanged(location);
    checkPositionIsInMapBounds(location.latitude, location.longitude);
  }

  @override
  Future<void> gotoToResultPage(int itemIndex) async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => SportGlobalResultView(resultId: itemIndex)),
    );
  }

  @override
  void handlePrepareCourse() {
    super.handlePrepareCourse();
    _popupMenuKey.currentState?.setState(() {
      _isReady = true;
    });
  }

  @override
  void handleStartCourse() {
    _isStarted = true;
    super.handleStartCourse();
  }

  @override
  void handleStopCourse() {
    super.handleStopCourse();
    _locationHelpOverlay?.stop();
  }

  void _askAidConfirmation(BuildContext context) {
    showConfirmationDialog(
      context,
      title: L10n.getString("aid_dialog_title"),
      message: L10n.getString("aid_dialog_message"),
      yesButtonText: L10n.getString("yes_aid"),
      yesButtonAction: _onCallAssistance,
      noButtonText: L10n.getString("no_aid"),
    );
  }

  void _onCallAssistance() {
    if (_locationHelpOverlay == null && isCourseStarted) {
      _locationHelpOverlay = LocationLayer(mapController, onlyAvatar: true);
      var lastLocation = lastKnownLocation;
      if (lastLocation != null) {
        mapController.centerOn(lastLocation.latitude, lastLocation.longitude);
      }
      mapController.addLayer(_locationHelpOverlay!);
      Timer(const Duration(seconds: 20), () {
        if (_locationHelpOverlay != null) {
          if (isCourseStarted) {
            incrementAssistanceCount();
          }
          mapController.removeLayer(_locationHelpOverlay!);
          _locationHelpOverlay = null;
        }
      });
    }
  }
}
