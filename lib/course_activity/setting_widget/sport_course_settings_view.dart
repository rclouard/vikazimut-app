// coverage:ignore-file
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';

import 'abstract_course_settings_view.dart';

class SportCourseSettingsView extends CourseSettingsView {
  final bool isGeocachingMode;

  SportCourseSettingsView(
    BuildContext context,
    this.isGeocachingMode,
    CourseFormat? defaultCourseFormat,
    ValidationMode? defaultValidationMode,
  ) : super(
          context,
          isGeocachingMode ? "playful_mode" : "sport_mode",
          defaultCourseFormat ?? CourseFormat.PRESET,
          defaultValidationMode ?? ValidationMode.GPS,
        );

  @override
  Widget buildFormatRadioButtonPanel() {
    return _SportFormatRadioButtonPanel(
      initialValue: courseFormat,
      action: (CourseFormat val) => courseFormat = val,
      isGeocachingMode: isGeocachingMode,
    );
  }

  @override
  Widget buildModeRadioButtonPanel() {
    return _SportModeRadioButtonPanel(
      initialValue: validationMode,
      action: (ValidationMode val) => validationMode = val,
    );
  }
}

@immutable
class _SportFormatRadioButtonPanel extends AbstractFormatRadioButtonPanel {
  final void Function(CourseFormat val) action;
  final CourseFormat initialValue;
  final bool isGeocachingMode;

  const _SportFormatRadioButtonPanel({required this.initialValue, required this.action, required this.isGeocachingMode});

  @override
  State createState() => _RadioButtonSportFormatWidget();
}

class _RadioButtonSportFormatWidget extends State<_SportFormatRadioButtonPanel> {
  late CourseFormat radioItem;

  get isGeocachingMode => widget.isGeocachingMode;

  @override
  void initState() {
    radioItem = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RadioListTile<CourseFormat>(
          groupValue: radioItem,
          title: Text(L10n.getString("preset_order")),
          value: CourseFormat.PRESET,
          onChanged: (CourseFormat? val) {
            setState(() {
              if (val != null) {
                widget.action(val);
                radioItem = val;
              }
            });
          },
        ),
        RadioListTile<CourseFormat>(
          groupValue: radioItem,
          title: Text(L10n.getString("free_order_and_score")),
          value: CourseFormat.FREE,
          onChanged: (CourseFormat? val) {
            setState(() {
              if (val != null) {
                widget.action(val);
                radioItem = val;
              }
            });
          },
        ),
      ],
    );
  }
}

@immutable
class _SportModeRadioButtonPanel extends AbstractModeRadioButtonPanel {
  _SportModeRadioButtonPanel({required action, required initialValue})
      : super(
          action,
          initialValue,
          "auto_validation",
          Platform.isAndroid ? "manual_validation_android" : "manual_validation_ios",
          "manual_validation_beacon",
        );
}
