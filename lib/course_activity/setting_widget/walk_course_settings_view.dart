// coverage:ignore-file
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';

import 'abstract_course_settings_view.dart';

class WalkCourseSettingsView extends CourseSettingsView {
  WalkCourseSettingsView(
    BuildContext context,
    CourseFormat? defaultCourseFormat,
    ValidationMode? defaultValidationMode,
  ) : super(
          context,
          "walk_mode",
          CourseFormat.FREE,
          defaultValidationMode ?? ValidationMode.GPS,
        );

  @override
  Widget buildFormatRadioButtonPanel() {
    return const _WalkFormatRadioButtonPanel();
  }

  @override
  Widget buildModeRadioButtonPanel() {
    return _WalkModeRadioButtonPanel(
      initialValue: validationMode,
      action: (ValidationMode val) {
        validationMode = val;
      },
    );
  }
}

@immutable
class _WalkFormatRadioButtonPanel extends AbstractFormatRadioButtonPanel {
  const _WalkFormatRadioButtonPanel();

  @override
  State createState() => _RadioButtonWalkFormatWidget();
}

class _RadioButtonWalkFormatWidget extends State {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RadioListTile<int>(
          groupValue: 0,
          title: Text(
            L10n.getString("walk_free_order"),
            style: const TextStyle(color: Colors.black),
          ),
          value: 0,
          onChanged: null,
        ),
      ],
    );
  }
}

@immutable
class _WalkModeRadioButtonPanel extends AbstractModeRadioButtonPanel {
  _WalkModeRadioButtonPanel({required action, required initialValue})
      : super(
          action,
          initialValue,
          "auto_validation",
          Platform.isAndroid ? "manual_validation_android" : "manual_validation_ios",
          "manual_validation_beacon",
        );
}
