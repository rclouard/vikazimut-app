// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

abstract class CourseSettingsView {
  late final AlertDialog _dialog;
  CourseFormat courseFormat;
  ValidationMode validationMode;

  CourseSettingsView(
    BuildContext context,
    String titleId,
    this.courseFormat,
    this.validationMode,
  ) {
    _dialog = QuestionDialog(
      title: L10n.getString(titleId),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              children: [
                const Divider(height: 10, thickness: 1, indent: 30, endIndent: 30, color: kGreenColor),
                Text(
                  L10n.getString("course_type"),
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontWeight: FontWeight.bold, color: kGreenColor),
                ),
                const Divider(height: 10, thickness: 1, indent: 30, endIndent: 30, color: kGreenColor),
                buildFormatRadioButtonPanel(),
              ],
            ),
            Column(
              children: [
                const Divider(height: 10, thickness: 1, indent: 30, endIndent: 30, color: kGreenColor),
                Text(
                  L10n.getString("validation_mode"),
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontWeight: FontWeight.bold, color: kGreenColor),
                ),
                const Divider(height: 10, thickness: 1, indent: 30, endIndent: 30, color: kGreenColor),
                buildModeRadioButtonPanel(),
              ],
            ),
          ],
        ),
      ),
      actions: [
        TextButton(
          child: Text(
            L10n.getString("ok"),
            style: const TextStyle(color: kGreenColor),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }

  Widget buildFormatRadioButtonPanel();

  Widget buildModeRadioButtonPanel();

  Future<void> showSettingDialog(BuildContext context) async {
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return _dialog;
      },
    );
  }
}

@immutable
abstract class AbstractFormatRadioButtonPanel extends StatefulWidget {
  const AbstractFormatRadioButtonPanel();
}

@immutable
abstract class AbstractModeRadioButtonPanel extends StatefulWidget {
  final void Function(ValidationMode val) action;
  final ValidationMode initialValue;
  final String messageAuto;
  final String messageManual;
  final String messageBeacon;

  const AbstractModeRadioButtonPanel(this.action, this.initialValue, this.messageAuto, this.messageManual, this.messageBeacon);

  @override
  State createState() => _RadioButtonModeWidget();
}

class _RadioButtonModeWidget extends State<AbstractModeRadioButtonPanel> {
  late ValidationMode radioItem;

  @override
  void initState() {
    radioItem = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        RadioListTile<ValidationMode>(
          groupValue: radioItem,
          value: ValidationMode.GPS,
          title: Text(L10n.getString(widget.messageAuto)),
          onChanged: (ValidationMode? val) {
            setState(() {
              if (val != null) {
                widget.action(val);
                radioItem = val;
              }
            });
          },
        ),
        RadioListTile<ValidationMode>(
          groupValue: radioItem,
          value: ValidationMode.MANUAL,
          title: Text(L10n.getString(widget.messageManual)),
          subtitle: Text(L10n.getString("validation_only_equipped_route"), style: const TextStyle(fontSize: 11)),
          onChanged: (ValidationMode? val) {
            setState(() {
              if (val != null) {
                widget.action(val);
                radioItem = val;
              }
            });
          },
        ),
        RadioListTile<ValidationMode>(
          groupValue: radioItem,
          value: ValidationMode.BEACON,
          title: Text(L10n.getString(widget.messageBeacon)),
          subtitle: Text(L10n.getString("validation_only_equipped_route"), style: const TextStyle(fontSize: 11)),
          onChanged: (ValidationMode? val) {
            setState(() {
              if (val != null) {
                widget.action(val);
                radioItem = val;
              }
            });
          },
        ),
      ],
    );
  }
}
