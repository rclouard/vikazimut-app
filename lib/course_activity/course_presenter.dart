import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vibration/vibration.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/gps_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/database/punch_time_entity.dart';
import 'package:vikazimut/database/waypoint_entity.dart';
import 'package:vikazimut/device/chronometer.dart';
import 'package:vikazimut/device/gps/gps_tracker_service.dart';
import 'package:vikazimut/device/nfc/nfc_scanner.dart';
import 'package:vikazimut/device/punch_device.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/result_activity/result.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/toast.dart';
import 'package:volume_controller/volume_controller.dart';

import 'abstract_course_view.dart';
import 'course.dart';
import 'live_tracking/live_tracking_manager.dart';
import 'setting_widget/abstract_course_settings_view.dart';
import 'subview/gps_accuracy_notifier.dart';
import 'validation/abstract_course_manager.dart';
import 'validation/course_manager_factory.dart';

class CoursePresenter with ChangeNotifier {
  static const _MIN_VOLUME_PERCENT = 0.6;
  static const _MAX_NUMBER_OF_GPS_POSITION_OUTSIDE_OF_MAP_BOUNDS_BEFORE_ALARM = 5;

  final AbstractCourseViewState _view;
  late final AbstractCourseManager _courseManager;
  late final Course _course;
  final GpsTrackerService _gpsTracker = GpsTrackerService();
  final Chronometer _chronometer = Chronometer();
  final GpsAccuracyNotifier _gpsAccuracyNotifier = GpsAccuracyNotifier();
  late final AudioPlayer _audioPlayer;
  CourseResultEntity? _courseResultEntity;

  late final ValidationMode _validationMode;
  bool _withMultimediaContent = false;
  bool _isStarted = false;
  bool _startProcedureLaunched = false;
  int _lastPunchedCheckpoint = -1;
  bool _withCalibration = true;
  bool _isRogaineInProgress = false;
  late final int _totalScore;

  /// Use this initial value to allow the orienteer to start outside the map without beeping
  int _numberOfOutOfMapBounds = _MAX_NUMBER_OF_GPS_POSITION_OUTSIDE_OF_MAP_BOUNDS_BEFORE_ALARM + 1;

  CoursePresenter(AbstractCourseViewState view, OrienteeringMap map, CourseResultEntity? courseResultEntity)
      : _courseResultEntity = courseResultEntity,
        _view = view {
    _totalScore = Result.calculateTotalScore(map.checkpoints);
    _course = Course(map, _chronometer);
    _gpsTracker.addListener(_view);
    _gpsTracker.addListener(_gpsAccuracyNotifier);
    _audioPlayer = AudioPlayer();
    _startAudioController();
  }

  @override
  void dispose() {
    _gpsTracker.stop();
    _gpsAccuracyNotifier.dispose();
    _chronometer.dispose();
    _audioPlayer.dispose();
    super.dispose();
  }

  int get lastPunchedCheckpoint => _lastPunchedCheckpoint;

  bool get isStartProcedureLaunched => _startProcedureLaunched;

  bool get isStarted => _isStarted;

  List<int> get punchTimes => _course.getPunchTimesInMilliseconds();

  int get nextCheckpoint => _course.getExpectedCheckpointIndex();

  List<GeodesicPoint> get waypoints => _gpsTracker.waypoints;

  set quizTotalPoints(int points) => _course.quizTotalPoints = points;

  int get quizTotalPoints => _course.quizTotalPoints;

  int get mapId => _course.map.id;

  CourseMode get courseMode => _courseManager.courseMode;

  CourseFormat get format => _courseManager.format;

  int getCurrentScore() => _course.currentScore;

  int getTotalScore() => _totalScore;

  int getNumberOfMissingPunches() => _course.getCheckpointCount() - getPunchedCheckpointCount();

  Widget getChronometerWidget() => _chronometer.getWidget();

  Widget getGpsInformationWidget() => _gpsAccuracyNotifier.getWidget(GpsConstants.minDistanceToCpInMeter);

  void incrementAssistanceCount() => _course.incrementAssistanceCount();

  GeodesicPoint? getLastKnownLocation() => _gpsTracker.getLastKnownPosition();

  String getCheckpointName(int checkpointId) => _course.map.getCheckpoint(checkpointId).id;

  bool isLastCheckpoint(int checkpointId) => _course.isLastCheckpoint(checkpointId);

  int getLastCheckpointId() => _course.getLastCheckpointId();

  bool isCheckpointAlreadyPunched(int checkpointId) => _course.isCheckpointAlreadyPunched(checkpointId);

  bool isAllCheckpointsPunchedExcept(int checkpointId) => _course.isAllCheckpointsPunchedExcept(checkpointId);

  bool isStartCheckpoint(int checkpointId) => _course.isStartCheckpoint(checkpointId);

  int getPunchedCheckpointCount() => _course.getPunchedCheckpointCount();

  int getExpectedCheckpointIndex() => _course.getExpectedCheckpointIndex();

  int getCheckpointIdFromName(String checkpointName) => _course.getCheckpointIdFromName(checkpointName);

  List<int> getClosestUnpunchedCheckpoints(GeodesicPoint location) => _course.getClosestUnpunchedCheckpoints(location);

  int getFirstUnpunchedCheckpointCloseToLocation(GeodesicPoint location, int minDistance) => _course.getFirstUnpunchedCheckpointCloseToLocation(location, minDistance);

  bool allIntermediateCheckpointsValidated() => _course.isAllIntermediateValidatedCheckpoints;

  void punchCheckpointFromDevice(int checkpointId, {required forced}) => _courseManager.punchCheckpointFromDevice(checkpointId, forced: forced);

  void _punchNamedCheckpointFromDevice(String checkpointName) => _courseManager.punchNamedCheckpointFromDevice(checkpointName);

  void punchCheckpointFromGPS(int checkpointId) => _courseManager.punchCheckpointFromGPS(checkpointId);

  void punchNamedCheckpointFromBeacon(String checkpointName) => _courseManager.punchNamedCheckpointFromBeacon(checkpointName);

  void punchNamedCheckpointFromQrCode(String qrCodeText) => _courseManager.punchCheckpointFromQrCode(qrCodeText);

  int distanceToNextCheckpoint(GeodesicPoint location, int missingCheckpointId) => _course.map.getDistanceToCheckpoint(location, missingCheckpointId);

  bool isStartAndFinishAtSamePosition() => _course.isStartAndFinishAtSamePosition();

  bool isCloseToCheckpoint(int checkpointId) => _course.isCloseToCheckpoint(checkpointId, getLastKnownLocation());

  void validate(int checkpointId, String message, bool forced) {
    _course.punchCheckpoint(checkpointId, forced, _courseManager.format);
    _displayLastPunched(checkpointId);
    _view.displayToastMessage(message, Toast.LENGTH_LONG);
    _view.updateCourseState();
    _showSuccess();
  }

  void invalidate(String title, String message) {
    _showFailure();
    _view.displayErrorDialog(title: title, message: message);
  }

  void invalidateButAskForBypass(final int checkpointId, String title, String message, bool forced) {
    _view.displayConfirmationDialog(
        title: title,
        message: message,
        yesButtonText: L10n.getString("yes_bypass"),
        yesButtonAction: () {
          _bypassPreviousCheckpoints(checkpointId);
          _validateCheckpoint(checkpointId, forced, _courseManager.format);
          displayMultimediaContents(checkpointId);
        },
        noButtonText: L10n.getString("no_continue"));
    _showFailure();
  }

  void checkPositionIsInMapBounds(double? latitude, double? longitude) {
    if (latitude == null || longitude == null) {
      return;
    }
    if (!_course.isInMapBounds(latitude, longitude)) {
      if (_numberOfOutOfMapBounds == _MAX_NUMBER_OF_GPS_POSITION_OUTSIDE_OF_MAP_BOUNDS_BEFORE_ALARM) {
        _playSound("alarm.mp3");
        _view.displayToastMessage(L10n.getString("out_of_bounds"), Toast.LENGTH_LONG, kRedColor);
      }
      _numberOfOutOfMapBounds++;
    } else {
      _numberOfOutOfMapBounds = 0;
    }
  }

  Future<void> displayMultimediaContents(int checkpointId, {void Function()? postAction}) async {
    if (_withMultimediaContent) {
      String? text = _course.map.getCheckpoint(checkpointId).text;
      if (text != null) {
        return _view.displayMultimediaContents(text, postAction);
      }
    }
    postAction?.call();
  }

  Future<void> restoreCourseDataFromDatabaseAfterCrash() async {
    CourseResultEntity courseResultEntity = _courseResultEntity!;
    final List<WaypointEntity> waypointEntities = await ResultDatabaseGateway.findAllWaypoints();
    final List<PunchTimeEntity> punchTimeEntities = await ResultDatabaseGateway.findAllPunchTimes();
    courseResultEntity.totalTimeInMillisecond = WaypointEntity.getTotalTimeInMs(waypointEntities);
    if (courseResultEntity.mode.isWalkMode) {
      _gpsTracker.setWaypoints(WaypointEntity.toGeodesicPoints(waypointEntities));
    }
    final punchTimes = PunchTimeEntity.toPunchTimeList(punchTimeEntities, courseResultEntity.checkpointCount);
    _course.resetPunchTimes(punchTimes);
    _chronometer.setValue(courseResultEntity.totalTimeInMillisecond);
  }

  Future<void> setCourseSettings(
    CourseMode courseMode,
    bool preconfiguredByPlanner,
    OrienteeringMap orienteeringMap,
    bool withMultimediaContent,
  ) async {
    _CourseSettings settings = await _getCourseSettings(preconfiguredByPlanner, orienteeringMap);
    await _setCourseUserChoices(courseMode, settings, withMultimediaContent);
    _view.initializeCourseMenu(settings.validationMode);
    await LiveTrackingManager.connectToLiveTrackingIfAny(orienteeringMap.id);
    await _initializeCourse(settings, courseMode);
  }

  void prepareCourse() {
    _gpsTracker.start(withWaypointList: _courseManager.courseMode == CourseMode.WALK);
    _view.handlePrepareCourse();
    PunchDevice.startListening(this, _validationMode, _withCalibration, _gpsTracker);
  }

  void startCourse() {
    if (_isStarted) return;
    _isStarted = true;
    final isResumingCourse = _courseResultEntity != null;
    if (isResumingCourse) {
      if (_isRogaineInProgress) {
        startRogaine_(_course, _gpsTracker, _courseResultEntity!);
      } else {
        resumeAfterCrash_(_course, _gpsTracker, _courseResultEntity!);
      }
      _courseResultEntity = null;
    } else {
      startRegularCourse_(_course, _gpsTracker, courseMode.index, _courseManager.format.index, _validationMode.index);
    }
    _view.handleStartCourse();
  }

  @visibleForTesting
  static void startRegularCourse_(Course course, GpsTrackerService gpsTracker, int courseModeIndex, int courseFormatIndex, int validationModeIndex) {
    int baseTimeInMillisecond = course.startChronometer();
    course.createAndStoreCourseResultEntity(
      mode: courseModeIndex,
      format: courseFormatIndex,
      validationMode: validationModeIndex,
      baseTimeInMillisecond: baseTimeInMillisecond,
    );
    gpsTracker.reset(baseTimeInMillisecond);
  }

  @visibleForTesting
  static void startRogaine_(Course course, GpsTrackerService gpsTracker, CourseResultEntity courseResultEntity, {bool useDatabase = false}) {
    final newBaseTime = DateTime.now().millisecondsSinceEpoch - courseResultEntity.totalTimeInMillisecond;
    var baseTimeInMillisecond = course.startChronometer(newBaseTime);
    course.setCourseResultEntity(courseResultEntity);
    gpsTracker.setBaseTime(baseTimeInMillisecond);
    courseResultEntity.runCount++;
    if (useDatabase) {
      ResultDatabaseGateway.updateRunCount(courseResultEntity.id!, courseResultEntity.runCount);
    }
  }

  @visibleForTesting
  static void resumeAfterCrash_(Course course, GpsTrackerService gpsTracker, CourseResultEntity courseResultEntity) {
    var baseTimeInMillisecond = course.startChronometer(courseResultEntity.baseTimeInMillisecond);
    course.setCourseResultEntity(courseResultEntity);
    gpsTracker.setBaseTime(baseTimeInMillisecond);
  }

  void stopCourse() async {
    _stopAllDevices();
    _isStarted = false;
    _view.handleStopCourse();
    int courseResultEntityId = await _course.storeResultIntoDatabase();
    await ResultDatabaseGateway.deleteAllPunchTimes();
    await ResultDatabaseGateway.deleteAllWaypoints();
    await LiveTrackingManager.closeSessionWithServer();

    // Used delayed to allow success sound completion
    await Future.delayed(const Duration(milliseconds: 600));
    await _view.gotoToResultPage(courseResultEntityId);
  }

  Future<void> quitCourseWithoutSavingResult() async {
    _stopAllDevices();
    await ResultDatabaseGateway.deleteAllPunchTimes();
    await ResultDatabaseGateway.deleteAllWaypoints();
    await LiveTrackingManager.closeSessionWithServer();
  }

  void onLocationChanged(Position location) {
    if (_isStarted && LiveTrackingManager.isLiveTrackingEnabled) {
      final int score = getCurrentScore();
      LiveTrackingManager.sendOrienteerPosition(location, score);
    }
  }

  void launchStartProcedure() {
    _startProcedureLaunched = true;
    _courseManager.punchCheckpointFromMenu(-1);
  }

  void stopCourseImmediately() {
    _courseManager.punchCheckpointFromMenu(_course.getLastCheckpointId());
  }

  void _resumeRogaineCourseDataFromDatabase(CourseResultEntity courseResultEntity) {
    courseResultEntity.sent = 0;
    final List<WaypointEntity> waypointEntities = WaypointEntity.convertFromString(courseResultEntity.gpsTrack);
    ResultDatabaseGateway.addAllWaypoints(waypointEntities);
    final List<PunchTimeEntity> punchTimeEntities = PunchTimeEntity.convertFromString(courseResultEntity.punchTimes!);
    ResultDatabaseGateway.addAllPunchTimes(punchTimeEntities);
    final punchTimes = PunchTimeEntity.toPunchTimeList(punchTimeEntities, courseResultEntity.checkpointCount);
    _course.resetPunchTimes(punchTimes);
    _chronometer.setValue(courseResultEntity.totalTimeInMillisecond);
  }

  void _startAudioController() async {
    AudioPlayer.global.setAudioContext(
      AudioContext(
        iOS: AudioContextIOS(
          category: AVAudioSessionCategory.playAndRecord,
          options: const {
            AVAudioSessionOptions.mixWithOthers,
            AVAudioSessionOptions.defaultToSpeaker,
          },
        ),
      ),
    );
    // We are forced to use the package VolumeController since audioplayers package has no function getVolume()
    double volume = await VolumeController.instance.getVolume();
    if (volume < _MIN_VOLUME_PERCENT) {
      VolumeController.instance.setVolume(_MIN_VOLUME_PERCENT);
    }
  }

  void _stopAllDevices() {
    _gpsTracker.stop();
    _course.stopChronometer();
    PunchDevice.stopListening();
  }

  void _showSuccess() {
    _playSound("success.mp3");
    _vibrate();
  }

  void _showFailure() {
    _playSound("failure.mp3");
    _vibrate();
  }

  void _vibrate() async {
    if (await Vibration.hasVibrator()) {
      Vibration.vibrate(duration: 1000);
    }
  }

  Future<void> _playSound(String rawSound) async {
    _audioPlayer.play(AssetSource('sound/$rawSound'));
  }

  void _bypassPreviousCheckpoints(int checkpointId) {
    _course.bypassPreviousCheckpoints(checkpointId);
  }

  void _validateCheckpoint(int checkpointId, bool forced, CourseFormat format) {
    _course.punchCheckpoint(checkpointId, forced, format);
  }

  void _displayLastPunched(int checkpointId) {
    _lastPunchedCheckpoint = checkpointId;
    notifyListeners();
  }

  Future<_CourseSettings> _getCourseSettings(bool preconfiguredByPlanner, OrienteeringMap orienteeringMap) async {
    final bool isResumingCourse = _courseResultEntity != null;
    final bool isPreconfiguredCourse = preconfiguredByPlanner && orienteeringMap.courseFormat != null && orienteeringMap.validationMode != null;
    final CourseFormat courseFormat;
    final ValidationMode validationMode;
    if (isResumingCourse) {
      courseFormat = CourseFormat.fromInt(_courseResultEntity!.format);
      validationMode = ValidationMode.fromInt(_courseResultEntity!.validationMode);
    } else if (isPreconfiguredCourse) {
      courseFormat = orienteeringMap.courseFormat!;
      validationMode = orienteeringMap.validationMode!;
    } else {
      final CourseSettingsView view = _view.buildCourseSettingsDialog(orienteeringMap.courseFormat, orienteeringMap.validationMode);
      await view.showSettingDialog(_view.context);
      courseFormat = view.courseFormat;
      validationMode = view.validationMode;
    }
    return _CourseSettings(courseFormat, validationMode);
  }

  Future<void> _setCourseUserChoices(CourseMode courseMode, _CourseSettings settings, bool withMultimediaContent) async {
    _withMultimediaContent = withMultimediaContent;
    _courseManager = CourseManagerFactory.create(this, courseMode, settings.courseFormat, settings.validationMode);
    _validationMode = settings.validationMode;
    await PunchDevice.initialize(settings.validationMode);
  }

  Future<void> _initializeCourse(_CourseSettings settings, CourseMode courseMode) async {
    final isResumingCourse = _courseResultEntity != null;
    if (isResumingCourse) {
      _withCalibration = false;
      prepareCourse();
      _startProcedureLaunched = true;
      _view.activateStartButton();
      startCourse();
    } else if (settings.courseFormat.isFreeOrder()) {
      if (courseMode == CourseMode.SPORT) {
        _courseResultEntity = await _course.findUncompletedFreeOrderSportCourse();
      }
      _isRogaineInProgress = _courseResultEntity != null && await _view.askConfirmationForRogaine();
      if (_isRogaineInProgress) {
        _withCalibration = false;
        await ResultDatabaseGateway.updateValidationMode(_courseResultEntity!.id!, _validationMode.index);
        _resumeRogaineCourseDataFromDatabase(_courseResultEntity!);
        var startCheckpoint = _course.getLastPunchedCheckpointIndex();
        _course.resetStartCheckpointAt(startCheckpoint, _courseManager.format);
        _view.displayStartNotifications(courseMode, settings.validationMode, startCheckpoint, _course.getCheckpointNameFromId(startCheckpoint));
      } else {
        _courseResultEntity = null;
        _view.displayStartNotifications(courseMode, settings.validationMode);
      }
      if (_validationMode.isManual()) {
        NfcScanner.startListening(callback: _punchNamedCheckpointFromDevice);
      }
    } else if (settings.courseFormat.isPresetOrder()) {
      _view.displayStartNotifications(courseMode, settings.validationMode);
      if (_validationMode.isManual()) {
        NfcScanner.startListening(callback: _punchNamedCheckpointFromDevice);
      }
    }
  }
}

class _CourseSettings {
  CourseFormat courseFormat;
  ValidationMode validationMode;

  _CourseSettings(this.courseFormat, this.validationMode);
}
