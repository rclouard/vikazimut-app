// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/storage_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

@immutable
class LiveTrackingDialog {
  static Future<bool> askPermissionForLiveTracking() async {
    BuildContext context = globalKeyContext.currentContext!;
    bool response = false;
    await showConfirmationDialog(
      context,
      title: L10n.getString("live_tracking_ask_permission"),
      message: L10n.getString("live_tracking_ask_permission_message"),
      yesButtonText: L10n.getString("live_tracking_permission_yes"),
      noButtonText: L10n.getString("live_tracking_permission_no"),
      yesButtonAction: () => response = true,
      noButtonAction: () => response = false,
    );
    return response;
  }

  static Future<String?> askNickname() async {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    BuildContext context = globalKeyContext.currentContext!;
    String? currentNickname = await getStoredNickname();
    if (context.mounted) {
      return await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          final pseudoTextEditorController = TextEditingController(text: currentNickname);
          return QuestionDialog(
            title: L10n.getString("nickname"),
            content: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      autofocus: true,
                      validator: (value) {
                        if (value == null || value.trim().isEmpty) {
                          return L10n.getString("error_no_nickname");
                        }
                        return null;
                      },
                      keyboardType: TextInputType.name,
                      maxLength: 20,
                      textCapitalization: TextCapitalization.words,
                      controller: pseudoTextEditorController,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(nicknameRegExp),
                      ],
                      decoration: InputDecoration(
                        labelText: L10n.getString("enter_nickname"),
                        labelStyle: const TextStyle(color: kGreenColor),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                            color: kGreenColor,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: const BorderSide(
                            color: kGreenColor,
                          ),
                        ),
                        fillColor: kGreenColor.withValues(alpha: 0.1),
                        filled: true,
                        counterStyle: const TextStyle(color: kGreenColor),
                      ),
                    ),
                    Text(
                      L10n.getString("data_protection"),
                      style: const TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
            ),
            actions: [
              TextButton(
                onPressed: () async {
                  if (formKey.currentState!.validate()) {
                    final NavigatorState state = Navigator.of(context);
                    final nickname = pseudoTextEditorController.text.trim();
                    if (nickname.isNotEmpty) {
                      await storeNickname(nickname);
                    }
                    state.pop(nickname);
                  }
                },
                child: Text(L10n.getString("send"), style: const TextStyle(color: kGreenColor)),
              ),
              TextButton(
                child: Text(L10n.getString("cancel"), style: const TextStyle(color: kGreenColor)),
                onPressed: () => Navigator.of(context).pop(null),
              ),
            ],
          );
        },
      );
    }
    return null;
  }
}
