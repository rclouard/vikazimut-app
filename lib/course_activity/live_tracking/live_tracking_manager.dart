import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/server_gateway/live_tracking_gateway.dart';

import 'live_tracking_dialog.dart';

@immutable
class LiveTrackingManager {
  static bool _isLiveTrackingEnabled = false;

  static bool get isLiveTrackingEnabled => _isLiveTrackingEnabled;

  static Future<void> connectToLiveTrackingIfAny(int courseId) async {
    await connectToLiveTrackingIfAny_(
      courseId,
      LiveTrackingGateway.getCurrentLiveTrackingSessionIfAny,
      LiveTrackingDialog.askPermissionForLiveTracking,
      LiveTrackingDialog.askNickname,
      LiveTrackingGateway.startSessionWithServer,
    );
  }

  @visibleForTesting
  static Future<void> connectToLiveTrackingIfAny_(
    int courseId,
    getCurrentLiveTrackingSessionIfAny,
    askPermissionForLiveTracking,
    askNickname,
    startSessionWithServer,
  ) async {
    int liveTrackingId = await getCurrentLiveTrackingSessionIfAny(courseId);
    if (liveTrackingId > 0) {
      bool response = await askPermissionForLiveTracking();
      if (response) {
        String? nickname = await askNickname();
        if (nickname != null) {
          _isLiveTrackingEnabled = await startSessionWithServer(liveTrackingId, nickname);
        }
      }
    }
  }

  static Future<void> closeSessionWithServer() async {
    await closeSessionWithServer_(LiveTrackingGateway.closeSessionWithServer);
  }

  @visibleForTesting
  static Future<void> closeSessionWithServer_(Future<void> Function() closeSessionWithServer) async {
    if (_isLiveTrackingEnabled) {
      await closeSessionWithServer();
      _isLiveTrackingEnabled = false;
    }
  }

  static void sendOrienteerPosition(Position location, int score) {
    LiveTrackingGateway.sendOrienteerPosition(position: location, score: score);
  }
}
