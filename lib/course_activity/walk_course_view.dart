// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/help_activity/manual_view.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_view/layers/flat_route_layer.dart';
import 'package:vikazimut/map_view/layers/location_layer.dart';
import 'package:vikazimut/map_view/map_controller.dart';
import 'package:vikazimut/result_activity/walk_global_result_view.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/theme.dart';

import 'abstract_course_view.dart';
import 'setting_widget/abstract_course_settings_view.dart';
import 'setting_widget/walk_course_settings_view.dart';
import 'start_widget/start_stop_button.dart';
import 'start_widget/text_bubble.dart';
import 'subview/last_punched_view.dart';

@immutable
class WalkCourseView extends AbstractCourseView {
  const WalkCourseView(
    super.selectedMap, {
    required super.courseMode,
    required super.preconfiguredByPlanner,
    required super.courseResultEntity,
  }) : super(withMultimediaContent: true);

  @override
  AbstractCourseViewState createState() => _WalkCourseViewState();
}

class _WalkCourseViewState extends AbstractCourseViewState {
  final GlobalKey<StartStopButtonState> _startMenuIconKey = GlobalKey<StartStopButtonState>();
  final GlobalKey<PopupMenuButtonState> _popupMenuKey = GlobalKey<PopupMenuButtonState>();
  FlatRouteLayer? _routeOverlay;
  LocationLayer? _locationOverlay;
  bool _isStarted = false;

  @override
  void initializeCourseMenu(ValidationMode validationMode) {
    _startMenuIconKey.currentState?.addMenuIcon(validationMode);
  }

  @override
  PreferredSizeWidget buildCourseAppBarWidget() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(95.0),
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: AppBar(
          toolbarHeight: 75,
          automaticallyImplyLeading: false,
          title: Column(
            children: [
              LastPunchedText(),
            ],
          ),
          actions: <Widget>[
            StartStopButton(
              key: _startMenuIconKey,
              parent: this,
              onPressed: closeTextBubble,
            ),
            const SizedBox(width: 20),
            IconButton(
              icon: const Icon(
                Icons.playlist_add_check,
                semanticLabel: "Show course state",
              ),
              tooltip: L10n.getString("course_state_message"),
              onPressed: () {
                closeTextBubble();
                showCourseState();
              },
              iconSize: 32,
            ),
            const SizedBox(width: 20),
            PopupMenuButton(
              color: Theme.of(context).primaryColor,
              icon: const Icon(
                Icons.more_vert,
                color: Colors.white,
                semanticLabel: "Show menu",
              ),
              key: _popupMenuKey,
              itemBuilder: (context) {
                closeTextBubble();
                return <PopupMenuEntry>[
                  PopupMenuItem(
                    value: 1,
                    child: Text(
                      L10n.getString("display_help_menu_item"),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  PopupMenuItem(
                    value: 2,
                    enabled: _isStarted,
                    child: Text(
                      L10n.getString("skip_missing_checkpoint"),
                      style: _isStarted ? const TextStyle(color: Colors.white) : const TextStyle(color: kOrangeColorDisabled),
                    ),
                  ),
                  const PopupMenuDivider(height: 10),
                  PopupMenuItem(
                    value: 3,
                    child: Text(
                      L10n.getString("quit_course"),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                ];
              },
              onSelected: (value) {
                switch (value) {
                  case 1:
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ManualView()));
                    break;
                  case 2:
                    forceValidationGPSCheckpoint(detailed: false);
                    break;
                  case 3:
                    if (isCourseStarted) {
                      askStopConfirmation(context);
                    } else {
                      askQuitConfirmation(context);
                    }
                    break;
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void displayTextBubbleAndStartLocation(TextBubble textBubble) {
    super.displayTextBubbleAndStartLocation(textBubble);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      textBubble.show(_startMenuIconKey);
    });
  }

  @override
  void onLocationChanged(Position location) {
    super.onLocationChanged(location);
    if (_routeOverlay != null) {
      List<GeodesicPoint> route = waypoints;
      GeodesicPoint? lastLocation = lastKnownLocation;
      if (lastLocation != null) {
        _routeOverlay!.drawRoute(route, lastLocation: lastLocation);
      }
    }
  }

  @override
  Future<void> gotoToResultPage(int itemIndex) async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => WalkGlobalResultView(resultId: itemIndex)),
    );
  }

  @override
  void handlePrepareCourse() {
    super.handlePrepareCourse();
    _addRouteOverlayOnMap(mapController);
    _addLocationOverlayOnMap(mapController);
    _popupMenuKey.currentState?.setState(() {
      _isStarted = true;
    });
  }

  @override
  void activateStartButton() {
    _startMenuIconKey.currentState?.setStarted();
  }

  @override
  void handleStartCourse() {
    _isStarted = true;
    super.handleStartCourse();
  }

  @override
  void handleStopCourse() {
    super.handleStopCourse();
    _locationOverlay?.stop();
    _routeOverlay?.stop();
  }

  @override
  CourseSettingsView buildCourseSettingsDialog(CourseFormat? defaultCourseFormat, ValidationMode? defaultValidationMode) {
    return WalkCourseSettingsView(context, defaultCourseFormat, defaultValidationMode);
  }

  @override
  Widget? buildBottomNavigationBar() => null;

  void _addLocationOverlayOnMap(MapController mapController) {
    _locationOverlay = LocationLayer(mapController, onlyAvatar: false);
    mapController.addLayer(_locationOverlay!);
  }

  void _addRouteOverlayOnMap(MapController mapController) {
    _routeOverlay = FlatRouteLayer(mapController);
    mapController.addLayer(_routeOverlay!);
  }
}
