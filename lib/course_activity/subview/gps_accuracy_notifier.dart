// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/device/gps/gps_location_listener.dart';
import 'package:vikazimut/l10n.dart';

class GpsAccuracyNotifier extends ChangeNotifier implements GpsLocationListener {
  double _accuracy = -1;

  Widget getWidget(int detectionRadius) => _GpsInfoView(gpsPanel: this, detectionRadius: detectionRadius);

  @override
  void onLocationChanged(Position location) {
    _accuracy = location.accuracy;
    notifyListeners();
  }
}

@immutable
class _GpsInfoView extends StatefulWidget {
  final GpsAccuracyNotifier gpsPanel;
  final int detectionRadius;

  const _GpsInfoView({required this.gpsPanel, required this.detectionRadius});

  @override
  _GpsAccuracyState createState() => _GpsAccuracyState();
}

class _GpsAccuracyState extends State<_GpsInfoView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ChangeNotifierProvider.value(
        value: widget.gpsPanel,
        child: Consumer<GpsAccuracyNotifier>(
          builder: (context, gpsPanel, child) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RichText(
                  text: TextSpan(
                    text: '${L10n.getString("gps_info_accuracy")} ',
                    style: const TextStyle(fontSize: 14),
                    children: <TextSpan>[
                      TextSpan(text: _formatAccuracy(gpsPanel), style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                      const TextSpan(text: ' m', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    text: '${L10n.getString("gps_info_detection_radius")} ',
                    style: const TextStyle(fontSize: 14),
                    children: <TextSpan>[
                      TextSpan(text: '${widget.detectionRadius}', style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                      const TextSpan(text: ' m', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  static String _formatAccuracy(GpsAccuracyNotifier gpsPanel) {
    if (gpsPanel._accuracy >= 0) {
      return gpsPanel._accuracy.toInt().toString();
    } else {
      return "   ";
    }
  }
}
