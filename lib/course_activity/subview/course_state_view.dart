// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/time.dart';

import '../course_presenter.dart';

@immutable
class CourseStateView {
  static const _ROW_HEIGHT = 32.0;
  late final QuestionDialog dialog;
  final ValueNotifier _updateCounter = ValueNotifier<int>(0);

  CourseStateView(BuildContext context, CoursePresenter presenter) {
    dialog = QuestionDialog(
      title: L10n.getString("course_state_message"),
      subtitle: _createCourseDescription(presenter),
      content: _buildDialogContent(context, presenter),
      actions: <Widget>[
        TextButton(
          child: Text(L10n.getString("ok"), style: const TextStyle(color: kGreenColor)),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }

  void updateContent() => _updateCounter.value++;

  Widget _buildDialogContent(BuildContext context, CoursePresenter presenter) {
    return ValueListenableBuilder(
      valueListenable: _updateCounter,
      builder: (context, value, child) {
        final List<DataRow> rows = _createTableRows(presenter.punchTimes, presenter);
        Widget table = _createTable(rows);
        ScrollController scrollController = _createScrollController(presenter, context);
        return Container(
          color: kGreenColor.withValues(alpha: 0.1),
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              if (presenter.format.isFreeOrder())
                Text.rich(
                  textAlign: TextAlign.start,
                  TextSpan(
                    text: L10n.getString("course_state_score"),
                    style: const TextStyle(fontWeight: FontWeight.bold),
                    children: <InlineSpan>[
                      TextSpan(
                        text: "${presenter.getCurrentScore()}/${presenter.getTotalScore()}",
                        style: const TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
              Text.rich(
                  textAlign: TextAlign.start,
                  TextSpan(
                    text: L10n.getString("course_state_missing_controls"),
                    style: const TextStyle(fontWeight: FontWeight.bold),
                    children: <InlineSpan>[
                      TextSpan(
                        text: "${presenter.getNumberOfMissingPunches()}",
                        style: const TextStyle(fontWeight: FontWeight.normal, color: Colors.red),
                      ),
                    ],
                  ),
                ),
              Expanded(
                child: Scrollbar(
                  controller: scrollController,
                  trackVisibility: true,
                  child: SingleChildScrollView(
                    controller: scrollController,
                    child: Column(
                      children: [
                        table,
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  ScrollController _createScrollController(CoursePresenter presenter, BuildContext context) {
    double scrollOffset = 180 + presenter.lastPunchedCheckpoint * _ROW_HEIGHT - (MediaQuery.of(context).size.height / 2.0);
    var scrollController = ScrollController(
      initialScrollOffset: scrollOffset,
      keepScrollOffset: false,
    );
    return scrollController;
  }

  List<DataRow> _createTableRows(List<int> punchTimes, CoursePresenter presenter) {
    final List<DataRow> rows = [];
    for (int i = 0; i < punchTimes.length; i++) {
      var checkpointName = ' (${presenter.getCheckpointName(i)})';
      String controlName;
      if (i == 0) {
        controlName = L10n.getString("cp_start") + checkpointName;
      } else if (i == punchTimes.length - 1) {
        controlName = L10n.getString("cp_finish") + checkpointName;
      } else {
        controlName = L10n.getString("checkpoint_number") + sprintf("%d", [i]) + checkpointName;
      }
      final String time;
      final Color color;
      if (punchTimes[i] >= 0) {
        time = formatTimeAsString(punchTimes[i]);
        color = Colors.black;
      } else {
        time = "_:_ _:_ _";
        color = kRedColor;
      }
      rows.add(
        DataRow(
          cells: [
            DataCell(Text(controlName, textAlign: TextAlign.left, style: TextStyle(color: color))),
            DataCell(Text(time)),
          ],
        ),
      );
    }
    return rows;
  }

  Widget _createTable(List<DataRow> rows) {
    Widget table = DataTable(
      horizontalMargin: 0,
      dataRowMaxHeight: _ROW_HEIGHT,
      dataRowMinHeight: _ROW_HEIGHT,
      columnSpacing: 10,
      border: TableBorder(
        horizontalInside: BorderSide(
          width: 1,
          color: kGreenColor.withValues(alpha: 0.3),
          style: BorderStyle.solid,
        ),
      ),
      dividerThickness: 0,
      columns: <DataColumn>[
        DataColumn(
          label: Align(
            alignment: Alignment.center,
            child: Text(L10n.getString("checkpoint_title")),
          ),
        ),
        DataColumn(
          label: Text(L10n.getString("visited_time_title"), textAlign: TextAlign.center),
        ),
      ],
      rows: rows,
    );
    return table;
  }

  static _createCourseDescription(CoursePresenter presenter) {
    String subtitle;
    final courseMode = presenter.courseMode;
    if (courseMode == CourseMode.SPORT) {
      subtitle = L10n.getString("sport_mode");
    } else if (courseMode == CourseMode.PLAYFUL) {
      subtitle = L10n.getString("playful_mode");
    } else {
      subtitle = L10n.getString("walk_mode");
    }
    subtitle += " - ";
    final format = presenter.format;
    if (format == CourseFormat.PRESET) {
      subtitle += L10n.getString("preset_order");
    } else {
      subtitle += L10n.getString("free_order");
    }
    return subtitle;
  }
}
