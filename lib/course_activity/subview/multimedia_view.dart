// coverage:ignore-file
import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../course_presenter.dart';

class MultimediaView {
  static Future<void> showAndExecActionAtTheEnd(
    final BuildContext context,
    String urlOrText,
    final VoidCallback? postAction,
    CoursePresenter coursePresenter,
  ) async {
    await showGeneralDialog(
      context: context,
      barrierColor: kOrangeColor,
      barrierDismissible: false,
      pageBuilder: (_, __, ___) {
        return SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 10,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: kOrangeColorDark,
                        width: 1,
                      ),
                      color: kOrangeColorUltraLight,
                    ),
                    child: _WebWidget(urlOrText: urlOrText, coursePresenter: coursePresenter),
                  ),
                ),
                SizedBox(
                  height: 70,
                  child: Container(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      child: Text(
                        L10n.getString("continue_route"),
                        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                      onPressed: () {
                        if (postAction != null) {
                          postAction();
                        }
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

@visibleForTesting
String createUrlFromText_(String urlOrText) {
  String formatToHtml(String text) {
    return """<!DOCTYPE html><html lang="${L10n.locale.languageCode}"><head><meta charset="UTF-8"><title></title><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body style="margin-top: 20px;"><pre style="font-family: helvetica, sans-serif">$text</pre></body></html>""";
  }

  if (urlOrText.trim().startsWith("http")) {
    return urlOrText.trim();
  } else {
    return Uri.dataFromString(formatToHtml(urlOrText), mimeType: 'text/html', encoding: Encoding.getByName('utf-8')).toString();
  }
}

@immutable
class _WebWidget extends StatefulWidget {
  final String _url;
  final CoursePresenter coursePresenter;

  _WebWidget({required urlOrText, required this.coursePresenter}) : _url = createUrlFromText_(urlOrText);

  @override
  _WebWidgetState createState() => _WebWidgetState();
}

class _WebWidgetState extends State<_WebWidget> {
  late final WebViewController _webViewController;
  int _currentDisplayedWidgetIndex = 1;
  bool _end = false;
  int _pointsForThisQuestion = 0; // Used to prevent counting the good answer several times

  @override
  void initState() {
    _webViewController = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(
        NavigationDelegate(
          onNavigationRequest: (NavigationRequest request) {
            if (_end) {
              // Workaround to stop audio when closing webview
              _webViewController.runJavaScript(
                  'document.querySelectorAll("video, audio").forEach(media => media.pause());'
              );
              return NavigationDecision.prevent;
            } else {
              return NavigationDecision.navigate;
            }
          },
          onPageStarted: (value) {
            if (mounted) {
              setState(() => _currentDisplayedWidgetIndex = 1);
            }
          },
          onPageFinished: (value) {
            if (mounted) {
              setState(() {
                _currentDisplayedWidgetIndex = 0;
                _end = true;
              });
            }
          },
        ),
      )
      ..addJavaScriptChannel('Vikazimut', onMessageReceived: (JavaScriptMessage javascript) {
        // Communication from Javascript to app
        int currentTotalPoints = math.max(0, widget.coursePresenter.quizTotalPoints);
        try {
          var pointEarned = double.parse(javascript.message);
          if (_pointsForThisQuestion == 0) {
            _pointsForThisQuestion = pointEarned.ceil();
            currentTotalPoints += pointEarned.ceil();
          }
        } catch (_) {}
        widget.coursePresenter.quizTotalPoints = currentTotalPoints;
      })
      ..loadRequest(Uri.parse(widget._url));
    super.initState();
  }

  @override
  void dispose() {
    // Workaround to stop audio when closing webview
    _webViewController.loadRequest(Uri.parse("about:blank"));
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: _currentDisplayedWidgetIndex,
      children: <Widget>[
        WebViewWidget(controller: _webViewController),
        const Center(
          child: CircularProgressIndicator(),
        ),
      ],
    );
  }
}
