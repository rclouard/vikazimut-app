// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';

import '../course_presenter.dart';

@immutable
class LastPunchedText extends StatefulWidget {
  @override
  LastPunchedTextState createState() => LastPunchedTextState();
}

class LastPunchedTextState extends State<LastPunchedText> {
  @override
  Widget build(BuildContext context) {
    return Consumer<CoursePresenter>(
      builder: (context, coursePresenter, child) {
        return Text(
          _formatText(coursePresenter),
          style: const TextStyle(fontSize: 16),
        );
      },
    );
  }

  String _formatText(CoursePresenter coursePresenter) {
    int checkpointId = coursePresenter.lastPunchedCheckpoint;
    if (checkpointId < 0) {
      return "";
    } else if (checkpointId == 0) {
      return '\u2713 ${L10n.getString("cp_start")}';
    } else if (coursePresenter.courseMode == CourseMode.WALK) {
      return '\u2713 ${L10n.getString("last_punched_display", [checkpointId])}';
    } else {
      String checkpointName = coursePresenter.getCheckpointName(coursePresenter.lastPunchedCheckpoint);
      return '\u2713 ${L10n.getString("last_punched_display_with_name", [checkpointId, checkpointName])}';
    }
  }
}
