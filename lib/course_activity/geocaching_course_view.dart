// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/result_activity/geocaching_global_result_view.dart';

import 'abstract_course_view.dart';
import 'sport_course_view.dart';

@immutable
class GeocachingCourseView extends SportCourseView {
  const GeocachingCourseView(
    super.selectedMap, {
    required super.courseMode,
    required super.preconfiguredByPlanner,
    required super.courseResultEntity,
  }) : super(withMultimediaContent: true);

  @override
  AbstractCourseViewState createState() => GeocachingCourseViewState();
}

class GeocachingCourseViewState extends SportCourseViewState {
  @override
  Future<void> gotoToResultPage(int itemIndex) async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => GeocachingGlobalResultView(resultId: itemIndex)),
    );
  }
}
