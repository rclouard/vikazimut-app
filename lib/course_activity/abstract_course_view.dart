// coverage:ignore-file
import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/device/compass/compass_widget.dart';
import 'package:vikazimut/device/gps/gps_location_listener.dart';
import 'package:vikazimut/device/qrcode/qrcode_scanner.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_view/layers/start_marker_layer.dart';
import 'package:vikazimut/map_view/map_controller.dart';
import 'package:vikazimut/map_view/map_view.dart';
import 'package:vikazimut/orienteering_map/lat_lon_box.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/toast.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

import 'course_presenter.dart';
import 'forcing/abstract_checkpoint_validation_forcing.dart';
import 'setting_widget/abstract_course_settings_view.dart';
import 'start_widget/start_toast_message.dart';
import 'start_widget/text_bubble.dart';
import 'subview/course_state_view.dart';
import 'subview/multimedia_view.dart';

@immutable
abstract class AbstractCourseView extends StatefulWidget {
  final OrienteeringMap _map;
  final CourseResultEntity? courseResultEntity;
  final CourseMode courseMode;
  final bool preconfiguredByPlanner;
  final bool withMultimediaContent;

  const AbstractCourseView(
    this._map, {
    required this.courseMode,
    required this.preconfiguredByPlanner,
    required this.courseResultEntity,
    required this.withMultimediaContent,
  });
}

abstract class AbstractCourseViewState extends State<AbstractCourseView> implements GpsLocationListener {
  late final CoursePresenter _coursePresenter;
  late final Future<ui.Image> _mapImageLoader;
  final MapController _mapController = MapController();
  StartMessenger? _startToastMessage;
  TextBubble? _textBubble;
  StartMarkerLayer? _startMarkerOverlay;
  bool _courseAlreadySet = false;
  bool darkMode = false;
  CourseStateView? _courseStateDialog;

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    _mapImageLoader = MapController.loadImage(File(widget._map.drawableName));
    _coursePresenter = CoursePresenter(this, widget._map, widget.courseResultEntity);
    if (widget.courseResultEntity != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await _coursePresenter.restoreCourseDataFromDatabaseAfterCrash();
        setState(() {});
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    closeTextBubble();
    WakelockPlus.disable();
    _coursePresenter.dispose();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CoursePresenter>.value(
      value: _coursePresenter,
      child: PopScope(
        canPop: false,
        onPopInvokedWithResult: (didPop, result) async {
          if (didPop) return;
          await _onBackPressed(context);
        },
        child: Scaffold(
          backgroundColor: (darkMode) ? Colors.black : kOrangeColor,
          appBar: buildCourseAppBarWidget(),
          body: _buildCourseBodyWidget(),
          bottomNavigationBar: buildBottomNavigationBar(),
        ),
      ),
    );
  }

  Widget _buildCourseBodyWidget() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.black,
      child: FutureBuilder<ui.Image>(
        future: _mapImageLoader,
        builder: (BuildContext context, AsyncSnapshot<ui.Image> image) {
          if (image.hasError) {
            return Center(child: Text("Error: ${image.error}"));
          } else if (!image.hasData) {
            return const Center(child: CircularProgressIndicator());
          } else {
            if (!_courseAlreadySet) {
              _courseAlreadySet = true;
              WidgetsBinding.instance.addPostFrameCallback((_) {
                _coursePresenter.setCourseSettings(widget.courseMode, widget.preconfiguredByPlanner, widget._map, widget.withMultimediaContent);
              });
            }
            return Stack(
              alignment: Alignment.bottomLeft,
              children: [
                _buildMapView(
                  context: context,
                  image: image.data!,
                  geoPosition: widget._map.getBoundingBox(),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: RepaintBoundary(child: CompassWidget(mapRotationInDegree: widget._map.rotation)),
                ),
              ],
            );
          }
        },
      ),
    );
  }

  PreferredSizeWidget buildCourseAppBarWidget();

  Widget? buildBottomNavigationBar();

  CourseSettingsView buildCourseSettingsDialog(CourseFormat? defaultCourseFormat, ValidationMode? defaultValidationMode);

  @override
  void onLocationChanged(Position location) => _coursePresenter.onLocationChanged(location);

  Widget get chronometerWidget => _coursePresenter.getChronometerWidget();

  Widget get gpsInformationWidget => _coursePresenter.getGpsInformationWidget();

  MapController get mapController => _mapController;

  List<GeodesicPoint> get waypoints => _coursePresenter.waypoints;

  GeodesicPoint? get lastKnownLocation => _coursePresenter.getLastKnownLocation();

  void incrementAssistanceCount() => _coursePresenter.incrementAssistanceCount();

  void initializeCourseMenu(ValidationMode validationMode);

  void startOrEndCourseWithAutoValidation(BuildContext context) {
    if (!_coursePresenter.isStartProcedureLaunched) {
      _coursePresenter.launchStartProcedure();
    } else {
      if (isCourseStarted) {
        askStopConfirmation(context);
      } else {
        askQuitConfirmation(context);
      }
    }
  }

  void activateStartButton();

  @mustCallSuper
  void handlePrepareCourse() {
    closeTextBubble();
    _startToastMessage?.displayMessage();
    WakelockPlus.enable();
    if (_startMarkerOverlay != null) {
      _mapController.removeLayer(_startMarkerOverlay!);
      _startMarkerOverlay = null;
    }
  }

  @mustCallSuper
  void handleStartCourse() {
    _closeStartToastMessage();
  }

  @mustCallSuper
  void handleStopCourse() async {
    WakelockPlus.disable();
  }

  Future<void> scanQrCode(BuildContext context) async {
    final String? qrCodeText = await Navigator.push<String>(
      context,
      MaterialPageRoute(builder: (context) => QrCodeScanner()),
    );
    if (qrCodeText != null) {
      _coursePresenter.punchNamedCheckpointFromQrCode(qrCodeText);
    }
  }

  Future<void> gotoToResultPage(int itemIndex);

  bool get isCourseStarted => _coursePresenter.isStarted;

  void closeTextBubble() {
    _textBubble?.hide();
    _textBubble = null;
  }

  void askQuitConfirmation(BuildContext context) {
    showPatternConfirmationDialog(
      context,
      title: L10n.getString("quit_course_confirmation_title"),
      message: L10n.getString("quit_course_confirmation_message"),
      yesButtonAction: _quitCourseWithoutSavingResult,
    );
  }

  void askStopConfirmation(BuildContext context) {
    showPatternConfirmationDialog(
      context,
      title: L10n.getString("stop_course_confirmation_title"),
      message: L10n.getString("quit_course_confirmation_message"),
      yesButtonAction: _coursePresenter.stopCourseImmediately,
    );
  }

  void showCourseState() async {
    _courseStateDialog = CourseStateView(context, _coursePresenter);
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return _courseStateDialog!.dialog;
      },
    );
    _courseStateDialog = null;
  }

  void notifyQRCheckpointIsMissing() => ManualValidationForcingHandler(context, _coursePresenter).execute(true);

  void forceValidationQRCheckpoint() => ManualValidationForcingHandler(context, _coursePresenter).execute(false);

  void forceValidationGPSCheckpoint({required bool detailed}) => AutoValidationForcingHandler(context, _coursePresenter).execute(detailed);

  Future<void> displayMultimediaContents(String text, final void Function()? postAction) => MultimediaView.showAndExecActionAtTheEnd(context, text, postAction, _coursePresenter);

  void checkPositionIsInMapBounds(double? latitude, double? longitude) => _coursePresenter.checkPositionIsInMapBounds(latitude, longitude);

  void displayToastMessage(String message, int duration, [Color? color]) => Toast.makeText(context, message, duration, color: color).show();

  void displayErrorDialog({required title, required String message}) => showErrorDialog(context, title: title, message: message);

  void displayConfirmationDialog({required String title, required String message, required String yesButtonText, required String noButtonText, VoidCallback? yesButtonAction, VoidCallback? noButtonAction}) {
    showConfirmationDialog(
      context,
      title: title,
      message: message,
      yesButtonText: yesButtonText,
      yesButtonAction: yesButtonAction,
      noButtonText: noButtonText,
      noButtonAction: noButtonAction,
    );
  }

  void displayStartNotifications(CourseMode courseMode, ValidationMode validationMode, [int startCheckpointId = 0, String startCheckpointName = ""]) {
    _startToastMessage = StartMessenger(courseMode, validationMode, startCheckpointId, startCheckpointName);
    _textBubble = TextBubble(courseMode, validationMode, startCheckpointId, startCheckpointName);
    displayTextBubbleAndStartLocation(_textBubble!);
  }

  @mustCallSuper
  void displayTextBubbleAndStartLocation(TextBubble textBubble) {
    _startMarkerOverlay = StartMarkerLayer(_mapController, widget._map.getCheckpointLocationFromId(0));
    _mapController.addLayer(_startMarkerOverlay!);
    _mapController.invalidate();
  }

  Future<bool> askConfirmationForRogaine() async {
    bool response = false;
    await showConfirmationDialog(
      context,
      title: L10n.getString("rogaine_continuation_warning_title"),
      message: L10n.getString("rogaine_continuation_warning_message"),
      yesButtonText: L10n.getString("rogaine_continuation_option1"),
      noButtonText: L10n.getString("rogaine_continuation_option2"),
      yesButtonAction: () => response = true,
      noButtonAction: () => response = false,
    );
    return response;
  }

  void updateCourseState() => _courseStateDialog?.updateContent();

  void _closeStartToastMessage() {
    _startToastMessage?.hideMessage();
    _startToastMessage = null;
  }

  void _quitCourseWithoutSavingResult() async {
    await _coursePresenter.quitCourseWithoutSavingResult();
    closeTextBubble();
    _closeStartToastMessage();
    if (mounted) {
      Navigator.of(context).pop();
    }
  }

  Future<void> _onBackPressed(BuildContext context) async {
    String message = L10n.getString("back_pressed_error");
    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
  }

  Widget _buildMapView({required BuildContext context, required ui.Image image, required LatLonBox geoPosition}) {
    var renderBox = context.findRenderObject() as RenderBox;
    return MapView(
      image: image,
      geoPosition: geoPosition,
      size: renderBox.size,
      mapController: _mapController,
      visible: !darkMode,
    );
  }
}
