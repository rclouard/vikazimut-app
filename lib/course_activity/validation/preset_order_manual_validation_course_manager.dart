import 'package:flutter/foundation.dart';
import 'package:vikazimut/l10n.dart';

import '../course_presenter.dart';
import 'abstract_course_manager.dart';

@immutable
class PresetOrderManualValidationCourseManager extends AbstractCourseManager {
  const PresetOrderManualValidationCourseManager(super.coursePresenter, super.mode, super.format);

  @override
  void doPunchCheckpointFromMenu(CoursePresenter coursePresenter, int checkpointId) {
    if (checkpointId < 0) {
      if (!coursePresenter.isStarted) {
        coursePresenter.prepareCourse();
      }
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      _forceStopCourse(coursePresenter);
    }
  }

  @override
  void doPunchCheckpointFromDevice(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (!coursePresenter.isStarted) {
      _punchCheckpointWhenNotStarted(coursePresenter, checkpointId, forced);
    } else {
      _punchCheckpointWhenStarted(coursePresenter, checkpointId, forced);
    }
  }

  @override
  void doPunchCheckpointFromGPS(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (!coursePresenter.isStarted) {
      if (coursePresenter.isStartCheckpoint(checkpointId)) {
        coursePresenter.startCourse();
        coursePresenter.validate(checkpointId, L10n.getString("scanned_start_checkpoint"), forced);
        coursePresenter.displayMultimediaContents(checkpointId);
      }
    } else if (checkpointId == coursePresenter.getExpectedCheckpointIndex()) {
      if (coursePresenter.isLastCheckpoint(checkpointId)) {
        coursePresenter.validate(checkpointId, L10n.getString("scanned_end_checkpoint"), forced);
        coursePresenter.displayMultimediaContents(checkpointId, postAction: () => coursePresenter.stopCourse());
      } else {
        coursePresenter.validate(checkpointId, L10n.getString("scanned_checkpoint", checkpointId.toString()), forced);
      }
    }
  }

  void _punchCheckpointWhenNotStarted(CoursePresenter coursePresenter, int checkpointId, bool forced) {
    if (coursePresenter.isStartCheckpoint(checkpointId)) {
      coursePresenter.prepareCourse();
      coursePresenter.startCourse();
      coursePresenter.validate(checkpointId, L10n.getString("scanned_start_checkpoint"), forced);
      coursePresenter.displayMultimediaContents(checkpointId);
    } else {
      coursePresenter.invalidate(L10n.getString("checkpoint_error_title"), L10n.getString("course_not_started_message"));
    }
  }

  void _punchCheckpointWhenStarted(final CoursePresenter coursePresenter, final int checkpointId, bool forced) {
    if (coursePresenter.isLastCheckpoint(checkpointId)) {
      coursePresenter.validate(checkpointId, L10n.getString("scanned_end_checkpoint"), forced);
      coursePresenter.displayMultimediaContents(checkpointId, postAction: () => coursePresenter.stopCourse());
    } else if (coursePresenter.isCheckpointAlreadyPunched(checkpointId)) {
      String message;
      if (checkpointId == 0) {
        message = L10n.getString("course_already_started_message");
      } else {
        message = L10n.getString("already_scanned_checkpoint_message", checkpointId.toString());
      }
      coursePresenter.invalidate(L10n.getString("checkpoint_error_title"), message);
    } else {
      int expectedCheckpoint = coursePresenter.getExpectedCheckpointIndex();
      if (checkpointId == expectedCheckpoint) {
        coursePresenter.validate(checkpointId, L10n.getString("scanned_checkpoint", checkpointId.toString()), forced);
        coursePresenter.displayMultimediaContents(checkpointId);
      } else {
        coursePresenter.invalidateButAskForBypass(
          checkpointId,
          L10n.getString("checkpoint_error_title"),
          L10n.getString("wrong_checkpoint_message", [expectedCheckpoint.toString(), expectedCheckpoint.toString()]),
          forced,
        );
      }
    }
  }

  void _forceStopCourse(CoursePresenter coursePresenter) => coursePresenter.stopCourse();
}
