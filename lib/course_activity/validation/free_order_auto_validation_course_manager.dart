import 'package:flutter/foundation.dart';
import 'package:vikazimut/l10n.dart';

import '../course_presenter.dart';
import 'abstract_course_manager.dart';

@immutable
class FreeOrderAutoValidationCourseManager extends AbstractCourseManager {
  const FreeOrderAutoValidationCourseManager(super.coursePresenter, super.mode, super.format);

  @override
  void doPunchCheckpointFromMenu(CoursePresenter coursePresenter, int checkpointId) {
    if (checkpointId < 0) {
      coursePresenter.prepareCourse();
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      _forceStopCourse(coursePresenter, checkpointId);
    }
  }

  @override
  void doPunchCheckpointFromDevice(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (checkpointId < 0) {
      coursePresenter.prepareCourse();
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      _punchFinishCheckpoint(coursePresenter, checkpointId, forced);
    } else {
      doPunchCheckpointFromGPS(coursePresenter, checkpointId, forced: forced);
    }
  }

  @override
  void doPunchCheckpointFromGPS(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (!coursePresenter.isStarted) {
      if (coursePresenter.isStartCheckpoint(checkpointId)) {
        coursePresenter.startCourse();
        coursePresenter.validate(checkpointId, L10n.getString("scanned_start_checkpoint"), forced);
        coursePresenter.displayMultimediaContents(checkpointId);
      }
    } else if (coursePresenter.isLastCheckpoint(checkpointId) && coursePresenter.allIntermediateCheckpointsValidated()) {
      _punchFinishCheckpoint(coursePresenter, checkpointId, forced);
    } else if (!coursePresenter.isLastCheckpoint(checkpointId) && !coursePresenter.isCheckpointAlreadyPunched(checkpointId)) {
      coursePresenter.validate(checkpointId, L10n.getString("scanned_checkpoint", checkpointId.toString()), forced);
      coursePresenter.displayMultimediaContents(checkpointId);
    }
  }

  void _punchFinishCheckpoint(CoursePresenter coursePresenter, int checkpointId, bool forced) {
    coursePresenter.validate(checkpointId, L10n.getString("scanned_end_checkpoint"), forced);
    coursePresenter.displayMultimediaContents(checkpointId, postAction: () => coursePresenter.stopCourse());
  }

  void _forceStopCourse(CoursePresenter coursePresenter, int checkpointId) {
    if (coursePresenter.isCloseToCheckpoint(checkpointId)) {
      _punchFinishCheckpoint(coursePresenter, checkpointId, false);
    } else {
      coursePresenter.stopCourse();
    }
  }
}
