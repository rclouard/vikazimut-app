import 'package:flutter/foundation.dart';
import 'package:vikazimut/constants/global_constants.dart';

import '../course_presenter.dart';
import 'abstract_course_manager.dart';
import 'free_order_auto_validation_course_manager.dart';
import 'free_order_manual_validation_course_manager.dart';
import 'preset_order_auto_validation_course_manager.dart';
import 'preset_order_manual_validation_course_manager.dart';
import 'walk_course_manager.dart';

@immutable
class CourseManagerFactory {
  const CourseManagerFactory._();

  static AbstractCourseManager create(
    CoursePresenter coursePresenter,
    CourseMode courseMode,
    CourseFormat courseFormat,
    ValidationMode validationMode,
  ) {
    if (courseMode == CourseMode.WALK) {
      return WalkValidationCourseManager(coursePresenter, CourseMode.WALK, courseFormat);
    } else if (courseFormat.isFreeOrder() && (validationMode == ValidationMode.MANUAL || validationMode == ValidationMode.BEACON)) {
      return FreeOrderManualValidationCourseManager(coursePresenter, courseMode, courseFormat);
    } else if (courseFormat.isPresetOrder() && (validationMode == ValidationMode.MANUAL || validationMode == ValidationMode.BEACON)) {
      return PresetOrderManualValidationCourseManager(coursePresenter, courseMode, courseFormat);
    } else if (courseFormat.isFreeOrder() && validationMode == ValidationMode.GPS) {
      return FreeOrderAutoValidationCourseManager(coursePresenter, courseMode, courseFormat);
    } else {
      return PresetOrderAutoValidationCourseManager(coursePresenter, courseMode, courseFormat);
    }
  }
}
