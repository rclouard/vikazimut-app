import 'package:flutter/foundation.dart';
import 'package:vikazimut/l10n.dart';

import '../course_presenter.dart';
import 'abstract_course_manager.dart';

@immutable
class PresetOrderAutoValidationCourseManager extends AbstractCourseManager {
  const PresetOrderAutoValidationCourseManager(super.coursePresenter, super.mode, super.format);

  @override
  void doPunchCheckpointFromMenu(CoursePresenter coursePresenter, int checkpointId) {
    if (checkpointId < 0) {
      coursePresenter.prepareCourse();
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      _forceStopCourse(coursePresenter);
    }
  }

  @override
  void doPunchCheckpointFromDevice(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (checkpointId < 0) {
      coursePresenter.prepareCourse();
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      _punchFinishCheckpoint(coursePresenter, checkpointId, forced);
    } else {
      doPunchCheckpointFromGPS(coursePresenter, checkpointId, forced: forced);
    }
  }

  @override
  void doPunchCheckpointFromGPS(CoursePresenter coursePresenter, final int checkpointId, {bool forced = false}) {
    if (!coursePresenter.isStarted) {
      if (coursePresenter.isStartCheckpoint(checkpointId)) {
        coursePresenter.startCourse();
        coursePresenter.validate(checkpointId, L10n.getString("scanned_start_checkpoint"), forced);
        coursePresenter.displayMultimediaContents(checkpointId);
      }
    } else if (checkpointId == coursePresenter.getExpectedCheckpointIndex()) {
      if (coursePresenter.isLastCheckpoint(checkpointId)) {
        _punchFinishCheckpoint(coursePresenter, checkpointId, forced);
      } else {
        coursePresenter.validate(checkpointId, L10n.getString("scanned_checkpoint", checkpointId.toString()), forced);
        coursePresenter.displayMultimediaContents(checkpointId);
      }
    }
  }

  void _punchFinishCheckpoint(CoursePresenter coursePresenter, final int checkpointId, bool forced) {
    coursePresenter.validate(checkpointId, L10n.getString("scanned_end_checkpoint"), forced);
    coursePresenter.displayMultimediaContents(checkpointId, postAction: () => coursePresenter.stopCourse());
  }

  void _forceStopCourse(CoursePresenter coursePresenter) => coursePresenter.stopCourse();
}
