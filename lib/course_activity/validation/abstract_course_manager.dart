import 'package:flutter/foundation.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/device/beacon/beacon_reader.dart';
import 'package:vikazimut/device/qrcode/qrcode_scanner.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/orienteering_map/checkpoint_out_of_bounds_exception.dart';

import '../course_presenter.dart';

@immutable
abstract class AbstractCourseManager {
  final CoursePresenter _coursePresenter;
  final CourseMode courseMode;
  final CourseFormat format;

  const AbstractCourseManager(this._coursePresenter, this.courseMode, this.format);

  void punchNamedCheckpointFromDevice(String checkpointName) {
    try {
      int checkpointId = _coursePresenter.getCheckpointIdFromName(checkpointName);
      punchCheckpointFromDevice(checkpointId);
    } on CheckpointOutOfBoundsException {
      _coursePresenter.invalidate(L10n.getString("checkpoint_error_title"), L10n.getString("unknown_checkpoint_message"));
    }
  }

  void punchNamedCheckpointFromBeacon(String checkpointName) {
    try {
      int checkpointId;
      if (checkpointName == BeaconReader.START_BEACON_ID) {
        checkpointId = 0;
      } else if (checkpointName == BeaconReader.END_BEACON_ID) {
        checkpointId = _coursePresenter.getLastCheckpointId();
      } else {
        checkpointId = _coursePresenter.getCheckpointIdFromName(checkpointName);
      }
      if (!_coursePresenter.isCheckpointAlreadyPunched(checkpointId)) {
        punchCheckpointFromGPS(checkpointId);
      }
    } catch (_) {}
  }

  void punchCheckpointFromDevice(int checkpointId, {bool forced = false}) => doPunchCheckpointFromDevice(_coursePresenter, checkpointId, forced: forced);

  void punchCheckpointFromMenu(int checkpointId) => doPunchCheckpointFromMenu(_coursePresenter, checkpointId);

  void doPunchCheckpointFromDevice(CoursePresenter coursePresenter, int checkpointId, {bool forced = false});

  void doPunchCheckpointFromMenu(CoursePresenter coursePresenter, int checkpointId);

  void punchCheckpointFromGPS(int checkpointId) => doPunchCheckpointFromGPS(_coursePresenter, checkpointId);

  void doPunchCheckpointFromGPS(CoursePresenter coursePresenter, int checkpointId, {bool forced = false});

  void punchCheckpointFromQrCode(String qrCodeText) {
    String checkpointId = QrCodeScanner.extractCheckpointIdFromQrCodeText(qrCodeText);
    punchNamedCheckpointFromDevice(checkpointId);
  }
}
