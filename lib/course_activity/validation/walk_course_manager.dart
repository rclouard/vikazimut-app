import 'package:flutter/foundation.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

import '../course_presenter.dart';
import 'abstract_course_manager.dart';

@immutable
class WalkValidationCourseManager extends AbstractCourseManager {
  const WalkValidationCourseManager(super.coursePresenter, super.mode, super.format);

  @override
  void doPunchCheckpointFromMenu(CoursePresenter coursePresenter, int checkpointId) {
    if (checkpointId < 0) {
      _startCourse(coursePresenter);
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      _forceStopCourse(coursePresenter);
    }
  }

  @override
  void doPunchCheckpointFromDevice(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (!coursePresenter.isStarted || checkpointId < 0) {
      _startCourse(coursePresenter);
    }
    _doPunchCheckpointFromDevice(coursePresenter, checkpointId, forced);
  }

  @override
  void doPunchCheckpointFromGPS(CoursePresenter coursePresenter, int checkpointId, {bool forced = false}) {
    if (!coursePresenter.isStarted || coursePresenter.isCheckpointAlreadyPunched(checkpointId)) {
      return;
    }
    if (coursePresenter.isAllCheckpointsPunchedExcept(checkpointId)) {
      _stopCourse(coursePresenter, checkpointId, forced);
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      if (!coursePresenter.isStartAndFinishAtSamePosition() || coursePresenter.getPunchedCheckpointCount() > 1) {
        _punchIntermediateCheckpoint(coursePresenter, checkpointId, forced).whenComplete(() {
          _requestEndOfCourse(coursePresenter);
        });
      }
    } else {
      _punchIntermediateCheckpoint(coursePresenter, checkpointId, forced);
    }
  }

  void _requestEndOfCourse(CoursePresenter coursePresenter) {
    showConfirmationDialog(
      globalKeyContext.currentContext!,
      title: "",
      message: L10n.getString("stop_walk_message"),
      yesButtonText: L10n.getString("stop_walk_yes"),
      noButtonText: L10n.getString("stop_walk_no"),
      yesButtonAction: () {
        coursePresenter.stopCourse();
      },
    );
  }

  void _doPunchCheckpointFromDevice(CoursePresenter coursePresenter, int checkpointId, bool forced) {
    if (!coursePresenter.isStarted || coursePresenter.isCheckpointAlreadyPunched(checkpointId)) {
      coursePresenter.invalidate(
        L10n.getString("checkpoint_error_title"),
        L10n.getString("already_scanned_checkpoint_message", [checkpointId.toString()]),
      );
      return;
    }
    if (coursePresenter.isAllCheckpointsPunchedExcept(checkpointId)) {
      _stopCourse(coursePresenter, checkpointId, forced);
    } else if (coursePresenter.isLastCheckpoint(checkpointId)) {
      if (!coursePresenter.isStartAndFinishAtSamePosition() || coursePresenter.getPunchedCheckpointCount() > 1) {
        _punchIntermediateCheckpoint(coursePresenter, checkpointId, forced).whenComplete(() {
          _requestEndOfCourse(coursePresenter);
        });
      }
    } else {
      _punchIntermediateCheckpoint(coursePresenter, checkpointId, forced);
    }
  }

  Future<void> _punchIntermediateCheckpoint(CoursePresenter coursePresenter, int checkpointId, bool forced) {
    if (checkpointId == 0) {
      coursePresenter.validate(checkpointId, L10n.getString("scanned_checkpoint", L10n.getString("cp_start")), forced);
    } else {
      coursePresenter.validate(checkpointId, L10n.getString("scanned_checkpoint", checkpointId.toString()), forced);
    }
    return coursePresenter.displayMultimediaContents(checkpointId);
  }

  void _startCourse(CoursePresenter coursePresenter) {
    coursePresenter.prepareCourse();
    coursePresenter.startCourse();
  }

  void _stopCourse(CoursePresenter coursePresenter, int checkpointId, bool forced) {
    coursePresenter.validate(checkpointId, L10n.getString("scanned_end_checkpoint"), forced);
    coursePresenter.displayMultimediaContents(checkpointId, postAction: () => coursePresenter.stopCourse());
  }

  void _forceStopCourse(CoursePresenter coursePresenter) => coursePresenter.stopCourse();
}
