import 'package:flutter/foundation.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';

import 'abstract_course_view.dart';
import 'geocaching_course_view.dart';
import 'sport_course_view.dart';
import 'walk_course_view.dart';

@immutable
class CourseViewFactory {
  const CourseViewFactory._();

  static AbstractCourseView createCourse(
    CourseMode courseMode,
    OrienteeringMap selectedMap, {
    required bool preconfigured,
    CourseResultEntity? courseResultEntity,
  }) {
    switch (courseMode) {
      case CourseMode.SPORT:
        return SportCourseView(
          selectedMap,
          courseMode: courseMode,
          preconfiguredByPlanner: preconfigured,
          courseResultEntity: courseResultEntity,
        );
      case CourseMode.PLAYFUL:
        return GeocachingCourseView(
          selectedMap,
          courseMode: courseMode,
          preconfiguredByPlanner: preconfigured,
          courseResultEntity: courseResultEntity,
        );
      case CourseMode.WALK:
        return WalkCourseView(
          selectedMap,
          courseMode: courseMode,
          preconfiguredByPlanner: preconfigured,
          courseResultEntity: courseResultEntity,
        );
    }
  }
}
