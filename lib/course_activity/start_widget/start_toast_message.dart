// coverage:ignore-file
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/utils/toast.dart';

class StartMessenger {
  Toast? _toast;
  late final bool _infinity;

  StartMessenger(CourseMode courseMode, ValidationMode validationFormat, int startCheckpointId, String startCheckpointName) {
    if (validationFormat == ValidationMode.GPS) {
      if (courseMode == CourseMode.WALK) {
        _toast = Toast.makeText(globalKeyContext.currentContext!, L10n.getString("walk_started"), Toast.LENGTH_LONG);
        _infinity = false;
      } else {
        if (startCheckpointId > 0) {
          _toast = Toast.makeText(globalKeyContext.currentContext!, sprintf(L10n.getString("wait_validation_for_start_rogaine"), [startCheckpointId, startCheckpointName]), Toast.INFINITY);
        } else {
          _toast = Toast.makeText(globalKeyContext.currentContext!, L10n.getString("wait_validation_for_start"), Toast.INFINITY);
        }
        _infinity = true;
      }
    } else {
      _infinity = false;
    }
  }

  void displayMessage() {
    _toast?.show();
  }

  void hideMessage() {
    if (_infinity) {
      _toast?.dismiss();
    }
    _toast = null;
  }
}
