// coverage:ignore-file
import 'dart:core';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';

@immutable
class TextBubble {
  late final _Bubble _bubble;

  TextBubble(CourseMode courseMode, ValidationMode validationFormat, int startCheckpointId, String startCheckpointName) {
    String text;
    String? header;
    if (courseMode == CourseMode.WALK) {
      if (validationFormat == ValidationMode.GPS) {
        text = L10n.getString("auto_start_button_message_walk_mode");
      } else if (validationFormat == ValidationMode.BEACON) {
        text = L10n.getString("auto_start_button_message_walk_mode");
      } else {
        text = L10n.getString("qrcode_start_button_message_walk_mode");
      }
    } else {
      if (validationFormat == ValidationMode.GPS) {
        if (startCheckpointId > 0) {
          text = sprintf(L10n.getString("auto_start_button_message_gps_sport_mode_rogaine"), [startCheckpointId, startCheckpointName]);
          header = L10n.getString("auto_start_button_header_sport_mode_rogaine");
        } else {
          text = L10n.getString("auto_start_button_message_gps_sport_mode");
        }
      } else if (validationFormat == ValidationMode.BEACON) {
        text = L10n.getString("auto_start_button_message_beacon_sport_mode");
      } else {
        if (Platform.isAndroid) {
          if (startCheckpointId > 0) {
            text = sprintf(L10n.getString("qrcode_start_button_message_sport_mode_android_rogaine"), [startCheckpointId, startCheckpointName]);
            header = L10n.getString("auto_start_button_header_sport_mode_rogaine");
          } else {
            text = L10n.getString("qrcode_start_button_message_sport_mode_android");
          }
        } else {
          if (startCheckpointId > 0) {
            text = sprintf(L10n.getString("qrcode_start_button_message_sport_mode_ios_rogaine"), [startCheckpointId, startCheckpointName]);
            header = L10n.getString("auto_start_button_header_sport_mode_rogaine");
          } else {
            text = L10n.getString("qrcode_start_button_message_sport_mode_ios");
          }
        }
      }
    }
    _bubble = _Bubble(
      backgroundColor: kGreenColor,
      header: header,
      message: text,
    );
  }

  void show(GlobalKey btnKey) {
    _bubble.show(widgetKey: btnKey);
  }

  void hide() {
    _bubble.dismiss();
  }
}

class _Bubble {
  static const double _itemWidth = 180.0;
  static const double _arrowHeight = 10.0;
  late OverlayEntry _entry;
  final String? _header;
  final String _message;
  late Offset _offset;
  late Rect _showRect;
  late Size _screenSize;
  late final Color _backgroundColor;
  bool _isShow = false;

  _Bubble({required Color backgroundColor, required String message, required String? header})
      : _backgroundColor = backgroundColor,
        _header = header,
        _message = message;

  void show({required GlobalKey widgetKey}) {
    var context = widgetKey.currentContext!;
    _showRect = _Bubble._getWidgetGlobalRect(widgetKey);
    _screenSize = View.of(context).physicalSize / View.of(context).devicePixelRatio;
    _offset = _calculateOffset();
    _entry = OverlayEntry(builder: (context) {
      return _buildBubbleLayout(_offset);
    });
    Overlay.of(context).insert(_entry);
    _isShow = true;
  }

  static Rect _getWidgetGlobalRect(GlobalKey key) {
    RenderBox? renderBox = key.currentContext!.findRenderObject() as RenderBox;
    var offset = renderBox.localToGlobal(Offset.zero);
    return Rect.fromLTWH(offset.dx, offset.dy, renderBox.size.width, renderBox.size.height);
  }

  Offset _calculateOffset() {
    double dx = _showRect.left + _showRect.width / 2.0 - _itemWidth / 2.0;
    if (dx < 10.0) {
      dx = 10.0;
    }
    if (dx + _itemWidth > _screenSize.width && dx > 10.0) {
      double tempDx = _screenSize.width - _itemWidth - 10;
      if (tempDx > 10) {
        dx = tempDx;
      }
    }
    double dy = _arrowHeight + _showRect.height + _showRect.top;
    return Offset(dx, dy);
  }

  LayoutBuilder _buildBubbleLayout(Offset offset) {
    return LayoutBuilder(builder: (context, constraints) {
      return Material(
        type: MaterialType.transparency,
        child: Stack(
          children: [
            Positioned(
              left: _showRect.left + _showRect.width / 2.0 - 7.5,
              top: offset.dy - _arrowHeight,
              child: CustomPaint(
                size: const Size(15.0, _arrowHeight),
                painter: _TrianglePainter(color: _backgroundColor),
              ),
            ),
            // menu content
            Positioned(
              left: offset.dx,
              top: offset.dy,
              child: SizedBox(
                width: _itemWidth,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                    width: _itemWidth,
                    padding: const EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      color: _backgroundColor,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Column(
                      children: [
                        if (_header != null)
                          Padding(
                            padding: const EdgeInsets.only(bottom: 15),
                            child: Text(
                              _header!,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: Colors.amberAccent,
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        Text(
                          _message,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            color: Colors.white,
                            decoration: TextDecoration.none,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    });
  }

  void dismiss() {
    if (_isShow) {
      _entry.remove();
      _isShow = false;
    }
  }
}

@immutable
class _TrianglePainter extends CustomPainter {
  final Color color;

  const _TrianglePainter({required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..strokeWidth = 2.0
      ..color = color
      ..style = PaintingStyle.fill;

    Path path = Path();
    path.moveTo(size.width / 2.0, 0.0);
    path.lineTo(0.0, size.height + 1);
    path.lineTo(size.width, size.height + 1);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
