// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';

import '../abstract_course_view.dart';

@immutable
class StartStopButton extends StatefulWidget {
  final AbstractCourseViewState parent;
  final VoidCallback onPressed;

  const StartStopButton({super.key, required this.parent, required this.onPressed});

  @override
  StartStopButtonState createState() => StartStopButtonState();
}

class StartStopButtonState extends State<StartStopButton> {
  late Icon _iconStartStop;
  ValidationMode? _validationMode;

  @override
  void initState() {
    _iconStartStop = const Icon(
      Icons.play_circle_outline,
      semanticLabel: "Start course",
    );
    super.initState();
  }

  void addMenuIcon(ValidationMode validationMode) {
    setState(() {
      _validationMode = validationMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_validationMode == null) {
      return const SizedBox.shrink();
    } else if (_validationMode == ValidationMode.GPS || _validationMode == ValidationMode.BEACON) {
      return IconButton(
        icon: _iconStartStop,
        tooltip: L10n.getString("start_stop_course_menu_item"),
        onPressed: () {
          setState(() {
            widget.onPressed();
            _iconStartStop = const Icon(
              Icons.stop_circle_outlined,
              semanticLabel: "Stop course",
            );
          });
          widget.parent.startOrEndCourseWithAutoValidation(context);
        },
        iconSize: 64,
      );
    } else {
      return IconButton(
        icon: const ImageIcon(
          AssetImage('assets/icons/icon_menu_qrcode.png'),
          semanticLabel: "Scan QR code",
        ),
        tooltip: L10n.getString("qr_code_menu_item"),
        onPressed: () {
          widget.onPressed();
          widget.parent.scanQrCode(context);
        },
        iconSize: 64,
      );
    }
  }

  void setStarted() {
    setState(() {
      widget.onPressed();
      _iconStartStop = const Icon(
        Icons.stop_circle_outlined,
        semanticLabel: "Stop course",
      );
    });
  }
}
