import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/database/waypoint_entity.dart';
import 'package:vikazimut/device/chronometer.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/course_leg_list.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_time.dart';

class Course {
  final OrienteeringMap _map;
  final Chronometer? _chronometer;
  final CourseLegList _courseLegs;
  CourseResultEntity? _courseResultEntity;
  int _nextCheckpointIndexToValidate = 0;
  int _startCheckpoint = 0;

  Course(this._map, [this._chronometer]) : _courseLegs = CourseLegList(_map.getCheckpointCount());

  OrienteeringMap get map => _map;

  int get quizTotalPoints => _courseResultEntity!.quizTotalPoints;

  bool get isAllIntermediateValidatedCheckpoints => _courseLegs.isAllIntermediateValidatedCheckpoints();

  int get currentScore => _courseLegs.currentScore;

  int getCheckpointCount() => _map.getCheckpointCount();

  int getCheckpointIdFromName(String checkpointNamed) => _map.getCheckpointIdFromName(checkpointNamed, _nextCheckpointIndexToValidate);

  String getCheckpointNameFromId(int checkpointId) => _map.getCheckpointNameFromId(checkpointId);

  List<int> getPunchTimesInMilliseconds() => _courseLegs.getPunchTimesAsMilliseconds();

  bool isInMapBounds(double latitude, double longitude) => _map.isInBounds(latitude, longitude);

  bool isStartAndFinishAtSamePosition() => _map.getCheckpoint(0).location.distanceToInMeter(_map.getCheckpoint(getLastCheckpointId()).location) < 5;

  int getLastCheckpointId() => _map.getCheckpointCount() - 1;

  bool isStartCheckpoint(int checkpointId) => checkpointId == _startCheckpoint;

  bool isLastCheckpoint(int checkpointId) => checkpointId == _map.getCheckpointCount() - 1;

  bool isCheckpointAlreadyPunched(int checkpointId) => _courseLegs.isCheckpointPunched(checkpointId);

  bool isAllCheckpointsPunchedExcept(int checkpointId) => _courseLegs.isAllCheckpointPunchedExcept(checkpointId);

  bool _isLastCheckpointPunched() => _courseLegs.isLastCheckpointPunched();

  int getPunchedCheckpointCount() => _courseLegs.countPunchedCheckpoints();

  int getExpectedCheckpointIndex() => _nextCheckpointIndexToValidate;

  int getLastPunchedCheckpointIndex() => _courseLegs.getLastPunchedCheckpointIndex();

  void setCourseResultEntity(CourseResultEntity courseResultEntity) => _courseResultEntity = courseResultEntity;

  void resetPunchTimes(List<PunchTime> punchTimes) {
    _courseLegs.resetPunchTimes(punchTimes, _map.checkpoints);
  }

  void resetStartCheckpointAt(int checkpointId, CourseFormat format) {
    Checkpoint checkpoint = _map.getCheckpoint(checkpointId);
    _startCheckpoint = checkpointId;
    int score = (format.isPresetOrder()) ? 1 : checkpoint.points;
    _courseLegs.unpunch(checkpointId, score);
  }

  int startChronometer([int? baseTimeInMillisecond]) {
    _nextCheckpointIndexToValidate = _courseLegs.getNextCheckpointIndexInPresetOrder();
    return _chronometer!.start(baseTimeInMillisecond);
  }

  void stopChronometer() {
    _chronometer?.stop();
    if (_courseResultEntity != null) {
      _courseResultEntity!.totalTimeInMillisecond = _chronometer?.elapsedInMillisecond ?? 0;
    }
  }

  void incrementAssistanceCount() {
    _courseResultEntity!.assistanceCount++;
    ResultDatabaseGateway.storeAssistanceCount(_courseResultEntity!.id!, _courseResultEntity!.assistanceCount);
  }

  set quizTotalPoints(int points) {
    _courseResultEntity!.quizTotalPoints = points;
    ResultDatabaseGateway.storeQuizPoints(_courseResultEntity!.id!, points);
  }

  void bypassPreviousCheckpoints(int checkpointId) {
    _nextCheckpointIndexToValidate = checkpointId + 1;
  }

  GeodesicPoint? punchCheckpoint(int checkpointId, bool forced, CourseFormat format) {
    int elapsedTime = _chronometer!.elapsedInMillisecond;
    Checkpoint checkpoint = _map.getCheckpoint(checkpointId);
    return punchCheckpoint_(checkpointId, checkpoint, elapsedTime, forced, format);
  }

  @visibleForTesting
  GeodesicPoint? punchCheckpoint_(int checkpointId, Checkpoint checkpoint, int elapsedTime, bool forced, CourseFormat format) {
    GeodesicPoint geodesicPoint = checkpoint.location;
    int score = (format.isPresetOrder()) ? 1 : checkpoint.points;
    _courseLegs.punch(checkpointId, score, elapsedTime, forced);
    ResultDatabaseGateway.addPunchTime(checkpointId, elapsedTime, forced);
    geodesicPoint.timestampInMillisecond = elapsedTime;
    _nextCheckpointIndexToValidate = checkpointId + 1;
    return geodesicPoint;
  }

  Future<void> createAndStoreCourseResultEntity({
    required int mode,
    required int format,
    required int baseTimeInMillisecond,
    required int validationMode,
  }) async {
    _courseResultEntity = CourseResultEntity(
      mode: mode,
      format: format,
      validationMode: validationMode,
      baseTimeInMillisecond: baseTimeInMillisecond,
      discipline: _map.discipline.index,
      mapName: _map.name,
      checkpointCount: _map.getCheckpointCount(),
      courseId: _map.id,
    );
    _courseResultEntity!.id = await ResultDatabaseGateway.insertCourseResult(_courseResultEntity!);
  }

  Future<int> storeResultIntoDatabase() async {
    assert(_courseResultEntity != null);
    final List<WaypointEntity> waypointEntities = await ResultDatabaseGateway.findAllWaypoints();
    final List<GeodesicPoint> waypoints = WaypointEntity.toGeodesicPoints(waypointEntities);
    _courseResultEntity!.punchTimes = _courseLegs.getFinalPunchTimeListAsFormattedString(waypoints, _map.checkpoints, _courseResultEntity!.format, _map.detectionRadius);
    _courseResultEntity!.gpsTrack = GeodesicPoint.waypointsToString(waypoints);
    _courseResultEntity!.finished = 1;
    _courseResultEntity!.completed = _isLastCheckpointPunched() ? 1 : 0;
    await ResultDatabaseGateway.updateCourseResult(_courseResultEntity!);
    return _courseResultEntity!.id!;
  }

  Future<CourseResultEntity?> findUncompletedFreeOrderSportCourse() async => ResultDatabaseGateway.findUncompletedFreeOrderSportCourse(_map.name);

  List<int> getClosestUnpunchedCheckpoints(GeodesicPoint location) {
    List<Checkpoint> unPunchedCheckpoints = Course.getAllUnpunchedCheckpoints_(_map, this);
    return getIndexesOfClosestUnpunchedCheckpoints(location, unPunchedCheckpoints);
  }

  int getFirstUnpunchedCheckpointCloseToLocation(GeodesicPoint location, int minDistance) {
    List<Checkpoint> unPunchedCheckpoints = Course.getAllUnpunchedCheckpoints_(_map, this);
    return getFirstUnpunchedCheckpoint(location, unPunchedCheckpoints, minDistance);
  }

  @visibleForTesting
  static List<Checkpoint> getAllUnpunchedCheckpoints_(OrienteeringMap map, Course course) {
    List<Checkpoint> unPunchedCheckpoints = [];
    for (int i = 0; i < map.getCheckpointCount(); i++) {
      Checkpoint checkpoint = map.getCheckpoint(i);
      int id = checkpoint.index;
      if (!course.isCheckpointAlreadyPunched(id)) {
        unPunchedCheckpoints.add(checkpoint);
      }
    }
    return unPunchedCheckpoints;
  }

  static List<int> getIndexesOfClosestUnpunchedCheckpoints(GeodesicPoint currentLocation, List<Checkpoint> unPunchedCheckpoints) {
    List<Checkpoint> closestCheckpoints = unPunchedCheckpoints.toList(growable: false);
    closestCheckpoints.sort((a, b) {
      final distance1 = currentLocation.distanceToInMeter(a.location);
      final distance2 = currentLocation.distanceToInMeter(b.location);
      return distance1.compareTo(distance2);
    });
    List<int> result = [];
    for (final checkpoint in closestCheckpoints) {
      result.add(checkpoint.index);
    }
    return result;
  }

  static int getFirstUnpunchedCheckpoint(GeodesicPoint currentLocation, List<Checkpoint> unPunchedCheckpoints, int minDistance) {
    for (final checkpoint in unPunchedCheckpoints) {
      double distance = currentLocation.distanceToInMeter(checkpoint.location);
      if (distance < minDistance) {
        return checkpoint.index;
      }
    }
    return -1;
  }

  bool isCloseToCheckpoint(int checkpointId, GeodesicPoint? currentLocation) {
    final Checkpoint checkpoint = _map.getCheckpoint(checkpointId);
    final GeodesicPoint checkpointLocation = checkpoint.location;
    return Course.isCloseToCheckpoint_(currentLocation, checkpointLocation, _map.detectionRadius);
  }

  @visibleForTesting
  static bool isCloseToCheckpoint_(GeodesicPoint? currentLocation, GeodesicPoint checkpointLocation, int minDistance) {
    if (currentLocation == null) {
      return false;
    } else {
      return currentLocation.distanceToInMeter(checkpointLocation) <= minDistance;
    }
  }
}
