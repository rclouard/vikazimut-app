import 'package:flutter/foundation.dart';
import 'package:vikazimut/l10n.dart';

import 'abstract_global_result_presenter.dart';
import 'result.dart';

class WalkGlobalResultPresenter extends AbstractGlobalResultPresenter {
  WalkGlobalResultPresenter({required int resultId}) : super(resultId);

  @override
  bool get isSportMode => false;

  @override
  bool get isStatisticsAvailable => false;

  @override
  String createCourseFormatMessage() => L10n.getString(createCourseFormatMessage_());

  @visibleForTesting
  String createCourseFormatMessage_() => "walk_mode";

  @override
  bool isSendResultAvailable(Result result) => false;
}
