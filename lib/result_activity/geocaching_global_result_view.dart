// coverage:ignore-file
import 'package:flutter/material.dart';

import 'abstract_global_result_view.dart';
import 'geocaching_global_result_presenter.dart';
import 'result.dart';
import 'track/sport_track_view.dart';

class GeocachingGlobalResultView extends AbstractGlobalResultView {
  GeocachingGlobalResultView({required int resultId}) : super(GeocachingGlobalResultPresenter(resultId: resultId));

  @override
  Widget trackPageRoute(Result result) {
    return SportTrackView(
      map: result.map!,
      waypoints: result.waypoints,
      punchTimes: result.punchTimes,
      totalTimeInMs: result.totalTimeWithoutPenaltyInMs,
    );
  }
}
