// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/result_activity/profile/profile_analysis_widget.dart';
import 'package:vikazimut/theme.dart';

import '../result.dart';
import 'decision_tree/decision_tree.dart';
import 'profile_chart_widget.dart';
import 'profile_presenter.dart';
import 'rating_slider_widget.dart';
import 'speed_in_kmh.dart';

class ProfileView extends StatelessWidget {
  final Result result;
  late final ProfilePresenter _presenter;
  late final List<SpeedInKmH> _data;
  late final OrienteerProfile _profile;

  ProfileView(this.result) {
    _presenter = ProfilePresenter(result.map!.discipline);
    _data = _presenter.calculateSpeedHistogramFromWaypoints(result.waypoints, result.totalTimeWithoutPenaltyInMs);
    _profile = _presenter.calculateOrienteeringLevel(_data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.getString("race_analysis_title")),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        primary: true,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Card(
              margin: const EdgeInsets.fromLTRB(5, 10, 5, 5),
              child: Container(
                padding: const EdgeInsets.all(5),
                child: Column(
                  children: [
                    Column(
                      children: [
                        Text(
                          L10n.getString("orienteer_profile_widget_speed_histogram_title"),
                          style: _titleTextStyle,
                        ),
                        Text(
                          L10n.getString("orienteer_profile_widget_speed_histogram_note"),
                          style: _subtitleTextStyle,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    buildProfileChart(_data, _presenter.discipline, _presenter.decisionTree),
                  ],
                ),
              ),
            ),
            Card(
              margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(Icons.military_tech, color: kOrangeColor),
                      const SizedBox(width: 8),
                      Text(L10n.getString("orienteer_profile_estimated_level"), style: _titleTextStyle),
                    ],
                  ),
                  if (_data.length > 2)
                    buildRatingSlider(context, _profile.orienteerGrade)
                  else
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        L10n.getString("orienteer_profile_estimated_level_error"),
                        style: const TextStyle(fontStyle: FontStyle.italic, color: kRedColor, fontSize: 12),
                      ),
                    ),
                ],
              ),
            ),
            Card(
              margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
              child: ProfileDescriptionWidget(_profile, titleStyle: _titleTextStyle),
            ),
          ],
        ),
      ),
    );
  }
}

const TextStyle _titleTextStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: kOrangeColor);
const TextStyle _subtitleTextStyle = TextStyle(fontSize: 12, color: Colors.grey);
