import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'decision_tree/decision_tree.dart';
import 'decision_tree/foresto_decision_tree.dart';
import 'decision_tree/mtbo_decision_tree.dart';
import 'decision_tree/skio_decision_tree.dart';
import 'decision_tree/urbano_decision_tree.dart';
import 'speed_histogram.dart';
import 'speed_in_kmh.dart';

class ProfilePresenter {
  final Discipline? discipline;
  late final DecisionTree decisionTree;

  ProfilePresenter(this.discipline) {
    decisionTree = _init(discipline);
  }

  List<SpeedInKmH> calculateSpeedHistogramFromWaypoints(List<GeodesicPoint> waypoints, int totalTimeInMilliseconds) => SpeedHistogram.calculateSpeedHistogramFromWaypoints(
        waypoints,
        decisionTree.maxSpeed,
        totalTimeInMilliseconds,
      );

  OrienteerProfile calculateOrienteeringLevel(List<SpeedInKmH> speedHistogram) => decisionTree.calculateOrienteeringLevel(speedHistogram);

  static DecisionTree _init(Discipline? discipline) {
    if (discipline == null) {
      return ForestODecisionTree();
    }
    switch (discipline) {
      case Discipline.FORESTO:
        return ForestODecisionTree();
      case Discipline.URBANO:
        return UrbanODecisionTree();
      case Discipline.MTBO:
        return MTBODecisionTree();
      case Discipline.SKIO:
        return SkiODecisionTree();
    }
  }
}
