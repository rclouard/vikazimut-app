// coverage:ignore-file
import 'package:community_charts_flutter/community_charts_flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/utils/speed_color_handler.dart';

import 'decision_tree/decision_tree.dart';
import 'speed_in_kmh.dart';

Widget buildProfileChart(List<SpeedInKmH> data, Discipline? discipline, DecisionTree decisionTree) {
  return Container(
    height: 180,
    width: 580,
    constraints: const BoxConstraints(minHeight: 180),
    padding: const EdgeInsets.only(left: 10, right: 10),
    child: _buildChart(_buildSpeedHistogramChart(data, discipline, decisionTree.speedPerBin), decisionTree.getTicksXAxis()),
  );
}

List<charts.Series<SpeedInKmH, String>> _buildSpeedHistogramChart(List<SpeedInKmH> data, Discipline? discipline, int speedPerBin) {
  SpeedColorHandler speedColorHandler = SpeedColorHandlerFactory.build(discipline);
  return [
    charts.Series<SpeedInKmH, String>(
      id: 'Speed profile',
      colorFn: (SpeedInKmH item, __) => charts.ColorUtil.fromDartColor(
        speedColorHandler.colorFromVelocity(speedPerBin * item.speed.toDouble()),
      ),
      domainFn: (SpeedInKmH item, _) => item.speed.toString(),
      measureFn: (SpeedInKmH item, _) => item.percent,
      data: data,
    )
  ];
}

Widget _buildChart(
  List<charts.Series<SpeedInKmH, String>> seriesList,
  List<charts.TickSpec<String>> staticTicksXAxis,
) {
  final staticTicksYAxis = [for (var i = 0; i <= 40; i += 5) charts.TickSpec(i)];

  return charts.BarChart(
    seriesList,
    animate: false,
    defaultInteractions: false,
    defaultRenderer: charts.BarRendererConfig(
      cornerStrategy: const charts.ConstCornerStrategy(7),
    ),
    primaryMeasureAxis: charts.NumericAxisSpec(
      tickFormatterSpec: charts.BasicNumericTickFormatterSpec.fromNumberFormat(NumberFormat("### '%")),
      tickProviderSpec: charts.StaticNumericTickProviderSpec(staticTicksYAxis),
    ),
    domainAxis: charts.OrdinalAxisSpec(
      tickProviderSpec: charts.StaticOrdinalTickProviderSpec(staticTicksXAxis),
      renderSpec: const charts.SmallTickRendererSpec(
        tickLengthPx: 10,
        labelOffsetFromAxisPx: 15,
        labelStyle: charts.TextStyleSpec(fontSize: 10, lineHeight: 1),
      ),
    ),
  );
}
