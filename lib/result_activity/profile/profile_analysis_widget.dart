// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';

import 'decision_tree/decision_tree.dart';

class ProfileDescriptionWidget extends StatelessWidget {
  final OrienteerProfile _profile;
  final TextStyle titleStyle;

  const ProfileDescriptionWidget(this._profile, {required this.titleStyle});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.troubleshoot, color: kOrangeColor),
            const SizedBox(width: 8),
            Text(L10n.getString("orienteer_profile_analysis_title"), style: titleStyle),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            _profile.characteristics ?? "",
            style: _textStyleText,
            strutStyle: const StrutStyle(leading: 0.3),
            textAlign: TextAlign.start,
          ),
        ),
        const SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.thumb_up_alt, color: kOrangeColor),
            const SizedBox(width: 8),
            Text(L10n.getString("orienteer_profile_advice_title"), style: titleStyle),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            _profile.advices ?? "",
            style: _textStyleText,
            strutStyle: const StrutStyle(leading: 0.3),
            textAlign: TextAlign.start,
          ),
        ),
      ],
    );
  }
}

const TextStyle _textStyleText = TextStyle(fontSize: 15);
