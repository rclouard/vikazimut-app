import 'package:flutter/foundation.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';

import 'abstract_global_result_presenter.dart';
import 'result.dart';

class SportGlobalResultPresenter extends AbstractGlobalResultPresenter {
  SportGlobalResultPresenter({required int resultId}) : super(resultId);

  @override
  bool get isSportMode => true;

  @override
  bool get isStatisticsAvailable => true;

  @override
  String createCourseFormatMessage() {
    if (result == null) {
      return "";
    }
    String courseMode = createCourseFormatMessagePart1_();
    String courseOrder = createCourseFormatMessagePart2_();
    return '${L10n.getString(courseMode)}\n${L10n.getString(courseOrder)}';
  }

  @visibleForTesting
  String createCourseFormatMessagePart1_() {
    String courseMode;
    if (result!.mode.isGeocachingMode) {
      courseMode = "playful_mode";
    } else {
      courseMode = "sport_mode";
    }
    return courseMode;
  }

  @visibleForTesting
  String createCourseFormatMessagePart2_() {
    if (result!.format.isPresetFormat) {
      return "preset_order";
    } else {
      return "free_order";
    }
  }

  @override
  bool isSendResultAvailable(Result result) => !result.isSent;
}
