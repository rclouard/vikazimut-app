import 'dart:core';

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/web_navigator.dart';

import 'abstract_global_result_view.dart';
import 'gpx_file/gpx_file.dart';
import 'result.dart';
import 'sending/result_sending_presenter.dart';

abstract class AbstractGlobalResultPresenter {
  AbstractResultDisplayState? _view;
  final int resultId;
  Result? _result;

  AbstractGlobalResultPresenter(this.resultId);

  Result? get result => _result;

  @visibleForTesting
  set result(Result? value) {
    _result = value;
  }

  bool get isSportMode;

  int? get totalCheckPoints => _result?.totalCheckpoints;

  bool get isStatisticsAvailable;

  set view(AbstractResultDisplayState view) => _view = view;

  String createCourseFormatMessage();

  String createVerdictMessage() {
    if (result == null) {
      return "";
    }
    if (result!.isAllCheckpointsScanned) {
      return L10n.getString("course_success");
    } else {
      return L10n.getString("course_failure_missing");
    }
  }

  bool isSendResultAvailable(Result result);

  String createPenaltyMessage() {
    if (_result != null && _result!.assistanceCount > 0) {
      return "${L10n.getString("penalty_message")} ${_result!.penaltyTimeAsString}";
    } else {
      return "";
    }
  }

  Future<Result?> getResultFromDatabase(int resultId) async {
    if (_result != null) {
      return _result;
    }
    CourseResultEntity? courseResultEntity = await ResultDatabaseGateway.findCourseResult(resultId);
    if (courseResultEntity != null) {
      _result = Result(courseResultEntity)..computeStatisticsFromResult();
      return _result;
    }
    return null;
  }

  void onSendResultToServer(BuildContext context) {
    if (_result!.waypoints.isEmpty) {
      showErrorDialog(
        context,
        title: L10n.getString("export_trace_to_gpx_file_empty_error_title"),
        message: L10n.getString("export_trace_to_gpx_file_empty_error_message"),
      );
    } else {
      _result?.isSent = true;
      _view!.updateSendButton();
      ResultSendingPresenter(this).onSendResultToServer(context);
    }
  }

  void handlePositiveSendingResultsToServerResponse() {
    _disableSentFlagForCourseEntity();
    _result?.isSent = true;
    _view!.updateSendButton();
    _launchWebBrowserWithTheRoutePage(_view!.context);
  }

  void handleNegativeSendingResultsToServerResponse(String message) {
    _result?.isSent = false;
    _view?.displayMessage(message);
    _view!.updateSendButton();
  }

  void exportGPXFile(BuildContext context, Result result) => GPXFile.exportGPXFile(context, result);

  void _disableSentFlagForCourseEntity() => ResultDatabaseGateway.disableSentFlagForCourseResult(resultId);

  void _launchWebBrowserWithTheRoutePage(BuildContext context) async {
    await showConfirmationDialog(
      context,
      title: L10n.getString("web_browser_dialog_title2"),
      message: L10n.getString("web_browser_dialog_message2"),
      yesButtonText: L10n.getString("web_browser_dialog_yes_button"),
      noButtonText: L10n.getString("no"),
      yesButtonAction: () => openWebBrowserOnVikazimutPage(context, ServerConstants.getWebResultPageURL(_result!.courseId)),
    );
  }

  void handleCancel() {
    _result?.isSent = false;
    _view!.updateSendButton();
  }
}
