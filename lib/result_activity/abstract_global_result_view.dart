// coverage:ignore-file
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/time.dart';
import 'package:vikazimut/utils/web_navigator.dart';

import 'abstract_global_result_presenter.dart';
import 'profile/profile_view.dart';
import 'result.dart';
import 'time/time_table_view.dart';

@immutable
abstract class AbstractGlobalResultView extends StatefulWidget {
  final AbstractGlobalResultPresenter presenter;

  const AbstractGlobalResultView(this.presenter);

  @override
  AbstractResultDisplayState createState() => AbstractResultDisplayState();

  Widget trackPageRoute(Result result);
}

class AbstractResultDisplayState extends State<AbstractGlobalResultView> {
  @override
  Widget build(BuildContext context) {
    widget.presenter.view = this;
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (didPop, result) async {
        if (didPop) return;
        final navigator = Navigator.of(context);
        final isBack = await _onBackPressed(context);
        if (isBack) navigator.pop();
      },
      child: FutureBuilder<Result?>(
        future: widget.presenter.getResultFromDatabase(widget.presenter.resultId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!.map == null) {
              return Scaffold(
                appBar: AppBar(
                  title: Text(snapshot.data!.mapName),
                ),
                body: const _NoMapErrorWidget(),
              );
            } else {
              return Scaffold(
                appBar: AppBar(
                  title: Text(snapshot.data!.mapName),
                ),
                bottomNavigationBar: BottomAppBar(
                  height: 58,
                  child: _ClubInformationWidget(snapshot.data!),
                ),
                body: _GlobalResultWidget(
                  result: snapshot.data!,
                  presenter: widget.presenter,
                  trackPageRoute: widget.trackPageRoute,
                ),
              );
            }
          } else if (snapshot.hasError) {
            return Scaffold(
              appBar: AppBar(),
              body: Center(
                child: Text("Error: ${snapshot.error}"),
              ),
            );
          } else {
            return Scaffold(
              appBar: AppBar(),
              body: Container(),
            );
          }
        },
      ),
    );
  }

  void updateSendButton() => setState(() {});

  void displayMessage(String message) {
    showWarningDialog(
      context,
      title: L10n.getString("export_trace_to_gpx_file"),
      message: message,
      yesButtonText: L10n.getString("ok"),
    );
  }

  Future<bool> _onBackPressed(BuildContext context) async {
    bool response = false;
    await showConfirmationDialog(
      context,
      title: L10n.getString("really_exit"),
      message: L10n.getString("result_exit_message"),
      yesButtonText: L10n.getString("quit_result"),
      noButtonText: L10n.getString("do_not_quit_result"),
      yesButtonAction: () => response = true,
      noButtonAction: () => response = false,
    );
    return response;
  }
}

@immutable
class _NoMapErrorWidget extends StatelessWidget {
  const _NoMapErrorWidget();

  @override
  Widget build(BuildContext context) {
    return buildDialogContent(
      context,
      dialogIcon: const Icon(Icons.close, size: 64, color: Colors.white),
      title: L10n.getString("error_title"),
      color: kRedDarkColor,
      message: L10n.getString("missing_map_message"),
      positiveButtonText: L10n.getString("ok"),
    );
  }
}

@immutable
class _GlobalResultWidget extends StatefulWidget {
  final Result result;
  final AbstractGlobalResultPresenter presenter;
  final Widget Function(Result result) trackPageRoute;

  const _GlobalResultWidget({
    required this.result,
    required this.presenter,
    required this.trackPageRoute,
  });

  @override
  State<_GlobalResultWidget> createState() => _GlobalResultWidgetState();
}

class _GlobalResultWidgetState extends State<_GlobalResultWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.all(4),
        constraints: const BoxConstraints(maxWidth: 380),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _VerdictWidget(
              context,
              widget.result,
              widget.presenter.createVerdictMessage(),
              widget.presenter.createCourseFormatMessage(),
            ),
            const SizedBox(height: 5),
            Flexible(
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: const BoxDecoration(
                    color: kOrangeColor,
                    borderRadius: BorderRadius.all(
                      Radius.circular(15.0),
                    ),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (widget.result.mode.isWalkMode || widget.result.mode.isGeocachingMode) _QuizWidget(widget.result, widget.presenter.totalCheckPoints),
                      if (widget.result.format.isFreeFormat) _ScoreWidget(widget.result),
                      _CourseTimeWidget(widget.result, widget.presenter.createPenaltyMessage()),
                      _CourseLengthWidget(widget.result),
                      _CourseAltitudeWidget(widget.result, widget.presenter.isSportMode),
                      _CoursePaceWidget(widget.result, widget.presenter.isSportMode, widget.result.map!.discipline),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(4, 2, 4, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: OutlinedButton(
                      style: OutlinedButtonStyle(kOrangeColor),
                      onPressed: widget.presenter.isStatisticsAvailable ? () => Navigator.push(context, MaterialPageRoute(builder: (context) => TimeTableView(widget.result))) : null,
                      child: Text(L10n.getString("show_detailed_times").toUpperCase()),
                    ),
                  ),
                  const SizedBox(width: 3),
                  Expanded(
                    child: OutlinedButton(
                      style: OutlinedButtonStyle(kOrangeColor),
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => widget.trackPageRoute(widget.result),
                        ),
                      ),
                      child: Text(L10n.getString("show_route_on_map").toUpperCase()),
                    ),
                  ),
                  const SizedBox(width: 3),
                  Expanded(
                    child: OutlinedButton(
                      style: OutlinedButtonStyle(kOrangeColor),
                      onPressed: widget.presenter.isStatisticsAvailable ? () => Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileView(widget.result))) : null,
                      child: Text(L10n.getString("show_statistics").toUpperCase()),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(5, 2, 5, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: Builder(
                      builder: (BuildContext context) {
                        return OutlinedButton(
                          style: OutlinedButtonStyle(kOrangeColor),
                          onPressed: () => widget.presenter.exportGPXFile(context, widget.result),
                          child: Text(L10n.getString("export_trace_to_gpx_file").toUpperCase()),
                        );
                      },
                    ),
                  ),
                  const SizedBox(width: 5),
                  Expanded(
                    child: OutlinedButton(
                      style: OutlinedButtonStyle(widget.presenter.isSendResultAvailable(widget.result) ? kGreenColor : kOrangeColorDisabled),
                      onPressed: widget.presenter.isSendResultAvailable(widget.result) ? () => widget.presenter.onSendResultToServer(context) : null,
                      child: Text(
                        L10n.getString("send_results_to_server").toUpperCase(),
                        style: widget.presenter.isSendResultAvailable(widget.result) ? const TextStyle(color: kGreenColor) : null,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

@immutable
class _VerdictWidget extends StatelessWidget {
  final Result result;
  final String verdictText;
  final String courseFormatText;

  const _VerdictWidget(BuildContext context, this.result, this.verdictText, this.courseFormatText);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: result.isAllCheckpointsScanned ? kGreenColor : kRedColor,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (result.isAllCheckpointsScanned)
            const Icon(
              Icons.check_circle,
              size: 38,
            )
          else
            const Icon(
              Icons.cancel_rounded,
              size: 38,
            ),
          const SizedBox(width: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                courseFormatText,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
              Text(
                verdictText.toUpperCase(),
                style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

@immutable
class _QuizWidget extends StatelessWidget {
  final Result result;
  final int? totalPoints;

  const _QuizWidget(this.result, this.totalPoints);

  @override
  Widget build(BuildContext context) {
    if (totalPoints == null || result.quizTotalPoints < 0) {
      return Container();
    } else {
      return Container(
        decoration: _separationLine,
        padding: const EdgeInsets.fromLTRB(8, 3, 3, 3),
        margin: const EdgeInsets.fromLTRB(3, 1, 3, 1),
        child: Row(
          children: [
            Image.asset(
              'assets/icons/icon_quiz.png',
              scale: 3,
            ),
            const SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  L10n.getString("quiz_message"),
                  style: _textStyle1,
                ),
                Text.rich(
                  TextSpan(
                    text: '${result.quizTotalPoints}',
                    style: _textStyle2,
                    children: <InlineSpan>[
                      TextSpan(
                        text: ' /${totalPoints! - 2}',
                        style: _textStyle3,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}

@immutable
class _ScoreWidget extends StatelessWidget {
  final Result result;

  const _ScoreWidget(this.result);

  @override
  Widget build(BuildContext context) {
    int totalScore = result.score;
    return Container(
      decoration: _separationLine,
      padding: const EdgeInsets.fromLTRB(8, 3, 3, 3),
      margin: const EdgeInsets.fromLTRB(3, 1, 3, 1),
      child: Row(
        children: [
          const Icon(
            Icons.sports_score,
            size: 42,
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                L10n.getString("score_message"),
                style: _textStyle1,
              ),
              Text.rich(
                TextSpan(
                  text: '$totalScore',
                  style: _textStyle2,
                  children: <InlineSpan>[
                    TextSpan(
                      text: ' /${result.totalScore}',
                      style: _textStyle3,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

@immutable
class _CourseTimeWidget extends StatelessWidget {
  final Result result;
  final String penaltyMessage;

  const _CourseTimeWidget(this.result, this.penaltyMessage);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: _separationLine,
      padding: const EdgeInsets.fromLTRB(8, 3, 3, 3),
      margin: const EdgeInsets.fromLTRB(3, 1, 3, 1),
      child: Row(
        children: [
          Image.asset(
            'assets/icons/icon_total_time.png',
            scale: 3,
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                L10n.getString("total_time_message"),
                style: _textStyle1,
              ),
              Text(
                result.totalTimeWithPenaltyAsString,
                style: _textStyle2,
              ),
              if (penaltyMessage.isNotEmpty)
                Text(
                  penaltyMessage,
                  style: _textStyle1.copyWith(color: Colors.white),
                ),
            ],
          ),
        ],
      ),
    );
  }
}

@immutable
class _CourseLengthWidget extends StatelessWidget {
  final Result result;

  const _CourseLengthWidget(this.result);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: _separationLine,
      padding: const EdgeInsets.fromLTRB(8, 3, 3, 3),
      margin: const EdgeInsets.fromLTRB(3, 1, 3, 1),
      child: Row(
        children: [
          Image.asset(
            'assets/icons/icon_route_length.png',
            scale: 3,
          ),
          const SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                L10n.getString("route_length_message"),
                style: _textStyle1,
              ),
              Text.rich(
                TextSpan(
                  text: L10n.formatNumber(result.actualRouteLengthInM.toInt(), 1),
                  style: _textStyle2,
                  children: const <InlineSpan>[
                    TextSpan(
                      text: ' m',
                      style: _textStyle3,
                    ),
                  ],
                ),
              ),
              Text(
                "${L10n.getString("theoretical_length_message")} ${L10n.formatNumber(result.totalTheoreticalCourseLengthInM.toInt(), 0)} m",
                style: _textStyle1,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

@immutable
class _CourseAltitudeWidget extends StatelessWidget {
  final Result result;
  final bool isSportMode;

  const _CourseAltitudeWidget(this.result, this.isSportMode);

  @override
  Widget build(BuildContext context) {
    var elevationGain = result.cumulativeElevationGainInM;
    if (elevationGain < 0) {
      return Container();
    } else {
      return Container(
        decoration: (isSportMode) ? _separationLine : null,
        padding: const EdgeInsets.fromLTRB(8, 3, 3, 3),
        margin: const EdgeInsets.fromLTRB(3, 1, 3, 1),
        child: Row(
          children: [
            Image.asset(
              'assets/icons/icon_altitude.png',
              scale: 3,
            ),
            const SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  L10n.getString("altitude_message"),
                  style: _textStyle1,
                ),
                Text.rich(
                  TextSpan(
                    text: L10n.formatNumber(elevationGain, 0),
                    style: _textStyle2,
                    children: const <InlineSpan>[
                      TextSpan(
                        text: ' m',
                        style: _textStyle3,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}

@immutable
class _CoursePaceWidget extends StatelessWidget {
  final Result result;
  final bool isSport;
  final Discipline discipline;

  const _CoursePaceWidget(this.result, this.isSport, this.discipline);

  @override
  Widget build(BuildContext context) {
    if (!isSport) {
      return Container();
    } else {
      return Container(
        padding: const EdgeInsets.fromLTRB(8, 3, 3, 3),
        margin: const EdgeInsets.fromLTRB(3, 1, 3, 1),
        child: Row(
          children: [
            Icon(
              Discipline.getIcon(discipline),
              size: 42,
            ),
            const SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  L10n.getString("reduction_to_kilometer_message"),
                  style: _textStyle1,
                ),
                Text.rich(
                  TextSpan(
                    text: formatTimeAsString(result.globalPaceInMillisecondByKm, withHour: false),
                    style: _textStyle2,
                    children: const <InlineSpan>[
                      TextSpan(
                        text: ' /km',
                        style: _textStyle3,
                      ),
                    ],
                  ),
                ),
                Text(
                  L10n.getString("average_speed_message"),
                  style: _textStyle1,
                ),
                Text.rich(
                  TextSpan(
                    text: L10n.formatNumber(result.globalAverageSpeedInKmH, 1),
                    style: _textStyle2,
                    children: const <InlineSpan>[
                      TextSpan(
                        text: ' km/h',
                        style: _textStyle3,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}

@immutable
class _ClubInformationWidget extends StatelessWidget {
  final Result result;

  const _ClubInformationWidget(this.result);

  @override
  Widget build(BuildContext context) {
    final String labelText;
    final String buttonText;
    final String? url;
    final Color color;
    if (result.map!.clubName != null) {
      labelText = L10n.getString("ad_club_label_text");
      buttonText = result.map!.clubName!;
      if (result.map!.clubUrl != null) {
        url = result.map!.clubUrl;
        color = kGreenColor;
      } else {
        url = null;
        color = Colors.white;
      }
    } else {
      color = kGreenColor;
      labelText = L10n.getString("ad_federation_label_text");
      buttonText = L10n.getString("ad_federation_button_text");
      url = L10n.getString("ad_federation_url_text");
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          labelText,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 12,
          ),
        ),
        const SizedBox(width: 5),
        (url == null)
            ? Flexible(
                child: Container(
                  margin: const EdgeInsets.fromLTRB(0, 2, 0, 2),
                  padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: color,
                      ),
                      borderRadius: BorderRadius.circular(15)),
                  child: Text(
                    buttonText,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: color,
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              )
            : Flexible(
                child: TextButton(
                  onPressed: () async {
                    openWebBrowserWithConfirmation(context, url!);
                  },
                  style: TextButton.styleFrom(
                    foregroundColor: Colors.white,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    backgroundColor: kGreenColor,
                    side: const BorderSide(
                      color: Colors.white,
                      width: 2,
                    ),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                  ),
                  child: Text(
                    buttonText,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
      ],
    );
  }
}

class OutlinedButtonStyle extends ButtonStyle {
  OutlinedButtonStyle(Color color)
      : super(
          foregroundColor: WidgetStateProperty.resolveWith<Color>((states) {
            if (states.contains(WidgetState.disabled)) {
              return kOrangeColorDisabled.withValues(alpha: 0.3);
            }
            return color;
          }),
          padding: WidgetStateProperty.all<EdgeInsets>(EdgeInsets.zero),
          side: WidgetStateProperty.resolveWith<BorderSide>((states) {
            if (states.contains(WidgetState.disabled)) {
              return BorderSide(width: 2, color: color.withValues(alpha: 0.3));
            }
            return BorderSide(width: 2, color: color);
          }),
          shape: WidgetStateProperty.all<OutlinedBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(35.0),
            ),
          ),
        );
}

const TextStyle _textStyle1 = TextStyle(
  color: kOrangeColorLighter,
  fontSize: 14,
);

const TextStyle _textStyle2 = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 22,
);

const TextStyle _textStyle3 = TextStyle(
  color: kOrangeColorLighter,
  fontWeight: FontWeight.bold,
  fontSize: 16,
);

BoxDecoration _separationLine = const BoxDecoration(
  border: Border(
    bottom: BorderSide(width: 1.5, color: kOrangeColorLighter),
  ),
);
