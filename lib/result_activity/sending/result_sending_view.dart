// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/storage_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';

import 'result_sending_presenter.dart';

class ResultSendingView {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  _ProgressBarState? _progressBarState;

  Future<void> build(BuildContext context, ResultSendingPresenter presenter) async {
    String? nickname = await getStoredNickname();
    final pseudoTextEditorController = TextEditingController(text: nickname);

    final dialog = StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
      return AlertDialog(
        contentPadding: const EdgeInsets.all(10),
        actionsPadding: const EdgeInsets.all(4),
        insetPadding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        title: Text(
          L10n.getString("send_result_title"),
          style: const TextStyle(color: kGreenColor, fontSize: 18),
        ),
        content: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _ProgressBar(this),
                TextFormField(
                  autofocus: true,
                  validator: (value) {
                    if (value == null || value.trim().isEmpty) {
                      return L10n.getString("error_no_nickname");
                    }
                    return null;
                  },
                  keyboardType: TextInputType.name,
                  maxLength: 20,
                  textCapitalization: TextCapitalization.words,
                  controller: pseudoTextEditorController,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(nicknameRegExp),
                  ],
                  decoration: InputDecoration(
                    labelText: L10n.getString("enter_nickname"),
                    labelStyle: const TextStyle(color: kGreenColor),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: const BorderSide(
                        color: kGreenColor,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: const BorderSide(
                        color: kGreenColor,
                      ),
                    ),
                    fillColor: kGreenColor.withValues(alpha: 0.1),
                    filled: true,
                    counterStyle: const TextStyle(color: kGreenColor),
                  ),
                ),
                Text(
                  L10n.getString("data_protection"),
                  style: const TextStyle(fontSize: 12),
                ),
              ],
            ),
          ),
        ),
        actions: [
          TextButton(
            child: Text(L10n.getString("send"), style: const TextStyle(color: kGreenColor)),
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                final NavigatorState state = Navigator.of(context);
                final nickname = pseudoTextEditorController.text.trim();
                if (nickname.isNotEmpty) {
                  await storeNickname(nickname);
                }
                state.pop();
                presenter.sendDataToServer(nickname);
              }
            },
          ),
          TextButton(
            child: Text(L10n.getString("cancel"), style: const TextStyle(color: kGreenColor)),
            onPressed: () {
              presenter.enableSendButton();
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    });
    if (context.mounted) {
      await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => dialog,
      );
    }
  }

  void showProgressBar() => _progressBarState?.show();

  void hideProgressBar() => _progressBarState?.hide();
}

class _ProgressBar extends StatefulWidget {
  final ResultSendingView resultSendingView;

  const _ProgressBar(this.resultSendingView);

  @override
  _ProgressBarState createState() => _ProgressBarState();
}

class _ProgressBarState extends State<_ProgressBar> {
  bool? _isVisible;

  @override
  void initState() {
    widget.resultSendingView._progressBarState = this;
    _isVisible = false;
    super.initState();
  }

  @override
  void dispose() {
    _isVisible = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_isVisible ?? false) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: LinearProgressIndicator(backgroundColor: kGreenColor.withValues(alpha: 0.1)),
      );
    } else {
      return const Text('');
    }
  }

  void hide() {
    if (_isVisible != null) {
      setState(() => _isVisible = false);
    }
  }

  void show() {
    if (_isVisible != null) {
      setState(() => _isVisible = true);
    }
  }
}
