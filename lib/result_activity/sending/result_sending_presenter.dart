import 'dart:core';

import 'package:flutter/material.dart';
import 'package:vikazimut/server_gateway/gateway_listener.dart';
import 'package:vikazimut/server_gateway/result_data.dart';
import 'package:vikazimut/server_gateway/result_gateway.dart';

import '../abstract_global_result_presenter.dart';
import 'result_sending_view.dart';

class ResultSendingPresenter implements GatewayListener {
  static const int _SENT_RETURN_CODE = 12345;

  final AbstractGlobalResultPresenter _resultPresenter;
  ResultSendingView? _view;

  ResultSendingPresenter(this._resultPresenter);

  void onSendResultToServer(BuildContext context) {
    _view = ResultSendingView()..build(context, this);
  }

  void sendDataToServer(String nickname) {
    if (_resultPresenter.result == null) {}
    final result = _resultPresenter.result!;
    ResultData resultData = ResultData(
      orienteer: nickname,
      format: result.format,
      totalTimeInMillisecond: result.totalTimeWithPenaltyInMs,
      gpsTrack: result.computeWayPointsAsString(),
      courseId: result.courseId,
      punchTimeInMillisecondList: result.punchTimes.map((punchTime) => punchTime.timestampInMillisecond).toList(),
      punchForceList: result.punchTimes.map((punchTime) => punchTime.forced).toList(),
      runCount: result.runCount,
    );
    _view!.showProgressBar();
    ResultGateway().sendToServer(resultData, this, _SENT_RETURN_CODE);
  }

  @override
  void handlePositiveSendingResultsToServerResponse(int code) {
    _view!.hideProgressBar();
    if (code == _SENT_RETURN_CODE) {
      _resultPresenter.handlePositiveSendingResultsToServerResponse();
    }
  }

  @override
  void handleNegativeSendingResultsToServerResponse(int code, String errorMessage) {
    _view!.hideProgressBar();
    if (code == _SENT_RETURN_CODE) {
      _resultPresenter.handleNegativeSendingResultsToServerResponse(errorMessage);
    }
  }

  void enableSendButton() {
    _resultPresenter.handleCancel();
  }
}
