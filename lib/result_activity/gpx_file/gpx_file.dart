import 'dart:core';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart' as path;
import 'package:share_plus/share_plus.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import '../result.dart';

class GPXFile {
  static void exportGPXFile(BuildContext context, Result result) async {
    final box = context.findRenderObject() as RenderBox?;
    String filename = _getFilename(result);
    final Directory dir = await path.getTemporaryDirectory();
    File file = File('${dir.path}/$filename');
    await file.writeAsString(buildGPXContentFromResult_(result));
    await Share.shareXFiles(
      [XFile(file.path)],
      text: filename,
      sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size,
    );
    file.delete();
  }

  static String _getFilename(Result result) {
    return "${result.mapName}-${result.totalTimeWithoutPenaltyAsString}.gpx";
  }

  @visibleForTesting
  static String buildGPXContentFromResult_(Result result) {
    var header = '<?xml version="1.0" encoding="UTF-8" standalone="no" ?><gpx version="1.1" creator="Vikazimut" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"><trk><name>${result.mapName}</name>';
    const footer = '</trk></gpx>';
    return header + _getTrack(result.waypoints) + footer;
  }

  static String _getTrack(List<GeodesicPoint> waypoints) {
    String gpxContents = '<trkseg>';
    for (var waypoint in waypoints) {
      final latitude = waypoint.latitude;
      final longitude = waypoint.longitude;
      final int milliseconds = waypoint.timestampInMillisecond;
      DateTime time = DateTime.fromMillisecondsSinceEpoch(milliseconds, isUtc: true);
      String isoDate = time.toIso8601String();
      final elevation = waypoint.altitude;
      gpxContents += '<trkpt lat="$latitude" lon="$longitude"><time>$isoDate</time><ele>$elevation</ele></trkpt>';
    }
    gpxContents += '</trkseg>';
    return gpxContents;
  }
}
