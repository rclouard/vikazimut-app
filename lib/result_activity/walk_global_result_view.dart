// coverage:ignore-file
import 'package:flutter/material.dart';

import 'result.dart';
import 'track/walk_track_view.dart';
import 'abstract_global_result_view.dart';
import 'walk_global_result_presenter.dart';

class WalkGlobalResultView extends AbstractGlobalResultView {
  WalkGlobalResultView({required int resultId}) : super(WalkGlobalResultPresenter(resultId: resultId));

  @override
  Widget trackPageRoute(Result result) {
    return WalkTrackView(
      map: result.map!,
      waypoints: result.waypoints,
      punchTimes: result.punchTimes,
      totalTimeInMs: result.totalTimeWithoutPenaltyInMs,
    );
  }
}
