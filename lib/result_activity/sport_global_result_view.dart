// coverage:ignore-file
import 'package:flutter/material.dart';

import 'result.dart';
import 'track/sport_track_view.dart';
import 'abstract_global_result_view.dart';
import 'sport_global_result_presenter.dart';

class SportGlobalResultView extends AbstractGlobalResultView {
  SportGlobalResultView({required int resultId}) : super(SportGlobalResultPresenter(resultId: resultId));

  @override
  Widget trackPageRoute(Result result) {
    return SportTrackView(
      map: result.map!,
      waypoints: result.waypoints,
      punchTimes: result.punchTimes,
      totalTimeInMs: result.totalTimeWithoutPenaltyInMs,
    );
  }
}
