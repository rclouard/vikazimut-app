import 'sport_global_result_presenter.dart';

class GeocachingGlobalResultPresenter extends SportGlobalResultPresenter {
  GeocachingGlobalResultPresenter({required super.resultId});
}
