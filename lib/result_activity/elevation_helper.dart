import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

// Note: Common file between the app and the website
@immutable
class ElevationHelper {
  static const double NO_ELEVATION = -10000.0;

  static const int _gaussianHalfSize = 9; // Filter size : ~ 9*1=9 seconds -> at 10 km/h -> 25 m
  static const int _meanHalfSize = 7;
  static const int _numberOfBadElevationValues = 7;

  const ElevationHelper._();

  static void cleanFirstElevationValues(List<GeodesicPoint> waypoints) {
    // Duplicate the 7th altitude to the 6th first altitudes to remove bad values at the beginning
    final last = math.min(_numberOfBadElevationValues, waypoints.length);
    final lastElevation = waypoints[last - 1].altitude;
    for (int i = 0; i < last; i++) {
      waypoints[i].altitude = lastElevation;
    }
  }

  static void filter(List<GeodesicPoint> waypoints) {
    cleanFirstElevationValues(waypoints);
    gaussianFilteringToFilterElevationCurve_(waypoints, _gaussianHalfSize);
    filterWithLowPassToRemoveSmallHolesOrHills_(waypoints, _meanHalfSize);
  }

  @visibleForTesting
  static void filterWithLowPassToRemoveSmallHolesOrHills_(List<GeodesicPoint> waypoints, int halfSize) {
    List<double> previous = List<double>.filled(halfSize, waypoints[0].altitude, growable: false);
    for (var i = 0; i < waypoints.length; i++) {
      double sum = 0;
      for (var j = 0; j < halfSize; j++) {
        sum += previous[j];
      }
      for (var j = 0; j < halfSize; j++) {
        if (i + j + 1 < waypoints.length) {
          sum += waypoints[i + j + 1].altitude;
        } else {
          sum += waypoints[waypoints.length - 1].altitude;
        }
      }
      sum += waypoints[i].altitude;
      if (halfSize - 1 >= 0) {
        for (var k = 1; k < halfSize; k++) {
          previous[k - 1] = previous[k];
        }
      }
      previous[halfSize - 1] = waypoints[i].altitude;
      waypoints[i].altitude = sum / (2.0 * halfSize + 1.0);
    }
  }

  @visibleForTesting
  static void gaussianFilteringToFilterElevationCurve_(List<GeodesicPoint> waypoints, int halfSize) {
    List<double> filter = List<double>.filled(halfSize * 2 + 1, 0, growable: false);
    final double gaussianSum = computeGaussianFilter_(halfSize, filter);
    List<double> previous = List<double>.filled(halfSize, waypoints[0].altitude, growable: false);
    for (var i = 0; i < waypoints.length; i++) {
      double sum = 0;
      for (int j = 0; j < halfSize; j++) {
        sum += previous[j] * filter[j];
      }
      for (var j = 0; j < halfSize; j++) {
        if (i + j + 1 < waypoints.length) {
          sum += waypoints[i + j + 1].altitude * filter[j + halfSize + 1];
        } else {
          sum += waypoints[waypoints.length - 1].altitude * filter[j + halfSize + 1];
        }
      }
      sum += waypoints[i].altitude * filter[halfSize];
      if (halfSize - 1 >= 0) {
        for (var k = 1; k < halfSize; k++) {
          previous[k - 1] = previous[k];
        }
      }
      previous[halfSize - 1] = waypoints[i].altitude;
      waypoints[i].altitude = sum / gaussianSum;
    }
  }

  @visibleForTesting
  static double computeGaussianFilter_(int halfSize, List<double> filter) {
    const double SCALE = 50.0;
    double sigma = halfSize / 3.0;
    double gaussianSum = 0;
    for (var i = 0; i < filter.length; i++) {
      double s = SCALE * math.exp(-((i - halfSize) * (i - halfSize) / (2.0 * sigma * sigma)));
      gaussianSum += s;
      filter[i] = s;
    }
    return gaussianSum;
  }

  static double calculateCumulativeElevationGain(List<GeodesicPoint> waypoints) {
    if (waypoints.isEmpty) {
      return -1;
    }
    filter(waypoints);
    return calculateCumulativeElevationGain_(waypoints);
  }

  @visibleForTesting
  static double calculateCumulativeElevationGain_(List<GeodesicPoint> waypoints) {
    final notEnoughPoints = waypoints.length < _numberOfBadElevationValues + 1;
    if (notEnoughPoints) {
      return NO_ELEVATION;
    }

    double positiveElevation = 0;
    for (int i = 1; i < waypoints.length; i++) {
      double elevation = waypoints[i].altitude - waypoints[i - 1].altitude;
      if (elevation > 0) {
        positiveElevation += elevation;
      }
    }
    return positiveElevation;
  }
}
