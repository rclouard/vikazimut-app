import 'package:vikazimut/route_data/geodesic_point.dart';

class Leg {
  final int index;
  late String id;
  late GeodesicPoint checkpointLocation;
  late int punchTime;
  late bool forced;
  late int duration;
  late double actualDistance;
  late double theoreticalDistance;
  late int paceInMnK;
  List<GeodesicPoint>? waypoints;

  Leg(this.index, {this.punchTime = 0});
}
