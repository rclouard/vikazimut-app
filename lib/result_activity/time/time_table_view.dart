// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/time.dart';

import '../leg.dart';
import '../result.dart';

@immutable
class TimeTableView extends StatelessWidget {
  final Result _result;
  late final String _title;

  TimeTableView(this._result) : _title = _result.mapName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            margin: const EdgeInsets.all(5),
            child: Theme(
              data: Theme.of(context).copyWith(dividerColor: kOrangeColor),
              child: DataTable(
                dataRowColor: WidgetStateProperty.all(Colors.white),
                horizontalMargin: 0,
                dataRowMinHeight: 55,
                dataRowMaxHeight: 55,
                columnSpacing: 0,
                headingRowColor: WidgetStateProperty.all(kOrangeColor),
                headingTextStyle: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold, color: Colors.white),
                dataTextStyle: const TextStyle(fontSize: 13.0, color: Colors.black),
                columns: _buildColumnHeaders(),
                rows: _buildRowContents(),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<DataColumn> _buildColumnHeaders() {
    return [
      DataColumn(label: Expanded(child: _TableLabel(L10n.getString("checkpoint_index")))),
      DataColumn(label: Expanded(child: _TableLabel(L10n.getString("interval_time")))),
      DataColumn(label: Expanded(child: _TableLabel(L10n.getString("interval_distance")))),
      DataColumn(label: Expanded(child: _TableLabel(L10n.getString("interval_pace")))),
      DataColumn(label: Expanded(child: _TableLabel(L10n.getString("cumulative_time")))),
    ];
  }

  List<DataRow> _buildRowContents() {
    // Rows: legs
    List<DataRow> rows = [];
    List<Leg> timeTable = _result.computeDetailedStatistics();
    for (int i = 1; i < timeTable.length; i++) {
      List<DataCell> cells = [
        DataCell(Center(child: _buildControlNumberCell(timeTable[i], timeTable.length))),
        DataCell(Center(child: _buildLegTimeCell(timeTable[i]))),
        DataCell(Center(child: _buildLegDistanceCell(timeTable[i]))),
        DataCell(Center(child: _buildPaceCell(timeTable[i]))),
        DataCell(Center(child: _buildCumulatedCell(timeTable[i]))),
      ];
      DataRow tableRow = DataRow(cells: cells);
      rows.add(tableRow);
    }

    // Last row: Total
    final totalLength = _result.actualRouteLengthInM;
    double totalTheoreticalDistance = _result.totalTheoreticalRouteLengthInM;

    DataRow totalTableRow = DataRow(
      color: WidgetStateProperty.all(kOrangeColorDisabled),
      cells: [
        DataCell(Center(child: _TextCenteredGreenBold(L10n.getString("total_row_title")))),
        DataCell(Center(child: _TextCenteredGreenBold(formatTimeAsString(_result.totalDurationInMs)))),
        DataCell(
          Center(
            child: Text.rich(
              textAlign: TextAlign.center,
              TextSpan(
                style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                children: [
                  TextSpan(text: L10n.formatNumber(totalLength.toInt(), 2)),
                  const _UnitText("m"),
                  const TextSpan(text: "\n"),
                  TextSpan(text: "(${L10n.formatNumber(totalTheoreticalDistance.toInt(), 2)}"),
                  const _UnitText("m"),
                  const TextSpan(text: ")\n"),
                  TextSpan(text: "+${Result.computeOvercostPercentage(totalLength, totalTheoreticalDistance)}"),
                  const TextSpan(text: "%", style: TextStyle(fontSize: 8)),
                ],
              ),
            ),
          ),
        ),
        DataCell(
          Center(
            child: Text.rich(
              textAlign: TextAlign.center,
              TextSpan(
                style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                children: [
                  TextSpan(text: _formatPace(_result.globalPaceInMillisecondByKm)),
                  const _UnitText("/km"),
                  const TextSpan(text: "\n"),
                  TextSpan(text: _formatSpeed(_result.globalAverageSpeedInKmH)),
                  const _UnitText("km/h"),
                ],
              ),
            ),
          ),
        ),
        DataCell(Center(child: _TextCenteredGreenBold(_result.totalTimeWithoutPenaltyAsString))),
      ],
    );
    rows.insert(0, totalTableRow);
    return rows;
  }

  _TextCentered _buildCumulatedCell(Leg leg) => _TextCentered(_formatLegCumulatedTime(leg.punchTime));

  Widget _buildPaceCell(Leg leg) {
    var pace = _formatPace(leg.paceInMnK);
    if (pace.isEmpty) {
      return const _TextCentered("");
    }
    var speed = _formatSpeed(Result.calculateAverageSpeedInKmh(leg.duration, leg.actualDistance));
    return Text.rich(
      textAlign: TextAlign.center,
      TextSpan(
        style: const TextStyle(color: Colors.black),
        children: [
          TextSpan(text: pace),
          const _UnitText('/km'),
          const TextSpan(text: "\n"),
          TextSpan(text: speed),
          const _UnitText('km/h'),
          const TextSpan(text: "\n"),
        ],
      ),
    );
  }

  Widget _buildLegDistanceCell(Leg leg) {
    String actualDistance = _formatActualDistance(leg.actualDistance.toInt());
    String theoreticalDistance = L10n.formatNumber(leg.theoreticalDistance.toInt(), 2);
    if (leg.punchTime <= 0) {
      return const Text("\n");
    } else {
      var overcost = Result.computeOvercostPercentage(leg.actualDistance, leg.theoreticalDistance);
      return Center(
        child: Text.rich(
          textAlign: TextAlign.center,
          TextSpan(
            style: const TextStyle(color: Colors.black),
            children: [
              TextSpan(text: actualDistance),
              const _UnitText('m'),
              const TextSpan(text: "\n"),
              TextSpan(text: "($theoreticalDistance"),
              const _UnitText('m'),
              const TextSpan(text: ")\n"),
              TextSpan(text: "+$overcost"),
              const TextSpan(text: "%", style: TextStyle(fontSize: 8)),
            ],
          ),
        ),
      );
    }
  }

  Widget _buildLegTimeCell(Leg leg) {
    return _TextCentered(_formatLegDuration(leg.duration, leg.forced));
  }

  Widget _buildControlNumberCell(Leg leg, int length) {
    if (leg.index < length - 1) {
      return Text.rich(
        textAlign: TextAlign.center,
        TextSpan(
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            color: kOrangeColor,
          ),
          children: [
            TextSpan(text: leg.index.toString()),
            TextSpan(text: "(${leg.id})", style: const TextStyle(fontSize: 10)),
          ],
        ),
      );
    } else {
      return _TextCenteredBold(L10n.getString("cp_finish"));
    }
  }

  static String _formatLegCumulatedTime(int time) {
    if (time <= 0) {
      return '';
    } else {
      return formatTimeAsString(time);
    }
  }

  static String _formatActualDistance(int distance) {
    if (distance <= 0) {
      return '';
    } else {
      return L10n.formatNumber(distance, 2);
    }
  }

  static String _formatPace(int pace) {
    if (pace <= 0) {
      return '';
    } else {
      return formatTimeAsString(pace, withHour: false);
    }
  }

  static String _formatSpeed(double speed) {
    if (speed <= 0) {
      return '';
    } else {
      return L10n.formatNumber(speed, 1);
    }
  }

  static String _formatLegDuration(int duration, bool forced) {
    if (duration <= 0) {
      return '';
    } else if (forced) {
      return "${formatTimeAsString(duration)}^";
    } else {
      return formatTimeAsString(duration);
    }
  }
}

class _TableLabel extends Text {
  const _TableLabel(super.text) : super(textAlign: TextAlign.center, style: const TextStyle(fontSize: 10));
}

class _TextCentered extends Text {
  const _TextCentered(super.text) : super(textAlign: TextAlign.center);
}

class _TextCenteredBold extends Text {
  const _TextCenteredBold(super.text)
      : super(
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              color: kOrangeColor,
            ));
}

class _TextCenteredGreenBold extends Text {
  const _TextCenteredGreenBold(super.text) : super(textAlign: TextAlign.center, style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black));
}

class _UnitText extends TextSpan {
  const _UnitText(String unit) : super(text: unit, style: const TextStyle(fontSize: 8));
}
