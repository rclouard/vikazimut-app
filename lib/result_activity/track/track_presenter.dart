import 'package:vikazimut/route_data/punch_position.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_time.dart';

class TrackPresenter {
  static List<PunchPosition> calculatePunchPositions(
    List<PunchTime> punchTimes,
    List<GeodesicPoint> waypoints,
  ) {
    List<PunchTime> punchedCheckpoints = [];
    for (int i = 0; i < punchTimes.length; i++) {
      if (punchTimes[i].timestampInMillisecond >= 0) {
        punchedCheckpoints.add(punchTimes[i]);
      }
    }
    punchedCheckpoints.sort((a, b) => a.timestampInMillisecond.compareTo(b.timestampInMillisecond));
    List<PunchPosition> punchPositions = [];
    int leg = 0;
    for (int i = 0; i < waypoints.length && leg < punchedCheckpoints.length; i++) {
      if (i == 0 || waypoints[i].timestampInMillisecond == punchedCheckpoints[leg].timestampInMillisecond) {
        punchPositions.add(PunchPosition(GeodesicPoint(waypoints[i].latitude, waypoints[i].longitude), punchedCheckpoints[leg].forced));
        ++leg;
      } else if (waypoints[i].timestampInMillisecond > punchedCheckpoints[leg].timestampInMillisecond) {
        // No waypoint at punch position -> linear interpolation
        var lat1 = waypoints[i - 1].latitude;
        var lon1 = waypoints[i - 1].longitude;
        var t1 = waypoints[i - 1].timestampInMillisecond;
        var lat2 = waypoints[i].latitude;
        var lon2 = waypoints[i].longitude;
        var t2 = waypoints[i].timestampInMillisecond;
        var tx = punchedCheckpoints[leg].timestampInMillisecond;
        var ratio = (tx - t1) / (t2 - t1);
        var lat = ratio * (lat2 - lat1) + lat1;
        var lon = ratio * (lon2 - lon1) + lon1;
        punchPositions.add(PunchPosition(GeodesicPoint(lat, lon), punchedCheckpoints[leg].forced));
        ++leg;
      }
    }
    return punchPositions;
  }
}
