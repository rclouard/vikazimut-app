import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_view/layers/animated_route_layer.dart';
import 'package:vikazimut/map_view/map_controller.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_time.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

import 'track_animation_widget.dart';

class TrackAnimationPresenter {
  static const int _LENGTH_OF_COMET_IN_MS = 60 * 1000;
  static const int _FRAME_PER_SECONDS = 45;
  static const int _VALIDATION_TIME_DELTA_AROUND_CHECKPOINT_IN_MS = 8000;

  late final MapController _mapController;
  late final AnimatedRouteLayer _animatedRouteLayer;
  late final TrackAnimationWidgetState _view;
  bool _isLayerAdded = false;
  late final List<int> _punchTimes;
  AnimationController? _controller;
  Function()? _postAction;
  bool _isPausing = false;

  TrackAnimationPresenter(
    TrackAnimationWidgetState view,
    MapController mapController,
    List<int> punchTimes,
  )   : _view = view,
        _mapController = mapController,
        _animatedRouteLayer = AnimatedRouteLayer(mapController),
        _punchTimes = punchTimes;

  void execute(List<GeodesicPoint> track, int currentTime, int totalTime) {
    if (track.isEmpty) {
      showErrorDialog(
        _view.context,
        title: L10n.getString("animation_error_title"),
        message: L10n.getString("animation_error_message"),
      );
      return;
    }
    if (!_isLayerAdded) {
      _mapController.addLayer(_animatedRouteLayer);
      _isLayerAdded = true;
    }
    final duration = (totalTime - currentTime) / _FRAME_PER_SECONDS;
    _controller = AnimationController(
      vsync: _view,
      duration: Duration(milliseconds: duration.toInt()),
    );
    _isPausing = false;
    var minTime = currentTime;
    var maxTime = totalTime;
    Animation trackAnimation = IntTween(begin: minTime, end: maxTime).animate(_controller!);
    _controller!.addListener(() {
      notify(trackAnimation.value);
      var subTrack = _getSubTrack(track, trackAnimation.value);
      _animatedRouteLayer.drawRoute(
        subTrack,
        isPunchLocation: isPunchLocation_(
          trackAnimation.value,
          _punchTimes,
          _VALIDATION_TIME_DELTA_AROUND_CHECKPOINT_IN_MS,
        ),
      );
    });
    _controller!.forward().whenComplete(() {
      stop();
    });
  }

  void changeCurrentPositionManually(List<GeodesicPoint> track, int currentTime, int totalTime) {
    if (currentTime >= totalTime) {
      _isPausing = false;
      stop();
    } else {
      var subTrack = _getSubTrack(track, currentTime);
      if (!_isLayerAdded) {
        _mapController.addLayer(_animatedRouteLayer);
        _isLayerAdded = true;
      }
      _animatedRouteLayer.drawRoute(
        subTrack,
        isPunchLocation: isPunchLocation_(
          currentTime,
          _punchTimes,
          _VALIDATION_TIME_DELTA_AROUND_CHECKPOINT_IN_MS,
        ),
      );
    }
  }

  void pause() {
    _controller?.stop(canceled: false);
    _isPausing = true;
  }

  void resume() {
    _mapController.removeLayer(_animatedRouteLayer);
    _isLayerAdded = false;
    _isPausing = false;
  }

  void stop() {
    _controller?.stop();
    _controller?.dispose();
    _controller = null;
    if (!_isPausing) {
      _mapController.removeLayer(_animatedRouteLayer);
      _isLayerAdded = false;
      notify(0);
      if (_postAction != null) {
        _postAction!();
      }
    }
  }

  void whenComplete(dynamic Function() action) {
    _postAction = action;
  }

  void dispose() {
    _controller?.dispose();
  }

  List<GeodesicPoint> _getSubTrack(List<GeodesicPoint> track, int currentTime) {
    int currentGPSPointIndex = findCurrentGPSPoint_(track, currentTime);
    return getSubList_(track, currentTime, currentGPSPointIndex);
  }

  @visibleForTesting
  static int findCurrentGPSPoint_(List<GeodesicPoint> track, int currentTime) {
    for (int i = 0; i < track.length; i++) {
      if (track[i].timestampInMillisecond >= currentTime) {
        return i;
      }
    }
    return 0;
  }

  @visibleForTesting
  static List<GeodesicPoint> getSubList_(List<GeodesicPoint> track, int currentTime, int currentTrackIndex) {
    if (track.length < 2 || currentTrackIndex >= track.length || currentTrackIndex <= 0) {
      return [];
    }
    var endTime = track[currentTrackIndex].timestampInMillisecond - _LENGTH_OF_COMET_IN_MS;
    var start = 0;
    for (int i = 0; i < track.length; i++) {
      if (track[i].timestampInMillisecond < endTime) {
        start = i;
      }
    }
    var subList = track.sublist(start, currentTrackIndex);
    // Add the last point at the specified time
    var t1 = track[currentTrackIndex - 1].timestampInMillisecond;
    var t2 = track[currentTrackIndex].timestampInMillisecond;
    double x = (currentTime - t1) / (t2 - t1);
    var latitude = x * track[currentTrackIndex].latitude + (1.0 - x) * track[currentTrackIndex - 1].latitude;
    var longitude = x * track[currentTrackIndex].longitude + (1.0 - x) * track[currentTrackIndex - 1].longitude;
    subList.add(GeodesicPoint(latitude, longitude, track[currentTrackIndex].timestampInMillisecond));
    return subList;
  }

  void notify(int value) {
    _view.update(value);
  }

  @visibleForTesting
  static bool isPunchLocation_(int currentTime, List<int> punchTimes, int deltaTime) {
    for (int i = 0; i < punchTimes.length; i++) {
      final punchTime = punchTimes[i];
      if ((i == 0 || punchTime >= 0) && currentTime >= punchTime - deltaTime / 4 && currentTime <= punchTime + (3 * deltaTime) / 4) {
        return true;
      }
    }
    return false;
  }

  static List<int> sortPunchTimesInChronologicalOrder(List<PunchTime> punchTimes) {
    List<int> sortedList = punchTimes.map((punchTime) => punchTime.timestampInMillisecond).toList();
    sortedList.sort((a, b) {
      if (a == b) {
        return 0;
      }
      if (a < 0) {
        return 1;
      }
      if (b < 0) {
        return -1;
      }
      return a.compareTo(b);
    });
    return sortedList;
  }
}
