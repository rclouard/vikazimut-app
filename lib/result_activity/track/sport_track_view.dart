// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/map_view/layers/abstract_route_layer.dart';
import 'package:vikazimut/map_view/layers/colored_route_layer.dart';
import 'package:vikazimut/map_view/map_controller.dart';

import 'abstract_track_view.dart';

class SportTrackView extends AbstractTrackView {
  SportTrackView({
    required super.map,
    required super.waypoints,
    required super.punchTimes,
    required super.totalTimeInMs,
  });

  @override
  AbstractRouteLayer createRouteOverlay(MapController mapController) => ColoredRouteLayer(mapController, map.discipline);

  @override
  PreferredSizeWidget? buildAppBarLegend() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(22.0),
      child: Container(
        height: 30,
        width: double.infinity,
        color: Colors.white,
        child: Image.asset(
          _getSpeedRangeFromDisciple(map.discipline),
          scale: 1.8,
        ),
      ),
    );
  }

  String _getSpeedRangeFromDisciple(Discipline discipline) {
    if (discipline.isSkiOrienteering()) {
      return 'assets/images/skio_speed_range.png';
    } else if (discipline.isMTBOrienteering()) {
      return 'assets/images/mtbo_speed_range.png';
    } else {
      return 'assets/images/footo_speed_range.png';
    }
  }
}
