// coverage:ignore-file

/// Widely inspired by https://github.com/happyharis
library;

import 'package:flutter/material.dart';

part 'digital_colon.dart';

part 'digital_number.dart';

class NeumorphicDigitalClock extends StatelessWidget {
  final num height;
  final num width;
  final Duration time;

  const NeumorphicDigitalClock({
    required this.height,
    required this.width,
    required this.time,
  });

  @override
  Widget build(BuildContext context) {
    List<_DigitalNumberWithBG> hourNumber = _createNumberTime(time.inHours);
    List<_DigitalNumberWithBG> minuteNumber = _createNumberTime(time.inMinutes);
    List<_DigitalNumberWithBG> secondNumber = _createNumberTime(time.inSeconds);
    return SizedBox(
      height: height * 0.47,
      width: width * 0.70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ...hourNumber,
          DigitalColon(height: height * 0.30, color: Colors.white),
          ...minuteNumber,
          DigitalColon(height: height * 0.30, color: Colors.white),
          ...secondNumber,
        ],
      ),
    );
  }

  List<_DigitalNumberWithBG> _createNumberTime(int numberTime) {
    final parsedNumberTime = numberTime % 60;
    final isNumberTimeTwoDigits = _isNumberTwoDigits(parsedNumberTime);
    final firstNumber = _firstDigit(parsedNumberTime);
    final tenDigit = isNumberTimeTwoDigits ? firstNumber : 0;
    final digit = isNumberTimeTwoDigits ? int.parse(parsedNumberTime.toString()[1]) : firstNumber;
    return [
      _DigitalNumberWithBG(
        height: height * 0.35,
        value: tenDigit,
      ),
      _DigitalNumberWithBG(
        height: height * 0.35,
        value: digit,
      ),
    ];
  }

  bool _isNumberTwoDigits(int number) => number.toString().length == 2;

  int _firstDigit(int number) => int.parse(number.toString()[0]);
}

class _DigitalNumberWithBG extends StatelessWidget {
  const _DigitalNumberWithBG({
    this.value = 0,
    required this.height,
  });

  final int value;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        // Foreground
        DigitalNumber(
          value: value,
          color: Colors.white,
          height: height,
        ),
        // Background
        DigitalNumber(
          value: 8,
          color: Colors.white24,
          height: height,
        ),
      ],
    );
  }
}
