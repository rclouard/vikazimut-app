// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/map_view/layers/abstract_route_layer.dart';
import 'package:vikazimut/map_view/layers/flat_route_layer.dart';
import 'package:vikazimut/map_view/map_controller.dart';

import 'abstract_track_view.dart';

class WalkTrackView extends AbstractTrackView {
  WalkTrackView({
    required super.map,
    required super.waypoints,
    required super.punchTimes,
    required super.totalTimeInMs,
  });

  @override
  AbstractRouteLayer createRouteOverlay(MapController mapController) => FlatRouteLayer(mapController);

  @override
  PreferredSizeWidget? buildAppBarLegend() => null;
}
