import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

class ReliefHelper {
  static const int _maxNumberOfWaypoints = 300;

  List<ReliefWaypoint>? _shortenedRoute;

  static List<int> computeAltitudeBounds(List<GeodesicPoint> route) {
    if (route.isEmpty) {
      return [0, 0];
    }
    var maxElevation = 0.0;
    var minElevation = double.infinity;
    for (int i = 0; i < route.length; i++) {
      final elevation = route[i].altitude;
      if (elevation <= 0) {
        continue;
      }
      if (elevation > maxElevation) {
        maxElevation = elevation;
      }
      if (elevation < minElevation) {
        minElevation = elevation;
      }
    }
    if (minElevation == double.infinity) {
      minElevation = 0;
    }
    return [minElevation.toInt(), maxElevation.toInt()];
  }

  Future<List<ReliefWaypoint>> shortenRoute(List<GeodesicPoint> route, int totalTimeInMillisecond) async {
    if (_shortenedRoute != null) {
      return _shortenedRoute!;
    }
    if (route.isEmpty) {
      return [];
    }
    _shortenedRoute = shortenRoute_(route, totalTimeInMillisecond, _maxNumberOfWaypoints);
    return _shortenedRoute!;
  }

  @visibleForTesting
  static List<ReliefWaypoint> shortenRoute_(List<GeodesicPoint> route, int totalTimeInMillisecond, int trackMaxLength) {
    List<ReliefWaypoint> shortenedRoute = [];
    final int step = totalTimeInMillisecond ~/ trackMaxLength;
    shortenedRoute.add(ReliefWaypoint(route[0].altitude, route[0].timestampInMillisecond, 0));
    GeodesicPoint? lastPoint;
    double currentCumulatedDistance = 0;
    int currentCumulatedTime = 0;
    double currentCumulatedAltitude = 0;
    int currentNumberOfCumulatedValues = 0;
    int index = 1;
    for (int t = step; t <= totalTimeInMillisecond; t += step) {
      int currentTime = t + route[0].timestampInMillisecond;
      while (index < route.length && route[index].timestampInMillisecond <= currentTime) {
        currentCumulatedDistance += GeodesicPoint.distanceBetweenInMeter(route[index].latitude, route[index].longitude, route[index - 1].latitude, route[index - 1].longitude);
        currentCumulatedTime += route[index].timestampInMillisecond - route[index - 1].timestampInMillisecond;
        currentCumulatedAltitude += route[index].altitude;
        currentNumberOfCumulatedValues++;
        lastPoint = route[index];
        index++;
      }
      if (lastPoint != null) {
        double speedInMs = 1000 * currentCumulatedDistance / currentCumulatedTime;
        double averageAltitude = currentCumulatedAltitude / currentNumberOfCumulatedValues;
        shortenedRoute.add(ReliefWaypoint(averageAltitude, lastPoint.timestampInMillisecond, speedInMs));
      }
      currentCumulatedDistance = 0;
      currentCumulatedTime = 0;
      currentCumulatedAltitude = 0;
      currentNumberOfCumulatedValues = 0;
      lastPoint = null;
    }
    if (shortenedRoute.last.timeInMillisecond != totalTimeInMillisecond) {
      double speedInMs;
      if (currentCumulatedTime == 0) {
        shortenedRoute.add(ReliefWaypoint(route.last.altitude, route.last.timestampInMillisecond, 0));
      } else {
        speedInMs = 1000 * currentCumulatedDistance / currentCumulatedTime;
        shortenedRoute.add(ReliefWaypoint(route.last.altitude, route.last.timestampInMillisecond, speedInMs));
      }
    }
    return shortenedRoute;
  }
}

@immutable
class ReliefWaypoint {
  final double altitude;
  final int timeInMillisecond;
  final double speedInMeterPerSecond;

  const ReliefWaypoint(this.altitude, this.timeInMillisecond, this.speedInMeterPerSecond);
}
