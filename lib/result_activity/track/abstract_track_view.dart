// coverage:ignore-file
import 'dart:io';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_view/layers/abstract_route_layer.dart';
import 'package:vikazimut/map_view/map_controller.dart';
import 'package:vikazimut/map_view/map_view.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/route_data/punch_time.dart';

import 'track_animation_widget.dart';
import 'track_presenter.dart';

@immutable
abstract class AbstractTrackView extends StatefulWidget {
  final OrienteeringMap map;
  final List<GeodesicPoint> waypoints;
  final List<PunchTime> punchTimes;
  final int totalTimeInMs;

  AbstractTrackView({
    required this.map,
    required this.waypoints,
    required this.punchTimes,
    required this.totalTimeInMs,
  }) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  AbstractTrackViewState createState() => AbstractTrackViewState();

  AbstractRouteLayer createRouteOverlay(MapController mapController);

  PreferredSizeWidget? buildAppBarLegend();
}

class AbstractTrackViewState extends State<AbstractTrackView> {
  final MapController _mapController = MapController();
  AbstractRouteLayer? _routeOverlay;
  late final Future<ui.Image> _imageLoader;

  @override
  void initState() {
    _imageLoader = MapController.loadImage(File(widget.map.drawableName));
    super.initState();
  }

  @override
  void dispose() {
    if (_routeOverlay != null) {
      _mapController.removeLayer(_routeOverlay!);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.map.name),
        bottom: widget.buildAppBarLegend(),
      ),
      backgroundColor: Colors.black,
      body: FutureBuilder<ui.Image>(
        future: _imageLoader,
        builder: (BuildContext context, AsyncSnapshot<ui.Image> snapshot) {
          if (snapshot.hasData) {
            var renderBox = context.findRenderObject() as RenderBox;
            MapView mapView = MapView(
              image: snapshot.data!,
              geoPosition: widget.map.getBoundingBox(),
              size: Size(renderBox.size.width, renderBox.size.height - TrackAnimationWidget.sliderHeight),
              mapController: _mapController,
            );
            SchedulerBinding.instance.addPostFrameCallback((_) => _addRouteOnMapViewAsLayer(_mapController));
            return Column(
              children: [
                Expanded(
                  child: mapView,
                ),
                TrackAnimationWidget(
                  mapController: _mapController,
                  route: widget.waypoints,
                  punchTimes: widget.punchTimes,
                  totalTime: Duration(milliseconds: widget.totalTimeInMs),
                  preAnimation: () => _mapController.removeLayer(_routeOverlay!),
                  postAnimation: () => _mapController.addLayer(_routeOverlay!),
                ),
              ],
            );
          } else if (snapshot.hasError) {
            return Text('${L10n.getString("error_prefix")} ${snapshot.error}');
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  void _addRouteOnMapViewAsLayer(MapController mapController) {
    _routeOverlay = widget.createRouteOverlay(mapController);
    mapController.addLayer(_routeOverlay!);
    _routeOverlay!.drawRoute(
      widget.waypoints,
      punchPositions: TrackPresenter.calculatePunchPositions(widget.punchTimes, widget.waypoints),
    );
  }
}
