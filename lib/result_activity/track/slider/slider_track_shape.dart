import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:vikazimut/theme.dart';

import '../relief_helper.dart';

@immutable
class AnimationSliderTrackShape extends SliderTrackShape {
  final List<int> punchTimes;
  final List<ReliefWaypoint> route;
  final int totalTime;
  final int minAltitude;
  final int maxAltitude;

  const AnimationSliderTrackShape({
    required this.totalTime,
    required this.punchTimes,
    required this.route,
    required this.minAltitude,
    required this.maxAltitude,
  });

  @override
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool? isEnabled,
    bool? isDiscrete,
  }) {
    final double thumbWidth = sliderTheme.thumbShape!.getPreferredSize(true, isDiscrete!).width;
    final double trackHeight = sliderTheme.trackHeight!;
    final double trackLeft = offset.dx + thumbWidth / 2;
    final double trackTop = offset.dy + (parentBox.size.height - trackHeight) / 2;
    final double trackWidth = parentBox.size.width - thumbWidth;
    return Rect.fromLTWH(trackLeft, trackTop + 10, trackWidth, trackHeight - 10);
  }

  @override
  void paint(
    PaintingContext context,
    Offset offset, {
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required Animation<double> enableAnimation,
    required TextDirection textDirection,
    required Offset thumbCenter,
    Offset? secondaryOffset,
    bool isDiscrete = false,
    bool isEnabled = false,
  }) {
    final Rect trackRect = getPreferredRect(
      parentBox: parentBox,
      offset: offset,
      sliderTheme: sliderTheme,
      isEnabled: isEnabled,
      isDiscrete: isDiscrete,
    );
    final Paint leftReliefPaint = Paint()
      ..style = PaintingStyle.fill
      ..shader = ui.Gradient.linear(
        Offset(0, trackRect.top),
        Offset(0, trackRect.bottom),
        [
          Colors.white,
          kOrangeColorDisabled,
        ],
      );
    final Paint rightReliefPaint = Paint()
      ..style = PaintingStyle.fill
      ..shader = ui.Gradient.linear(
        Offset(0, trackRect.top),
        Offset(0, trackRect.bottom),
        [Colors.white, kOrangeColorLight],
      );
    final Paint horizontalLinePaint = Paint()
      ..color = kOrangeColorUltraLight
      ..strokeWidth = 0.5
      ..style = PaintingStyle.stroke;
    final Paint tickPaint = Paint()
      ..color = kOrangeColorUltraLight
      ..strokeWidth = 0.5
      ..style = PaintingStyle.stroke;
    final Paint topLeftReliefPaint = Paint()
      ..color = Colors.white
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke;
    final Paint topRightReliefPaint = Paint()
      ..color = Colors.white
      ..strokeWidth = 2
      ..strokeJoin = StrokeJoin.miter
      ..style = PaintingStyle.stroke;
    _displayHorizontalGridLines(context, trackRect, horizontalLinePaint);
    _displayPunchTimesAsTicks(trackRect, context, tickPaint);
    _displayRelief(trackRect, thumbCenter, leftReliefPaint, topLeftReliefPaint, rightReliefPaint, topRightReliefPaint, context);
  }

  void _displayHorizontalGridLines(PaintingContext context, Rect trackRect, Paint paint) {
    context.canvas.drawLine(Offset(trackRect.left, trackRect.top), Offset(trackRect.right, trackRect.top), paint);
    context.canvas.drawLine(Offset(trackRect.left, (3 * trackRect.top + trackRect.bottom) / 4), Offset(trackRect.right, (3 * trackRect.top + trackRect.bottom) / 4), paint);
    context.canvas.drawLine(Offset(trackRect.left, (trackRect.bottom + trackRect.top) / 2), Offset(trackRect.right, (trackRect.bottom + trackRect.top) / 2), paint);
    context.canvas.drawLine(Offset(trackRect.left, (trackRect.top + 3 * trackRect.bottom) / 4), Offset(trackRect.right, (trackRect.top + 3 * trackRect.bottom) / 4), paint);
    context.canvas.drawLine(Offset(trackRect.left, trackRect.bottom), Offset(trackRect.right, trackRect.bottom), paint);
  }

  void _displayRelief(Rect trackRect, Offset thumbCenter, Paint leftReliefPaint, Paint topLeftReliefPaint, Paint rightReliefPaint, Paint topRightReliefPaint, PaintingContext context) {
    final elevationBound = maxAltitude - minAltitude;
    final reliefTrackHeight = trackRect.height - 6;
    final reliefBottom = trackRect.bottom;

    Path reliefBeforeThumb = Path();
    Path topReliefBeforeThumb = Path();
    List<Offset?> relief1Bounds = _displayPartOfRelief(trackRect, 0, thumbCenter.dx, elevationBound, reliefTrackHeight, reliefBottom, reliefBeforeThumb, topReliefBeforeThumb);
    Offset? firstRelief1Offset = relief1Bounds[0];
    Offset? lastRelief1Offset = relief1Bounds[1];

    Path reliefAfterThumb = Path();
    Path topReliefAfterThumb = Path();
    List<Offset?> relief2Bounds = _displayPartOfRelief(trackRect, thumbCenter.dx, trackRect.width, elevationBound, reliefTrackHeight, reliefBottom, reliefAfterThumb, topReliefAfterThumb);
    Offset? firstRelief2Offset = relief2Bounds[0];
    Offset? lastRelief2Offset = relief2Bounds[1];

    // Interpolation
    if (lastRelief1Offset != null && firstRelief2Offset != null) {
      var yOffset = (thumbCenter.dx - lastRelief1Offset.dx) / (firstRelief2Offset.dx - lastRelief1Offset.dx) * (firstRelief2Offset.dy - lastRelief1Offset.dy) + lastRelief1Offset.dy;
      lastRelief1Offset = firstRelief2Offset = Offset(thumbCenter.dx, yOffset);
      reliefBeforeThumb.lineTo(lastRelief1Offset.dx, lastRelief1Offset.dy);
      topReliefBeforeThumb.lineTo(lastRelief1Offset.dx, lastRelief1Offset.dy);
    }
    if (lastRelief1Offset != null) {
      reliefBeforeThumb.lineTo(lastRelief1Offset.dx, reliefBottom);
      reliefBeforeThumb.lineTo(firstRelief1Offset!.dx, reliefBottom);
    }
    if (lastRelief2Offset != null) {
      reliefAfterThumb.lineTo(lastRelief2Offset.dx, reliefBottom);
      reliefAfterThumb.lineTo(firstRelief2Offset!.dx, reliefBottom);
      reliefAfterThumb.lineTo(firstRelief2Offset.dx, firstRelief2Offset.dy);
      topReliefAfterThumb.moveTo(relief2Bounds[0]!.dx, relief2Bounds[0]!.dy);
      topReliefAfterThumb.lineTo(firstRelief2Offset.dx, firstRelief2Offset.dy);
    }

    context.canvas.drawPath(reliefBeforeThumb, leftReliefPaint);
    context.canvas.drawPath(topReliefBeforeThumb, topLeftReliefPaint);
    context.canvas.drawPath(reliefAfterThumb, rightReliefPaint);
    context.canvas.drawPath(topReliefAfterThumb, topRightReliefPaint);
  }

  void _displayPunchTimesAsTicks(Rect trackRect, PaintingContext context, Paint paint) {
    for (int i = 0; i < punchTimes.length; i++) {
      final punchTime = punchTimes[i];
      if (punchTime <= totalTime && punchTime >= 0) {
        final xOffset = trackRect.left + punchTimes[i] * trackRect.width / totalTime;
        context.canvas.drawLine(Offset(xOffset, trackRect.top), Offset(xOffset, trackRect.top + trackRect.height), paint);
      }
    }
  }

  List<Offset?> _displayPartOfRelief(Rect trackRect, double minRelief, double maxRelief, int elevationBound, double reliefTrackHeight, double reliefBottom, Path relief, Path topRelief) {
    List<Offset?> reliefPartBounds = [null, null];
    for (int i = 0; i < route.length; i++) {
      final xOffset = route[i].timeInMillisecond * trackRect.width / totalTime;
      final reliefLeft = trackRect.left + xOffset;
      if (reliefLeft > maxRelief) {
        break;
      }
      if (reliefLeft < minRelief) {
        continue;
      }
      double yOffset;
      if (elevationBound == 0) {
        yOffset = 1 + reliefTrackHeight / 2;
      } else {
        yOffset = 1 + reliefTrackHeight * (route[i].altitude - minAltitude) / elevationBound;
      }
      reliefPartBounds[1] = Offset(reliefLeft, reliefBottom - yOffset);
      if (reliefPartBounds[0] == null) {
        reliefPartBounds[0] = reliefPartBounds[1];
        relief.moveTo(reliefPartBounds[0]!.dx, reliefPartBounds[0]!.dy);
        topRelief.moveTo(reliefPartBounds[0]!.dx, reliefPartBounds[0]!.dy);
      } else {
        relief.lineTo(reliefPartBounds[1]!.dx, reliefPartBounds[1]!.dy);
        topRelief.lineTo(reliefPartBounds[1]!.dx, reliefPartBounds[1]!.dy);
      }
    }
    return reliefPartBounds;
  }
}
