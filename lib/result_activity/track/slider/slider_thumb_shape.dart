import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';

import '../relief_helper.dart';

@immutable
class AnimationSlideThumbShape extends SliderComponentShape {
  final List<ReliefWaypoint> route;
  final int totalTimeInMillisecond;
  final Color textColor;

  const AnimationSlideThumbShape({
    required this.route,
    required this.totalTimeInMillisecond,
    required this.textColor,
  });

  @override
  Size getPreferredSize(bool isEnabled, bool isDiscrete) => const Size.fromRadius(4);

  @override
  void paint(
    PaintingContext context,
    Offset center, {
    required Animation<double> activationAnimation,
    required Animation<double> enableAnimation,
    required bool isDiscrete,
    required TextPainter labelPainter,
    required RenderBox parentBox,
    required SliderThemeData sliderTheme,
    required TextDirection textDirection,
    required double value,
    required double textScaleFactor,
    required Size sizeWithOverflow,
  }) {
    // Draw thumb line
    final thumbPaint = Paint()
      ..color = sliderTheme.thumbColor!
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.5;
    context.canvas.drawLine(Offset(center.dx, 15), Offset(center.dx, center.dy + sliderTheme.trackHeight! / 2 - 5), thumbPaint);

    // Draw thumb label on top of the thumb
    final textPainter = TextPainter(textDirection: TextDirection.ltr);
    final double speed = getSpeedInKmhForCurrentThumbPosition_(route, value, totalTimeInMillisecond);
    textPainter.text = TextSpan(
      style: TextStyle(
        color: textColor,
        fontSize: 8,
      ),
      text: '${L10n.formatNumber(speed, 1).padLeft(4)} km/h',
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: double.maxFinite,
    );
    const int textWidth = 40;
    final Rect rect = Rect.fromLTWH(center.dx - textWidth / 2 - 3, 3, textWidth + 6, 12);
    final Paint thumbLabelBorderPaint = Paint()
      ..color = sliderTheme.thumbColor!
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1;
    context.canvas.drawRRect(RRect.fromRectAndRadius(rect, const Radius.circular(100.0)), thumbLabelBorderPaint);
    textPainter.paint(context.canvas, Offset(center.dx + 19 - textPainter.width, 5));
  }

  @visibleForTesting
  static double getSpeedInKmhForCurrentThumbPosition_(List<ReliefWaypoint> trackReliefPoint, double value, int totalTimeInMillisecond) {
    if (trackReliefPoint.isEmpty || value == 0) {
      return 0;
    }
    final currentTimeInMillisecond = value * totalTimeInMillisecond;
    for (int i = 1; i < trackReliefPoint.length; i++) {
      final time = trackReliefPoint[i].timeInMillisecond;
      if (time >= currentTimeInMillisecond) {
        final double speed = trackReliefPoint[i].speedInMeterPerSecond;
        return 3.6 * speed;
      }
    }
    return 0;
  }
}
