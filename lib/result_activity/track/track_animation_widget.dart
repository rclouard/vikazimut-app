// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/map_view/map_controller.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_time.dart';
import 'package:vikazimut/theme.dart';

import 'digital_clock/neumorphic_digital_clock.dart';
import 'relief_helper.dart';
import 'slider/slider_thumb_shape.dart';
import 'slider/slider_track_shape.dart';
import 'track_animation_presenter.dart';

@immutable
class TrackAnimationWidget extends StatefulWidget {
  static const double sliderHeight = 100.0;

  final MapController mapController;
  final List<GeodesicPoint> route;
  final List<PunchTime> punchTimes;
  final Duration totalTime;
  final Function() preAnimation;
  final Function() postAnimation;

  const TrackAnimationWidget({
    required this.route,
    required this.mapController,
    required this.punchTimes,
    required this.totalTime,
    required this.preAnimation,
    required this.postAnimation,
  });

  @override
  TrackAnimationWidgetState createState() => TrackAnimationWidgetState();
}

class TrackAnimationWidgetState extends State<TrackAnimationWidget> with TickerProviderStateMixin {
  final ReliefHelper _reliefHelper = ReliefHelper();
  late final TrackAnimationPresenter _animation;
  bool _userIsMovingSlider = false;
  bool _isRunning = false;
  bool _isPausing = false;
  double _sliderValue = 0;
  late int _maxAltitude;
  late int _minAltitude;
  int _currentTimeInMillisecond = 0;
  late final List<int> _sortedPunchTimes;

  @override
  void initState() {
    final List<int> elevationBounds = ReliefHelper.computeAltitudeBounds(widget.route);
    _minAltitude = elevationBounds[0];
    _maxAltitude = elevationBounds[1];
    _sortedPunchTimes = TrackAnimationPresenter.sortPunchTimesInChronologicalOrder(widget.punchTimes);
    _animation = TrackAnimationPresenter(this, widget.mapController, _sortedPunchTimes)
      ..whenComplete(() {
        widget.postAnimation();
        setState(() {
          _isRunning = false;
        });
      });
    super.initState();
  }

  @override
  void dispose() {
    _animation.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!_userIsMovingSlider) {
      _sliderValue = _getSliderValue();
    }
    return Container(
      height: TrackAnimationWidget.sliderHeight,
      padding: const EdgeInsets.only(left: 3),
      decoration: const BoxDecoration(
        color: kOrangeColor,
      ),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 10),
              NeumorphicDigitalClock(
                height: 32,
                width: 80,
                time: _getDuration(_sliderValue),
              ),
              _buildPlayPauseButton(),
            ],
          ),
          _buildCurrentTimeLabel(),
          _buildAnimationSliderBar(),
          const SizedBox(width: 15),
        ],
      ),
    );
  }

  void update(int value) {
    setState(() {
      _currentTimeInMillisecond = value;
    });
  }

  Widget _buildCurrentTimeLabel() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 15.0, 0, 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            "$_maxAltitude m",
            style: const TextStyle(fontSize: 9, color: Colors.white),
          ),
          Text(
            "$_minAltitude m",
            style: const TextStyle(fontSize: 9, color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget _buildAnimationSliderBar() {
    return FutureBuilder(
        future: _reliefHelper.shortenRoute(widget.route, widget.totalTime.inMilliseconds),
        builder: (BuildContext context, AsyncSnapshot<List<ReliefWaypoint>> route) {
          if (route.hasData) {
            return Expanded(
              child: SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  trackHeight: 80.0,
                  thumbColor: Colors.white,
                  trackShape: AnimationSliderTrackShape(
                    totalTime: widget.totalTime.inMilliseconds,
                    route: route.data!,
                    punchTimes: _sortedPunchTimes,
                    minAltitude: _minAltitude,
                    maxAltitude: _maxAltitude,
                  ),
                  thumbShape: AnimationSlideThumbShape(
                    totalTimeInMillisecond: widget.totalTime.inMilliseconds,
                    route: route.data!,
                    textColor: Colors.white,
                  ),
                  overlayShape: const RoundSliderOverlayShape(overlayRadius: 14.0),
                  overlayColor: Colors.white.withAlpha(32),
                ),
                child: Slider(
                  value: _sliderValue,
                  onChangeStart: (_isRunning) ? null : (value) => _userIsMovingSlider = true,
                  onChanged: (_isRunning)
                      ? null
                      : (value) => setState(() {
                            _sliderValue = value;
                            _currentTimeInMillisecond = (value * widget.totalTime.inMilliseconds).toInt();
                            _moveAnimationManually(_currentTimeInMillisecond);
                          }),
                  onChangeEnd: (_isRunning) ? null : (value) => _userIsMovingSlider = false,
                ),
              ),
            );
          } else {
            return Container();
          }
        });
  }

  Widget _buildPlayPauseButton() {
    return IconButton(
      icon: (_isRunning)
          ? const Icon(
              semanticLabel: "Stop animation",
              Icons.pause_circle_outline,
              size: 32,
            )
          : const Icon(
              semanticLabel: "Run animation",
              Icons.play_circle_outline,
              size: 32,
            ),
      color: Colors.white,
      onPressed: () {
        setState(() {
          if (!_isRunning) {
            if (!_isPausing) {
              _runAnimation();
            } else {
              _resumeAnimation();
              _isPausing = false;
            }
            _isRunning = true;
          } else {
            _isRunning = false;
            _isPausing = true;
            _pauseAnimation();
          }
        });
      },
    );
  }

  double _getSliderValue() {
    if (widget.totalTime.inMilliseconds == 0) {
      return 0;
    } else {
      return _currentTimeInMillisecond / widget.totalTime.inMilliseconds;
    }
  }

  Duration _getDuration(double sliderValue) {
    final seconds = widget.totalTime.inSeconds * sliderValue;
    return Duration(seconds: seconds.toInt());
  }

  void _moveAnimationManually(int time) {
    widget.preAnimation();
    _animation.changeCurrentPositionManually(widget.route, time, widget.totalTime.inMilliseconds);
  }

  void _resumeAnimation() {
    _animation.resume();
    _animation.execute(widget.route, _currentTimeInMillisecond, widget.totalTime.inMilliseconds);
  }

  void _runAnimation() {
    widget.preAnimation();
    _animation.execute(widget.route, _currentTimeInMillisecond, widget.totalTime.inMilliseconds);
  }

  void _pauseAnimation() {
    _animation.pause();
  }
}
