import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/orienteering_map/orienteering_map.dart';
import 'package:vikazimut/orienteering_map/orienteering_map_factory.dart';
import 'package:vikazimut/result_activity/leg_helper.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';
import 'package:vikazimut/route_data/punch_time.dart';
import 'package:vikazimut/utils/time.dart';

import 'elevation_helper.dart';
import 'leg.dart';
import 'punch_time_helper.dart';

class Result {
  @visibleForTesting
  static const int timePenaltyPerAssistance = 5 * 60 * 1000;

  late final int _courseId;
  late final String _mapName;
  late final int _format;
  late final int _mode;
  late final int _quizTotalPoints;
  late final int _assistanceCount;
  late final List<GeodesicPoint> _waypoints;
  late final OrienteeringMap? _map;
  late final List<PunchTime> _punchTimes;
  late final int _runCount;
  late bool isSent;

  late final double _globalAverageSpeedInKmH;
  late final int _globalPaceInMillisecondByKm;
  late final int _cumulativeElevationGainInM;
  late final double _actualRouteLengthInM;
  late final double _totalTheoreticalRouteLengthInM;
  late final double _totalTheoreticalCourseLengthInM;
  late final int _totalDurationInMs;
  late final int _actualDurationInMs;
  late final int _score;
  late final int _totalScore;

  Result(CourseResultEntity courseResultEntity) {
    try {
      _courseId = courseResultEntity.courseId;
      _mapName = courseResultEntity.mapName;
      _waypoints = getGpsRouteAsWaypoints_(courseResultEntity.gpsTrack!);
      _assistanceCount = courseResultEntity.assistanceCount;

      _format = courseResultEntity.format;
      _mode = courseResultEntity.mode;
      isSent = courseResultEntity.isSent;
      _runCount = courseResultEntity.runCount;
      _quizTotalPoints = courseResultEntity.quizTotalPoints;
      _punchTimes = PunchTimeHelper.readPunchTimesFromString(courseResultEntity.punchTimes!);
      _map = OrienteeringMapFactory.createOrienteeringMapFromXmlAndKmlAndImgFiles(_courseId, _mapName);
    } catch (_) {
      _map = null;
    }
  }

  void computeStatisticsFromResult() {
    _totalDurationInMs = Result.calculateTotalDurationInMs_(_waypoints);
    _actualDurationInMs = Result.calculateActualRouteDurationInMs_(_waypoints);
    _actualRouteLengthInM = Result.calculateActualRouteLengthInM_(_waypoints);
    _globalAverageSpeedInKmH = Result.calculateAverageSpeedInKmh(_totalDurationInMs, _actualRouteLengthInM);
    _cumulativeElevationGainInM = ElevationHelper.calculateCumulativeElevationGain(_waypoints).toInt();

    if (_map != null) {
      _score = Result.calculateScore_(_map!.checkpoints, _punchTimes);
      _totalScore = Result.calculateTotalScore(_map!.checkpoints);
      _totalTheoreticalRouteLengthInM = Result.calculateTheoreticalRouteLength_(_map!.checkpoints, _punchTimes);
      _totalTheoreticalCourseLengthInM = _map!.theoreticalLengthInMeter;
      _globalPaceInMillisecondByKm = Result.calculatePaceInMsByKm(_actualDurationInMs, _totalTheoreticalRouteLengthInM);
    }
  }

  String get mapName => _mapName;

  List<GeodesicPoint> get waypoints => _waypoints;

  String get penaltyTimeAsString => formatTimeAsString(_assistanceCount * timePenaltyPerAssistance);

  int get totalCheckpoints => _map!.getCheckpointCount();

  List<PunchTime> get punchTimes => _punchTimes;

  bool get isAllCheckpointsScanned => PunchTimeHelper.isAllCheckpointsScanned(_punchTimes);

  int get totalTimeWithoutPenaltyInMs => _totalDurationInMs;

  int get totalTimeWithPenaltyInMs => _totalDurationInMs + _assistanceCount * timePenaltyPerAssistance;

  String get totalTimeWithPenaltyAsString => formatTimeAsString(totalTimeWithPenaltyInMs);

  String get totalTimeWithoutPenaltyAsString => formatTimeAsString(totalTimeWithoutPenaltyInMs);

  int get format => _format;

  int get courseId => _courseId;

  int get runCount => _runCount;

  OrienteeringMap? get map => _map;

  int get mode => _mode;

  int get quizTotalPoints => _quizTotalPoints;

  int get assistanceCount => _assistanceCount;

  int get score => _score;

  get totalScore => _totalScore;

  double get actualRouteLengthInM => _actualRouteLengthInM;

  double get totalTheoreticalCourseLengthInM => _totalTheoreticalCourseLengthInM;

  int get cumulativeElevationGainInM => _cumulativeElevationGainInM;

  int get globalPaceInMillisecondByKm => _globalPaceInMillisecondByKm;

  double get globalAverageSpeedInKmH => _globalAverageSpeedInKmH;

  int get totalDurationInMs => _totalDurationInMs;

  double get totalTheoreticalRouteLengthInM => _totalTheoreticalRouteLengthInM;

  String computeWayPointsAsString() => computeWaypointsAsString_(_waypoints);

  List<Leg> computeDetailedStatistics() => getDetailedStatistics_(_map!.checkpoints, _punchTimes, _waypoints);

  static int calculatePaceInMsByKm(int totalTimeInMillisecond, double lengthInMeter) {
    double estimateDistance = lengthInMeter;
    if (estimateDistance <= 0 || totalTimeInMillisecond <= 0) {
      return 0;
    }
    return (totalTimeInMillisecond * 1000) ~/ estimateDistance;
  }

  static double calculateAverageSpeedInKmh(int totalTimeInMillisecond, double lengthInMeter) {
    if (lengthInMeter <= 0 || totalTimeInMillisecond <= 0) {
      return 0;
    }
    return 3600 * lengthInMeter / totalTimeInMillisecond;
  }

  static int computeOvercostPercentage(double realDistance, double theoreticalDistance) {
    if (theoreticalDistance == 0 || realDistance - theoreticalDistance < 0) {
      return 0;
    } else {
      return 100 * (realDistance - theoreticalDistance) ~/ theoreticalDistance;
    }
  }

  @visibleForTesting
  static double calculateActualRouteLengthInM_(List<GeodesicPoint> waypoints) {
    if (waypoints.isEmpty) {
      return 0;
    }
    double distance = 0;
    for (int i = 1; i < waypoints.length; i++) {
      final point1 = waypoints[i - 1];
      final point2 = waypoints[i];
      distance += point1.distanceToInMeter(point2);
    }
    return distance;
  }

  @visibleForTesting
  static int calculateTotalDurationInMs_(List<GeodesicPoint> waypoints) {
    if (waypoints.isEmpty) {
      return 0;
    } else {
      return waypoints.last.timestampInMillisecond;
    }
  }

  @visibleForTesting
  static int calculateActualRouteDurationInMs_(List<GeodesicPoint> waypoints) {
    if (waypoints.isEmpty) {
      return 0;
    }
    int totalTime = 0;
    for (int i = 1; i < waypoints.length; i++) {
      final point1 = waypoints[i - 1];
      final point2 = waypoints[i];
      final time = point2.timestampInMillisecond - point1.timestampInMillisecond;
      totalTime += time;
    }
    return totalTime;
  }

  @visibleForTesting
  static List<GeodesicPoint> getGpsRouteAsWaypoints_(String gpsRoute) {
    List<GeodesicPoint> route = [];
    if (gpsRoute.isNotEmpty) {
      List<String> geodesicPoints = gpsRoute.split(";");
      for (final point in geodesicPoints) {
        if (point.isNotEmpty) {
          List<String> coordinates = point.split(",");
          double latitude = double.parse(coordinates[0]);
          double longitude = double.parse(coordinates[1]);
          int elapsedTime = int.parse(coordinates[2]);
          double altitude = double.parse(coordinates[3]);
          route.add(GeodesicPoint(latitude, longitude, elapsedTime, altitude));
        }
      }
    }
    return route;
  }

  @visibleForTesting
  static String computeWaypointsAsString_(List<GeodesicPoint> waypoints) {
    const int baseTime = 0;
    String gpxString = "";
    for (GeodesicPoint geodesicPoint in waypoints) {
      double latitude = geodesicPoint.latitude;
      double longitude = geodesicPoint.longitude;
      double altitude = geodesicPoint.altitude;
      int relativePunchTime = geodesicPoint.timestampInMillisecond - baseTime;
      gpxString += sprintf(ServerConstants.gpsCoordinateTimedFormat, [latitude, longitude, relativePunchTime, altitude]);
    }
    return gpxString;
  }

  @visibleForTesting
  static double calculateTheoreticalRouteLength_(List<Checkpoint> checkpoints, List<PunchTime> punchTimes) {
    List<Leg> legs = buildLegsFromPunchTimes_(checkpoints, punchTimes);
    LegHelper.sortLegsByPunchTimes(legs);
    var totalTheoreticalDistance = 0.0;
    for (var i = 1; i < legs.length; i++) {
      if (legs[i].punchTime > 0) {
        totalTheoreticalDistance += legs[i - 1].checkpointLocation.distanceToInMeter(legs[i].checkpointLocation);
      }
    }
    return totalTheoreticalDistance;
  }

  @visibleForTesting
  List<Leg> getDetailedStatistics_(
    List<Checkpoint> checkpoints,
    List<PunchTime> punchTimes,
    List<GeodesicPoint> waypoints,
  ) {
    // Remember that leg[1] is the first leg while leg[0] is only used to store the start checkpoint
    final List<Leg> legs = buildLegsFromPunchTimes_(checkpoints, punchTimes);
    LegHelper.sortLegsByPunchTimes(legs);
    LegHelper.splitRouteIntoLegs(waypoints, legs);
    _computeStatsByLeg(legs);
    if (_format.isPresetFormat) {
      LegHelper.sortLegsByControlIndex(legs);
    }
    return legs;
  }

  static void _computeStatsByLeg(List<Leg> legs) {
    double computeTheoreticalDistanceOfLeg(List<Leg> legs, int i) {
      return legs[i - 1].checkpointLocation.distanceToInMeter(legs[i].checkpointLocation);
    }

    int computeDurationForLeg(List<Leg> legs, int i) {
      return legs[i].punchTime - legs[i - 1].punchTime;
    }

    double computeActualDistanceForLeg(List<Leg> legs, int i) {
      final waypoints = legs[i].waypoints;
      double distance = 0;
      if (waypoints != null) {
        for (int j = 1; j < waypoints.length; j++) {
          distance += waypoints[j - 1].distanceToInMeter(waypoints[j]);
        }
      }
      return distance;
    }

    for (var i = 1; i < legs.length; i++) {
      final theoreticalDistance = computeTheoreticalDistanceOfLeg(legs, i);
      legs[i].theoreticalDistance = theoreticalDistance;
      var actualDistance = computeActualDistanceForLeg(legs, i);
      if (actualDistance < theoreticalDistance) {
        actualDistance = theoreticalDistance;
      }
      legs[i].actualDistance = actualDistance;
      final actualDuration = computeDurationForLeg(legs, i);
      legs[i].duration = actualDuration;
      legs[i].paceInMnK = calculatePaceInMsByKm(actualDuration, theoreticalDistance.toDouble());
    }
  }

  @visibleForTesting
  static List<Leg> buildLegsFromPunchTimes_(List<Checkpoint> checkpoints, List<PunchTime> punchTimes) {
    List<Leg> legs = List.generate(checkpoints.length, (index) => Leg(index));
    for (int i = 0; i < checkpoints.length; i++) {
      final Leg leg = legs[i];
      final Checkpoint checkpoint = checkpoints[i];
      final punchTime = punchTimes[i];
      leg.id = checkpoint.id;
      if (i == 0) {
        leg.punchTime = 0;
      } else {
        leg.punchTime = punchTime.timestampInMillisecond;
      }
      leg.forced = punchTime.forced;
      leg.checkpointLocation = checkpoint.location;
    }
    return legs;
  }

  @visibleForTesting
  static int calculateScore_(List<Checkpoint> checkpoints, List<PunchTime> punchTimes) {
    int totalScore = 0;
    for (int i = 1; i < checkpoints.length - 1; i++) {
      var points = checkpoints[i].points;
      if (punchTimes[i].timestampInMillisecond > 0) {
        totalScore += points;
      }
    }
    return totalScore;
  }

  static int calculateTotalScore(List<Checkpoint> checkpoints) {
    int totalScore = 0;
    for (int i = 1; i < checkpoints.length - 1; i++) {
      var points = checkpoints[i].points;
      totalScore += points;
    }
    return totalScore;
  }
}
