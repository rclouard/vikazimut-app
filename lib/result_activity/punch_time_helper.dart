import 'package:flutter/material.dart';
import 'package:vikazimut/route_data/punch_time.dart';

@immutable
class PunchTimeHelper {
  const PunchTimeHelper._();

  static List<PunchTime> readPunchTimesFromString(String punchTimesAsString) {
    if (punchTimesAsString.isEmpty) {
      return [];
    }
    List<String> legs = punchTimesAsString.split(";");
    List<PunchTime> result = [];
    for (int i = 0; i < legs.length; i++) {
      List<String> parts = legs[i].split(":");
      var time = int.parse(parts[0]);
      var forced = parts.length > 1;
      result.add(PunchTime(time, forced));
    }
    return result;
  }

  static bool isAllCheckpointsScanned(List<PunchTime> cumulativeTimes) {
    for (int i = 1; i < cumulativeTimes.length; i++) {
      if (cumulativeTimes[i].timestampInMillisecond < 0) {
        return false;
      }
    }
    return true;
  }
}
