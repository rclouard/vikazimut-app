import 'package:flutter/foundation.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'checkpoint.dart';
import 'punch_time.dart';

class CourseLegList {
  final List<PunchTime> _punchTimes;
  int _currentScore = 0;

  CourseLegList(int checkpointsCount) : _punchTimes = List<PunchTime>.generate(checkpointsCount, (_) => PunchTime(), growable: false);

  int get currentScore => _currentScore;

  @visibleForTesting
  List<PunchTime> get punchTimes => _punchTimes;

  void resetPunchTimes(List<PunchTime> punchTimes, List<Checkpoint> checkpoints) {
    _currentScore = 0;
    for (var i = 0; i < punchTimes.length; ++i) {
      _punchTimes[i] = punchTimes[i];
      if (_isRouteCheckpoint(i) && _punchTimes[i].timestampInMillisecond != PunchTime.UNPUNCHED) {
        _currentScore += checkpoints[i].points;
      }
    }
  }

  bool _isRouteCheckpoint(int checkpointIndex) => checkpointIndex > 0 && checkpointIndex < _punchTimes.length - 1;

  void punch(int checkpointIndex, int score, int time, bool forced) {
    if (_isRouteCheckpoint(checkpointIndex)) {
      _currentScore += score;
    }
    _punchTimes[checkpointIndex].timestampInMillisecond = time;
    _punchTimes[checkpointIndex].forced = forced;
  }

  void unpunch(int checkpointIndex, int score) {
    if (_isRouteCheckpoint(checkpointIndex)) {
      _currentScore -= score;
    }
    _punchTimes[checkpointIndex].timestampInMillisecond = PunchTime.UNPUNCHED;
    _punchTimes[checkpointIndex].forced = false;
  }

  bool isAllIntermediateValidatedCheckpoints() {
    int count = 0;
    for (var punchTime in _punchTimes) {
      if (punchTime.timestampInMillisecond == PunchTime.UNPUNCHED) {
        count++;
      }
    }
    return count == 1;
  }

  bool isCheckpointPunched(int checkpointIndex) => _punchTimes[checkpointIndex].timestampInMillisecond != PunchTime.UNPUNCHED;

  bool isAllCheckpointPunchedExcept(int checkpointIndex) {
    for (int i = 0; i < _punchTimes.length; i++) {
      if (_punchTimes[i].timestampInMillisecond == PunchTime.UNPUNCHED && checkpointIndex != i) {
        return false;
      }
    }
    return true;
  }

  bool isLastCheckpointPunched() {
    return _punchTimes.last.timestampInMillisecond != PunchTime.UNPUNCHED;
  }

  int countPunchedCheckpoints() {
    int count = 0;
    for (var punchTime in _punchTimes) {
      if (punchTime.timestampInMillisecond != PunchTime.UNPUNCHED) {
        count++;
      }
    }
    return count;
  }

  List<int> getPunchTimesAsMilliseconds() {
    return _punchTimes.map((PunchTime punchTime) => punchTime.timestampInMillisecond).toList(growable: false);
  }

  int getNextCheckpointIndexInPresetOrder() => getNextCheckpointIndexInPresetOrder_(_punchTimes);

  @visibleForTesting
  static int getNextCheckpointIndexInPresetOrder_(List<PunchTime> punchTimes) {
    for (var i = 0; i < punchTimes.length; ++i) {
      if (punchTimes[i].timestampInMillisecond < 0) {
        return i;
      }
    }
    return 0;
  }

  int getLastPunchedCheckpointIndex() => getLastPunchedCheckpointIndex_(_punchTimes);

  @visibleForTesting
  static int getLastPunchedCheckpointIndex_(List<PunchTime> punchTimes) {
    int lastPunchTimeIndex = 0;
    int lastPunchTime = 0;
    for (var i = 1; i < punchTimes.length; ++i) {
      if (punchTimes[i].timestampInMillisecond > lastPunchTime) {
        lastPunchTime = punchTimes[i].timestampInMillisecond;
        lastPunchTimeIndex = i;
      }
    }
    return lastPunchTimeIndex;
  }

  String? getFinalPunchTimeListAsFormattedString(
    List<GeodesicPoint> waypoints,
    List<Checkpoint> checkpoints,
    int format,
    int detectionRadius,
  ) {
    final List<PunchTime> augmentedPunchTimes = PunchTime.punchCheckpointsAfterMissedPunches(_punchTimes, waypoints, checkpoints, format, detectionRadius);
    return PunchTime.createPunchTimesAsFormattedString(augmentedPunchTimes);
  }
}
