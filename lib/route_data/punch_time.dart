import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/route_data/checkpoint.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

class PunchTime {
  static const int UNPUNCHED = -1;
  int timestampInMillisecond;
  bool forced;

  PunchTime([this.timestampInMillisecond = UNPUNCHED, this.forced = false]);

  static String createPunchTimesAsFormattedString(List<PunchTime> punchTimes) {
    StringBuffer legTimes = StringBuffer("${punchTimes[0].timestampInMillisecond}");
    for (int i = 1; i < punchTimes.length; i++) {
      if (punchTimes[i].forced) {
        legTimes.write(";${punchTimes[i].timestampInMillisecond}:true");
      } else {
        legTimes.write(";${punchTimes[i].timestampInMillisecond}");
      }
    }
    return legTimes.toString();
  }

  /// In preset order, punch artificially a checkpoint that is not punched
  /// but a waypoint passes close enough to the checkpoint.
  static List<PunchTime> punchCheckpointsAfterMissedPunches(
    List<PunchTime> punchTimes,
    List<GeodesicPoint> waypoints,
    List<Checkpoint> checkpoints,
    int format,
    int maxDistanceToCPInMeter,
  ) {
    if (format != CourseFormat.PRESET.index || waypoints.isEmpty) {
      return punchTimes;
    }
    final List<Map<String, dynamic>> tmpPunchTimes = [];
    var minUnpunchedCheckpointIndex = 0;
    for (int i = 0; i < punchTimes.length; i++) {
      if (punchTimes[i].timestampInMillisecond >= 0) {
        tmpPunchTimes.add({
          "time": punchTimes[i].timestampInMillisecond,
          "forced": punchTimes[i].forced,
          "distance": 0,
          "close": true,
        });
      } else {
        if (minUnpunchedCheckpointIndex == 0) {
          minUnpunchedCheckpointIndex = i;
        }
        tmpPunchTimes.add({
          "time": -1,
          "forced": true,
          "distance": maxDistanceToCPInMeter,
          "close": false,
        });
      }
    }
    bool isAllPunched = minUnpunchedCheckpointIndex == 0;
    if (isAllPunched) {
      return punchTimes;
    }

    // 1. General case without start and end
    var currentCheckpointToPunch = minUnpunchedCheckpointIndex;
    for (var waypoint in waypoints) {
      for (int i = minUnpunchedCheckpointIndex; i < tmpPunchTimes.length - 1; i++) {
        if (tmpPunchTimes[i]["close"]) {
          continue;
        }
        var distance = GeodesicPoint.distanceBetweenInMeter(waypoint.latitude, waypoint.longitude, checkpoints[i].location.latitude, checkpoints[i].location.longitude);
        if (distance <= tmpPunchTimes[i]["distance"]) {
          tmpPunchTimes[i]["distance"] = distance;
          tmpPunchTimes[i]["time"] = waypoint.timestampInMillisecond;
          if (i < currentCheckpointToPunch) {
            // In preset order, unpunch all next checkpoints if punched by hazard
            for (int j = i + 1; j < tmpPunchTimes.length - 1; j++) {
              tmpPunchTimes[j]["time"] = -1;
              tmpPunchTimes[j]["distance"] = maxDistanceToCPInMeter;
              tmpPunchTimes[j]["close"] = false;
            }
          }
        }
        // Punch next control point -> close current checkpoint
        if (currentCheckpointToPunch != i && distance <= maxDistanceToCPInMeter) {
          if (tmpPunchTimes[currentCheckpointToPunch]["time"] > 0) {
            tmpPunchTimes[currentCheckpointToPunch]["close"] = true;
          }
          currentCheckpointToPunch = i;
          // Checkpoints between current and i are supposed to be unpunched in preset order.
        }
      }
    }

    // 2. Special case of end checkpoint
    var lastPunchedCheckpointIndex = tmpPunchTimes.length - 1;
    if (punchTimes[lastPunchedCheckpointIndex].timestampInMillisecond < 0) {
      for (int i = tmpPunchTimes.length - 2; i >= 0; i--) {
        if (tmpPunchTimes[i]["time"] > 0) {
          lastPunchedCheckpointIndex = i;
          break;
        }
      }
      for (var waypoint in waypoints) {
        if (waypoint.timestampInMillisecond < tmpPunchTimes[lastPunchedCheckpointIndex]["time"]) {
          continue;
        }
        var distance = GeodesicPoint.distanceBetweenInMeter(waypoint.latitude, waypoint.longitude, checkpoints.last.location.latitude, checkpoints.last.location.longitude);
        if (distance <= tmpPunchTimes.last["distance"]) {
          tmpPunchTimes.last["distance"] = distance;
          tmpPunchTimes.last["time"] = waypoint.timestampInMillisecond;
          tmpPunchTimes.last["close"] = true;
        }
      }
      // At worst, use the last
      if (!tmpPunchTimes.last["close"]) {
        tmpPunchTimes.last["time"] = waypoints.last.timestampInMillisecond;
      }
    }

    List<PunchTime> resultedPunchTimes = [];
    for (int i = 0; i < tmpPunchTimes.length; i++) {
      resultedPunchTimes.add(PunchTime(tmpPunchTimes[i]["time"], tmpPunchTimes[i]["forced"]));
    }
    return resultedPunchTimes;
  }
}
