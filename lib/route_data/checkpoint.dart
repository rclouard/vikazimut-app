import 'package:flutter/foundation.dart';

import 'geodesic_point.dart';

@immutable
class Checkpoint {
  final int index;
  final String id;
  final String? text;
  final GeodesicPoint location;
  final int points;

  Checkpoint(
    this.index,
    this.id,
    double latitude,
    double longitude,
    this.points, [
    this.text,
  ]) : location = GeodesicPoint(latitude, longitude);
}
