import 'dart:math';

import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/result_activity/elevation_helper.dart';

class GeodesicPoint {
  static const double _EARTH_RADIUS_IN_METER = 6378137.0;
  final double longitude;
  final double latitude;
  double altitude;
  int timestampInMillisecond;

  GeodesicPoint(this.latitude, this.longitude, [this.timestampInMillisecond = 0, this.altitude = ElevationHelper.NO_ELEVATION]);

  /// @see <a href="https://en.wikipedia.org/wiki/Haversine_formula">Haversine formula</a>
  double distanceToInMeter(final GeodesicPoint other) {
    return distanceBetweenInMeter(latitude, longitude, other.latitude, other.longitude);
  }

  static double distanceBetweenInMeter(
    double startLatitude,
    double startLongitude,
    double endLatitude,
    double endLongitude,
  ) {
    double sqr(double x) => x * x;
    double deg2rad(double degree) => degree * pi / 180;

    var deltaLatitude = deg2rad(endLatitude - startLatitude);
    var deltaLongitude = deg2rad(endLongitude - startLongitude);
    var a = sqr(sin(deltaLatitude / 2)) + sqr(sin(deltaLongitude / 2)) * cos(deg2rad(startLatitude)) * cos(deg2rad(endLatitude));
    var c = 2 * asin(sqrt(a));
    return _EARTH_RADIUS_IN_METER * c;
  }

  static String waypointsToString(List<GeodesicPoint> waypoints) {
    StringBuffer gpxString = StringBuffer("");
    for (final geodesicPoint in waypoints) {
      gpxString.write(sprintf(ServerConstants.gpsCoordinateTimedFormat, [
        geodesicPoint.latitude,
        geodesicPoint.longitude,
        geodesicPoint.timestampInMillisecond,
        geodesicPoint.altitude,
      ]));
    }
    return gpxString.toString();
  }
}
