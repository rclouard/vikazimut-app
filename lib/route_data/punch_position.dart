import 'package:flutter/foundation.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

@immutable
class PunchPosition {
  final GeodesicPoint position;
  final bool forced;

  const PunchPosition(this.position, this.forced);
}
