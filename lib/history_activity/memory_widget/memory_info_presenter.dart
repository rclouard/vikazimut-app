import 'package:disk_space_2/disk_space_2.dart';
import 'package:flutter/foundation.dart';
import 'package:vikazimut/database/database_gateway.dart';

class MemoryInfoPresenter extends ChangeNotifier {
  double _memoryUsed = 0;
  String _unitMemoryUsed = "megabyte";
  double _memoryFree = 0;
  String _unitMemoryFree = "megabyte";
  bool _disposed = false;

  MemoryInfoPresenter() {
    updateMemorySize();
  }

  double get memoryUsed => _memoryUsed;

  String get unitMemoryUsed => _unitMemoryUsed;

  double get memoryFree => _memoryFree;

  String get unitMemoryFree => _unitMemoryFree;

  bool get isAlive => !_disposed;

  @override
  void dispose() {
    _disposed = true;
    super.dispose();
  }

  Future<void> updateMemorySize() async {
    await Future.delayed(Duration(seconds: 2));
    final memoryUsedInMb = await ResultDatabaseGateway.getTotalSizeOfDatabaseInMb();
    final memoryUsedFormat = formatSize_(memoryUsedInMb);
    _memoryUsed = memoryUsedFormat[0];
    _unitMemoryUsed = memoryUsedFormat[1];
    final freeMemory = await DiskSpace.getFreeDiskSpace ?? 0;
    final memoryFreeFormat = formatSize_(freeMemory);
    _memoryFree = memoryFreeFormat[0];
    _unitMemoryFree = memoryFreeFormat[1];
    if (!_disposed) {
      notifyListeners();
    }
  }

  @visibleForTesting
  List formatSize_(double sizeInMb) {
    if (sizeInMb > 1024.0) {
      return [sizeInMb / 1024.0, "gigabyte"];
    } else {
      return [sizeInMb, "megabyte"];
    }
  }
}
