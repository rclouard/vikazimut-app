// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';

@immutable
class MemoryInfoWidget extends StatelessWidget {
  static const _VALUE_FONT_STYLE = TextStyle(color: Colors.white, fontSize: 14);
  static const _UNIT_FONT_STYLE = TextStyle(color: Colors.white, fontSize: 10);
  final double _memoryUsed;
  final String _unitMemoryUsed;
  final double _memoryFree;
  final String _unitMemoryFree;

  const MemoryInfoWidget(this._memoryUsed, this._unitMemoryUsed, this._memoryFree, this._unitMemoryFree);

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        style: _VALUE_FONT_STYLE,
        text: L10n.getString("memory_map_used", [_memoryUsed]),
        children: <InlineSpan>[
          TextSpan(
            text: ' ${L10n.getString(_unitMemoryUsed)}',
            style: _UNIT_FONT_STYLE,
          ),
          TextSpan(
            text: ' (${L10n.getString("memory_map_free", [_memoryFree])}',
            style: _VALUE_FONT_STYLE,
          ),
          TextSpan(
            text: ' ${L10n.getString(_unitMemoryFree)}',
            style: _UNIT_FONT_STYLE,
          ),
          const TextSpan(
            text: ")",
            style: _VALUE_FONT_STYLE,
          ),
        ],
      ),
      textAlign: TextAlign.center,
    );
  }
}
