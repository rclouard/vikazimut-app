// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/result_activity/sport_global_result_view.dart';
import 'package:vikazimut/result_activity/walk_global_result_view.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/time.dart';

@immutable
class HistoryItemWidget extends StatelessWidget {
  final CourseResultEntity item;

  const HistoryItemWidget({required this.item});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(4, 4, 4, 4),
      child: ElevatedButton(
        onPressed: () {
          if (item.mode.isWalkMode) {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => WalkGlobalResultView(resultId: item.id!)));
          } else {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SportGlobalResultView(resultId: item.id!)));
          }
        },
        style: ButtonStyle(
          elevation: WidgetStateProperty.all(5),
          shape: WidgetStateProperty.all<OutlinedBorder>(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          ),
        ),
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.zero,
          minVerticalPadding: 4,
          horizontalTitleGap: 0,
          leading: _buildIconFromCourseMode(item.mode, item.discipline),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.mapName,
                style: const TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: kOrangeColor),
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  const Icon(Icons.calendar_month_outlined, color: kGreenColor),
                  Text(
                    getLocalizedDate(item.baseTimeInMillisecond),
                    style: const TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  const SizedBox(width: 10),
                  const Icon(Icons.timer_outlined, color: kYellowColor),
                  Text(
                    formatTimeAsString(item.totalTimeInMillisecond),
                    style: const TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ],
              ),
            ],
          ),
          minLeadingWidth: 35,
          trailing: (item.isSent) ? const Icon(Icons.send_to_mobile_outlined) : null,
        ),
      ),
    );
  }

  Widget _buildIconFromCourseMode(int mode, int discipline) {
    if (mode.isWalkMode) {
      return const Icon(Icons.directions_walk);
    }
    Icon icon = Icon(Discipline.getIcon(Discipline.fromInt(discipline)));
    if (mode.isGeocachingMode) {
      return Stack(
        children: [
          const Positioned(
            top: -5,
            right: 0,
            child: Text(
              "*",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            ),
          ),
          icon,
        ],
      );
    }
    return icon;
  }
}
