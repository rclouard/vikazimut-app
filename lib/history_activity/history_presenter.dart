import 'package:flutter/foundation.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/database/punch_time_entity.dart';
import 'package:vikazimut/database/waypoint_entity.dart';

@immutable
class HistoryPresenter {
  static Future<List<CourseResultEntity>> getFinishedCoursesFromDatabase() async {
    List<CourseResultEntity> courses = await ResultDatabaseGateway.findAllFinishedCourseResults();
    courses.sort(sortByDecreasingDate_);
    return courses;
  }

  static void deleteCourseResult(CourseResultEntity courseResult) => ResultDatabaseGateway.deleteCourseResult(courseResult);

  static Future<void> saveCourseResult(CourseResultEntity courseResult) => ResultDatabaseGateway.insertCourseResult(courseResult);

  static Future<void> storeUnfinishedCourse(CourseResultEntity courseResultEntity) async {
    final waypointEntities = await ResultDatabaseGateway.findAllWaypoints();
    final punchTimeEntities = await ResultDatabaseGateway.findAllPunchTimes();
    courseResultEntity.totalTimeInMillisecond = WaypointEntity.getTotalTimeInMs(waypointEntities);
    courseResultEntity.gpsTrack = WaypointEntity.convertToString(waypointEntities);
    courseResultEntity.punchTimes = PunchTimeEntity.convertToString(punchTimeEntities, courseResultEntity.checkpointCount);
    courseResultEntity.finished = 1;
    await ResultDatabaseGateway.updateCourseResult(courseResultEntity);
    await ResultDatabaseGateway.deleteAllPunchTimes();
    await ResultDatabaseGateway.deleteAllWaypoints();
  }

  @visibleForTesting
  static int sortByDecreasingDate_(CourseResultEntity a, CourseResultEntity b) => b.baseTimeInMillisecond.compareTo(a.baseTimeInMillisecond);
}
