// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/history_activity/memory_widget/memory_info_presenter.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';

import 'history_item_widget.dart';
import 'history_presenter.dart';
import 'memory_widget/memory_info_widget.dart';

@immutable
class HistoryView extends StatefulWidget {
  @override
  HistoryViewState createState() => HistoryViewState();
}

class HistoryViewState extends State<HistoryView> {
  late final Future<List<CourseResultEntity>> _courseResults;
  late final MemoryInfoPresenter _memoryInfoPresenter ;

  @override
  void initState() {
    _courseResults = HistoryPresenter.getFinishedCoursesFromDatabase();
    _memoryInfoPresenter = MemoryInfoPresenter();
    super.initState();
  }

  @override
  void dispose() {
    if (_memoryInfoPresenter.isAlive) {
      _memoryInfoPresenter.dispose();
    }
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      child: Scaffold(
        appBar: AppBar(
          title: Text(L10n.getString("history_activity_title")),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(14.0),
            child: Column(
              children: [
                ChangeNotifierProvider(
                  create: (context) => _memoryInfoPresenter,
                  child: Consumer<MemoryInfoPresenter>(
                    builder: (BuildContext context, MemoryInfoPresenter presenter, Widget? child) {
                      return MemoryInfoWidget(
                        presenter.memoryUsed,
                        presenter.unitMemoryUsed,
                        presenter.memoryFree,
                        presenter.unitMemoryFree,
                      );
                    },
                  ),
                ),
                Text(
                  L10n.getString("history_help_message"),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).appBarTheme.titleTextStyle?.color,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
        ),
        body: FutureBuilder<List<CourseResultEntity>>(
          future: _courseResults,
          initialData: List.empty(),
          builder: (context, entity) {
            if (entity.hasData) {
              return ListView.builder(
                itemCount: entity.data!.length,
                itemBuilder: (context, index) {
                  CourseResultEntity item = entity.data![index];
                  return Dismissible(
                    key: ValueKey(item),
                    background: const _DeleteIcon(alignment: AlignmentDirectional.centerStart),
                    secondaryBackground: const _DeleteIcon(alignment: AlignmentDirectional.centerEnd),
                    child: HistoryItemWidget(item: item),
                    onDismissed: (_) => _removeItemFromList(context, index, entity.data!),
                  );
                },
              );
            } else if (entity.hasError) {
              return Center(
                child: Text("${entity.error}"),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

  void _removeItemFromList(BuildContext context, int index, List<CourseResultEntity> data) {
    final CourseResultEntity swipedCourseResult = data[index];
    setState(() => data.removeAt(index));
    HistoryPresenter.deleteCourseResult(swipedCourseResult);
    _memoryInfoPresenter.updateMemorySize();
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(L10n.getString("delete_history_item")),
        duration: const Duration(seconds: 5),
        action: SnackBarAction(
            label: L10n.getString("undo_label"),
            textColor: Colors.yellow,
            onPressed: () {
              HistoryPresenter.saveCourseResult(swipedCourseResult);
              setState(() => data.insert(index, swipedCourseResult));
              _memoryInfoPresenter.updateMemorySize();
            }),
      ),
    );
  }
}

@immutable
class _DeleteIcon extends StatelessWidget {
  final AlignmentDirectional alignment;

  const _DeleteIcon({required this.alignment});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kRedColor,
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      alignment: alignment,
      child: const Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }
}
