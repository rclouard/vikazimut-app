import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:intl/intl.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/main.dart';

@visibleForTesting
const List<String> SUPPORTED_LOCALES = ['en', 'fr', 'it', 'de', 'pt', 'cs', 'pl'];

List<Locale> buildListWithSupportedLocales() {
  return buildListWithSupportedLocales_(SUPPORTED_LOCALES);
}

@visibleForTesting
List<Locale> buildListWithSupportedLocales_(List<String> localesAsStrings) => localesAsStrings.map((item) => Locale(item)).toList();

@immutable
class L10n {
  static Locale _locale = const Locale('en');
  static late Map<dynamic, dynamic> _localizedValues;
  static NumberFormat _formatter1 = NumberFormat('#,##0.#', _locale.languageCode);
  static NumberFormat _formatter2 = NumberFormat('#,##0.##', _locale.languageCode);

  const L10n._();

  static Locale get locale => _locale;

  static L10n of(BuildContext context) {
    return Localizations.of<L10n>(context, L10n)!;
  }

  static Future<L10n> load(Locale locale) async {
    _locale = locale;
    L10n translations = const L10n._();
    String jsonContent = await rootBundle.loadString("l10n/${locale.languageCode}.json");
    _localizedValues = json.decode(jsonContent);
    _formatter1 = NumberFormat('#,##0.#', locale.toString());
    _formatter2 = NumberFormat('#,##0.##', locale.toString());
    return translations;
  }

  static get currentLanguage => _locale.languageCode;

  static String getString(String key, [var parameter]) {
    if (globalKeyContext.currentContext == null) {
      return key;
    }
    return L10n.of(globalKeyContext.currentContext!)._getString(key, parameter);
  }

  static String formatNumber(num number, int length) {
    if (length <= 1) {
      return _formatter1.format(number);
    } else {
      return _formatter2.format(number);
    }
  }

  String _getString(String key, [var parameter]) {
    if (_localizedValues[key] == null) {
      return '** $key not found';
    }
    if (parameter != null) {
      if (parameter is List) {
        return sprintf(_localizedValues[key], parameter);
      } else {
        return sprintf(_localizedValues[key], [parameter]);
      }
    } else {
      return _localizedValues[key];
    }
  }
}

@immutable
class TranslationsDelegate extends LocalizationsDelegate<L10n> {
  const TranslationsDelegate();

  @override
  bool isSupported(Locale locale) => SUPPORTED_LOCALES.contains(locale.languageCode);

  @override
  Future<L10n> load(Locale locale) => L10n.load(locale);

  @override
  bool shouldReload(TranslationsDelegate old) => false;
}
