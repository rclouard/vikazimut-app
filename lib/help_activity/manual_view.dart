// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';

import 'manual.dart';
import 'manual_presenter.dart';

class ManualView extends StatelessWidget {
  static const double _BODY_FONT_SIZE = 16;
  static const double _TITLE_FONT_SIZE = 18;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(L10n.getString("manual_title"))),
      body: FutureBuilder<Manual>(
        future: ManualPresenter.loadCGUArticles(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(10),
                color: Colors.white,
                child: Column(
                  children: [
                    _getImage(snapshot.data!.image),
                    _buildSections(context, snapshot.data!.sections),
                  ],
                ),
              ),
            );
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }

  Widget _buildSections(BuildContext context, List<Section> sections) {
    return Column(
      children: sections
          .map<Widget>(
            (section) => Container(
              margin: const EdgeInsets.symmetric(vertical: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    section.title,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: _TITLE_FONT_SIZE,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(5.0),
                        child: section.menuIcon != null ? _getImage(section.menuIcon!) : const SizedBox(width: 30),
                      ),
                      _buildActionContents(preamble: section.preamble, actions: section.actions),
                    ],
                  ),
                ],
              ),
            ),
          )
          .toList(),
    );
  }

  Widget _buildActionContents({
    required String? preamble,
    required List<String> actions,
  }) {
    List<Widget> widgets = [];
    if (preamble != null) {
      widgets.add(Text(
        preamble,
        style: const TextStyle(
          fontSize: _BODY_FONT_SIZE,
        ),
      ));
    }
    for (final paragraph in actions) {
      widgets.add(Text(
        paragraph,
        textAlign: TextAlign.left,
        softWrap: true,
        style: const TextStyle(
          fontSize: _BODY_FONT_SIZE,
        ),
      ));
    }
    return Expanded(
      // Workaround for Google Play Console warning: Semantic issue "Content labeling" (version 238)
      child: ExcludeSemantics(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: widgets,
        ),
      ),
    );
  }

  Image _getImage(String imageName) {
    return Image.asset('l10n/manual/$imageName', scale: 2.0);
  }
}
