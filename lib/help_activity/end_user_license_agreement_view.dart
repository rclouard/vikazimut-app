// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';

import 'end_user_license_agreement.dart';
import 'end_user_license_agreement_presenter.dart';

class EndUserLicenseAgreementView extends StatelessWidget {
  static const double _BODY_FONT_SIZE = 15;
  static const double _TITLE_FONT_SIZE = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.getString("end_user_license_agreement_title")),
      ),
      body: FutureBuilder<EndUserLicenseAgreement>(
        future: EndUserLicenseAgreementPresenter.loadCGUArticles(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Container(
                padding: const EdgeInsets.all(8),
                color: Colors.white,
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      margin: const EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        snapshot.data!.preamble,
                        style: const TextStyle(
                          fontSize: _BODY_FONT_SIZE,
                        ),
                      ),
                    ),
                    _buildArticles(snapshot.data!.articles),
                  ],
                ),
              ),
            );
          } else {
            return const SizedBox.shrink();
          }
        },
      ),
    );
  }

  Widget _buildArticles(List<Article> articles) {
    List<Widget> blocks = [];
    for (final article in articles) {
      var color = Color(EndUserLicenseAgreementPresenter.getColorFromHex(article.color));
      blocks.add(
        Container(
          decoration: BoxDecoration(
            color: color,
            borderRadius: const BorderRadius.all(
              Radius.circular(10.0),
            ),
          ),
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.all(10.0),
          width: double.infinity,
          alignment: Alignment.topRight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                article.title,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: _TITLE_FONT_SIZE,
                ),
              ),
              const SizedBox(height: 10),
              Column(
                children: article.paragraphs
                    .map(
                      (String paragraph) => Text(
                        paragraph,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: _BODY_FONT_SIZE,
                        ),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
        ),
      );
    }
    return Column(children: blocks);
  }
}
