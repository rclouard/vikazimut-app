import 'dart:async' show Future;
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;
import 'package:vikazimut/l10n.dart';

import 'end_user_license_agreement.dart';

class EndUserLicenseAgreementPresenter {
  static Future<EndUserLicenseAgreement> loadCGUArticles() async {
    String jsonContent = await rootBundle.loadString('l10n/general-conditions-of-use-${L10n.currentLanguage}.json');
    var parsedJson = jsonDecode(jsonContent);
    return EndUserLicenseAgreement.fromJson(parsedJson);
  }

  static int getColorFromHex(String hexColor) => int.parse("0xFF${hexColor.substring(1)}");
}
