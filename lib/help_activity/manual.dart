import 'package:flutter/foundation.dart';

@immutable
class Manual {
  final String image;
  final List<Section> sections;

  const Manual({required this.image, required this.sections});

  factory Manual.fromJson(Map<String, dynamic> parsedJson) {
    List<Section> articles = [];
    for (var article in parsedJson["sections"]) {
      articles.add(Section.fromJson(article));
    }
    return Manual(
      image: parsedJson["image"],
      sections: articles,
    );
  }
}

@immutable
class Section {
  final String? menuIcon;
  final String title;
  final String? preamble;
  final List<String> actions;

  const Section({
    required this.menuIcon,
    required this.title,
    this.preamble,
    required this.actions,
  });

  factory Section.fromJson(dynamic article) {
    List<String> actions = [];
    for (var paragraph in article["actions"]) {
      actions.add(paragraph);
    }
    return Section(
      menuIcon: article['menu_icon'],
      title: article['title'],
      preamble: article['preamble'],
      actions: actions,
    );
  }
}
