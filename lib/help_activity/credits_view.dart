// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';

class CreditsView extends StatelessWidget {
  static const List<String> _ENSICAEN_STUDENTS = [
    "Florentin BLANC (2016)",
    "Denis CHEN (2016)",
    "Tristan FAUQUETTE (2016)",
    "Annick VOLCY (2016)",
    "",
    "Vincent DUPLESSIS (2017)",
    "Benjamin HOUX (2017)",
    "Axel OLLIVIER (2017)",
    "Yann PELLEGRINI (2017)",
    "Elodie PROUX (2017)",
    "",
    "Samuel ANSEL (2018)",
    "Sylvain GAUVREAU (2018)",
    "Antonio MANCUSO (2018)",
    "Loïc PETIT (2018)",
    "Clément PODEVIN (2018)",
    "",
    "Aboubakrine NIANE (2019)",
    "Lauren SILVA ROLAN SAMPAIO (2019)",
    "Mathieu SERAPHIM (2019)",
    "",
    "Ilyass AZIRAR (2021)",
    "Amine KHALFAOUI (2021)",
    "Reda RGUIG (2021)",
    "Ilias LAHBABI (2021)",
    "",
    "Blaise HECQUET (2023)",
    "Moris Lorys KAMGANG (2023)",
    "Tom LE CAM (2023)",
    "Come LESCARMONTIER (2023)",
    "Robin SALMI (2023)",
  ];

  static const List<String> _UNICAEN_STUDENTS = [
    "Martin FÉAUX DE LA CROIX (2020)",
    "Antoine BOITEAU (2021)",
    "Suliac LAVENANT (2021)",
    "Clémentine LEROY (2021)",
    "Quentin LIREUX (2022)",
    "Quentin LEGOT (2022)",
    "Mohamed AKADDAR (2023)",
  ];
  static const List<String> _SUPERVISORS = [
    "Régis CLOUARD (ENSICAEN)",
    "Alain LEBRET (ENSICAEN)",
    "Eric PIGEON (UNICAEN & Vik'Azim)",
  ];
  static const List<String> _TRANSLATORS = [
    "Olivier PATTUS (Italiano)",
    "Koni EHRBAR, Eric HOYOIS (Deutsch)",
    "Margarida GONÇALVES NOVO (Português)",
    "Pavla CLAQUIN, Petra KAŇÁKOVÁ (Čeština)",
    "Oliwier KUSMIERCZYK (Polski)",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.getString("credits_title")),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(10),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 10),
              Text(
                L10n.getString("vikazimut_presentation_ensicaen"),
                textAlign: TextAlign.center,
                style: _textStyle,
              ),
              Image.asset(
                'assets/images/ensicaen_logo.webp',
                scale: 2,
              ),
              Text(
                "${L10n.getString("vikazimut_student")} ENSICAEN",
                style: _titleStyle,
              ),
              Text(
                formatNamesInColumn_(_ENSICAEN_STUDENTS),
                textAlign: TextAlign.center,
                style: _textStyle,
              ),
              const SizedBox(height: 5),
              Text(
                L10n.getString("vikazimut_presentation_unicaen"),
                textAlign: TextAlign.center,
                style: _textStyle,
              ),
              Image.asset(
                'assets/images/unicaen_logo.webp',
                scale: 2,
              ),
              const SizedBox(height: 5),
              Text(
                "${L10n.getString("vikazimut_student")} UNICAEN",
                style: _titleStyle,
              ),
              Text(
                formatNamesInColumn_(_UNICAEN_STUDENTS),
                textAlign: TextAlign.center,
                style: _textStyle,
              ),
              const SizedBox(height: 10),
              Text(
                L10n.getString("vikazimut_supervisors"),
                style: _titleStyle,
              ),
              Text(
                formatNamesInColumn_(_SUPERVISORS),
                textAlign: TextAlign.center,
                style: _textStyle,
              ),
              const SizedBox(height: 5),
              Text(
                L10n.getString("vikazimut_translator"),
                style: _titleStyle,
              ),
              Text(
                formatNamesInColumn_(_TRANSLATORS),
                textAlign: TextAlign.center,
                style: _textStyle,
              ),
              const SizedBox(height: 5),
              Text(
                L10n.getString("tester_team"),
                style: _titleStyle,
              ),
              Image.asset(
                'assets/images/testers.webp',
                fit: BoxFit.fitWidth,
              ),
            ],
          ),
        ),
      ),
    );
  }

  static const TextStyle _titleStyle = TextStyle(color: kOrangeColor, fontWeight: FontWeight.bold, fontSize: 20.0);

  static const TextStyle _textStyle = TextStyle(color: Colors.black, fontSize: 14.0);

  @visibleForTesting
  static String formatNamesInColumn_(List<String> names) => names.join("\n");
}
