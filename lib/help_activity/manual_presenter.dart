import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;
import 'package:vikazimut/l10n.dart';

import 'manual.dart';

class ManualPresenter {
  static Future<Manual> loadCGUArticles() async {
    String jsonContent = await rootBundle.loadString('l10n/manual/manual-${L10n.currentLanguage}.json');
    var parsedJson = jsonDecode(jsonContent);
    return Manual.fromJson(parsedJson);
  }
}
