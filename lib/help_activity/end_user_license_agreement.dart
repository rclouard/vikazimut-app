class EndUserLicenseAgreement {
  final String preamble;
  final List<Article> articles;

  const EndUserLicenseAgreement({required this.preamble, required this.articles});

  factory EndUserLicenseAgreement.fromJson(Map<String, dynamic> parsedJson) {
    List<Article> articles = [];
    for (var article in parsedJson["articles"]) {
      articles.add(Article.fromJson(article));
    }
    return EndUserLicenseAgreement(
      preamble: parsedJson["preamble"],
      articles: articles,
    );
  }
}

class Article {
  final String title;
  final String color;
  final List<String> paragraphs;

  const Article({required this.title, required this.color, required this.paragraphs});

  factory Article.fromJson(dynamic article) {
    List<String> paragraphs = [];
    for (var paragraph in article["body"]) {
      paragraphs.add(paragraph);
    }
    return Article(
      title: article["title"],
      color: article["color"],
      paragraphs: paragraphs,
    );
  }
}
