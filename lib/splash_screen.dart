// coverage:ignore-file
import 'package:flutter/material.dart';

@immutable
class SplashScreen extends StatefulWidget {
  final Widget home;

  SplashScreen({super.key, required this.home, VoidCallback? doInBackground}) {
    doInBackground?.call();
  }

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _animation;
  late final Tween<double> _tween;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => widget.home));
      }
    });
    _tween = Tween(begin: 0, end: 0.8);
    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastOutSlowIn,
    );
    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ScaleTransition(
        scale: _tween.animate(_animation),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset(
            'assets/images/splash.webp',
            isAntiAlias: true,
          ),
        ),
      ),
    );
  }
}
