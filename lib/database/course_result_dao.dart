import 'package:floor/floor.dart';

import 'course_result_entity.dart';
import 'database_singleton.dart';
import 'punch_time_entity.dart';
import 'waypoint_entity.dart';

@dao
abstract class CourseResultDao {
  @Insert(onConflict: OnConflictStrategy.replace)
  Future<int> insertCourseResult(CourseResultEntity courseResultEntity);

  @delete
  Future<void> deleteCourseResult(CourseResultEntity courseResultEntity);

  @update
  Future<void> updateCourseResult(CourseResultEntity courseResultEntity);

  @Query("UPDATE ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} SET sent = 1 WHERE id =:id")
  Future<void> updateSentFlagForCourseResult(int id);

  @Query("SELECT * FROM ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} WHERE id = :id")
  Future<CourseResultEntity?> findCourseResult(int id);

  @Query("SELECT * FROM ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} WHERE finished = 0")
  Future<List<CourseResultEntity>> findAllUnfinishedCourse();

  @Query("SELECT * FROM ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} WHERE finished = 1")
  Future<List<CourseResultEntity>> findAllFinishedCourseResults();

  @Query("SELECT * FROM ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} WHERE completed = 0 and mapName like :mapName and format = 1 and mode = 0")
  Future<List<CourseResultEntity>> findUncompletedFreeOrderSportCourse(String mapName);

  @Query("UPDATE ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} SET runCount = :runCount WHERE id =:id")
  Future<void> updateRunCount(int id, int runCount);

  @Query("UPDATE ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} SET validationMode = :validationMode WHERE id =:id")
  Future<void> updateValidationMode(int id, int validationMode);

  @insert
  Future<void> insertWaypoint(WaypointEntity waypointEntity);

  @Query('DELETE FROM ${DatabaseSingleton.WAYPOINT_TABLE_NAME}')
  Future<void> deleteAllWaypoints();

  @Query('DELETE FROM ${DatabaseSingleton.PUNCH_TIME_TABLE_NAME}')
  Future<void> deleteAllPunchTimes();

  @Query("SELECT * FROM ${DatabaseSingleton.WAYPOINT_TABLE_NAME}")
  Future<List<WaypointEntity>> findAllWaypoints();

  @Query("SELECT * FROM ${DatabaseSingleton.PUNCH_TIME_TABLE_NAME}")
  Future<List<PunchTimeEntity>> findAllPunchTimes();

  @insert
  Future<void> insertPunchTime(PunchTimeEntity punchTimeEntity);

  @Query("UPDATE ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} SET assistanceCount = :assistanceCount WHERE id = :id")
  Future<void> updateAssistanceCountForCourseResult(int id, int assistanceCount);

  @Query("UPDATE ${DatabaseSingleton.COURSE_RESULT_TABLE_NAME} SET quizTotalPoints = :points WHERE id = :id")
  Future<void> updateQuizPointsForCourseResult(int id, int points);

  @insert
  Future<void> insertAllWaypoints(List<WaypointEntity> waypointEntities);

  @insert
  Future<void> insertAllPunchTimes(List<PunchTimeEntity> punchTimeEntities);
}
