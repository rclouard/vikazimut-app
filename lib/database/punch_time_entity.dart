import 'package:floor/floor.dart';
import 'package:flutter/foundation.dart';
import 'package:vikazimut/route_data/punch_time.dart';

import 'database_singleton.dart';

@Entity(tableName: DatabaseSingleton.PUNCH_TIME_TABLE_NAME)
class PunchTimeEntity {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final int timestampInMillisecond;
  final int forced;
  final int checkpointId;

  PunchTimeEntity({
    this.id,
    required this.checkpointId,
    required this.timestampInMillisecond,
    required this.forced,
  });

  static String convertToString(List<PunchTimeEntity> punchTimeEntities, int checkpointCount) {
    final punchTimes = toPunchTimeList(punchTimeEntities, checkpointCount);
    return convertToString_(punchTimes);
  }

  @visibleForTesting
  static String convertToString_(List<PunchTime> punchTimes) {
    StringBuffer legTimes = StringBuffer("${punchTimes[0].timestampInMillisecond}");
    for (int i = 1; i < punchTimes.length; i++) {
      if (punchTimes[i].forced) {
        legTimes.write(";${punchTimes[i].timestampInMillisecond}:true");
      } else {
        legTimes.write(";${punchTimes[i].timestampInMillisecond}");
      }
    }
    for (int i = punchTimes.length; i < punchTimes.length; i++) {
      legTimes.write(";${PunchTime.UNPUNCHED}");
    }
    return legTimes.toString();
  }

  static List<PunchTimeEntity> convertFromString(String punchTimesString) {
    List<PunchTimeEntity> result = [];
    var punchTimeStrings = punchTimesString.split(";");
    var checkpointId = 0;
    for (final punchTimeString in punchTimeStrings) {
      var punchTimePart = punchTimeString.split(":");
      int timestampInMillisecond = int.parse(punchTimePart[0]);
      int forced;
      if (punchTimePart.length > 1) {
        forced = bool.parse(punchTimePart[1]) ? 1 : 0;
      } else {
        forced = 0;
      }
      result.add(PunchTimeEntity(checkpointId: checkpointId++, timestampInMillisecond: timestampInMillisecond, forced: forced));
    }
    return result;
  }

  static List<PunchTime> toPunchTimeList(List<PunchTimeEntity> punchTimeEntities, int checkpointCount) {
    List<PunchTime> punchTimes = List<PunchTime>.generate(checkpointCount, (index) => PunchTime(PunchTime.UNPUNCHED));
    for (int i = 0; i < punchTimeEntities.length; i++) {
      int index = punchTimeEntities[i].checkpointId;
      punchTimes[index] = PunchTime(punchTimeEntities[i].timestampInMillisecond, punchTimeEntities[i].forced > 0);
    }
    return punchTimes;
  }
}
