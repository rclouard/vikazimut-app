// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database_singleton.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

// ignore: avoid_classes_with_only_static_members
class $FloorDatabaseSingleton {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$DatabaseSingletonBuilder databaseBuilder(String name) =>
      _$DatabaseSingletonBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$DatabaseSingletonBuilder inMemoryDatabaseBuilder() =>
      _$DatabaseSingletonBuilder(null);
}

class _$DatabaseSingletonBuilder {
  _$DatabaseSingletonBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  /// Adds migrations to the builder.
  _$DatabaseSingletonBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$DatabaseSingletonBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<DatabaseSingleton> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$DatabaseSingleton();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$DatabaseSingleton extends DatabaseSingleton {
  _$DatabaseSingleton([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  CourseResultDao? _courseResultDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 5,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `course_result_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `mode` INTEGER NOT NULL, `format` INTEGER NOT NULL, `validationMode` INTEGER NOT NULL, `discipline` INTEGER NOT NULL, `mapName` TEXT NOT NULL, `courseId` INTEGER NOT NULL, `baseTimeInMillisecond` INTEGER NOT NULL, `checkpointCount` INTEGER NOT NULL, `assistanceCount` INTEGER NOT NULL, `quizTotalPoints` INTEGER NOT NULL, `totalTimeInMillisecond` INTEGER NOT NULL, `gpsTrack` TEXT, `punchTimes` TEXT, `finished` INTEGER NOT NULL, `completed` INTEGER NOT NULL, `runCount` INTEGER NOT NULL, `sent` INTEGER NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `waypoint_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `latitude` REAL NOT NULL, `longitude` REAL NOT NULL, `timestampInMillisecond` INTEGER NOT NULL, `altitude` REAL NOT NULL)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `punch_time_table` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `timestampInMillisecond` INTEGER NOT NULL, `forced` INTEGER NOT NULL, `checkpointId` INTEGER NOT NULL)');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  CourseResultDao get courseResultDao {
    return _courseResultDaoInstance ??=
        _$CourseResultDao(database, changeListener);
  }
}

class _$CourseResultDao extends CourseResultDao {
  _$CourseResultDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _courseResultEntityInsertionAdapter = InsertionAdapter(
            database,
            'course_result_table',
            (CourseResultEntity item) => <String, Object?>{
                  'id': item.id,
                  'mode': item.mode,
                  'format': item.format,
                  'validationMode': item.validationMode,
                  'discipline': item.discipline,
                  'mapName': item.mapName,
                  'courseId': item.courseId,
                  'baseTimeInMillisecond': item.baseTimeInMillisecond,
                  'checkpointCount': item.checkpointCount,
                  'assistanceCount': item.assistanceCount,
                  'quizTotalPoints': item.quizTotalPoints,
                  'totalTimeInMillisecond': item.totalTimeInMillisecond,
                  'gpsTrack': item.gpsTrack,
                  'punchTimes': item.punchTimes,
                  'finished': item.finished,
                  'completed': item.completed,
                  'runCount': item.runCount,
                  'sent': item.sent
                }),
        _waypointEntityInsertionAdapter = InsertionAdapter(
            database,
            'waypoint_table',
            (WaypointEntity item) => <String, Object?>{
                  'id': item.id,
                  'latitude': item.latitude,
                  'longitude': item.longitude,
                  'timestampInMillisecond': item.timestampInMillisecond,
                  'altitude': item.altitude
                }),
        _punchTimeEntityInsertionAdapter = InsertionAdapter(
            database,
            'punch_time_table',
            (PunchTimeEntity item) => <String, Object?>{
                  'id': item.id,
                  'timestampInMillisecond': item.timestampInMillisecond,
                  'forced': item.forced,
                  'checkpointId': item.checkpointId
                }),
        _courseResultEntityUpdateAdapter = UpdateAdapter(
            database,
            'course_result_table',
            ['id'],
            (CourseResultEntity item) => <String, Object?>{
                  'id': item.id,
                  'mode': item.mode,
                  'format': item.format,
                  'validationMode': item.validationMode,
                  'discipline': item.discipline,
                  'mapName': item.mapName,
                  'courseId': item.courseId,
                  'baseTimeInMillisecond': item.baseTimeInMillisecond,
                  'checkpointCount': item.checkpointCount,
                  'assistanceCount': item.assistanceCount,
                  'quizTotalPoints': item.quizTotalPoints,
                  'totalTimeInMillisecond': item.totalTimeInMillisecond,
                  'gpsTrack': item.gpsTrack,
                  'punchTimes': item.punchTimes,
                  'finished': item.finished,
                  'completed': item.completed,
                  'runCount': item.runCount,
                  'sent': item.sent
                }),
        _courseResultEntityDeletionAdapter = DeletionAdapter(
            database,
            'course_result_table',
            ['id'],
            (CourseResultEntity item) => <String, Object?>{
                  'id': item.id,
                  'mode': item.mode,
                  'format': item.format,
                  'validationMode': item.validationMode,
                  'discipline': item.discipline,
                  'mapName': item.mapName,
                  'courseId': item.courseId,
                  'baseTimeInMillisecond': item.baseTimeInMillisecond,
                  'checkpointCount': item.checkpointCount,
                  'assistanceCount': item.assistanceCount,
                  'quizTotalPoints': item.quizTotalPoints,
                  'totalTimeInMillisecond': item.totalTimeInMillisecond,
                  'gpsTrack': item.gpsTrack,
                  'punchTimes': item.punchTimes,
                  'finished': item.finished,
                  'completed': item.completed,
                  'runCount': item.runCount,
                  'sent': item.sent
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<CourseResultEntity>
      _courseResultEntityInsertionAdapter;

  final InsertionAdapter<WaypointEntity> _waypointEntityInsertionAdapter;

  final InsertionAdapter<PunchTimeEntity> _punchTimeEntityInsertionAdapter;

  final UpdateAdapter<CourseResultEntity> _courseResultEntityUpdateAdapter;

  final DeletionAdapter<CourseResultEntity> _courseResultEntityDeletionAdapter;

  @override
  Future<void> updateSentFlagForCourseResult(int id) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE course_result_table SET sent = 1 WHERE id =?1',
        arguments: [id]);
  }

  @override
  Future<CourseResultEntity?> findCourseResult(int id) async {
    return _queryAdapter.query(
        'SELECT * FROM course_result_table WHERE id = ?1',
        mapper: (Map<String, Object?> row) => CourseResultEntity(
            id: row['id'] as int?,
            mode: row['mode'] as int,
            format: row['format'] as int,
            validationMode: row['validationMode'] as int,
            discipline: row['discipline'] as int,
            mapName: row['mapName'] as String,
            courseId: row['courseId'] as int,
            baseTimeInMillisecond: row['baseTimeInMillisecond'] as int,
            checkpointCount: row['checkpointCount'] as int,
            assistanceCount: row['assistanceCount'] as int,
            quizTotalPoints: row['quizTotalPoints'] as int,
            totalTimeInMillisecond: row['totalTimeInMillisecond'] as int,
            gpsTrack: row['gpsTrack'] as String?,
            punchTimes: row['punchTimes'] as String?,
            sent: row['sent'] as int,
            runCount: row['runCount'] as int,
            finished: row['finished'] as int,
            completed: row['completed'] as int),
        arguments: [id]);
  }

  @override
  Future<List<CourseResultEntity>> findAllUnfinishedCourse() async {
    return _queryAdapter.queryList(
        'SELECT * FROM course_result_table WHERE finished = 0',
        mapper: (Map<String, Object?> row) => CourseResultEntity(
            id: row['id'] as int?,
            mode: row['mode'] as int,
            format: row['format'] as int,
            validationMode: row['validationMode'] as int,
            discipline: row['discipline'] as int,
            mapName: row['mapName'] as String,
            courseId: row['courseId'] as int,
            baseTimeInMillisecond: row['baseTimeInMillisecond'] as int,
            checkpointCount: row['checkpointCount'] as int,
            assistanceCount: row['assistanceCount'] as int,
            quizTotalPoints: row['quizTotalPoints'] as int,
            totalTimeInMillisecond: row['totalTimeInMillisecond'] as int,
            gpsTrack: row['gpsTrack'] as String?,
            punchTimes: row['punchTimes'] as String?,
            sent: row['sent'] as int,
            runCount: row['runCount'] as int,
            finished: row['finished'] as int,
            completed: row['completed'] as int));
  }

  @override
  Future<List<CourseResultEntity>> findAllFinishedCourseResults() async {
    return _queryAdapter.queryList(
        'SELECT * FROM course_result_table WHERE finished = 1',
        mapper: (Map<String, Object?> row) => CourseResultEntity(
            id: row['id'] as int?,
            mode: row['mode'] as int,
            format: row['format'] as int,
            validationMode: row['validationMode'] as int,
            discipline: row['discipline'] as int,
            mapName: row['mapName'] as String,
            courseId: row['courseId'] as int,
            baseTimeInMillisecond: row['baseTimeInMillisecond'] as int,
            checkpointCount: row['checkpointCount'] as int,
            assistanceCount: row['assistanceCount'] as int,
            quizTotalPoints: row['quizTotalPoints'] as int,
            totalTimeInMillisecond: row['totalTimeInMillisecond'] as int,
            gpsTrack: row['gpsTrack'] as String?,
            punchTimes: row['punchTimes'] as String?,
            sent: row['sent'] as int,
            runCount: row['runCount'] as int,
            finished: row['finished'] as int,
            completed: row['completed'] as int));
  }

  @override
  Future<List<CourseResultEntity>> findUncompletedFreeOrderSportCourse(
      String mapName) async {
    return _queryAdapter.queryList(
        'SELECT * FROM course_result_table WHERE completed = 0 and mapName like ?1 and format = 1 and mode = 0',
        mapper: (Map<String, Object?> row) => CourseResultEntity(id: row['id'] as int?, mode: row['mode'] as int, format: row['format'] as int, validationMode: row['validationMode'] as int, discipline: row['discipline'] as int, mapName: row['mapName'] as String, courseId: row['courseId'] as int, baseTimeInMillisecond: row['baseTimeInMillisecond'] as int, checkpointCount: row['checkpointCount'] as int, assistanceCount: row['assistanceCount'] as int, quizTotalPoints: row['quizTotalPoints'] as int, totalTimeInMillisecond: row['totalTimeInMillisecond'] as int, gpsTrack: row['gpsTrack'] as String?, punchTimes: row['punchTimes'] as String?, sent: row['sent'] as int, runCount: row['runCount'] as int, finished: row['finished'] as int, completed: row['completed'] as int),
        arguments: [mapName]);
  }

  @override
  Future<void> updateRunCount(
    int id,
    int runCount,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE course_result_table SET runCount = ?2 WHERE id =?1',
        arguments: [id, runCount]);
  }

  @override
  Future<void> updateValidationMode(
    int id,
    int validationMode,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE course_result_table SET validationMode = ?2 WHERE id =?1',
        arguments: [id, validationMode]);
  }

  @override
  Future<void> deleteAllWaypoints() async {
    await _queryAdapter.queryNoReturn('DELETE FROM waypoint_table');
  }

  @override
  Future<void> deleteAllPunchTimes() async {
    await _queryAdapter.queryNoReturn('DELETE FROM punch_time_table');
  }

  @override
  Future<List<WaypointEntity>> findAllWaypoints() async {
    return _queryAdapter.queryList('SELECT * FROM waypoint_table',
        mapper: (Map<String, Object?> row) => WaypointEntity(
            latitude: row['latitude'] as double,
            longitude: row['longitude'] as double,
            timestampInMillisecond: row['timestampInMillisecond'] as int,
            altitude: row['altitude'] as double,
            id: row['id'] as int?));
  }

  @override
  Future<List<PunchTimeEntity>> findAllPunchTimes() async {
    return _queryAdapter.queryList('SELECT * FROM punch_time_table',
        mapper: (Map<String, Object?> row) => PunchTimeEntity(
            id: row['id'] as int?,
            checkpointId: row['checkpointId'] as int,
            timestampInMillisecond: row['timestampInMillisecond'] as int,
            forced: row['forced'] as int));
  }

  @override
  Future<void> updateAssistanceCountForCourseResult(
    int id,
    int assistanceCount,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE course_result_table SET assistanceCount = ?2 WHERE id = ?1',
        arguments: [id, assistanceCount]);
  }

  @override
  Future<void> updateQuizPointsForCourseResult(
    int id,
    int points,
  ) async {
    await _queryAdapter.queryNoReturn(
        'UPDATE course_result_table SET quizTotalPoints = ?2 WHERE id = ?1',
        arguments: [id, points]);
  }

  @override
  Future<int> insertCourseResult(CourseResultEntity courseResultEntity) {
    return _courseResultEntityInsertionAdapter.insertAndReturnId(
        courseResultEntity, OnConflictStrategy.replace);
  }

  @override
  Future<void> insertWaypoint(WaypointEntity waypointEntity) async {
    await _waypointEntityInsertionAdapter.insert(
        waypointEntity, OnConflictStrategy.abort);
  }

  @override
  Future<void> insertPunchTime(PunchTimeEntity punchTimeEntity) async {
    await _punchTimeEntityInsertionAdapter.insert(
        punchTimeEntity, OnConflictStrategy.abort);
  }

  @override
  Future<void> insertAllWaypoints(List<WaypointEntity> waypointEntities) async {
    await _waypointEntityInsertionAdapter.insertList(
        waypointEntities, OnConflictStrategy.abort);
  }

  @override
  Future<void> insertAllPunchTimes(
      List<PunchTimeEntity> punchTimeEntities) async {
    await _punchTimeEntityInsertionAdapter.insertList(
        punchTimeEntities, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateCourseResult(CourseResultEntity courseResultEntity) async {
    await _courseResultEntityUpdateAdapter.update(
        courseResultEntity, OnConflictStrategy.abort);
  }

  @override
  Future<void> deleteCourseResult(CourseResultEntity courseResultEntity) async {
    await _courseResultEntityDeletionAdapter.delete(courseResultEntity);
  }
}
