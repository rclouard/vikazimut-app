import 'dart:async';

import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'course_result_dao.dart';
import 'course_result_entity.dart';
import 'punch_time_entity.dart';
import 'waypoint_entity.dart';

part 'database_singleton.g.dart';

@Database(version: 5, entities: [CourseResultEntity, WaypointEntity, PunchTimeEntity])
abstract class DatabaseSingleton extends FloorDatabase {
  static const String DATABASE_NAME = "vikazimut";
  static const String COURSE_RESULT_TABLE_NAME = "course_result_table";
  static const String WAYPOINT_TABLE_NAME = "waypoint_table";
  static const String PUNCH_TIME_TABLE_NAME = "punch_time_table";

  static final _migration4to5 = Migration(4, 5, (database) async {
    await database.execute("DELETE FROM $COURSE_RESULT_TABLE_NAME");
    await database.execute("ALTER TABLE $COURSE_RESULT_TABLE_NAME ADD COLUMN runCount INTEGER NOT NULL DEFAULT 1");
    await database.execute("ALTER TABLE $COURSE_RESULT_TABLE_NAME ADD COLUMN finished INTEGER NOT NULL DEFAULT 0");
  });

  static DatabaseSingleton? _instance;

  CourseResultDao get courseResultDao;

  Future<int> insertCourseResult(CourseResultEntity courseResultEntity) async => courseResultDao.insertCourseResult(courseResultEntity);

  Future<void> updateCourseResult(CourseResultEntity courseResultEntity) async => courseResultDao.updateCourseResult(courseResultEntity);

  Future<void> updateRunCount(int id, int runCount) async => courseResultDao.updateRunCount(id, runCount);

  Future<void> updateValidationMode(int id, int validationMode) async => courseResultDao.updateValidationMode(id, validationMode);

  Future<void> deleteCourseResult(CourseResultEntity courseResultEntity) async => courseResultDao.deleteCourseResult(courseResultEntity);

  Future<CourseResultEntity?> findCourseResult(int itemId) async => courseResultDao.findCourseResult(itemId);

  Future<List<CourseResultEntity>> findAllFinishedCourseResults() async => courseResultDao.findAllFinishedCourseResults();

  Future<List<CourseResultEntity>> findUnfinishedCourse() async => courseResultDao.findAllUnfinishedCourse();

  Future<List<CourseResultEntity>> findUncompletedFreeOrderSportCourse(String mapName) async => courseResultDao.findUncompletedFreeOrderSportCourse(mapName);

  static Future<DatabaseSingleton> getInstance() async => _instance ??= await _buildDatabase();

  static Future<DatabaseSingleton> _buildDatabase() async {
    return await $FloorDatabaseSingleton.databaseBuilder('$DATABASE_NAME.db').addMigrations([_migration4to5]).build();
  }

  Future<void> updateSentFlagInCourseResult(int courseResultEntityId) async => courseResultDao.updateSentFlagForCourseResult(courseResultEntityId);

  Future<void> updateAssistanceCountForCourseResult(int courseResultEntityId, int assistanceCount) async => courseResultDao.updateAssistanceCountForCourseResult(courseResultEntityId, assistanceCount);

  Future<void> addWaypoint(GeodesicPoint waypoint) async {
    courseResultDao.insertWaypoint(
      WaypointEntity(
        latitude: waypoint.latitude,
        longitude: waypoint.longitude,
        timestampInMillisecond: waypoint.timestampInMillisecond,
        altitude: waypoint.altitude,
      ),
    );
  }

  Future<void> updateQuizPointsForCourseResult(int courseResultEntityId, int points) async => courseResultDao.updateQuizPointsForCourseResult(courseResultEntityId, points);

  Future<void> addPunchTime(int checkpointId, int elapsedTime, bool forced) async {
    courseResultDao.insertPunchTime(
      PunchTimeEntity(
        checkpointId: checkpointId,
        timestampInMillisecond: elapsedTime,
        forced: forced ? 1 : 0,
      ),
    );
  }

  Future<void> deleteAllWaypoints() async => courseResultDao.deleteAllWaypoints();

  Future<void> deleteAllPunchTimes() async => courseResultDao.deleteAllPunchTimes();

  Future<List<WaypointEntity>> findAllWaypoints() async => courseResultDao.findAllWaypoints();

  Future<List<PunchTimeEntity>> findAllPunchTimes() async => courseResultDao.findAllPunchTimes();

  Future<void> addAllWaypoints(List<WaypointEntity> waypointEntities) async => courseResultDao.insertAllWaypoints(waypointEntities);

  Future<void> addAllPunchTimes(List<PunchTimeEntity> punchTimeEntities) async => courseResultDao.insertAllPunchTimes(punchTimeEntities);
}
