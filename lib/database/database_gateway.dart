import 'package:flutter/foundation.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'course_result_entity.dart';
import 'database_singleton.dart';
import 'punch_time_entity.dart';
import 'waypoint_entity.dart';

class ResultDatabaseGateway {
  const ResultDatabaseGateway._();

  static Future<int> insertCourseResult(CourseResultEntity courseResultEntity) async {
    final database = await DatabaseSingleton.getInstance();
    return database.insertCourseResult(courseResultEntity);
  }

  static Future<void> deleteCourseResult(CourseResultEntity courseResultEntity) async {
    final database = await DatabaseSingleton.getInstance();
    database.deleteCourseResult(courseResultEntity);
  }

  static Future<List<CourseResultEntity>> findAllFinishedCourseResults() async {
    final database = await DatabaseSingleton.getInstance();
    return database.findAllFinishedCourseResults();
  }

  static Future<CourseResultEntity?> findCourseResult(int itemId) async {
    final database = await DatabaseSingleton.getInstance();
    return database.findCourseResult(itemId);
  }

  static Future<int> getNumberOfCourseResults() async {
    List<CourseResultEntity> courseResults = await ResultDatabaseGateway.findAllFinishedCourseResults();
    return courseResults.length;
  }

  static Future<void> storeAssistanceCount(int courseResultEntityId, int assistanceCount) async {
    final database = await DatabaseSingleton.getInstance();
    database.updateAssistanceCountForCourseResult(courseResultEntityId, assistanceCount);
  }

  static Future<void> storeQuizPoints(int courseResultEntityId, int points) async {
    final database = await DatabaseSingleton.getInstance();
    database.updateQuizPointsForCourseResult(courseResultEntityId, points);
  }

  static Future<void> addPunchTime(int checkpointId, int elapsedTime, bool forced) async {
    final database = await DatabaseSingleton.getInstance();
    database.addPunchTime(checkpointId, elapsedTime, forced);
  }

  static Future<void> addWaypoint(GeodesicPoint waypoint) async {
    final database = await DatabaseSingleton.getInstance();
    database.addWaypoint(waypoint);
  }

  static Future<void> deleteAllWaypoints() async {
    final database = await DatabaseSingleton.getInstance();
    database.deleteAllWaypoints();
  }

  static Future<void> deleteAllPunchTimes() async {
    final database = await DatabaseSingleton.getInstance();
    database.deleteAllPunchTimes();
  }

  static Future<void> updateCourseResult(CourseResultEntity courseResultEntity) async {
    final database = await DatabaseSingleton.getInstance();
    database.updateCourseResult(courseResultEntity);
  }

  static Future<void> disableSentFlagForCourseResult(int itemId) async {
    final database = await DatabaseSingleton.getInstance();
    database.updateSentFlagInCourseResult(itemId);
  }

  static Future<CourseResultEntity?> findUncompletedFreeOrderSportCourse(String mapName) async {
    final database = await DatabaseSingleton.getInstance();
    var courseResultEntity = await database.findUncompletedFreeOrderSportCourse(mapName);
    if (courseResultEntity.isEmpty) {
      return null;
    }
    return courseResultEntity.last;
  }

  static Future<void> updateRunCount(int id, int runCount) async {
    final database = await DatabaseSingleton.getInstance();
    database.updateRunCount(id, runCount);
  }

  static Future<void> updateValidationMode(int id, int validationMode) async {
    final database = await DatabaseSingleton.getInstance();
    database.updateValidationMode(id, validationMode);
  }

  static Future<CourseResultEntity?> findUnfinishedCourse() async {
    final database = await DatabaseSingleton.getInstance();
    var courseResultEntity = await database.findUnfinishedCourse();
    if (courseResultEntity.isEmpty) {
      return null;
    }
    return courseResultEntity.last;
  }

  static Future<List<WaypointEntity>> findAllWaypoints() async {
    final database = await DatabaseSingleton.getInstance();
    return findAllWaypoints_(database);
  }

  static Future<List<PunchTimeEntity>> findAllPunchTimes() async {
    final database = await DatabaseSingleton.getInstance();
    return database.findAllPunchTimes();
  }

  @visibleForTesting
  static Future<List<WaypointEntity>> findAllWaypoints_(DatabaseSingleton database) async {
    final List<WaypointEntity> waypoints = await database.findAllWaypoints();
    waypoints.sort();
    return waypoints;
  }

  static Future<void> addAllWaypoints(List<WaypointEntity> waypointEntities) async {
    final database = await DatabaseSingleton.getInstance();
    await database.addAllWaypoints(waypointEntities);
  }

  static Future<void> addAllPunchTimes(List<PunchTimeEntity> punchTimesEntities) async {
    final database = await DatabaseSingleton.getInstance();
    database.addAllPunchTimes(punchTimesEntities);
  }

  static Future<double> getTotalSizeOfDatabaseInMb() async {
    final database = await DatabaseSingleton.getInstance();
    List<Map<String, Object?>> list = await database.database.rawQuery("SELECT s.*, c.* FROM pragma_page_size as s JOIN pragma_page_count as c");
    if (list.isNotEmpty) {
      int pageSizeInBytes = list[0]["page_size"] as int? ?? 0;
      int pageCount = list[0]["page_count"] as int? ?? 0;
      return (pageCount * pageSizeInBytes / (1024 * 1024)).toDouble();
    } else {
      return 0.0;
    }
  }
}
