import 'package:floor/floor.dart';
import 'package:sprintf/sprintf.dart';
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/route_data/geodesic_point.dart';

import 'database_singleton.dart';

@Entity(tableName: DatabaseSingleton.WAYPOINT_TABLE_NAME)
class WaypointEntity implements Comparable<WaypointEntity> {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final double latitude;
  final double longitude;
  final int timestampInMillisecond;
  final double altitude;

  WaypointEntity({
    required this.latitude,
    required this.longitude,
    required this.timestampInMillisecond,
    required this.altitude,
    this.id,
  });

  static String convertToString(List<WaypointEntity> waypoints) {
    StringBuffer gpxString = StringBuffer("");
    for (final waypoint in waypoints) {
      double latitude = waypoint.latitude;
      double longitude = waypoint.longitude;
      int relativePunchTime = waypoint.timestampInMillisecond;
      double altitude = waypoint.altitude;
      gpxString.write(sprintf(ServerConstants.gpsCoordinateTimedFormat, [latitude, longitude, relativePunchTime, altitude]));
    }
    return gpxString.toString();
  }

  static List<WaypointEntity> convertFromString(String? waypointsString) {
    if (waypointsString == null) {
      return [];
    }
    var waypointSubString = waypointsString.split(";");
    List<WaypointEntity> result = [];
    for (var waypointString in waypointSubString) {
      if (waypointString.isEmpty) break;
      var waypointPart = waypointString.split(",");
      if (waypointPart.length != 4) break;
      var latitude = waypointPart[0];
      var longitude = waypointPart[1];
      var timestampInMillisecond = waypointPart[2];
      var altitude = waypointPart[3];

      result.add(WaypointEntity(
        latitude: double.parse(latitude),
        longitude: double.parse(longitude),
        timestampInMillisecond: int.parse(timestampInMillisecond),
        altitude: double.parse(altitude),
      ));
    }
    return result;
  }

  static int getTotalTimeInMs(List<WaypointEntity> waypoints) {
    if (waypoints.isEmpty) {
      return 0;
    } else {
      return waypoints.last.timestampInMillisecond;
    }
  }

  @override
  int compareTo(WaypointEntity other) => timestampInMillisecond.compareTo(other.timestampInMillisecond);

  static List<GeodesicPoint> toGeodesicPoints(List<WaypointEntity> waypoints) {
    return waypoints.map((waypoint) {
      return GeodesicPoint(
        waypoint.latitude,
        waypoint.longitude,
        waypoint.timestampInMillisecond,
        waypoint.altitude,
      );
    }).toList();
  }
}
