import 'package:floor/floor.dart';

import 'database_singleton.dart';

@Entity(tableName: DatabaseSingleton.COURSE_RESULT_TABLE_NAME)
class CourseResultEntity {
  @PrimaryKey(autoGenerate: true)
  int? id;
  final int mode;
  final int format;
  final int validationMode;
  final int discipline;
  final String mapName;
  final int courseId;
  final int baseTimeInMillisecond;
  final int checkpointCount;

  int assistanceCount;
  int quizTotalPoints;
  int totalTimeInMillisecond;

  String? gpsTrack;
  String? punchTimes;
  int finished;
  int completed;
  int runCount;
  int sent;

  CourseResultEntity({
    this.id,
    required this.mode,
    required this.format,
    required this.validationMode,
    required this.discipline,
    required this.mapName,
    required this.courseId,
    required this.baseTimeInMillisecond,
    required this.checkpointCount,
    this.assistanceCount = 0,
    this.quizTotalPoints = -1,
    this.totalTimeInMillisecond = 0,
    this.gpsTrack,
    this.punchTimes,
    this.sent = 0,
    this.runCount = 1,
    this.finished = 0,
    this.completed = 0,
  });

  bool get isSent => sent > 0;
  bool get isFinished => finished > 0;
}
