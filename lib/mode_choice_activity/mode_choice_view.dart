// coverage:ignore-file
import 'dart:async';

import 'package:app_links/app_links.dart';
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/constants/server_constants.dart';
import 'package:vikazimut/history_activity/history_view.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/map_choice_activity/map_choice_view.dart';
import 'package:vikazimut/theme.dart';
import 'package:vikazimut/utils/alert_dialog.dart';
import 'package:vikazimut/utils/bottom_app_bar_item.dart';
import 'package:vikazimut/utils/web_navigator.dart';

import 'bottom_menu_bar.dart';
import 'course_mode_button_widget.dart';
import 'geolocation_permission_helper.dart';
import 'header_image_widget.dart';
import 'mode_choice_presenter.dart';

@immutable
class ModeChoiceView extends StatefulWidget {
  @override
  ModeChoiceViewState createState() => ModeChoiceViewState();
}

class ModeChoiceViewState extends State<ModeChoiceView> with RouteAware {
  late final ValueNotifier<int> _numberOfItemsInHistory;
  final AppLinks _appLinks = AppLinks();
  late final StreamSubscription _deepLinkSubscription;
  bool _hasFocus = false;

  @override
  void initState() {
    _numberOfItemsInHistory = ValueNotifier<int>(0);
    ModeChoicePresenter.updateNumberOfCoursesInHistory(_numberOfItemsInHistory);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await ModeChoicePresenter.checkDevicesSupport();
      await ModeChoicePresenter.restoreUnfinishedCourseIfAny(this, _numberOfItemsInHistory);
      await _initDeepLinking();
    });
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _subscribeInOrderToGetTheFocusStatus();
    super.didChangeDependencies();
  }

  @override
  void didPush() {
    _hasFocus = false;
  }

  @override
  void didPopNext() {
    _hasFocus = true;
    ModeChoicePresenter.updateNumberOfCoursesInHistory(_numberOfItemsInHistory);
  }

  @override
  void dispose() {
    _numberOfItemsInHistory.dispose();
    _deepLinkSubscription.cancel();
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leadingWidth: 180,
        leading: Padding(
          padding: const EdgeInsets.fromLTRB(8, 4, 40, 0),
          child: BottomAppBarItem(
            icon: const ImageIcon(
              AssetImage('assets/icons/icon_menu_web.png'),
              size: 31,
            ),
            text: L10n.getString("mode_choice_menu_course_map"),
            onPressed: () => openWebBrowserWithConfirmation(context, '${ServerConstants.serverBaseUrl}/worldmap'),
          ),
        ),
        actions: <Widget>[
          ValueListenableBuilder<int>(
            valueListenable: _numberOfItemsInHistory,
            builder: (BuildContext context, int value, child) => Visibility(
              visible: value > 0,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 0, 40, 0),
                child: BottomAppBarItem(
                  text: L10n.getString("mode_choice_menu_history"),
                  icon: Badge(
                    backgroundColor: kGreenColor,
                    alignment: Alignment.topRight,
                    offset: const Offset(15, -3),
                    label: Text(
                      "$value",
                      style: const TextStyle(color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold),
                    ),
                    child: const ImageIcon(
                      AssetImage('assets/icons/icon_menu_history.png'),
                      size: 32,
                    ),
                  ),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HistoryView()),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      body: ListView(children: [
        HeaderImageWidget(),
        Padding(
          padding: const EdgeInsets.fromLTRB(30, 20, 30, 15),
          child: ElevatedButton.icon(
            label: Text(
              L10n.getString("map_choice_qrcode"),
            ),
            icon: const Icon(Icons.qr_code_2_sharp, size: 32, color: Colors.white,),
            style: ButtonStyle(
              backgroundColor: WidgetStateProperty.all<Color>(kGreenColor),
              foregroundColor: WidgetStateProperty.all<Color>(Colors.white),
              elevation: WidgetStateProperty.all(5),
              padding: WidgetStateProperty.all<EdgeInsets>(const EdgeInsets.fromLTRB(0, 8, 0, 8)),
              shape: WidgetStateProperty.all<OutlinedBorder>(
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(35.0)),
              ),
              overlayColor: WidgetStateProperty.all(kOrangeColorLight),
            ),
            onPressed: () {
              GeolocationPermissionHelper.checkGeolocationActivation().then((bool value) {
                if (value) {
                  ModeChoicePresenter.loadAndOpenCourseFromQrCode();
                }
              });
            },
          ),
        ),
        CourseModeButtonWidget(
          title: L10n.getString("sport_mode_uppercase"),
          firstText: L10n.getString("without_gps"),
          firstIcon: Icons.location_off,
          secondText: L10n.getString("with_chronometer"),
          secondIcon: Icons.timer,
          onPressed: () {
            GeolocationPermissionHelper.checkGeolocationActivation().then((value) {
              if (value && context.mounted) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MapChoiceView(CourseMode.SPORT)),
                );
              }
            });
          },
        ),
        CourseModeButtonWidget(
          title: L10n.getString("playful_mode_uppercase"),
          firstText: L10n.getString("without_gps"),
          firstIcon: Icons.location_off,
          secondText: L10n.getString("mode_with_multimedia_contents"),
          secondIcon: Icons.quiz,
          onPressed: () {
            GeolocationPermissionHelper.checkGeolocationActivation().then((value) {
              if (value && context.mounted) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MapChoiceView(CourseMode.PLAYFUL)),
                );
              }
            });
          },
        ),
        CourseModeButtonWidget(
          title: L10n.getString("walk_mode_uppercase"),
          firstText: L10n.getString("with_gps"),
          firstIcon: Icons.location_on,
          secondText: L10n.getString("mode_with_multimedia_contents"),
          secondIcon: Icons.quiz,
          onLongPressed: () {
            GeolocationPermissionHelper.checkGeolocationActivation().then((value) {
              if (value && context.mounted) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MapChoiceView(CourseMode.WALK, isPlannerMode: true)),
                );
              }
            });
          },
          onPressed: () {
            GeolocationPermissionHelper.checkGeolocationActivation().then((value) {
              if (value && context.mounted) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MapChoiceView(CourseMode.WALK, isPlannerMode: false)),
                );
              }
            });
          },
        ),
      ]),
      bottomNavigationBar: const BottomMenuBar(),
    );
  }

  Future<bool> showConfirmationRestoreCourse() async {
    bool response = false;
    await showConfirmationDialog(
      context,
      title: L10n.getString("mode_choice_resume_course_title"),
      message: L10n.getString("mode_choice_resume_course_message"),
      yesButtonText: L10n.getString("mode_choice_resume_course_ok_button"),
      noButtonText: L10n.getString("mode_choice_resume_course_cancel_button"),
      yesButtonAction: () => response = true,
      noButtonAction: () => response = false,
    );
    return response;
  }

  void displayErrorDialog({required String title, required String message}) {
    showErrorDialog(context, title: title, message: message);
  }

  void _subscribeInOrderToGetTheFocusStatus() => routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);

  Future<void> _initDeepLinking() async {
    _deepLinkSubscription = _appLinks.uriLinkStream.listen((Uri uri) {
      if (_hasFocus) {
        GeolocationPermissionHelper.checkGeolocationActivation().then((bool value) {
          if (value) {
            ModeChoicePresenter.loadAndOpenCourseFromDeepLink(uri.toString());
          }
        });
      }
    }, onError: (_) {
      // Nothing to do, too bad!
    });
    Uri? initialDeepLink = await _appLinks.getInitialLink();
    if (initialDeepLink != null) {
      GeolocationPermissionHelper.checkGeolocationActivation().then((bool value) async {
        if (value) {
          await ModeChoicePresenter.loadAndOpenCourseFromDeepLink(initialDeepLink.toString());
        }
      });
    }
  }
}
