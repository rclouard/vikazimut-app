// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/theme.dart';

import '../course_mode_button_widget.dart';

@immutable
class CourseModeDialog {
  static Future<CourseMode?> display(BuildContext context, String routeName) {
    var alert = AlertDialog(
      title: Text(
        routeName,
        style: const TextStyle(color: kGreenColor, fontSize: 20),
        textAlign: TextAlign.center,
      ),
      contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 20),
      insetPadding: EdgeInsets.zero,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      content: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CourseModeButtonWidget(
              title: L10n.getString("sport_mode_uppercase"),
              firstText: L10n.getString("without_gps"),
              firstIcon: Icons.location_off,
              secondText: L10n.getString("with_chronometer"),
              secondIcon: Icons.timer,
              onPressed: () {
                Navigator.of(context).pop(CourseMode.SPORT);
              },
            ),
            CourseModeButtonWidget(
              title: L10n.getString("playful_mode_uppercase"),
              firstText: L10n.getString("without_gps"),
              firstIcon: Icons.location_off,
              secondText: L10n.getString("mode_with_multimedia_contents"),
              secondIcon: Icons.quiz,
              onPressed: () {
                Navigator.of(context).pop(CourseMode.PLAYFUL);
              },
            ),
            CourseModeButtonWidget(
              title: L10n.getString("walk_mode_uppercase"),
              firstText: L10n.getString("with_gps"),
              firstIcon: Icons.location_on,
              secondText: L10n.getString("mode_with_multimedia_contents"),
              secondIcon: Icons.quiz,
              onPressed: () {
                Navigator.of(context).pop(CourseMode.WALK);
              },
            ),
          ],
        ),
      ),
    );
    return showDialog<CourseMode>(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
