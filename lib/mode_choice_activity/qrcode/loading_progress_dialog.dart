// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_choice_activity/course_map_widget.dart';
import 'package:vikazimut/theme.dart';

@immutable
class LoadingProgressDialog extends IProgressBarWidget {
  final BuildContext _context;
  final String _courseName;

  const LoadingProgressDialog(this._context, this._courseName);

  void showLoaderDialog() {
    var courseNameText = (_courseName.isEmpty) ? "" : _courseName;
    var alert = AlertDialog(
      contentPadding: const EdgeInsets.all(0),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            height: 80,
            decoration: const BoxDecoration(
              color: kGreenColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
            ),
            child: const Center(
              child: CircularProgressIndicator(color: Colors.white),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(10),
            child: Text.rich(
              textAlign: TextAlign.center,
              TextSpan(
                text: L10n.getString("loading_course_message"),
                children: <InlineSpan>[
                  TextSpan(
                    text: '\n$courseNameText',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: _context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void hide() {
    Navigator.of(_context).pop();
  }

  @override
  void updateProgress(double value) {}
}
