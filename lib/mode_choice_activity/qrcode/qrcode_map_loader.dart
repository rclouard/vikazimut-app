import 'package:flutter/material.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/device/qrcode/qrcode_scanner.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/map_choice_activity/course_map.dart';
import 'package:vikazimut/map_choice_activity/course_map_list.dart';
import 'package:vikazimut/map_choice_activity/map_choice_presenter.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

import 'course_mode_dialog.dart';
import 'loading_progress_dialog.dart';

class QrCodeMapLoader {
  static const String qrCodeCourseIdLeadingText = "vikazimut-";

  static void loadAndOpenCourseFromQrCodeReader() async {
    final String? qrCodeText = await Navigator.of(globalKeyContext.currentContext!).push<String>(
      MaterialPageRoute(builder: (context) => QrCodeScanner()),
    );
    if (qrCodeText == null) {
      return;
    }
    await loadAndOpenCourseFromUri(qrCodeText);
  }

  static Future<void> loadAndOpenCourseFromUri(String uri) async {
    String? deepLink = extractQrCodeTextFromInitialUri_(uri);
    if (deepLink == null) {
      await _displayErrorMessage(L10n.getString("error_course_qrcode"));
      return;
    }
    if (!deepLink.startsWith(qrCodeCourseIdLeadingText)) {
      await _displayErrorMessage(L10n.getString("error_course_qrcode"));
      return;
    }
    MapId? mapId = extractMapIdFromQRCodeContent_(deepLink);
    if (mapId == null) {
      await _displayErrorMessage(L10n.getString("error_course_qrcode"));
      return;
    }
    final progressDialog = LoadingProgressDialog(globalKeyContext.currentContext!, mapId.name);
    progressDialog.showLoaderDialog();
    CourseMap? courseMap = CourseMap.fromMapName(id: mapId.id, name: mapId.name);
    MapChoicePresenter presenter = MapChoicePresenter(CourseMode.SPORT);
    if (!CourseMapList.isAlreadyDownloaded(mapId.id)) {
      presenter.startDownloading(
        globalKeyContext.currentContext!,
        courseMap,
        progressDialog,
        onSuccess: () async => await _launchCourse(presenter, courseMap, progressDialog),
        onFailure: ({String? key}) => progressDialog.hide(),
      );
    } else {
      await _launchCourse(presenter, courseMap, progressDialog);
    }
  }

  static Future<void> _displayErrorMessage(String message) async {
    await showErrorDialog(
      globalKeyContext.currentContext!,
      title: L10n.getString("error_title"),
      message: message,
    );
  }

  static Future<void> _launchCourse(
    MapChoicePresenter presenter,
    CourseMap courseMap,
    LoadingProgressDialog progressDialog,
  ) async {
    final map = presenter.loadCourse(courseMap.id, courseMap.name);
    await Future.delayed(const Duration(milliseconds: 500));
    progressDialog.hide();

    if (map.courseMode == null) {
      CourseMode? courseMode = await CourseModeDialog.display(globalKeyContext.currentContext!, courseMap.name);
      map.courseMode = courseMode;
    }
    if (map.courseMode != null) {
      presenter.openCourseViewWithoutOpeningSettingDialog(presenter.createCourseFromQrCode(map));
    }
  }

  @visibleForTesting
  static MapId? extractMapIdFromQRCodeContent_(String qrCodeText) {
    if (!qrCodeText.startsWith(qrCodeCourseIdLeadingText)) {
      return null;
    }
    String message = qrCodeText.substring(qrCodeCourseIdLeadingText.length);
    var index = message.indexOf("-");
    final String idText = (index < 0) ? message : message.substring(0, index);
    final String nameText = (index < 0) ? "" : message.substring(index + 1);
    int? mapId = int.tryParse(idText);
    if (mapId == null) {
      return null;
    } else {
      return MapId(mapId, nameText);
    }
  }

  @visibleForTesting
  static String? extractQrCodeTextFromInitialUri_(String deepLink) {
    if (deepLink.startsWith(qrCodeCourseIdLeadingText)) {
      return deepLink;
    }
    Uri? initialDeepUri = Uri.tryParse(deepLink);
    if (initialDeepUri == null || initialDeepUri.scheme != "https" || initialDeepUri.host != "vikazimut.vikazim.fr" || initialDeepUri.path != "/course/" || initialDeepUri.queryParameters.isEmpty) {
      return null;
    }
    String? id = initialDeepUri.queryParameters["id"];
    String? name = initialDeepUri.queryParameters["name"];
    if (id == null || name == null) {
      return null;
    }
    return "vikazimut-$id-$name";
  }
}

@immutable
class MapId {
  final int id;
  final String name;

  const MapId(this.id, this.name);
}
