// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

class VersionNumberWidget extends StatelessWidget {
  final String versionNumber;

  const VersionNumberWidget({required this.versionNumber});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 5.0),
      child: Text(
        filterVersionNumber_(versionNumber),
        style: const TextStyle(
          fontSize: 12,
          color: Colors.white,
          shadows: [
            Shadow(
              offset: Offset(-0.7, 0.7),
              color: Colors.black,
              blurRadius: 5,
            ),
          ],
        ),
      ),
    );
  }

  @visibleForTesting
  static String filterVersionNumber_(final String versionNumber) => "v$versionNumber";
}

class AppVersionNumber {
  static String _applicationVersionNumber = "";

  static String get applicationVersionNumber => _applicationVersionNumber;

  static Future<void> build() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    _applicationVersionNumber = packageInfo.version;
  }
}
