import 'package:flutter/foundation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/constants/global_constants.dart';
import 'package:vikazimut/database/course_result_entity.dart';
import 'package:vikazimut/database/database_gateway.dart';
import 'package:vikazimut/device/nfc/nfc_helper.dart';
import 'package:vikazimut/history_activity/history_presenter.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/map_choice_activity/map_choice_presenter.dart';

import 'geolocation_permission_helper.dart';
import 'mode_choice_view.dart';
import 'qrcode/qrcode_map_loader.dart';

class ModeChoicePresenter {
  static void resetParameters() {
    NfcHelper.resetPreferences();
    Geolocator.openAppSettings();
  }

  static Future<void> checkDevicesSupport() async {
    await GeolocationPermissionHelper.checkGeolocationPermission();
    await NfcHelper.checkNfcSupport();
  }

  static void loadAndOpenCourseFromQrCode() => QrCodeMapLoader.loadAndOpenCourseFromQrCodeReader();

  static Future<void> loadAndOpenCourseFromDeepLink(String uri) => QrCodeMapLoader.loadAndOpenCourseFromUri(uri);

  static Future<void> updateNumberOfCoursesInHistory(ValueNotifier<int> numberOfItemsInHistory) async {
    numberOfItemsInHistory.value = await ResultDatabaseGateway.getNumberOfCourseResults();
  }

  static Future<void> restoreUnfinishedCourseIfAny(ModeChoiceViewState view, ValueNotifier<int> numberOfItemsInHistory) async {
    CourseResultEntity? courseResultEntity = await ResultDatabaseGateway.findUnfinishedCourse();
    if (courseResultEntity != null) {
      bool confirmation = await view.showConfirmationRestoreCourse();
      if (confirmation) {
        final CourseMode courseMode = CourseMode.fromInt(courseResultEntity.mode);
        MapChoicePresenter presenter = MapChoicePresenter(courseMode);
        if (!presenter.loadCourseFromDatabase(courseResultEntity)) {
          view.displayErrorDialog(
            title: L10n.getString("mode_choice_resume_course_error_title"),
            message: L10n.getString("mode_choice_resume_course_error_message"),
          );
        }
      } else {
        await HistoryPresenter.storeUnfinishedCourse(courseResultEntity);
        updateNumberOfCoursesInHistory(numberOfItemsInHistory);
      }
    }
  }
}
