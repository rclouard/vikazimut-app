// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:vikazimut/help_activity/credits_view.dart';
import 'package:vikazimut/help_activity/end_user_license_agreement_view.dart';
import 'package:vikazimut/help_activity/manual_view.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/utils/bottom_app_bar_item.dart';

import 'mode_choice_presenter.dart';

@immutable
class BottomMenuBar extends StatelessWidget {
  const BottomMenuBar();

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: Theme(
        data: Theme.of(context).copyWith(iconTheme: const IconThemeData(size: 31, color: Colors.white)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            BottomAppBarItem(
              icon: const Icon(
                Icons.settings,
                size: 34,
              ),
              text: L10n.getString("settings_menu_item"),
              onPressed: () => ModeChoicePresenter.resetParameters(),
            ),
            BottomAppBarItem(
              icon: const Icon(
                Icons.help,
                size: 34,
              ),
              text: L10n.getString("display_help_menu_item"),
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ManualView())),
            ),
            BottomAppBarItem(
              icon: const Icon(
                Icons.policy,
                size: 34,
              ),
              text: L10n.getString("end_user_license_agreement_item"),
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => EndUserLicenseAgreementView())),
            ),
            BottomAppBarItem(
              icon: const Icon(
                Icons.copyright,
                size: 34,
              ),
              text: L10n.getString("credits_menu_item"),
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CreditsView())),
            ),
          ],
        ),
      ),
    );
  }
}
