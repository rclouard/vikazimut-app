// coverage:ignore-file
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vikazimut/theme.dart';

@immutable
class CourseModeButtonWidget extends StatelessWidget {
  final String title;
  final String firstText;
  final IconData firstIcon;
  final String secondText;
  final IconData secondIcon;
  final VoidCallback onPressed;
  final VoidCallback? onLongPressed;

  const CourseModeButtonWidget({
    required this.title,
    required this.firstText,
    required this.firstIcon,
    required this.secondText,
    required this.secondIcon,
    required this.onPressed,
    this.onLongPressed,
  });

  @override
  Widget build(BuildContext context) {
    Timer? timer;
    bool isLongPressed = false;
    return Container(
      margin: const EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 5.0),
      child: Material(
        color: Colors.white,
        elevation: 5,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: InkWell(
            onTapCancel: () => timer?.cancel(),
            onTapDown: (_) {
              timer = Timer(const Duration(seconds: 3), () {
                onLongPressed?.call();
                isLongPressed = true;
              });
            },
            onTapUp: (_) {
              timer?.cancel();
              if (!isLongPressed) {
                onPressed.call();
              } else {
                isLongPressed = false;
              }
            },
            child: Column(
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    color: kOrangeColor,
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Icon(
                        firstIcon,
                        color: kOrangeColor,
                      ),
                    ),
                    Text(firstText),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: Icon(
                        secondIcon,
                        color: kOrangeColor,
                      ),
                    ),
                    Text(secondText),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
