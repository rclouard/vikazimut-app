// coverage:ignore-file
import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'version_number_widget.dart';

class HeaderImageWidget extends StatelessWidget {
  static const List<String> _IMAGES = [
    "image_header1.webp",
    "image_header2.webp",
    "image_header3.webp",
    "image_header4.webp",
    "image_header5.webp",
    "image_header6.webp",
  ];
  static int? _randomNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.42),
            child: _getRandomImage(),
          ),
          VersionNumberWidget(versionNumber: AppVersionNumber.applicationVersionNumber),
        ],
      ),
    );
  }

  Image _getRandomImage() {
    String imageName = _IMAGES[getRandomNumber_(_IMAGES.length)];
    return Image.asset(
      'assets/images/$imageName',
      fit: BoxFit.cover,
    );
  }

  @visibleForTesting
  static int getRandomNumber_(int maxValue) {
    _randomNumber ??= math.Random().nextInt(maxValue);
    return _randomNumber!;
  }
}
