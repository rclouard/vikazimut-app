// coverage:ignore-file

import 'package:geolocator/geolocator.dart';
import 'package:vikazimut/l10n.dart';
import 'package:vikazimut/main.dart';
import 'package:vikazimut/utils/alert_dialog.dart';

class GeolocationPermissionHelper {
  static Future<void> checkGeolocationPermission() async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      await showWarningDialog(
        globalKeyContext.currentContext!,
        title: L10n.getString("confirm_gps_title"),
        message: L10n.getString("confirm_gps_message"),
        yesButtonText: L10n.getString("ok"),
        yesAction: () => _requestPermission(),
      );
    }
  }

  static Future<bool> checkGeolocationActivation() async {
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      await showConfirmationDialog(
        globalKeyContext.currentContext!,
        title: L10n.getString("warning_title"),
        message: L10n.getString("gps_denied_message"),
        yesButtonText: L10n.getString("allow_gps_service"),
        noButtonText: L10n.getString("deny_gps_service"),
        yesButtonAction: Geolocator.openAppSettings,
      );
      return false;
    }
    var serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await showConfirmationDialog(
        globalKeyContext.currentContext!,
        title: L10n.getString("warning_title"),
        message: L10n.getString("gps_disabled_message"),
        yesButtonText: L10n.getString("enable_gps_service"),
        noButtonText: L10n.getString("disable_gps_service"),
        yesButtonAction: Geolocator.openLocationSettings,
      );
      return false;
    }
    return true;
  }

  static void _requestPermission() async {
    var permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.deniedForever || permission == LocationPermission.denied) {
      showErrorDialog(
        globalKeyContext.currentContext!,
        title: L10n.getString("error_title"),
        message: L10n.getString("impossible_without_location_provider"),
      );
    }
  }
}
