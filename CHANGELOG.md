# Changelog

- All notable changes to this project will be documented in this file.

- The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
  and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Publishing on Google Play

### Add symbol file for debugging purpose

Go to [PROJECT_DIR]\build\app\intermediates\merged_native_libs\release\out\lib
3 folders exist inside:

- arm64-v8a
- armeabi-v7a
- x86_64

Archive the 3 folders in a .zip file. Name doesn't matter.
Caveat: Do not compressed the ./lib folder

Upload this new *.zip file as Symbol File.

## 3.15.0 [249]

- Ajout du ski-orientation
- Révision des résultats

- Added ski-orienteering
- Revised results

## 3.14.5 [245]
- Suppression du repositionement de l'instant de validation (retour en arrière).
- Removal of relocating the validation moment (rollback).

## 3.14.4 [245]
- Correction d'un bug avec les iBeacons sur Android
Modifications du paquet beacon_scanner pour Android:
1. Flutter Plugins/beacon_scanner_android-0.0.4/android/build.gradle
50: implementation 'org.altbeacon:android-beacon-library:2.20.7'

2. beacon_scanner_android-0.0.4/android/src/main/kotlin/com/lukangagames/plugins/beaconscanner/BeaconScannerPlugin.kt
old:
182:  } catch (ignored: RuntimeException) {
183:  }
184:  result.success("STATE_UNSUPPORTED")
new:
182: } catch (ignored: RuntimeException) {
183:    result.success("STATE_UNSUPPORTED")
184: }

- Fixed bug with iBeacon on Android

## 3.14.3 [244]
- Ajout de la possibilité de stopper une rogaine

- Added a way to stop a rogaine

## 3.14.2 [239]
- Mise à jour de la traduction portugaise

- Updated Portuguese translation

## 3.14.1 [243]
- Correction d'un bug lié au lancement du navigateur Internet

- Fixed a bug related to launching the Internet browser

## 3.14.0 [238]
- Changement de l'algorithme de calcul du profil d'orienteur dans la page résultat
- Correction de la position de validation pour tenir compte du délai entre l'instant de validation et le signalement à l'orienteur
- Changement du label 'mode geocaching' par 'mode ludique' pour éviter la confusion
- Mise à jour des traductions italienne, tchèque et portugaise
 
- Changed the algorithm for calculating orienteer profile in the result page
- Correction of the validation position to take into account the delay between the moment of validation and the reporting to the orienteer
- Changed label 'geocaching mode' to 'playful mode' to avoid confusion 
- Updated Italian, Czech and Portuguese translations

## 3.13.4 [233]

- Mise à jour des dépendances
- Révision de l'animation de la trace à partir du curseur
- Révision de la durée de l'aide à 20 s pour "Je suis perdu"

- Updated dependencies
- Revised trace animation from slider
- Extended duration of help to 20 s for "I'm lost"

## 3.13.3 [232]

- Correction d'un bug lié à la captation de la trace GPS
- Révision du mode Rogaine
- Correction d'un bug lors du chargement d'un parcours

- Fixed a bug related to GPS track capture
- Reviewed Rogaine mode
- Fixed bug on course downloading

## 3.13.0 [228]

- Révision du suivi en direct pour intégrer le score et une queue de comète de 30 s
- Complète révision du code

- Revised live tracking to integrate score and a 30s comet tail
- Refactored code

## 3.12.6 [226]

- Correction d'un bug pour la géolocalisation avec Android 14

- Fixed bug on geolocation for Android 14

## 3.12.5 [224]

- Amélioration des performances énergétiques
- Amélioration de l'interface
- Correction d'un bug d'affichage lors de l'envoi du résultat

- Improved energy performances
- Enhanced user interface
- Fixed display bug when sending result to server

## 3.12.4 [218]

- Ajout d'un bouton pour cacher la carte en cours de parcours pour économiser l'énergie

- Added a button to hide the map during the course to save energy

## 3.12.3 [217]

- Correction d'un bug sur le départ avec le NFC

- Fixed bug when starting course with NFC

## 3.12.2 [216]

- Amélioration du menu de la page de course
- Ajout de la reprise du suivi en direct après un crash

- Enhanced course page menu
- Added resume live tracking after crash

## 3.13.1 [230]

- Ajout du suivi en direct de la trace
- Correction d'un bug dans la version portugaise

- Added live tracking
- Fixed bug on Portuguese version

## 3.12.0 [210]

- Ajout du suivi en direct de la trace

- Added live tracking

## 3.11.1 [210]

- Correction du nom du fichier GPX lors du partage 

- Fixed name of GPX file during sharing 

## 3.11.0 [210]

- Révision du mode ordre libre pour intégrer le mode Rogaine et la course au score
- Ajout du flash pour scanner les codes QR
- Ajout de la traduction polonaise

- Revised free order mode to integrate the Rogaine mode and score course
- Added torch for scanning QR codes
- Added Polish translation

## 3.10.2 [195]

- Correction de bugs sur la trace
- Ajout du partage de fichier GPX
- Revision de la validation par iBeacons

- Fixed bug on tracks
- Added share GPX file
- Revised validation from iBeacons

## 3.9.8 [187]

- Correction de bugs lors de la lecture des codes QR
- Ajout du partage de la trace

- Fixed bugs with reading QR codes
- Added share track

## 3.9.5 [183]

- Correction d'un bug dans le profil 
- Correction d'un bug dans la reprise de parcours en cas de fin prématurée
- Changement de l'interface pour la page de choix de la carte

- Fixed bug in profile
- Fixed bug with the course recovery
- Changed interface for the map choice page 

## 3.9.3 [181]

- Correction d'un bug dans la reprise de course après arrêt inopiné

- Fixed bug in resuming course after crash

## 3.9.2 [180]

- Révision de l'expérience utilisateur pour la page des parcours

- Revised user experience for page Course List

## 3.9.1 [179]

- Correction d'un bug lié au mode de course après lancement d'un parcours à partir de code QR 

- Fixed bug on course mode after launching course from QR code 

## 3.9.0 [177]

- Utilisation d'une application de navigation installée sur le mobile pour guider vers le départ d'un parcours.
- Ajout du chargement et ouvrir l'application automatiquement à partir d'un code QR d'un parcours.
- Amélioration du lecteur de code QR.

- Use of a navigation application installed on the mobile to guide to the start of a course.
- Add download and open application from QR code of a course.
- Improve the QR code reader.

## 3.8.0 [172]
- ADDED: launch app and course from QR code
- UPDATE: UI
- UPDATE: code

- AJOUT : lancement de l'application puis de la course à partir d'un code QR.
- MAJ : interface graphique
- MAJ : code

## 3.7.2 [168]

- BUG: Course resume after crash
- ADD: Open Bluetooth settings in case of iBeacon validation
- UPDATE: UI with material 3
- UPDATE: Removed validation distance based on GPS accuracy

- BUG : Correction du bug lié à la reprise parcours après un crash  
- AJOUT : Ouvre la page de réglage du Bluetooth dans le cas d'une validation par iBeacons
- MAJ: Adaptation à Material 3
- MAJ: Suppression de la validation en fonction de la précision du GPS

- Updated UI with material 3
- Removed validation distance based on GPS accuracy

- Mise à jour de l'interface avec Material 3
- Suppression de la validation en fonction de la précision du GPS

## 3.7.1-b1 [161]

- Enhanced memory footprint during course
- Fixed bug on Beacons

- Amélioration de l'empreinte mémoire durant la course
- Correction d'un bug sur les eBeacons

## 3.7.0 [160]

- Added possibility to resume the course after app crash
- Fix bug on iPhone sound volume
- Fix bug on iPhone quiz count

- Ajout de la reprise de course après un arrêt prématuré de l'application
- Augmentation du volume du son de validation sur iPhone
- Correction de l'erreur de comptage des points en mode géocaching sur iPhone

## 3.6.0 [159]

- Added validation of checkpoints after MP in preset order
- Added intermediate storage of course data
- Upgraded Beacon to Android 13
- Revise compass
- Added store forced validation with punch time- Adapted size of the avatar and thickness of track in walk mode to zoom factor
- Adapted thickness of track in animation to zoom factor
- Revised some texts from interface
- Added speeds filtering for profile calculation

- Ajout de la validation des postes après un PM en ordre imposé
- Ajout de la sauvegarde intermédiaire du parcours
- Mise à jour des Beacons pour Android 13
- Amélioration de l'animation de la boussole
- Repérage de la validation forcée des balises
- Ajout d'un lissage des vitesses pour calculer le profil de l'orienteur
- Adaptation au zoom de la taille de l'avatar et de l'épaisseur des traces
- Adaptation au zoom de l'épaisseur de la trace dans l'animation
- Changement du texte "Abandonner le parcours"

## 3.5.0

## Updated

- Updated german translation
- Updated italian translation

## Changed

- Increased compare button visibility

## Added

- Added distinction between FootO and MTBO for drawing tracks and classifying profiles
- Added launch a course protected by secret key from QR code
- Added button at the top of the first page to open Vikazimut website

## 3.4.6 []

## Added

- Added GPS accuracy display at the bottom of the course screen

## Updated

- Updated Portuguese translation

## 3.4.5 [143]

## Added

- Added launch course directly from QR code
- Added private course in walk mode (planner assistance)
- Added export GPS track in a GPX file on the result page

## Changed

- Changed validation of the finish checkpoint when stopping free order course close to the checkpoint

## 3.4.3 [138]

## Changed

- Changed some texts
- Enhanced some code

## 3.4.2 [137]

## Added

- Added validation by iBeacon
- Added a button to remove all downloaded maps
- Added the geocaching mode: sport + quiz
- Added evaluation of the orienteer profile from the analysis of the speed histogram
- Added an icon to report that a result is sent in history page
- Added persistence for the last entered nickname in the send box

## Changed

- Changed avatar in walk mode

## Fixed
-

## 3.3.9 [131]

## Added

- Enhance quality factor of the map image
- Added Czech translation
- Added Portuguese translation

## Fixed

- Fixed spelling mistake on Portuguese version

# Changed

- Immediately display avatar when trigger "I'm lost"
- Increased size of avatar for "I'm lost" in sport mode and for walk mode
- Changed location to enable/disable Google service Disabled button send just after sending results to avoid multiple sending
- Refactored all the code Changed compass Changed animation size wrt the image size
- Added auto-focusing on current position for track animation
- Added auto-focusing on current position for track animation Added Portuguese translation

## [3.3.0] - 2022-03-09

## Added

- Added german translation
- Added loading course from QR code
- Added analysis of course
- Added load and start course from QR code
- Added configurable control radius GPS detection

## Changed

- Enhanced the course state window to ensure visibility of last punched checkpoint
- Increased font size of the last punched checkpoint in the course window
- Revised design of track screen Used a discrete colorization of result track

# Removed

- Downloaded map first in map list

## [3.2.3] - 2021-12-06

## Added

- Removed unused icons
- Changed map sort to put downloaded maps at the top of the list.
- Marked touristic routes in walk mode with a label.

## [3.2.2] - 2021-11-20

## Changed

- Reduced length of the comet tail for course animation.
- Changed design of the map choice to highlight the location button.
- Changed design of the result to add club information and link to their website.

## Fixed

## Added

## [3.2.1] - 2021-10-26

## Changed

- Changed the way to get the points from the quiz pages

## Added

## Fixed

- Fixed bug search keyboard produces scrolling of the map

## 107 [3.2.0] - 2021-10-25

## Changed

- Enhancement of the design of some UI part

## Added

- Addition of a back button on the credits page Addition of instruction in the user manual on how to calibrate the compass
- In walk mode, addition of a quiz at the control points In walk mode, addition of validation from QR codes
- In walk mode, addition of a marker on the map to indicate the start position

## 101 [3.1.4] - 2021-08-28

### Fixed

- Bug on free mode with autopunch: stop the route when passing by the end CP even when all other CP are not punched

### Changed

- Enhanced track animation
- Removed animation for the credits page

## 98 [3.1.2] - 2021-08-10

### Fixed

- Android setting for reading HTML files (info boxes)

### Changed

- Turning up to Android 11

## 97 [3.1.1] - 2021-08-08

### Fixed

- Turning up to Android 11 new security rules

### Added

- Italian translation
- Animation of the track

## 90 [3.1.0] - 2021-07-23

### Fixed

- Disable insecure http url for the multimedia pages (touristic walk mode).

### Added

- Back button on the QR code page
- New statistics for course (speed, distance overcost out of straight line)
- Speed in km/h in the track legend
- Animation of the track in real-time

## 89 [3.0.1] - 2021-06-21

### Fixed

- Fix bug on map choice

## 85 [3.0.0] - 2021-06-08

### Changed

- Complete rewrite of the Android code in Flutter (new common Android & iOS).

### Added

- Trigger an alarm when the orienteer leaves the map.
- Name of the creator of the map in the list of cards.
- Search for maps from the name of the creator.
- Save routes in walk mode.
