# Contribuer au développement de l'application mobile

## Licence

Le projet est placé sous la licence publique générale GNU amoindrie
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html).
Ceci impose que les futurs développements se fassent aussi sous cette licence.

## Contribuer

La contribution nécessite d'avoir un compte sur `gitlab.ecole.ensicaen.fr` et d'être référencé
dans le projet comme développeur afin de pouvoir pousser des "merge request".

Pour une contribution extérieure, il est possible d'envoyer une "merge request"
par courriel en utilisant un `git format-patch` à l'un des contributeurs du projet :

```
git format-patch HEAD~<n>
```

où `n` est le nombre de commits à inclure dans le patch.

## Installer la distribution

Le projet est développé avec le kit de développement Flutter.
Le code source de l'application Vikazimut est accessible sur le
GitLab du projet :
[https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app](https://gitlab.ecole.ensicaen.fr/rclouard/vikazimut-app).

Toutefois, le fichier `/lib/keys.dart` contenant les clés de chiffrement
des images de carte d'orientation est volontairement absent.
Ce fichier est à récupérer auprès du responsable de projet.

## Compiler la distribution

Avant de compiler, il est nécessaire de procéder à l'installation des bibliothèques incluses
dans le projet.
Elles sont référencées dans le fichier `/pubspec.yaml`.

L'installation se fait par la commande Shell :

```
$ dart --disable-analytics
$ flutter pub get
```

Pour certaines bibliothèques, il faut, en plus, procéder à leur mise en place :

- La base de données : `floor`. La mise en place se fait par la commande Shell :

```
$ dart run build_runner build --delete-conflicting-outputs
```

- Le splashscreen : `flutter_native_splash`. La mise en place se fait par la commande Shell :

```
$ dart run flutter_native_splash:create
```

La construction des icônes d'application pour Android et iOS se fait par :

```
$ dart run flutter_launcher_icons:main
```

Finalement, l'exécution se fait par :

```
$ flutter run lib/main.dart
```

## Déployer sur Google play

1. Construire l'APK Android&nbsp;:

    ```
     $ flutter build appbundle --release
     ```

2. Déposer l'archive sur Google Play

   Si nécessaire, il faut aussi déposer les symboles de debogages
   (error Google Play : App Bundle contains native code, and you've not uploaded debug symbols)
     
   Aller dans build/app/intermediates/merged_native_libs/release/mergeReleaseNativeLibs/out/lib

   Construire une archive (symbols.zip) avec les trois dossiers :
      arm64-v8a
      armeabi-v7a
      x86_64

   Téléverser l'archive dans Google Play.

3. Voir la [documentation Flutter](https://docs.flutter.dev/deployment/android) pour déposer  une application sur [https://play.google.com](https://play.google.com)

    Cela nécessite de signer l'application. Pour cela, demander la documentation confidentielle 
    auprès du responsable de projet.

## Déployer sur Apple Store

Pour le détail du déploiement sur iOS, voir la [documentation Flutter](https://docs.flutter.dev/deployment/ios).

1. Construire l'IPA iOS&nbsp;:

    ```
    $ flutter build ipa
    $ open build/ios/archive/Runner.xcarchive
    ```

2.  Dans XCode

    1. Valider l'application
    2. Distribuer l'application

3. Créer la nouvelle version sur https://appstoreconnect.apple.com